(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-pages-home9-home9-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home9/home9.page.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home9/home9.page.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n   \r\n        <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-title mode=ios>\r\n      {{config.appName|translate}}\r\n      <!-- <ion-img src=\"assets/logo_header.png\" alt=\"logo\"></ion-img> -->\r\n    </ion-title>\r\n    <ion-buttons slot=\"end\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-button fill=\"clear\" routerLink=\"/search\" routerDirection=\"forward\">\r\n        <ion-icon slot=\"icon-only\" name=\"search\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button fill=\"clear\" routerLink=\"/cart\" routerDirection=\"forward\">\r\n        <ion-icon name=\"basket\"></ion-icon>\r\n        <ion-badge color=\"secondary\">{{shared.cartTotalItems()}}</ion-badge>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    \r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding-bottom\">\r\n  <ion-row class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"apps\"></ion-icon>\r\n      {{'Categories' | translate }}\r\n    </ion-button>\r\n    <ion-button id=\"second\" fill=\"clear\" (click)=\"openCategoryPage()\">\r\n      <ion-icon slot=\"end\" name=\"caret-forward\"></ion-icon>\r\n      {{ 'Shop More' | translate }}\r\n    </ion-button>\r\n  </ion-row>\r\n  <!-- categories component -->\r\n  <app-categories [type]=\"'grid'\"></app-categories>\r\n\r\n  <ion-segment [(ngModel)]=\"segments\">\r\n    <ion-segment-button value=\"topSeller\">{{ 'Top Seller' | translate }}</ion-segment-button>\r\n    <ion-segment-button value=\"deals\">{{ 'Deals' | translate }} </ion-segment-button>\r\n    <ion-segment-button value=\"mostLiked\"> {{ 'Most Liked' | translate }}</ion-segment-button>\r\n  </ion-segment>\r\n  <div [ngSwitch]=\"segments\">\r\n\r\n    <!-- first swipe slider -->\r\n    <ion-slides *ngSwitchCase=\"'topSeller'\" [options]=\"sliderConfig\">\r\n      <ion-slide *ngFor=\"let p of shared.tab1\">\r\n        <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n      </ion-slide>\r\n      <ion-slide>\r\n        <ion-button fill=\"clear\" (click)=\"openProducts('top seller')\"> {{'Shop More'| translate}}\r\n          <ion-icon name=\"caret-forward\"></ion-icon>\r\n        </ion-button>\r\n      </ion-slide>\r\n    </ion-slides>\r\n\r\n    <!-- 2nd swipe slider -->\r\n    <ion-slides *ngSwitchCase=\"'deals'\" [options]=\"sliderConfig\">\r\n      <ion-slide *ngFor=\"let p of shared.tab2\">\r\n        <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n      </ion-slide>\r\n      <ion-slide>\r\n        <ion-button fill=\"clear\" (click)=\"openProducts('special')\"> {{'Shop More'| translate}}\r\n          <ion-icon name=\"caret-forward\"></ion-icon>\r\n        </ion-button>\r\n      </ion-slide>\r\n    </ion-slides>\r\n\r\n    <!-- 3rd swipe slider -->\r\n    <ion-slides *ngSwitchCase=\"'mostLiked'\" [options]=\"sliderConfig\">\r\n      <ion-slide *ngFor=\"let p of shared.tab3\">\r\n        <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n      </ion-slide>\r\n      <ion-slide>\r\n        <ion-button fill=\"clear\" (click)=\"openProducts('most liked')\"> {{'Shop More'| translate}}\r\n          <ion-icon name=\"caret-forward\"></ion-icon>\r\n        </ion-button>\r\n      </ion-slide>\r\n    </ion-slides>\r\n  </div>\r\n\r\n  <!-- For Vendor List -->\r\n  <!--<app-vendor-list></app-vendor-list>-->\r\n\r\n  <ion-row class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"albums\"></ion-icon>\r\n      {{'All Products' | translate }}\r\n    </ion-button>\r\n    <ion-button id=\"second\" fill=\"clear\" (click)=\"openProducts('top seller')\">\r\n      <ion-icon slot=\"end\" name=\"caret-forward\"></ion-icon>\r\n      {{ 'Shop More' | translate }}\r\n    </ion-button>\r\n  </ion-row>\r\n\r\n  <ion-grid class=\"ion-no-padding\">\r\n    <ion-row class=\"ion-no-padding\">\r\n      <ion-col *ngFor=\"let p of products\" size=\"6\" class=\"ion-no-padding\">\r\n        <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n  <!-- infinite scroll -->\r\n  <ion-infinite-scroll #infinite (ionInfinite)=\"getProducts()\">\r\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\r\n  </ion-infinite-scroll>\r\n\r\n\r\n</ion-content>\r\n<ion-fab vertical=\"bottom\" horizontal=\"end\" *ngIf=\"scrollTopButton\">\r\n  <ion-fab-button color=\"secondary\" (click)=\"scrollToTop()\">\r\n    <ion-icon name=\"arrow-up\"></ion-icon>\r\n  </ion-fab-button>\r\n</ion-fab>");

/***/ }),

/***/ "./src/app/home-pages/home9/home9.module.ts":
/*!**************************************************!*\
  !*** ./src/app/home-pages/home9/home9.module.ts ***!
  \**************************************************/
/*! exports provided: Home9PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Home9PageModule", function() { return Home9PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _home9_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home9.page */ "./src/app/home-pages/home9/home9.page.ts");
/* harmony import */ var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/pipes/pipes.module */ "./src/pipes/pipes.module.ts");
/* harmony import */ var src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/components/share/share.module */ "./src/components/share/share.module.ts");









const routes = [
    {
        path: '',
        component: _home9_page__WEBPACK_IMPORTED_MODULE_6__["Home9Page"]
    }
];
let Home9PageModule = class Home9PageModule {
};
Home9PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
            src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__["ShareModule"],
        ],
        declarations: [_home9_page__WEBPACK_IMPORTED_MODULE_6__["Home9Page"]]
    })
], Home9PageModule);



/***/ }),

/***/ "./src/app/home-pages/home9/home9.page.scss":
/*!**************************************************!*\
  !*** ./src/app/home-pages/home9/home9.page.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("app-product {\n  width: 100%;\n}\n\n.swiper-slide {\n  width: 40%;\n}\n\nion-slides ion-slide:last-child {\n  height: auto;\n}\n\nion-segment ion-segment-button {\n  font-size: 0.75rem;\n}\n\nion-fab {\n  position: fixed;\n}\n\nion-fab ion-fab-button {\n  --background: var(--ion-color-primary);\n}\n\nion-grid ion-row {\n  margin-right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9ob21lLXBhZ2VzL2hvbWU5L2hvbWU5LnBhZ2Uuc2NzcyIsInNyYy9hcHAvaG9tZS1wYWdlcy9ob21lOS9ob21lOS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFBO0FDQ0Y7O0FEQ0E7RUFDRSxVQUFBO0FDRUY7O0FERUk7RUFDRSxZQUFBO0FDQ047O0FESUU7RUFDRSxrQkFBQTtBQ0RKOztBRElBO0VBQ0UsZUFBQTtBQ0RGOztBREVFO0VBQ0Usc0NBQUE7QUNBSjs7QURJRTtFQUNJLGtCQUFBO0FDRE4iLCJmaWxlIjoic3JjL2FwcC9ob21lLXBhZ2VzL2hvbWU5L2hvbWU5LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImFwcC1wcm9kdWN0IHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4uc3dpcGVyLXNsaWRlIHtcclxuICB3aWR0aDogNDAlO1xyXG59XHJcbmlvbi1zbGlkZXMge1xyXG4gIGlvbi1zbGlkZSB7XHJcbiAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbmlvbi1zZWdtZW50IHtcclxuICBpb24tc2VnbWVudC1idXR0b24ge1xyXG4gICAgZm9udC1zaXplOiAwLjc1cmVtO1xyXG4gIH1cclxufVxyXG5pb24tZmFiIHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgaW9uLWZhYi1idXR0b24ge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgfVxyXG59XHJcbmlvbi1ncmlke1xyXG4gIGlvbi1yb3d7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICB9XHJcbn0iLCJhcHAtcHJvZHVjdCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uc3dpcGVyLXNsaWRlIHtcbiAgd2lkdGg6IDQwJTtcbn1cblxuaW9uLXNsaWRlcyBpb24tc2xpZGU6bGFzdC1jaGlsZCB7XG4gIGhlaWdodDogYXV0bztcbn1cblxuaW9uLXNlZ21lbnQgaW9uLXNlZ21lbnQtYnV0dG9uIHtcbiAgZm9udC1zaXplOiAwLjc1cmVtO1xufVxuXG5pb24tZmFiIHtcbiAgcG9zaXRpb246IGZpeGVkO1xufVxuaW9uLWZhYiBpb24tZmFiLWJ1dHRvbiB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xufVxuXG5pb24tZ3JpZCBpb24tcm93IHtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/home-pages/home9/home9.page.ts":
/*!************************************************!*\
  !*** ./src/app/home-pages/home9/home9.page.ts ***!
  \************************************************/
/*! exports provided: Home9Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Home9Page", function() { return Home9Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/providers/config/config.service */ "./src/providers/config/config.service.ts");
/* harmony import */ var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/providers/shared-data/shared-data.service */ "./src/providers/shared-data/shared-data.service.ts");
/* harmony import */ var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/providers/app-events/app-events.service */ "./src/providers/app-events/app-events.service.ts");






let Home9Page = class Home9Page {
    constructor(nav, config, appEventsService, shared) {
        this.nav = nav;
        this.config = config;
        this.appEventsService = appEventsService;
        this.shared = shared;
        this.segments = "topSeller"; //first segment by default 
        this.scrollTopButton = false; //for scroll down fab 
        //for product slider after banner
        this.sliderConfig = {
            slidesPerView: this.config.productSlidesPerPage,
            spaceBetween: 0
        };
        this.products = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        this.page = 0;
        this.count = 0;
        this.loadingServerData = false;
    }
    ionViewDidEnter() {
        this.shared.hideSplashScreen();
    }
    // For FAB Scroll
    onScroll(e) {
        if (e.detail.scrollTop >= 500) {
            this.scrollTopButton = true;
        }
        if (e.detail.scrollTop < 500) {
            this.scrollTopButton = false;
        }
    }
    // For Scroll To Top Content
    scrollToTop() {
        this.content.scrollToTop(700);
        this.scrollTopButton = false;
    }
    openProducts(value) {
        this.nav.navigateForward(this.config.currentRoute + "/products/0/0/" + value);
    }
    openCategoryPage() {
        this.nav.navigateForward(this.config.currentRoute + "/" + this.config.getCurrentCategoriesPage() + "/0/0");
    }
    getProducts() {
        if (this.loadingServerData)
            return 0;
        if (this.page == 0) {
            this.count++;
            this.loadingServerData = false;
        }
        this.loadingServerData = true;
        let data = {};
        if (this.shared.customerData.customers_id != null)
            data.customers_id = this.shared.customerData.customers_id;
        data.page_number = this.page;
        data.language_id = this.config.langId;
        data.currency_code = this.config.currecnyCode;
        this.config.postHttp('getallproducts', data).then((data) => {
            let dat = data.product_data;
            this.infinite.complete();
            if (this.page == 0) {
                this.products = new Array;
            }
            if (dat.length != 0) {
                this.page++;
                for (let value of dat) {
                    this.products.push(value);
                }
            }
            if (dat.length == 0) {
                this.infinite.disabled = true;
            }
            this.loadingServerData = false;
        });
    }
    ngOnInit() {
        this.getProducts();
    }
    ionViewWillEnter() {
        if (!this.config.appInProduction) {
            this.config.productCardStyle = "14";
        }
    }
};
Home9Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"] },
    { type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__["AppEventsService"] },
    { type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"])
], Home9Page.prototype, "content", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"])
], Home9Page.prototype, "infinite", void 0);
Home9Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home9',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home9.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home9/home9.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home9.page.scss */ "./src/app/home-pages/home9/home9.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"],
        src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__["AppEventsService"],
        src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"]])
], Home9Page);



/***/ })

}]);
//# sourceMappingURL=home-pages-home9-home9-module-es2015.js.map