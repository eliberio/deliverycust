function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["news-news-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/news/news.page.html":
  /*!***************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/news/news.page.html ***!
    \***************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppNewsNewsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      {{'News'| translate }}\r\n    </ion-title>\r\n\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"news-page\">\r\n  <!-- *ngIf=\"post.image\" -->\r\n  <ion-slides pager=true paginationType=\"bullets\" class=\"animate-item\">\r\n    <ion-slide *ngFor=\"let post of featuredPosts\" (click)=\"showPostDetail(post)\">\r\n      <ion-img src=\"{{config.imgUrl+post.news_image}}\" class=\"animate-item\"></ion-img>\r\n    </ion-slide>\r\n  </ion-slides>\r\n  <!-- top Segments  -->\r\n  <ion-segment [(ngModel)]=\"segments\">\r\n    <ion-segment-button value=\"newest\">{{'Newest' |translate}}</ion-segment-button>\r\n    <ion-segment-button value=\"categories\">{{ 'Categories' | translate }} </ion-segment-button>\r\n  </ion-segment>\r\n\r\n  <div [ngSwitch]=\"segments\">\r\n\r\n    <div *ngSwitchCase=\"'newest'\">\r\n      <ion-list class=\"posts-list\">\r\n        <ion-item lines=\"full\" *ngFor=\"let post of posts\" (click)=\"showPostDetail(post)\" class=\"animate-item\">\r\n          <ion-thumbnail slot=\"start\">\r\n            <ion-img src=\"{{config.imgUrl+post.news_image}}\"></ion-img>\r\n          </ion-thumbnail>\r\n          <ion-label>\r\n            <h2>{{post.news_name}}\r\n              <br>\r\n              <small>\r\n                <ion-icon name=\"time\"></ion-icon>\r\n                <ion-label>\r\n                  {{post.created_at|date}}\r\n                </ion-label>\r\n              </small>\r\n            </h2>\r\n            <p [innerHTML]=\"post.news_description\"></p>\r\n          </ion-label>\r\n        </ion-item>\r\n        <ion-infinite-scroll #infinite (ionInfinite)=\"getPosts()\">\r\n          <ion-infinite-scroll-content></ion-infinite-scroll-content>\r\n        </ion-infinite-scroll>\r\n      </ion-list>\r\n    </div>\r\n\r\n    <div *ngSwitchCase=\"'categories'\">\r\n      <ion-list class=\"categories-list\">\r\n        <ion-card *ngFor=\"let cat of categories\" (click)=\"openPostsPage(cat.name,cat.id)\" class=\"animate-item\">\r\n          <img *ngIf=\"cat.image\" src=\"{{config.imgUrl+cat.image}}\">\r\n          <div>\r\n            <h2>{{cat.name }}</h2>\r\n            <p>{{cat.total_news}} {{'Posts'|translate}}</p>\r\n          </div>\r\n        </ion-card>\r\n      </ion-list>\r\n    </div>\r\n  </div>\r\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/news/news.module.ts":
  /*!*************************************!*\
    !*** ./src/app/news/news.module.ts ***!
    \*************************************/

  /*! exports provided: NewsPageModule */

  /***/
  function srcAppNewsNewsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewsPageModule", function () {
      return NewsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _news_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./news.page */
    "./src/app/news/news.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _news_page__WEBPACK_IMPORTED_MODULE_6__["NewsPage"]
    }];

    var NewsPageModule = function NewsPageModule() {
      _classCallCheck(this, NewsPageModule);
    };

    NewsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]],
      declarations: [_news_page__WEBPACK_IMPORTED_MODULE_6__["NewsPage"]]
    })], NewsPageModule);
    /***/
  },

  /***/
  "./src/app/news/news.page.scss":
  /*!*************************************!*\
    !*** ./src/app/news/news.page.scss ***!
    \*************************************/

  /*! exports provided: default */

  /***/
  function srcAppNewsNewsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".news-page ion-segment {\n  border-bottom: 1px solid #dedede;\n}\n.news-page ion-segment ion-segment-button {\n  --indicator-color-checked: var(--ion-color-primary) !important;\n  --color-checked: var(--ion-color-primary);\n  --color: var(--ion-color-primary);\n}\n.news-page ion-slides ion-img {\n  width: 100%;\n}\n.news-page .posts-list ion-item ion-thumbnail {\n  margin-bottom: auto;\n}\n.news-page .posts-list ion-item ion-label h2 {\n  font-size: 16px;\n  font-weight: bold;\n  white-space: normal;\n}\n.news-page .posts-list ion-item ion-label h2 small {\n  display: flex;\n  align-items: center;\n  font-size: 14px;\n  color: #747474;\n  font-weight: normal;\n  margin-top: 5px;\n}\n.news-page .posts-list ion-item ion-label h2 small ion-label {\n  padding-left: 4px;\n}\n.news-page .posts-list ion-item ion-label p p:not(:first-child) {\n  display: none;\n}\n.news-page .posts-list ion-item ion-label p p:first-child {\n  white-space: normal;\n  line-height: 1.4;\n  -webkit-line-clamp: 4;\n  display: -webkit-box;\n  box-sizing: border-box;\n  -webkit-box-orient: vertical;\n}\n.news-page .categories-list {\n  padding-top: 0;\n  padding-bottom: 0;\n}\n.news-page .categories-list ion-card {\n  height: 235px;\n}\n.news-page .categories-list ion-card img {\n  -webkit-filter: brightness(0.6);\n          filter: brightness(0.6);\n  width: 100%;\n}\n.news-page .categories-list ion-card div {\n  position: absolute;\n  top: 32%;\n  text-align: center;\n  width: 100%;\n  color: white;\n  font-weight: bold;\n  height: 250px;\n}\n.news-page .categories-list ion-card div h2 {\n  margin-top: 18px;\n  font-size: var(--heading-font-size);\n  font-weight: bold;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n.news-page .categories-list ion-card div p {\n  font-size: var(--sub-heading-font-size);\n  margin-top: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9uZXdzL25ld3MucGFnZS5zY3NzIiwic3JjL2FwcC9uZXdzL25ld3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsZ0NBQUE7QUNBSjtBRENJO0VBQ0UsOERBQUE7RUFDQSx5Q0FBQTtFQUNBLGlDQUFBO0FDQ047QURHSTtFQUNFLFdBQUE7QUNETjtBRE1NO0VBQ0UsbUJBQUE7QUNKUjtBRE9RO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUNMVjtBRE1VO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUNKWjtBREtZO0VBQ0UsaUJBQUE7QUNIZDtBRFFVO0VBQ0UsYUFBQTtBQ05aO0FEUVU7RUFDRSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7QUNOWjtBRGFFO0VBQ0UsY0FBQTtFQUNBLGlCQUFBO0FDWEo7QURhSTtFQUNFLGFBQUE7QUNYTjtBRFlNO0VBQ0UsK0JBQUE7VUFBQSx1QkFBQTtFQUNBLFdBQUE7QUNWUjtBRFlNO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtBQ1ZSO0FEV1E7RUFDRSxnQkFBQTtFQUNBLG1DQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsdUJBQUE7QUNUVjtBRFdRO0VBQ0UsdUNBQUE7RUFDQSxhQUFBO0FDVFYiLCJmaWxlIjoic3JjL2FwcC9uZXdzL25ld3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm5ld3MtcGFnZSB7XHJcbiAgaW9uLXNlZ21lbnQge1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkZWRlZGU7XHJcbiAgICBpb24tc2VnbWVudC1idXR0b24ge1xyXG4gICAgICAtLWluZGljYXRvci1jb2xvci1jaGVja2VkOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSkgIWltcG9ydGFudDtcclxuICAgICAgLS1jb2xvci1jaGVja2VkOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICAgIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgIH1cclxuICB9XHJcbiAgaW9uLXNsaWRlcyB7XHJcbiAgICBpb24taW1nIHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5wb3N0cy1saXN0IHtcclxuICAgIGlvbi1pdGVtIHtcclxuICAgICAgaW9uLXRodW1ibmFpbCB7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogYXV0bztcclxuICAgICAgfVxyXG4gICAgICBpb24tbGFiZWwge1xyXG4gICAgICAgIGgyIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcclxuICAgICAgICAgIHNtYWxsIHtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzc0NzQ3NDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICAgICAgICBpb24tbGFiZWwge1xyXG4gICAgICAgICAgICAgIHBhZGRpbmctbGVmdDogNHB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHAge1xyXG4gICAgICAgICAgcDpub3QoOmZpcnN0LWNoaWxkKSB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBwOmZpcnN0LWNoaWxkIHtcclxuICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcclxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDEuNDtcclxuICAgICAgICAgICAgLXdlYmtpdC1saW5lLWNsYW1wOiA0O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICAgICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5jYXRlZ29yaWVzLWxpc3Qge1xyXG4gICAgcGFkZGluZy10b3A6IDA7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcclxuXHJcbiAgICBpb24tY2FyZCB7XHJcbiAgICAgIGhlaWdodDogMjM1cHg7XHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgZmlsdGVyOiBicmlnaHRuZXNzKDAuNik7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIH1cclxuICAgICAgZGl2IHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiAzMiU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBoZWlnaHQ6IDI1MHB4O1xyXG4gICAgICAgIGgyIHtcclxuICAgICAgICAgIG1hcmdpbi10b3A6IDE4cHg7XHJcbiAgICAgICAgICBmb250LXNpemU6IHZhcigtLWhlYWRpbmctZm9udC1zaXplKTtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgICB9XHJcbiAgICAgICAgcCB7XHJcbiAgICAgICAgICBmb250LXNpemU6IHZhcigtLXN1Yi1oZWFkaW5nLWZvbnQtc2l6ZSk7XHJcbiAgICAgICAgICBtYXJnaW4tdG9wOiAwO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iLCIubmV3cy1wYWdlIGlvbi1zZWdtZW50IHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkZWRlZGU7XG59XG4ubmV3cy1wYWdlIGlvbi1zZWdtZW50IGlvbi1zZWdtZW50LWJ1dHRvbiB7XG4gIC0taW5kaWNhdG9yLWNvbG9yLWNoZWNrZWQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KSAhaW1wb3J0YW50O1xuICAtLWNvbG9yLWNoZWNrZWQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xufVxuLm5ld3MtcGFnZSBpb24tc2xpZGVzIGlvbi1pbWcge1xuICB3aWR0aDogMTAwJTtcbn1cbi5uZXdzLXBhZ2UgLnBvc3RzLWxpc3QgaW9uLWl0ZW0gaW9uLXRodW1ibmFpbCB7XG4gIG1hcmdpbi1ib3R0b206IGF1dG87XG59XG4ubmV3cy1wYWdlIC5wb3N0cy1saXN0IGlvbi1pdGVtIGlvbi1sYWJlbCBoMiB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XG59XG4ubmV3cy1wYWdlIC5wb3N0cy1saXN0IGlvbi1pdGVtIGlvbi1sYWJlbCBoMiBzbWFsbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICM3NDc0NzQ7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cbi5uZXdzLXBhZ2UgLnBvc3RzLWxpc3QgaW9uLWl0ZW0gaW9uLWxhYmVsIGgyIHNtYWxsIGlvbi1sYWJlbCB7XG4gIHBhZGRpbmctbGVmdDogNHB4O1xufVxuLm5ld3MtcGFnZSAucG9zdHMtbGlzdCBpb24taXRlbSBpb24tbGFiZWwgcCBwOm5vdCg6Zmlyc3QtY2hpbGQpIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cbi5uZXdzLXBhZ2UgLnBvc3RzLWxpc3QgaW9uLWl0ZW0gaW9uLWxhYmVsIHAgcDpmaXJzdC1jaGlsZCB7XG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjQ7XG4gIC13ZWJraXQtbGluZS1jbGFtcDogNDtcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XG59XG4ubmV3cy1wYWdlIC5jYXRlZ29yaWVzLWxpc3Qge1xuICBwYWRkaW5nLXRvcDogMDtcbiAgcGFkZGluZy1ib3R0b206IDA7XG59XG4ubmV3cy1wYWdlIC5jYXRlZ29yaWVzLWxpc3QgaW9uLWNhcmQge1xuICBoZWlnaHQ6IDIzNXB4O1xufVxuLm5ld3MtcGFnZSAuY2F0ZWdvcmllcy1saXN0IGlvbi1jYXJkIGltZyB7XG4gIGZpbHRlcjogYnJpZ2h0bmVzcygwLjYpO1xuICB3aWR0aDogMTAwJTtcbn1cbi5uZXdzLXBhZ2UgLmNhdGVnb3JpZXMtbGlzdCBpb24tY2FyZCBkaXYge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMzIlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBoZWlnaHQ6IDI1MHB4O1xufVxuLm5ld3MtcGFnZSAuY2F0ZWdvcmllcy1saXN0IGlvbi1jYXJkIGRpdiBoMiB7XG4gIG1hcmdpbi10b3A6IDE4cHg7XG4gIGZvbnQtc2l6ZTogdmFyKC0taGVhZGluZy1mb250LXNpemUpO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG59XG4ubmV3cy1wYWdlIC5jYXRlZ29yaWVzLWxpc3QgaW9uLWNhcmQgZGl2IHAge1xuICBmb250LXNpemU6IHZhcigtLXN1Yi1oZWFkaW5nLWZvbnQtc2l6ZSk7XG4gIG1hcmdpbi10b3A6IDA7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/news/news.page.ts":
  /*!***********************************!*\
    !*** ./src/app/news/news.page.ts ***!
    \***********************************/

  /*! exports provided: NewsPage */

  /***/
  function srcAppNewsNewsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewsPage", function () {
      return NewsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");

    var NewsPage = /*#__PURE__*/function () {
      function NewsPage(navCtrl, config, loading, shared) {
        var _this2 = this;

        _classCallCheck(this, NewsPage);

        this.navCtrl = navCtrl;
        this.config = config;
        this.loading = loading;
        this.shared = shared;
        this.featuredPosts = new Array();
        this.segments = 'newest'; //WordPress intergation

        this.categories = new Array(); //page varible

        this.page = 0; //WordPress intergation

        this.posts = new Array(); //page varible

        this.page2 = 0;
        this.httpRunning = true; //========================================= tab newest categories ===============================================================================

        this.getCategories = function () {
          var _this = this;

          var dat = {};
          dat.language_id = this.config.langId;
          dat.currency_code = this.config.currecnyCode;
          dat.page_number = this.page2;
          this.config.postHttp('allnewscategories', dat).then(function (data) {
            if (_this.page2 == 0) {
              _this.categories = [];
            }

            if (data.success == 1) {
              _this.page2++;
              data.data.forEach(function (value, index) {
                _this.categories.push(value);
              }); // console.log(data.data.length);

              _this.getCategories();
            }

            if (data.data.length < 9) {
              // if we get less than 10 products then infinite scroll will de disabled
              if (_this.categories.length != 0) {//this.shared.toast('All Categories Loaded!');
              }
            }
          }, function (response) {// console.log("Error while loading categories from the server");
            // console.log(response);
          });
        };

        var dat = {};
        dat.language_id = this.config.langId;
        dat.currency_code = this.config.currecnyCode;
        dat.is_feature = 1;
        this.config.postHttp('getallnews', dat).then(function (data) {
          _this2.featuredPosts = data.news_data;
        });
        this.getPosts();
        this.getCategories();
      } //============================================================================================  
      //getting list of posts


      _createClass(NewsPage, [{
        key: "getPosts",
        value: function getPosts() {
          var _this3 = this;

          this.httpRunning = true;
          var dat = {};
          dat.language_id = this.config.langId;
          dat.currency_code = this.config.currecnyCode;
          dat.page_number = this.page;
          this.config.postHttp('getallnews', dat).then(function (data) {
            _this3.httpRunning = false;

            _this3.infinite.complete(); //stopping infinite scroll loader


            if (_this3.page == 0) {
              _this3.posts = [];
              _this3.infinite.disabled == false;
            }

            if (data.success == 1) {
              _this3.page++;
              data.news_data.forEach(function (value, index) {
                _this3.posts.push(value);
              });
            }

            if (data.news_data.length < 9) {
              // if we get less than 10 products then infinite scroll will de disabled
              //disabling infinite scroll
              _this3.infinite.disabled == true;

              if (_this3.posts.length != 0) {// this.shared.toast('All Posts Loaded!');
              }
            }
          }, function (response) {// console.log("Error while loading posts from the server");
            // console.log(response);
          });
        }
      }, {
        key: "showPostDetail",
        //============================================================================================  
        //getting list of sub categories from the server
        value: function showPostDetail(post) {
          this.shared.singlePostData = post;
          console.log(this.shared.singlePostData);
          this.navCtrl.navigateForward(this.config.currentRoute + "/news-detail");
        }
      }, {
        key: "openPostsPage",
        value: function openPostsPage(name, id) {
          this.navCtrl.navigateForward(this.config.currentRoute + "/news-list/" + id + "/" + name);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return NewsPage;
    }();

    NewsPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"], {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"])], NewsPage.prototype, "infinite", void 0);
    NewsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-news',
      encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./news.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/news/news.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./news.page.scss */
      "./src/app/news/news.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]])], NewsPage);
    /***/
  }
}]);
//# sourceMappingURL=news-news-module-es5.js.map