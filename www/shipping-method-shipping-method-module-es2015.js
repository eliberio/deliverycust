(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["shipping-method-shipping-method-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shipping-method/shipping-method.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shipping-method/shipping-method.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>{{'Shipping Method'|translate}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n  <ion-list>\r\n    <ion-radio-group name=\"radio-group\" (ionChange)=\"setMethod($event)\">\r\n      <div *ngFor=\"let m of shippingMethod\">\r\n        <div *ngIf=\"m.services.length!=0\">\r\n          <ion-list-header>\r\n            {{m.name}}\r\n          </ion-list-header>\r\n\r\n          <ion-item lines=\"full\" *ngFor=\"let s of m.services\">\r\n            <ion-label>{{s.name+' ---- '+s.rate+' '+s.currencyCode}}</ion-label>\r\n            <ion-radio name=\"{{s.name}}\" [value]=\"s\"></ion-radio>\r\n          </ion-item>\r\n        </div>\r\n      </div>\r\n    </ion-radio-group>\r\n  </ion-list>\r\n</ion-content>\r\n<ion-footer>\r\n  <ion-button expand=\"full\" color=\"secondary\" (click)=\"openOrderPage()\" [disabled]=\"selectedMethod\">\r\n    {{'Next'|translate}}</ion-button>\r\n</ion-footer>");

/***/ }),

/***/ "./src/app/shipping-method/shipping-method.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/shipping-method/shipping-method.module.ts ***!
  \***********************************************************/
/*! exports provided: ShippingMethodPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingMethodPageModule", function() { return ShippingMethodPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _shipping_method_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shipping-method.page */ "./src/app/shipping-method/shipping-method.page.ts");
/* harmony import */ var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/pipes/pipes.module */ "./src/pipes/pipes.module.ts");








const routes = [
    {
        path: '',
        component: _shipping_method_page__WEBPACK_IMPORTED_MODULE_6__["ShippingMethodPage"]
    }
];
let ShippingMethodPageModule = class ShippingMethodPageModule {
};
ShippingMethodPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
            src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]
        ],
        declarations: [_shipping_method_page__WEBPACK_IMPORTED_MODULE_6__["ShippingMethodPage"]]
    })
], ShippingMethodPageModule);



/***/ }),

/***/ "./src/app/shipping-method/shipping-method.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/shipping-method/shipping-method.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-footer ion-button {\n  height: 45px;\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9zaGlwcGluZy1tZXRob2Qvc2hpcHBpbmctbWV0aG9kLnBhZ2Uuc2NzcyIsInNyYy9hcHAvc2hpcHBpbmctbWV0aG9kL3NoaXBwaW5nLW1ldGhvZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFDRSxZQUFBO0VBQ0EsU0FBQTtBQ0FKIiwiZmlsZSI6InNyYy9hcHAvc2hpcHBpbmctbWV0aG9kL3NoaXBwaW5nLW1ldGhvZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tZm9vdGVyIHtcclxuICBpb24tYnV0dG9uIHtcclxuICAgIGhlaWdodDogNDVweDtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcbn1cclxuIiwiaW9uLWZvb3RlciBpb24tYnV0dG9uIHtcbiAgaGVpZ2h0OiA0NXB4O1xuICBtYXJnaW46IDA7XG59Il19 */");

/***/ }),

/***/ "./src/app/shipping-method/shipping-method.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/shipping-method/shipping-method.page.ts ***!
  \*********************************************************/
/*! exports provided: ShippingMethodPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShippingMethodPage", function() { return ShippingMethodPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/providers/shared-data/shared-data.service */ "./src/providers/shared-data/shared-data.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/providers/config/config.service */ "./src/providers/config/config.service.ts");
/* harmony import */ var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/providers/loading/loading.service */ "./src/providers/loading/loading.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");








let ShippingMethodPage = class ShippingMethodPage {
    constructor(navCtrl, shared, http, storage, config, loading, applicationRef) {
        this.navCtrl = navCtrl;
        this.shared = shared;
        this.http = http;
        this.storage = storage;
        this.config = config;
        this.loading = loading;
        this.applicationRef = applicationRef;
        this.shippingMethod = new Array;
        this.selectedMethod = true;
        //================================================================================
        //calcualting products total weight
        this.calculateWeight = function () {
            var pWeight = 0;
            var totalWeight = 0;
            for (let value of this.shared.cartProducts) {
                pWeight = parseFloat(value.weight);
                if (value.unit == 'kilo') {
                    pWeight = parseFloat(value.weight) * 1000;
                }
                //  else {
                totalWeight = totalWeight + (pWeight * value.customers_basket_quantity);
                //   }
                //  console.log(totalWeight);
            }
            return totalWeight;
        };
        this.loading.show();
        var dat = {};
        dat.tax_zone_id = this.shared.orderDetails.tax_zone_id;
        // data.shipping_method = this.shared.orderDetails.shipping_method;
        // data.shipping_method = 'upsShipping';
        // data.shipping_method_code = this.shared.orderDetails.shipping_method_code;
        dat.state = this.shared.orderDetails.delivery_state;
        dat.customer_id = this.shared.customerData.customers_id;
        dat.city = this.shared.orderDetails.delivery_city;
        dat.country_id = this.shared.orderDetails.delivery_country_id;
        dat.postcode = this.shared.orderDetails.delivery_postcode;
        dat.zone = this.shared.orderDetails.delivery_zone;
        dat.street_address = this.shared.orderDetails.delivery_street_address;
        dat.products_weight = this.calculateWeight();
        dat.products_weight_unit = 'g';
        dat.products = this.getProducts();
        dat.language_id = config.langId;
        dat.currency_code = config.currecnyCode;
        console.log(dat);
        this.config.postHttp('getrate', dat).then((data) => {
            this.loading.hide();
            if (data.success == 1) {
                var m = data.data.shippingMethods;
                console.log(data);
                this.shippingMethod = Object.keys(m).map(function (key) { return m[key]; });
                this.shared.orderDetails.total_tax = data.data.tax;
            }
        });
    }
    setMethod(event) {
        this.selectedMethod = false;
        let data = event.detail.value;
        this.shared.orderDetails.shipping_cost = data.rate;
        this.shared.orderDetails.shipping_method = data.name + '(' + data.shipping_method + ')';
    }
    openOrderPage() {
        this.navCtrl.navigateForward(this.config.currentRoute + "/order");
    }
    ngOnInit() {
    }
    getProducts() {
        let temp = [];
        this.shared.cartProducts.forEach(element => {
            temp.push({
                customers_basket_quantity: element.customers_basket_quantity,
                final_price: element.final_price,
                price: element.price,
                products_id: element.products_id,
                total: element.total,
                unit: element.unit,
                weight: element.weight
            });
        });
        return temp;
    }
};
ShippingMethodPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
    { type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_5__["ConfigService"] },
    { type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"] }
];
ShippingMethodPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-shipping-method',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./shipping-method.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shipping-method/shipping-method.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./shipping-method.page.scss */ "./src/app/shipping-method/shipping-method.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"],
        src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_5__["ConfigService"],
        src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"]])
], ShippingMethodPage);

// attributes: element.attributes,
// cart_id: element.cart_id,
// categories: element.categories,
// customers_basket_quantity: element.customers_basket_quantity,
// final_price: element.final_price,
// model: element.model,
// on_sale: element.on_sale,
// price: element.price,
// products_id: element.products_id,
// products_name: element.products_name,
// subtotal: element.subtotal,
// total: element.total,
// unit: element.unit,
// weight: element.weight


/***/ })

}]);
//# sourceMappingURL=shipping-method-shipping-method-module-es2015.js.map