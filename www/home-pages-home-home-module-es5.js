function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-pages-home-home-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home/home.page.html":
  /*!**************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home/home.page.html ***!
    \**************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHomePagesHomeHomePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-title mode=ios>\r\n      {{config.appName|translate}}\r\n      <!-- <ion-img src=\"assets/logo_header.png\" alt=\"logo\"></ion-img> -->\r\n    </ion-title>\r\n    <ion-buttons slot=\"end\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-button fill=\"clear\" routerLink=\"/search\" routerDirection=\"forward\">\r\n        <ion-icon slot=\"icon-only\" name=\"search\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button fill=\"clear\" routerLink=\"/cart\" routerDirection=\"forward\">\r\n        <ion-icon name=\"basket\"></ion-icon>\r\n        <ion-badge color=\"secondary\">{{shared.cartTotalItems()}}</ion-badge>\r\n      </ion-button>\r\n    </ion-buttons>\r\n\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content [scrollEvents]=\"true\" (ionScroll)=\"onScroll($event)\" class=\"home-page-1 ion-padding-bottom\">\r\n  <app-banner></app-banner>\r\n  <!-- Flash sale items products -->\r\n  <div class=\"module\" *ngIf=\"shared.flashSaleProducts.length!=0\">\r\n    <ion-row class=\"top-icon-header\">\r\n      <ion-button fill=\"clear\">\r\n        <ion-icon slot=\"start\" name=\"time\"></ion-icon>\r\n        {{'Flash Sale'|translate}}\r\n      </ion-button>\r\n    </ion-row>\r\n\r\n    <!-- recently viewed swipe slider -->\r\n    <ion-slides #recentSlider [options]=\"sliderConfig\">\r\n      <ion-slide *ngFor=\"let p of shared.flashSaleProducts\">\r\n        <app-product [data]=\"p\" [type]=\"'flash'\"></app-product>\r\n      </ion-slide>\r\n    </ion-slides>\r\n  </div>\r\n  <ion-segment [(ngModel)]=\"segments\">\r\n    <ion-segment-button value=\"topSeller\">{{ 'Top Seller' | translate }}</ion-segment-button>\r\n    <ion-segment-button value=\"deals\">{{ 'Deals' | translate }} </ion-segment-button>\r\n    <ion-segment-button value=\"mostLiked\"> {{ 'Most Liked' | translate }}</ion-segment-button>\r\n  </ion-segment>\r\n  <div [ngSwitch]=\"segments\">\r\n\r\n    <!-- first swipe slider -->\r\n    <ion-slides *ngSwitchCase=\"'topSeller'\" [options]=\"sliderConfig\">\r\n      <ion-slide *ngFor=\"let p of shared.tab1\">\r\n        <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n      </ion-slide>\r\n      <ion-slide>\r\n        <ion-button fill=\"clear\" (click)=\"openProducts('top seller')\"> {{'Shop More'| translate}}\r\n          <ion-icon name=\"caret-forward\"></ion-icon>\r\n        </ion-button>\r\n      </ion-slide>\r\n    </ion-slides>\r\n\r\n    <!-- 2nd swipe slider -->\r\n    <ion-slides *ngSwitchCase=\"'deals'\" [options]=\"sliderConfig\">\r\n      <ion-slide *ngFor=\"let p of shared.tab2\">\r\n        <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n      </ion-slide>\r\n      <ion-slide>\r\n        <ion-button fill=\"clear\" (click)=\"openProducts('special')\"> {{'Shop More'| translate}}\r\n          <ion-icon name=\"caret-forward\"></ion-icon>\r\n        </ion-button>\r\n      </ion-slide>\r\n    </ion-slides>\r\n\r\n    <!-- 3rd swipe slider -->\r\n    <ion-slides *ngSwitchCase=\"'mostLiked'\" [options]=\"sliderConfig\">\r\n      <ion-slide *ngFor=\"let p of shared.tab3\">\r\n        <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n      </ion-slide>\r\n      <ion-slide>\r\n        <ion-button fill=\"clear\" (click)=\"openProducts('most liked')\"> {{'Shop More'| translate}}\r\n          <ion-icon name=\"caret-forward\"></ion-icon>\r\n        </ion-button>\r\n      </ion-slide>\r\n    </ion-slides>\r\n\r\n  </div>\r\n\r\n  <!-- recently view Heading -->\r\n  <ion-row *ngIf=\"shared.recentViewedProducts.length!=0\" class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"list\"></ion-icon>\r\n      {{'Recently Viewed'|translate}}\r\n    </ion-button>\r\n  </ion-row>\r\n\r\n  <!-- recently viewed swipe slider -->\r\n  <ion-slides #recentSlider [options]=\"sliderConfig\">\r\n    <ion-slide *ngFor=\"let p of shared.recentViewedProducts\">\r\n      <app-product [data]=\"p\" [type]=\"'recent'\"></app-product>\r\n    </ion-slide>\r\n  </ion-slides>\r\n\r\n  <!-- For Vendor List -->\r\n  <!--<app-vendor-list></app-vendor-list>-->\r\n\r\n  <!-- for scrollable segment and all products -->\r\n  <app-sliding-tabs [type]=\"'text'\"></app-sliding-tabs>\r\n\r\n</ion-content>\r\n<ion-fab vertical=\"bottom\" horizontal=\"end\" *ngIf=\"scrollTopButton\">\r\n  <ion-fab-button color=\"secondary\" (click)=\"scrollToTop()\">\r\n    <ion-icon name=\"arrow-up\"></ion-icon>\r\n  </ion-fab-button>\r\n</ion-fab>";
    /***/
  },

  /***/
  "./src/app/home-pages/home/home.module.ts":
  /*!************************************************!*\
    !*** ./src/app/home-pages/home/home.module.ts ***!
    \************************************************/

  /*! exports provided: HomePageModule */

  /***/
  function srcAppHomePagesHomeHomeModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePageModule", function () {
      return HomePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./home.page */
    "./src/app/home-pages/home/home.page.ts");
    /* harmony import */


    var src_components_share_share_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/components/share/share.module */
    "./src/components/share/share.module.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
    }];

    var HomePageModule = function HomePageModule() {
      _classCallCheck(this, HomePageModule);
    };

    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_components_share_share_module__WEBPACK_IMPORTED_MODULE_7__["ShareModule"], src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"]],
      declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })], HomePageModule);
    /***/
  },

  /***/
  "./src/app/home-pages/home/home.page.scss":
  /*!************************************************!*\
    !*** ./src/app/home-pages/home/home.page.scss ***!
    \************************************************/

  /*! exports provided: default */

  /***/
  function srcAppHomePagesHomeHomePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".home-page-1 .swiper-slide {\n  width: 40%;\n}\n.home-page-1 .module {\n  padding-bottom: 10px;\n}\n.home-page-1 ion-segment ion-segment-button {\n  font-size: 0.75rem;\n}\n.home-page-1 ion-slides ion-slide:last-child {\n  height: auto;\n}\n.home-page-1 app-product {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9ob21lLXBhZ2VzL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL2hvbWUtcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLFVBQUE7QUNBSjtBREVFO0VBQ0Usb0JBQUE7QUNBSjtBRElJO0VBQ0Usa0JBQUE7QUNGTjtBRE9NO0VBQ0UsWUFBQTtBQ0xSO0FEU0U7RUFDRSxXQUFBO0FDUEoiLCJmaWxlIjoic3JjL2FwcC9ob21lLXBhZ2VzL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaG9tZS1wYWdlLTEge1xyXG4gIC5zd2lwZXItc2xpZGUge1xyXG4gICAgd2lkdGg6IDQwJTtcclxuICB9XHJcbiAgLm1vZHVsZSB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICB9XHJcbiAgLy8gaWYgZGF0YSBsb2FkXHJcbiAgaW9uLXNlZ21lbnQge1xyXG4gICAgaW9uLXNlZ21lbnQtYnV0dG9uIHtcclxuICAgICAgZm9udC1zaXplOiAwLjc1cmVtO1xyXG4gICAgfVxyXG4gIH1cclxuICBpb24tc2xpZGVzIHtcclxuICAgIGlvbi1zbGlkZSB7XHJcbiAgICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIGFwcC1wcm9kdWN0IHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuXHJcbiAgLy8gaW9uLWZhYiB7XHJcbiAgLy8gICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgLy8gICBpb24tZmFiLWJ1dHRvbiB7XHJcbiAgLy8gICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gIC8vICAgfVxyXG4gIC8vIH1cclxufVxyXG4iLCIuaG9tZS1wYWdlLTEgLnN3aXBlci1zbGlkZSB7XG4gIHdpZHRoOiA0MCU7XG59XG4uaG9tZS1wYWdlLTEgLm1vZHVsZSB7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xufVxuLmhvbWUtcGFnZS0xIGlvbi1zZWdtZW50IGlvbi1zZWdtZW50LWJ1dHRvbiB7XG4gIGZvbnQtc2l6ZTogMC43NXJlbTtcbn1cbi5ob21lLXBhZ2UtMSBpb24tc2xpZGVzIGlvbi1zbGlkZTpsYXN0LWNoaWxkIHtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuLmhvbWUtcGFnZS0xIGFwcC1wcm9kdWN0IHtcbiAgd2lkdGg6IDEwMCU7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/home-pages/home/home.page.ts":
  /*!**********************************************!*\
    !*** ./src/app/home-pages/home/home.page.ts ***!
    \**********************************************/

  /*! exports provided: HomePage */

  /***/
  function srcAppHomePagesHomeHomePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePage", function () {
      return HomePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/providers/app-events/app-events.service */
    "./src/providers/app-events/app-events.service.ts");

    var HomePage = /*#__PURE__*/function () {
      function HomePage(nav, config, appEventsService, router, shared) {
        _classCallCheck(this, HomePage);

        this.nav = nav;
        this.config = config;
        this.appEventsService = appEventsService;
        this.router = router;
        this.shared = shared;
        this.segments = "topSeller"; //first segment by default 

        this.scrollTopButton = false; //for scroll down fab 
        //for product slider after banner

        this.sliderConfig = {
          slidesPerView: this.config.productSlidesPerPage,
          spaceBetween: 0
        };
      }

      _createClass(HomePage, [{
        key: "setMethod",
        value: function setMethod(d) {
          console.log(d);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          this.shared.hideSplashScreen();
        }
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          if (!this.config.appInProduction) {
            this.config.productCardStyle = "1";
          }
        } // For FAB Scroll

      }, {
        key: "onScroll",
        value: function onScroll(e) {
          if (e.detail.scrollTop >= 500) {
            this.scrollTopButton = true;
          }

          if (e.detail.scrollTop < 500) {
            this.scrollTopButton = false;
          }
        } // For Scroll To Top Content

      }, {
        key: "scrollToTop",
        value: function scrollToTop() {
          this.content.scrollToTop(700);
          this.scrollTopButton = false;
        }
      }, {
        key: "openProducts",
        value: function openProducts(value) {
          this.nav.navigateForward(this.config.currentRoute + "/products/0/0/" + value);
        }
      }]);

      return HomePage;
    }();

    HomePage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"]
      }, {
        type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_6__["AppEventsService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"], {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"])], HomePage.prototype, "content", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('recentSlider', {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonSlides"])], HomePage.prototype, "slider", void 0);
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-home',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./home.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home/home.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./home.page.scss */
      "./src/app/home-pages/home/home.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"], src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_6__["AppEventsService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]])], HomePage);
    /***/
  }
}]);
//# sourceMappingURL=home-pages-home-home-module-es5.js.map