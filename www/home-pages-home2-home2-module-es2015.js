(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-pages-home2-home2-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home2/home2.page.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home2/home2.page.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n\r\n    <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-title mode=ios>\r\n      {{config.appName|translate}}\r\n      <!-- <ion-img src=\"assets/logo_header.png\" alt=\"logo\"></ion-img> -->\r\n    </ion-title>\r\n    <ion-buttons slot=\"end\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-button fill=\"clear\" routerLink=\"/search\" routerDirection=\"forward\">\r\n        <ion-icon slot=\"icon-only\" name=\"search\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button fill=\"clear\" routerLink=\"/cart\" routerDirection=\"forward\">\r\n        <ion-icon name=\"basket\"></ion-icon>\r\n        <ion-badge color=\"secondary\">{{shared.cartTotalItems()}}</ion-badge>\r\n      </ion-button>\r\n    </ion-buttons>\r\n\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content [scrollEvents]=\"true\" (ionScroll)=\"onScroll($event)\" class=\"ion-padding-bottom\">\r\n  <app-banner></app-banner>\r\n  <!-- Flash sale items products -->\r\n\r\n  <div class=\"module\" *ngIf=\"shared.flashSaleProducts.length!=0\">\r\n    <ion-row class=\"top-icon-header\">\r\n      <ion-button fill=\"clear\">\r\n        <ion-icon slot=\"start\" name=\"time\"></ion-icon>\r\n        {{'Flash Sale'|translate}}\r\n      </ion-button>\r\n    </ion-row>\r\n\r\n    <!-- recently viewed swipe slider -->\r\n    <ion-slides #recentSlider [options]=\"sliderConfig\">\r\n      <ion-slide *ngFor=\"let p of shared.flashSaleProducts\">\r\n        <app-product [data]=\"p\" [type]=\"'flash'\"></app-product>\r\n      </ion-slide>\r\n    </ion-slides>\r\n  </div>\r\n  <app-sliding-tabs [type]=\"'image'\"></app-sliding-tabs>\r\n\r\n</ion-content>\r\n\r\n<ion-fab vertical=\"bottom\" horizontal=\"end\" *ngIf=\"scrollTopButton\">\r\n  <ion-fab-button color=\"secondary\" (click)=\"scrollToTop()\">\r\n    <ion-icon name=\"arrow-up\"></ion-icon>\r\n  </ion-fab-button>\r\n</ion-fab>");

/***/ }),

/***/ "./src/app/home-pages/home2/home2.module.ts":
/*!**************************************************!*\
  !*** ./src/app/home-pages/home2/home2.module.ts ***!
  \**************************************************/
/*! exports provided: Home2PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Home2PageModule", function() { return Home2PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _home2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home2.page */ "./src/app/home-pages/home2/home2.page.ts");
/* harmony import */ var src_components_share_share_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/components/share/share.module */ "./src/components/share/share.module.ts");
/* harmony import */ var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/pipes/pipes.module */ "./src/pipes/pipes.module.ts");









const routes = [
    {
        path: '',
        component: _home2_page__WEBPACK_IMPORTED_MODULE_6__["Home2Page"]
    }
];
let Home2PageModule = class Home2PageModule {
};
Home2PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_8__["PipesModule"],
            src_components_share_share_module__WEBPACK_IMPORTED_MODULE_7__["ShareModule"],
        ],
        declarations: [_home2_page__WEBPACK_IMPORTED_MODULE_6__["Home2Page"]]
    })
], Home2PageModule);



/***/ }),

/***/ "./src/app/home-pages/home2/home2.page.scss":
/*!**************************************************!*\
  !*** ./src/app/home-pages/home2/home2.page.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-fab {\n  position: fixed;\n}\nion-fab ion-fab-button {\n  --background: var(--ion-color-primary);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9ob21lLXBhZ2VzL2hvbWUyL2hvbWUyLnBhZ2Uuc2NzcyIsInNyYy9hcHAvaG9tZS1wYWdlcy9ob21lMi9ob21lMi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFBO0FDQ0o7QURBSTtFQUNFLHNDQUFBO0FDRU4iLCJmaWxlIjoic3JjL2FwcC9ob21lLXBhZ2VzL2hvbWUyL2hvbWUyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1mYWIge1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgaW9uLWZhYi1idXR0b24ge1xyXG4gICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgIH1cclxuICB9IiwiaW9uLWZhYiB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbn1cbmlvbi1mYWIgaW9uLWZhYi1idXR0b24ge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/home-pages/home2/home2.page.ts":
/*!************************************************!*\
  !*** ./src/app/home-pages/home2/home2.page.ts ***!
  \************************************************/
/*! exports provided: Home2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Home2Page", function() { return Home2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/providers/shared-data/shared-data.service */ "./src/providers/shared-data/shared-data.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/providers/config/config.service */ "./src/providers/config/config.service.ts");
/* harmony import */ var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/providers/app-events/app-events.service */ "./src/providers/app-events/app-events.service.ts");






let Home2Page = class Home2Page {
    constructor(shared, appEventsService, config) {
        this.shared = shared;
        this.appEventsService = appEventsService;
        this.config = config;
        this.scrollTopButton = false; //for scroll down fab 
        this.sliderConfig = {
            slidesPerView: this.config.productSlidesPerPage,
            spaceBetween: 0
        };
    }
    // For FAB Scroll
    onScroll(e) {
        if (e.detail.scrollTop >= 500) {
            this.scrollTopButton = true;
        }
        if (e.detail.scrollTop < 500) {
            this.scrollTopButton = false;
        }
    }
    // For Scroll To Top Content
    scrollToTop() {
        this.content.scrollToTop(700);
        this.scrollTopButton = false;
    }
    ngOnInit() {
    }
    ionViewDidEnter() {
        this.shared.hideSplashScreen();
    }
    ionViewWillEnter() {
        if (!this.config.appInProduction) {
            this.config.productCardStyle = "5";
        }
    }
};
Home2Page.ctorParameters = () => [
    { type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__["SharedDataService"] },
    { type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__["AppEventsService"] },
    { type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonContent"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonContent"])
], Home2Page.prototype, "content", void 0);
Home2Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home2',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home2.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home2/home2.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home2.page.scss */ "./src/app/home-pages/home2/home2.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__["SharedDataService"],
        src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__["AppEventsService"],
        src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"]])
], Home2Page);



/***/ })

}]);
//# sourceMappingURL=home-pages-home2-home2-module-es2015.js.map