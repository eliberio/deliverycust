function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
  /***/
  "./$$_lazy_route_resource lazy recursive":
  /*!******************************************************!*\
    !*** ./$$_lazy_route_resource lazy namespace object ***!
    \******************************************************/

  /*! no static exports found */

  /***/
  function $$_lazy_route_resourceLazyRecursive(module, exports, __webpack_require__) {
    var map = {
      "./about-us/about-us.module": ["./src/app/about-us/about-us.module.ts", "about-us-about-us-module"],
      "./add-review/add-review.module": ["./src/app/add-review/add-review.module.ts", "add-review-add-review-module"],
      "./address-pages/addresses/addresses.module": ["./src/app/address-pages/addresses/addresses.module.ts", "address-pages-addresses-addresses-module"],
      "./address-pages/billing-address/billing-address.module": ["./src/app/address-pages/billing-address/billing-address.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "address-pages-billing-address-billing-address-module"],
      "./address-pages/shipping-address/shipping-address.module": ["./src/app/address-pages/shipping-address/shipping-address.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "address-pages-shipping-address-shipping-address-module"],
      "./cart/cart.module": ["./src/app/cart/cart.module.ts", "default~cart-cart-module~order-order-module", "cart-cart-module"],
      "./categorie-pages/categories/categories.module": ["./src/app/categorie-pages/categories/categories.module.ts", "categorie-pages-categories-categories-module"],
      "./categorie-pages/categories2/categories2.module": ["./src/app/categorie-pages/categories2/categories2.module.ts", "categorie-pages-categories2-categories2-module"],
      "./categorie-pages/categories3/categories3.module": ["./src/app/categorie-pages/categories3/categories3.module.ts", "categorie-pages-categories3-categories3-module"],
      "./categorie-pages/categories4/categories4.module": ["./src/app/categorie-pages/categories4/categories4.module.ts", "categorie-pages-categories4-categories4-module"],
      "./categorie-pages/categories5/categories5.module": ["./src/app/categorie-pages/categories5/categories5.module.ts", "categorie-pages-categories5-categories5-module"],
      "./categorie-pages/categories6/categories6.module": ["./src/app/categorie-pages/categories6/categories6.module.ts", "categorie-pages-categories6-categories6-module"],
      "./contact-us/contact-us.module": ["./src/app/contact-us/contact-us.module.ts", "contact-us-contact-us-module"],
      "./home-pages/home/home.module": ["./src/app/home-pages/home/home.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "home-pages-home-home-module"],
      "./home-pages/home10/home10.module": ["./src/app/home-pages/home10/home10.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "home-pages-home10-home10-module"],
      "./home-pages/home2/home2.module": ["./src/app/home-pages/home2/home2.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "home-pages-home2-home2-module"],
      "./home-pages/home3/home3.module": ["./src/app/home-pages/home3/home3.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "home-pages-home3-home3-module"],
      "./home-pages/home4/home4.module": ["./src/app/home-pages/home4/home4.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "home-pages-home4-home4-module"],
      "./home-pages/home5/home5.module": ["./src/app/home-pages/home5/home5.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "home-pages-home5-home5-module"],
      "./home-pages/home6/home6.module": ["./src/app/home-pages/home6/home6.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "home-pages-home6-home6-module"],
      "./home-pages/home7/home7.module": ["./src/app/home-pages/home7/home7.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "home-pages-home7-home7-module"],
      "./home-pages/home8/home8.module": ["./src/app/home-pages/home8/home8.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "home-pages-home8-home8-module"],
      "./home-pages/home9/home9.module": ["./src/app/home-pages/home9/home9.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "home-pages-home9-home9-module"],
      "./intro/intro.module": ["./src/app/intro/intro.module.ts", "intro-intro-module"],
      "./my-account/my-account.module": ["./src/app/my-account/my-account.module.ts", "my-account-my-account-module"],
      "./my-order-detail/my-order-detail.module": ["./src/app/my-order-detail/my-order-detail.module.ts", "my-order-detail-my-order-detail-module"],
      "./my-orders/my-orders.module": ["./src/app/my-orders/my-orders.module.ts", "my-orders-my-orders-module"],
      "./news-detail/news-detail.module": ["./src/app/news-detail/news-detail.module.ts", "news-detail-news-detail-module"],
      "./news-list/news-list.module": ["./src/app/news-list/news-list.module.ts", "news-list-news-list-module"],
      "./news/news.module": ["./src/app/news/news.module.ts", "news-news-module"],
      "./order/order.module": ["./src/app/order/order.module.ts", "default~cart-cart-module~order-order-module", "order-order-module"],
      "./product-detail/product-detail.module": ["./src/app/product-detail/product-detail.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "product-detail-product-detail-module"],
      "./products/products.module": ["./src/app/products/products.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "products-products-module"],
      "./reviews/reviews.module": ["./src/app/reviews/reviews.module.ts", "reviews-reviews-module"],
      "./search/search.module": ["./src/app/search/search.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "search-search-module"],
      "./settings/settings.module": ["./src/app/settings/settings.module.ts", "settings-settings-module"],
      "./shipping-method/shipping-method.module": ["./src/app/shipping-method/shipping-method.module.ts", "shipping-method-shipping-method-module"],
      "./thank-you/thank-you.module": ["./src/app/thank-you/thank-you.module.ts", "thank-you-thank-you-module"],
      "./wish-list/wish-list.module": ["./src/app/wish-list/wish-list.module.ts", "default~address-pages-billing-address-billing-address-module~address-pages-shipping-address-shipping~c5bb259f", "wish-list-wish-list-module"]
    };

    function webpackAsyncContext(req) {
      if (!__webpack_require__.o(map, req)) {
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      var ids = map[req],
          id = ids[0];
      return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function () {
        return __webpack_require__(id);
      });
    }

    webpackAsyncContext.keys = function webpackAsyncContextKeys() {
      return Object.keys(map);
    };

    webpackAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
    module.exports = webpackAsyncContext;
    /***/
  },

  /***/
  "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$":
  /*!*****************************************************************************************************************************************!*\
    !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
    \*****************************************************************************************************************************************/

  /*! no static exports found */

  /***/
  function node_modulesIonicCoreDistEsmLazyRecursiveEntryJs$IncludeEntryJs$ExcludeSystemEntryJs$(module, exports, __webpack_require__) {
    var map = {
      "./ion-action-sheet-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-action-sheet-ios.entry.js", "common", 0],
      "./ion-action-sheet-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-action-sheet-md.entry.js", "common", 1],
      "./ion-alert-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-alert-ios.entry.js", "common", 2],
      "./ion-alert-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-alert-md.entry.js", "common", 3],
      "./ion-app_8-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-app_8-ios.entry.js", "common", 4],
      "./ion-app_8-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-app_8-md.entry.js", "common", 5],
      "./ion-avatar_3-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-avatar_3-ios.entry.js", "common", 6],
      "./ion-avatar_3-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-avatar_3-md.entry.js", "common", 7],
      "./ion-back-button-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-back-button-ios.entry.js", "common", 8],
      "./ion-back-button-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-back-button-md.entry.js", "common", 9],
      "./ion-backdrop-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-backdrop-ios.entry.js", 10],
      "./ion-backdrop-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-backdrop-md.entry.js", 11],
      "./ion-button_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-button_2-ios.entry.js", "common", 12],
      "./ion-button_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-button_2-md.entry.js", "common", 13],
      "./ion-card_5-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-card_5-ios.entry.js", "common", 14],
      "./ion-card_5-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-card_5-md.entry.js", "common", 15],
      "./ion-checkbox-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-checkbox-ios.entry.js", "common", 16],
      "./ion-checkbox-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-checkbox-md.entry.js", "common", 17],
      "./ion-chip-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-chip-ios.entry.js", "common", 18],
      "./ion-chip-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-chip-md.entry.js", "common", 19],
      "./ion-col_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-col_3.entry.js", 20],
      "./ion-datetime_3-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-datetime_3-ios.entry.js", "common", 21],
      "./ion-datetime_3-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-datetime_3-md.entry.js", "common", 22],
      "./ion-fab_3-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-fab_3-ios.entry.js", "common", 23],
      "./ion-fab_3-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-fab_3-md.entry.js", "common", 24],
      "./ion-img.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-img.entry.js", 25],
      "./ion-infinite-scroll_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-ios.entry.js", "common", 26],
      "./ion-infinite-scroll_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-md.entry.js", "common", 27],
      "./ion-input-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-input-ios.entry.js", "common", 28],
      "./ion-input-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-input-md.entry.js", "common", 29],
      "./ion-item-option_3-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item-option_3-ios.entry.js", "common", 30],
      "./ion-item-option_3-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item-option_3-md.entry.js", "common", 31],
      "./ion-item_8-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item_8-ios.entry.js", "common", 32],
      "./ion-item_8-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item_8-md.entry.js", "common", 33],
      "./ion-loading-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-loading-ios.entry.js", "common", 34],
      "./ion-loading-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-loading-md.entry.js", "common", 35],
      "./ion-menu_3-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-menu_3-ios.entry.js", "common", 36],
      "./ion-menu_3-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-menu_3-md.entry.js", "common", 37],
      "./ion-modal-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-modal-ios.entry.js", "common", 38],
      "./ion-modal-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-modal-md.entry.js", "common", 39],
      "./ion-nav_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-nav_2.entry.js", "common", 40],
      "./ion-popover-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-popover-ios.entry.js", "common", 41],
      "./ion-popover-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-popover-md.entry.js", "common", 42],
      "./ion-progress-bar-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-progress-bar-ios.entry.js", "common", 43],
      "./ion-progress-bar-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-progress-bar-md.entry.js", "common", 44],
      "./ion-radio_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-radio_2-ios.entry.js", "common", 45],
      "./ion-radio_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-radio_2-md.entry.js", "common", 46],
      "./ion-range-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-range-ios.entry.js", "common", 47],
      "./ion-range-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-range-md.entry.js", "common", 48],
      "./ion-refresher_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-refresher_2-ios.entry.js", "common", 49],
      "./ion-refresher_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-refresher_2-md.entry.js", "common", 50],
      "./ion-reorder_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-reorder_2-ios.entry.js", "common", 51],
      "./ion-reorder_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-reorder_2-md.entry.js", "common", 52],
      "./ion-ripple-effect.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-ripple-effect.entry.js", 53],
      "./ion-route_4.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-route_4.entry.js", "common", 54],
      "./ion-searchbar-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-searchbar-ios.entry.js", "common", 55],
      "./ion-searchbar-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-searchbar-md.entry.js", "common", 56],
      "./ion-segment_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-segment_2-ios.entry.js", "common", 57],
      "./ion-segment_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-segment_2-md.entry.js", "common", 58],
      "./ion-select_3-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-select_3-ios.entry.js", "common", 59],
      "./ion-select_3-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-select_3-md.entry.js", "common", 60],
      "./ion-slide_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-slide_2-ios.entry.js", 61],
      "./ion-slide_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-slide_2-md.entry.js", 62],
      "./ion-spinner.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-spinner.entry.js", "common", 63],
      "./ion-split-pane-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-split-pane-ios.entry.js", 64],
      "./ion-split-pane-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-split-pane-md.entry.js", 65],
      "./ion-tab-bar_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-ios.entry.js", "common", 66],
      "./ion-tab-bar_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-md.entry.js", "common", 67],
      "./ion-tab_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-tab_2.entry.js", "common", 68],
      "./ion-text.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-text.entry.js", "common", 69],
      "./ion-textarea-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-textarea-ios.entry.js", "common", 70],
      "./ion-textarea-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-textarea-md.entry.js", "common", 71],
      "./ion-toast-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toast-ios.entry.js", "common", 72],
      "./ion-toast-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toast-md.entry.js", "common", 73],
      "./ion-toggle-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toggle-ios.entry.js", "common", 74],
      "./ion-toggle-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toggle-md.entry.js", "common", 75],
      "./ion-virtual-scroll.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-virtual-scroll.entry.js", 76]
    };

    function webpackAsyncContext(req) {
      if (!__webpack_require__.o(map, req)) {
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      var ids = map[req],
          id = ids[0];
      return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function () {
        return __webpack_require__(id);
      });
    }

    webpackAsyncContext.keys = function webpackAsyncContextKeys() {
      return Object.keys(map);
    };

    webpackAsyncContext.id = "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$";
    module.exports = webpackAsyncContext;
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
  /*!**************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
    \**************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAppComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-app [class.green-theme]=\"config.appTheme=='green'\" [class.plum-theme]=\"config.appTheme=='plum'\"\r\n  [class.magnesium-theme]=\"config.appTheme=='magnesium'\" [class.salmon-theme]=\"config.appTheme=='salmon'\"\r\n  [class.blue-theme]=\"config.appTheme=='blue'\" [class.pink-theme]=\"config.appTheme=='pink'\"\r\n  [class.orange-theme]=\"config.appTheme=='orange'\" [class.maroon-theme]=\"config.appTheme=='maroon'\"\r\n  [class.cayanne-theme]=\"config.appTheme=='cayanne'\" [class.red-theme]=\"config.appTheme=='red'\"\r\n  [class.sea-theme]=\"config.appTheme=='sea'\" [class.sky-theme]=\"config.appTheme=='sky'\"\r\n  [class.grape-theme]=\"config.appTheme=='grape'\" [class.dark-theme]=\"config.darkMode==true\"\r\n  [class.light-theme]=\"config.darkMode==false\" [class.default-theme]=\"config.appTheme=='default'\"\r\n  [class.white-theme]=\"config.appTheme=='white'\"\r\n  \r\n  [class.theme15]=\"config.appTheme=='theme15'\"\r\n  [class.theme16]=\"config.appTheme=='theme16'\"\r\n  [class.theme17]=\"config.appTheme=='theme17'\"\r\n  [class.theme18]=\"config.appTheme=='theme18'\"\r\n\r\n  [class.light-mode-plus-white-theme]=\"config.appTheme=='white' && config.darkMode==false\"\r\n  [class.dark-mode-plus-white-theme]=\"config.appTheme=='white' && config.darkMode==true\"\r\n  [class.light-mode-plus-black-theme]=\"config.appTheme=='default' && config.darkMode==false\"\r\n  [class.dark-mode-plus-black-theme]=\"config.appTheme=='default' && config.darkMode==true\"\r\n  class=\"copyPrimaryToSecondary\">\r\n\r\n  <ion-split-pane contentId=\"main-content\" content=\"main-content\">\r\n    <ion-menu type=\"overlay\" *ngIf=\"!config.appNavigationTabs\" contentId=\"main-content\" content=\"main-content\">\r\n      <ion-header>\r\n        <ion-toolbar (click)=\"click()\">\r\n          <ion-title>{{config.appName|translate}}</ion-title>\r\n          <ion-buttons slot=\"end\">\r\n            <ion-button fill=\"clear\" (click)=\"openCurrencyPage()\">\r\n              <ion-icon slot=\"icon-only\" name=\"logo-usd\"></ion-icon>\r\n            </ion-button>\r\n            <ion-button fill=\"clear\" (click)=\"openLanguagePage()\">\r\n              <ion-icon name=\"globe\"></ion-icon>\r\n            </ion-button>\r\n          </ion-buttons>\r\n        </ion-toolbar>\r\n      </ion-header>\r\n      <ion-content>\r\n\r\n        <ion-list class=\"ion-no-padding\">\r\n          <ion-item lines=\"none\" *ngIf=\"shared.customerData.customers_id==null\" (click)=\"openLoginPage()\">\r\n            <div></div>\r\n            <ion-item lines=\"none\">\r\n              <ion-avatar>\r\n                <img src=\"assets/avatar.png\" />\r\n              </ion-avatar>\r\n              <ion-label class=\"ion-padding\">\r\n                <h2>{{ 'Login & Register' | translate }}</h2>\r\n                <p>{{ 'Please login or create an account for free' | translate }}</p>\r\n              </ion-label>\r\n            </ion-item>\r\n          </ion-item>\r\n\r\n          <ion-item lines=\"none\" *ngIf=\"shared.customerData.customers_id!=null\" (click)=\"openPage('/settings')\">\r\n            <div></div>\r\n            <ion-item lines=\"none\">\r\n              <ion-avatar class=\"nameTextAvatar\">\r\n                <ion-fab-button id=\"nametext\">{{getNameFirstLetter()}}</ion-fab-button>\r\n              </ion-avatar>\r\n              <ion-label class=\"ion-padding\">\r\n                <h2>{{shared.customerData.customers_firstname +\"&nbsp;\"+shared.customerData.customers_lastname}}</h2>\r\n                <p>{{shared.customerData.email}}</p>\r\n              </ion-label>\r\n            </ion-item>\r\n          </ion-item>\r\n\r\n\r\n\r\n          <ion-item lines=\"none\" *ngFor=\"let p of getLeftItems()\" class=\"ion-no-padding\">\r\n            <ion-grid class=\"ion-no-padding\">\r\n              <ion-row class=\"ion-no-padding\" (click)=\"openPage(p.url)\" *ngIf=\"!p.items\">\r\n                <ion-col class=\"ion-align-self-center\" size=\"2\">\r\n                  <img *ngIf=\"showImg()\" src=\"{{p.img}}\">\r\n                  <ion-icon *ngIf=\"!showImg()\" name=\"{{p.icon}}\"></ion-icon>\r\n                </ion-col>\r\n                <ion-col class=\"col-text\" class=\"ion-align-self-center\" size=\"8\">\r\n                  <p>\r\n                    {{p.name | translate}}\r\n                  </p>\r\n                </ion-col>\r\n              </ion-row>\r\n              <ion-row class=\"ion-no-padding\" *ngIf=\"p.items\" (click)=\"expandItem(p)\">\r\n                <ion-col class=\"ion-align-self-center\" size=\"2\">\r\n                  <img *ngIf=\"showImg()\" src=\"{{p.img}}\">\r\n                  <ion-icon *ngIf=\"!showImg()\" name=\"{{p.icon}}\"></ion-icon>\r\n                </ion-col>\r\n                <ion-col class=\"col-text\" class=\"ion-align-self-center\" size=\"8\">\r\n                  <p>\r\n                    {{p.name | translate}}\r\n                  </p>\r\n                </ion-col>\r\n                <ion-col class=\"ion-align-self-center\" size=\"2\">\r\n                  <ion-icon name=\"add\" *ngIf=\"!p.expanded\"></ion-icon>\r\n                  <ion-icon name=\"remove\" *ngIf=\"p.expanded\"></ion-icon>\r\n                </ion-col>\r\n              </ion-row>\r\n              <app-menu-component expandHeight=\"700px\" *ngIf=\"p.items\" [expanded]=\"p.expanded\" class=\"ion-no-padding\">\r\n                <ion-item class=\"item-inner-list\" lines=\"none\" *ngFor=\"let list of p.items;\"\r\n                  (click)=\"openPage(list.url)\">\r\n                  <ion-grid>\r\n                    <ion-row>\r\n                      <ion-col class=\"ion-align-self-center\" size=\"2\">\r\n                        <ion-icon name=\"remove\"></ion-icon>\r\n                      </ion-col>\r\n                      <ion-col class=\"ion-align-self-center\">\r\n                        <p><span *ngIf=\"p.name!='Shop'\">{{p.name | translate}} - </span> {{list.name| translate}}</p>\r\n                      </ion-col>\r\n                    </ion-row>\r\n                  </ion-grid>\r\n                </ion-item>\r\n              </app-menu-component>\r\n            </ion-grid>\r\n          </ion-item>\r\n        </ion-list>\r\n      </ion-content>\r\n    </ion-menu>\r\n    <ion-router-outlet id=\"main-content\"></ion-router-outlet>\r\n  </ion-split-pane>\r\n</ion-app>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/blank-modal/blank-modal.page.html":
  /*!************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modals/blank-modal/blank-modal.page.html ***!
    \************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppModalsBlankModalBlankModalPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/currency-list/currency-list.page.html":
  /*!****************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modals/currency-list/currency-list.page.html ***!
    \****************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppModalsCurrencyListCurrencyListPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button fill=\"clear\" (click)=\"dismiss()\">\r\n        <ion-icon name=\"close\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-no-padding\">{{\"Select Currency\"|translate}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-list>\r\n    <ion-radio-group [(ngModel)]=\"currency\" (ionChange)=\"updateCurrency()\">\r\n      <ion-item *ngFor=\"let v of currencyList\">\r\n        <ion-label>{{v.title}}({{v.symbol_left}}{{v.symbol_right}})</ion-label>\r\n        <ion-radio [value]=\"v\" ></ion-radio>\r\n      </ion-item>\r\n    </ion-radio-group>\r\n  </ion-list>\r\n</ion-content>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/edit-address/edit-address.page.html":
  /*!**************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modals/edit-address/edit-address.page.html ***!
    \**************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppModalsEditAddressEditAddressPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button fill=\"clear\" (click)=\"dismiss()\">\r\n        <ion-icon name=\"close\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title>{{'Shipping Address'|translate}}</ion-title>\r\n\r\n  </ion-toolbar>\r\n\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <form #loginForm=\"ngForm\">\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-list>\r\n          <ion-item>\r\n            <ion-input type=\"text\" placeholder=\"{{'First Name'|translate}}\" name=\"firstname\"\r\n              [(ngModel)]=\"shippingData.entry_firstname\" required></ion-input>\r\n          </ion-item>\r\n\r\n          <ion-item>\r\n            <ion-input type=\"text\" placeholder=\"{{'Last Name'|translate}}\" name=\"lastname\"\r\n              [(ngModel)]=\"shippingData.entry_lastname\" required></ion-input>\r\n          </ion-item>\r\n          <ion-item>\r\n            <ion-input type=\"text\" placeholder=\"{{'Address'|translate}}\" name=\"street\"\r\n              [(ngModel)]=\"shippingData.entry_street_address\" required></ion-input>\r\n          </ion-item>\r\n          <ion-item>\r\n            <ion-input type=\"text\" placeholder=\"{{'Country'|translate}}\" name=\"country_name\" (click)=\"selectCountryPage()\"\r\n              [readonly]=\"true\" [(ngModel)]=\"shippingData.entry_country_name\" required></ion-input>\r\n          </ion-item>\r\n          <ion-item>\r\n            <ion-input type=\"text\" placeholder=\"{{'Zone'|translate}}\" required name=\"zone_name\" (click)=\"selectZonePage()\"\r\n              [readonly]=\"true\" [(ngModel)]=\"shippingData.entry_zone\"></ion-input>\r\n          </ion-item>\r\n          <ion-item>\r\n            <ion-input type=\"text\" placeholder=\"{{'City'|translate}}\" name=\"city\" [(ngModel)]=\"shippingData.entry_city\"\r\n              required></ion-input>\r\n          </ion-item>\r\n          <ion-item>\r\n            <ion-input type=\"text\" placeholder=\"{{'Post code'|translate}}\" name=\"postcode\"\r\n              [(ngModel)]=\"shippingData.entry_postcode\" required></ion-input>\r\n          </ion-item>\r\n\r\n        </ion-list>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n  </form>\r\n  <ion-button expand=\"full\" color=\"secondary\" *ngIf=\"type=='update'\" (click)=\"updateShippingAddress()\"\r\n    [disabled]=\"!loginForm.form.valid\">{{'Update Address'|translate}}</ion-button>\r\n  <ion-button expand=\"full\" color=\"secondary\" *ngIf=\"type=='add'\" (click)=\"addShippingAddress()\"\r\n    [disabled]=\"!loginForm.form.valid\">{{'Save Address'|translate}}</ion-button>\r\n</ion-content>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/forgot-password/forgot-password.page.html":
  /*!********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modals/forgot-password/forgot-password.page.html ***!
    \********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppModalsForgotPasswordForgotPasswordPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button fill=\"clear\" (click)=\"dismiss()\">\r\n        <ion-icon name=\"close\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-no-padding\">{{'Forgot Password'|translate}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <form #loginForm=\"ngForm\" class=\"form\" (ngSubmit)=\"forgetPassword()\">\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Email'|translate}}</ion-label>\r\n      <ion-input type=\"email\" name=\"customers_email_address\" [(ngModel)]=\"formData.email\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item *ngIf=\"errorMessage!=''\" lines=\"none\">\r\n      <ion-label>\r\n        {{errorMessage| translate}}\r\n      </ion-label>\r\n    </ion-item>\r\n\r\n    <ion-button disabled=\"true\" expand=\"full\" color=\"secondary\" type=\"submit\" [disabled]=\"!loginForm.form.valid\">\r\n      {{'Send'|translate}}\r\n    </ion-button>\r\n  </form>\r\n</ion-content>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/language/language.page.html":
  /*!******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modals/language/language.page.html ***!
    \******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppModalsLanguageLanguagePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button fill=\"clear\" (click)=\"dismiss()\">\r\n        <ion-icon name=\"close\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-no-padding\">{{'Select Language'|translate}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n<ion-content>\r\n  <ion-list>\r\n    <ion-radio-group [(ngModel)]=\"selectedLanguage\" (ionChange)=\"updateLanguage(selectedLanguage)\">\r\n      <ion-item *ngFor=\"let l of languages\">\r\n        <ion-img src=\"{{config.imgUrl+l.image}}\" style=\"width:80px;height:60px\" class=\"ion-padding-end\"></ion-img>\r\n        <ion-label>{{l.name}}</ion-label>\r\n        <ion-radio [value]=\"l\" ></ion-radio>\r\n      </ion-item>\r\n    </ion-radio-group>\r\n  </ion-list>\r\n</ion-content>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/login/login.page.html":
  /*!************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modals/login/login.page.html ***!
    \************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppModalsLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button fill=\"clear\" (click)=\"dismiss()\">\r\n        <ion-icon name=\"close\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-no-padding\">{{'Login'|translate}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <form #loginForm=\"ngForm\" class=\"form\" (ngSubmit)=\"login()\">\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Email'|translate}}</ion-label>\r\n      <ion-input type=\"text\" name=\"email\" [(ngModel)]=\"formData.email\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Password'|translate}}</ion-label>\r\n      <ion-input type=\"password\" name=\"password\" [(ngModel)]=\"formData.password\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item lines=\"none\" *ngIf=\"errorMessage!=''\">\r\n      <ion-label>\r\n        {{errorMessage| translate}}\r\n      </ion-label>\r\n    </ion-item>\r\n\r\n\r\n    <ion-button [disabled]=\"!loginForm.form.valid\" color=\"secondary\" expand=\"full\" type=\"submit\">\r\n      {{ 'Login' | translate }}\r\n    </ion-button>\r\n\r\n  </form>\r\n\r\n  <ion-button expand=\"full\" fill=\"clear\" color=\"dark\" (click)=\"openForgetPasswordPage()\">{{ \"I've forgotten my\r\n      password?\" | translate }}</ion-button>\r\n\r\n  <ion-button expand=\"full\" *ngIf=\"config.fbButton==1\" class=\"fb-button\" (click)=\"facebookLogin()\">{{ 'Login with' | translate }}\r\n    <ion-icon name=\"logo-facebook\"></ion-icon>\r\n  </ion-button>\r\n  <ion-button color=\"danger\" expand=\"full\" *ngIf=\"config.googleButton==1\" (click)=\"googleLogin()\">\r\n    {{ 'Login with' | translate }}\r\n    <ion-icon name=\"logo-google\"></ion-icon>\r\n  </ion-button>\r\n  <ion-button expand=\"block\" fill=\"outline\" (click)=\"openSignUpPage()\">{{ 'Register' | translate }}</ion-button>\r\n  <ion-button expand=\"block\" *ngIf=\"!hideGuestLogin\" [disabled]=\"shared.cartProducts.length==0\" (click)=\"guestLogin()\">\r\n    {{'Continue as a Guest'|translate}}</ion-button>\r\n  <!-- *ngIf=\"config.guestCheckOut && hideGuestLogin\" -->\r\n</ion-content>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/privacy-policy/privacy-policy.page.html":
  /*!******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modals/privacy-policy/privacy-policy.page.html ***!
    \******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppModalsPrivacyPolicyPrivacyPolicyPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button fill=\"clear\" (click)=\"dismiss()\">\r\n        <ion-icon name=\"close\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title>{{'Privacy Policy'|translate}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-text>\r\n    <p [innerHTML]=\"shared.privacyPolicy\">\r\n    </p>\r\n  </ion-text>\r\n</ion-content>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/refund-policy/refund-policy.page.html":
  /*!****************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modals/refund-policy/refund-policy.page.html ***!
    \****************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppModalsRefundPolicyRefundPolicyPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button fill=\"clear\" (click)=\"dismiss()\">\r\n        <ion-icon name=\"close\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title>{{'Refund Policy'|translate}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-text>\r\n    <p [innerHTML]=\"shared.refundPolicy\">\r\n    </p>\r\n  </ion-text>\r\n</ion-content>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/select-country/select-country.page.html":
  /*!******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modals/select-country/select-country.page.html ***!
    \******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppModalsSelectCountrySelectCountryPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button fill=\"clear\" (click)=\"dismiss()\">\r\n        <ion-icon name=\"close\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title>{{'Country'|translate}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <ion-searchbar (ionInput)=\"getItems($event)\" placeholder=\"{{'Search'|translate}}\" #Searchbar></ion-searchbar>\r\n  <ion-list>\r\n    <ion-item *ngFor=\"let item of items\" (click)=\"selectCountry(item)\">\r\n      {{ item.countries_name }}\r\n    </ion-item>\r\n  </ion-list>\r\n</ion-content>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/select-zones/select-zones.page.html":
  /*!**************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modals/select-zones/select-zones.page.html ***!
    \**************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppModalsSelectZonesSelectZonesPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button fill=\"clear\" (click)=\"dismiss()\">\r\n        <ion-icon name=\"close\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title>{{'Zone'|translate}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content class=\"page-select-zones\">\r\n  <ion-searchbar (ionInput)=\"getItems($event)\" placeholder=\"{{'Search'|translate}}\" autofocus></ion-searchbar>\r\n  <ion-list>\r\n    <ion-item *ngFor=\"let item of items\" (click)=\"selectZone(item)\">\r\n      {{ item.zone_name }}\r\n    </ion-item>\r\n    <ion-item (click)=\"selectZone('other')\">\r\n      {{'other'|translate}}\r\n    </ion-item>\r\n  </ion-list>\r\n</ion-content>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/sign-up/sign-up.page.html":
  /*!****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modals/sign-up/sign-up.page.html ***!
    \****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppModalsSignUpSignUpPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button fill=\"clear\" (click)=\"dismiss()\">\r\n        <ion-icon name=\"close\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-no-padding\">{{'Create an Account'|translate}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <form #loginForm=\"ngForm\" class=\"form\" (ngSubmit)=\"registerUser()\">\r\n\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'First Name'|translate}}</ion-label>\r\n      <ion-input type=\"text\" name=\"customers_firstname\" [(ngModel)]=\"formData.customers_firstname\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Last Name'|translate}}</ion-label>\r\n      <ion-input type=\"text\" name=\"customers_lastname\" [(ngModel)]=\"formData.customers_lastname\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Mobile'|translate}}</ion-label>\r\n      <ion-input type=\"number\" inputmode=\"tel\" name=\"Mobile\" [(ngModel)]=\"formData.customers_telephone\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n        <ion-label position=\"floating\">{{'Email'|translate}}</ion-label>\r\n        <ion-input type=\"email\" email name=\"email\" [(ngModel)]=\"formData.email\" required>\r\n        </ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\">{{'Password'|translate}}</ion-label>\r\n      <ion-input type=\"password\" name=\"c_d\" [(ngModel)]=\"formData.password\" required>\r\n      </ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item *ngIf=\"errorMessage!=''\" lines=\"none\">\r\n      <ion-label>\r\n        {{errorMessage| translate}}\r\n      </ion-label>\r\n    </ion-item>\r\n\r\n    <p>{{\"Creating an account means you’re okay with our\"|translate}}\r\n      <a (click)=\"openTermServicesPage()\">{{'Term and Services'|translate}}</a>,\r\n      <a (click)=\"openPrivacyPolicyPage()\">{{'Privacy Policy'|translate}}</a> {{'and'|translate}}\r\n      <a (click)=\"openRefundPolicyPage()\">{{'Refund Policy'|translate}}</a>\r\n    </p>\r\n\r\n    <ion-button disabled=\"true\" expand=\"full\" color=\"secondary\" type=\"submit\" [disabled]=\"!loginForm.form.valid\">\r\n      {{'Register'|translate}}\r\n    </ion-button>\r\n  </form>\r\n</ion-content>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/term-services/term-services.page.html":
  /*!****************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modals/term-services/term-services.page.html ***!
    \****************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppModalsTermServicesTermServicesPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button fill=\"clear\" (click)=\"dismiss()\">\r\n        <ion-icon name=\"close\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title>{{'Term and Services'|translate}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-text>\r\n    <p [innerHTML]=\"shared.termServices\">\r\n    </p>\r\n  </ion-text>\r\n</ion-content>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/product-detail/product-detail.page.html":
  /*!***********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/product-detail/product-detail.page.html ***!
    \***********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppProductDetailProductDetailPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>{{product.products_name}}</ion-title>\r\n  </ion-toolbar>\r\n\r\n  <ion-toolbar *ngIf=\"!is_upcomming && product.flash_start_date\" color=\"danger\">\r\n    <ion-title class=\"sub-ion-title ion-text-center\">\r\n      <div class=\"div-time\">\r\n        <ion-icon name=\"time\" item-start></ion-icon> &nbsp;\r\n        {{'Discount ends in'|translate}} :&nbsp;\r\n        <app-timer [data]=\"product\"></app-timer>\r\n      </div>\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content class=\"product-detail-page\">\r\n  <div class=\"product-tags-top\">\r\n    <div class=\"product-tag-new\" *ngIf=\"checkProductNew()\">{{'New'|translate}}</div>\r\n  </div>\r\n\r\n  <div class=\"icons share-like\">\r\n    <ion-icon name=\"share\" (click)=\"share()\"></ion-icon>\r\n    <ion-icon *ngIf=\"product.isLiked!=0\" name=\"heart\" (click)=\"clickWishList()\"></ion-icon>\r\n    <ion-icon *ngIf=\"product.isLiked==0\" name=\"heart-outline\" (click)=\"clickWishList()\"></ion-icon>\r\n  </div>\r\n  <ion-slides class=\"product-slides\" pager=\"true\" [options]=\"sliderConfig\">\r\n\r\n    <ion-slide *ngIf=\"product!=null\">\r\n      <img src=\"{{config.imgUrl+product.products_image}}\" (click)=\"zoomImage(config.imgUrl+product.products_image)\">\r\n    </ion-slide>\r\n    <ion-slide *ngFor=\"let b of product.images\" (click)=\"zoomImage(config.imgUrl+b.image)\">\r\n      <div>\r\n        <img src=\"{{config.imgUrl+b.image}}\">\r\n      </div>\r\n    </ion-slide>\r\n  </ion-slides>\r\n\r\n  <ion-grid class=\"product-detail-title\">\r\n    <ion-row>\r\n      <div class=\"product-tags\">\r\n        <div class=\"product-tag-off\" *ngIf=\"product.discount_price!=null\">{{pDiscount()}}{{'OFF'|translate}}</div>\r\n      </div>\r\n      <!-- 2.0 updates -->\r\n\r\n      <ion-col class=\"price-group\" size=\"12\">\r\n        <span *ngIf=\"!product.flash_start_date\">\r\n          <span class=\"product-price-normal-through\"\r\n            *ngIf=\"product.discount_price!=null\">{{product_price |curency}}</span>\r\n          <span class=\"product-price-normal\" *ngIf=\"product.discount_price==null\">{{product_price |curency}}</span>\r\n          <span class=\"product-price-normal\" *ngIf=\"product.discount_price!=null\">{{discount_price |curency}}</span>\r\n        </span>\r\n        <span *ngIf=\"product.flash_start_date\">\r\n          <span class=\"product-price-normal-through\">{{product_price |curency}}</span>\r\n          <span class=\"product-price-normal\">{{flash_price |curency}}</span>\r\n        </span>\r\n\r\n        <span class=\"product-outstock\" *ngIf=\"cartButton=='outOfStock'\">{{'Out of Stock'|translate}}</span>\r\n        <span class=\"product-instock\" *ngIf=\"cartButton=='addToCart'\">{{'In Stock'|translate}}</span>\r\n      </ion-col>\r\n\r\n      <ion-col class=\"product-title\" size=\"12\">\r\n        <h3>{{product.products_name}}\r\n          <br>\r\n          <small *ngFor=\"let c of product.categories; let i = index\">{{c.categories_name}}<span\r\n              *ngIf=\"product.categories.length!=i+1\">,</span>&nbsp;</small>\r\n        </h3>\r\n        <div class=\"product-ratings\">\r\n          <ion-row class=\"product-rating animate\" (click)=\"openReviewsPage()\">\r\n            <div class=\"stars-outer\">\r\n              <div class=\"stars-inner\" [style.width]=\"ratingPercentage()\"></div>\r\n            </div>\r\n            <ion-col size=\"7\">\r\n              <h6>{{product.reviewed_customers.length}}&nbsp;{{'rating and review'|translate}}</h6>\r\n            </ion-col>\r\n          </ion-row>\r\n        </div>\r\n        <p class=\"ion-no-margin\">{{'Likes'|translate}}&nbsp;({{product.products_liked}})</p>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n  <ion-grid class=\"product-detail-header\">\r\n    <ion-row>\r\n      <ion-col class=\"left\" size=\"6\">\r\n        <ion-row class=\"ion-align-items-center\">\r\n          <ion-col class=\"qty-name\" size=\"12\">{{'Quantity' |translate}}</ion-col>\r\n          <ion-col size=\"2.7\" class=\"qty-vlue ion-text-right\">\r\n            <ion-button size=\"small\" color=\"secondary\" (click)=\"qunatityMinus(product);\">\r\n              <ion-icon name=\"remove\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n          <ion-col size=\"3\" class=\"qty-vlue\">\r\n            <ion-input type=number class=\"dgi\" name=\"q\" [(ngModel)]=\"product.cartQuantity\" (focusout)=\"quantityChange()\">\r\n            </ion-input>\r\n          </ion-col>\r\n          <ion-col size=\"3\" class=\"qty-vlue\">\r\n            <!-- <span class=\"dgi\">{{quantity}}</span> -->\r\n            <ion-button size=\"small\" color=\"secondary\" (click)=\"qunatityPlus(product);\">\r\n              <ion-icon name=\"add\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-col>\r\n\r\n      <ion-col class=\"ion-text-end\" size=\"6\">\r\n        <ion-row class=\"ion-no-margin\">\r\n          <ion-col class=\"ion-text-right\" class=\"ttl-name\" size=\"12\">{{'Total' |translate}}&nbsp;{{'Price' |translate}}\r\n          </ion-col>\r\n          <ion-col class=\"ion-text-right\" class=\"ttl-vlue\" size=\"12\">\r\n            {{ product.cartQuantity*current_price|curency}}</ion-col>\r\n        </ion-row>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n  <ion-grid class=\"product-detail-content ion-no-padding\"\r\n    *ngIf=\"product.products_description!=null && product.products_description!=''\">\r\n    <ion-row class=\"top-icon-header heading\">\r\n      <ion-button fill=\"clear\">\r\n        <ion-icon slot=\"start\" name=\"list\"></ion-icon>\r\n        {{'Product Description'|translate}}\r\n      </ion-button>\r\n    </ion-row>\r\n    <ion-row>\r\n      <ion-col size=\"12\" style=\"padding:0 10px\">\r\n        <div class=\"product-description\" [innerHTML]=\"product.products_description\"></div>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n  <ion-grid class=\"product-detail-content ion-no-padding\" *ngIf=\"product.attributes.length!=0\">\r\n    <ion-row class=\"top-icon-header heading\">\r\n      <ion-button fill=\"clear\">\r\n        <ion-icon slot=\"start\" name=\"list\"></ion-icon>\r\n        {{'Techincal details'|translate}}\r\n      </ion-button>\r\n    </ion-row>\r\n\r\n\r\n\r\n    <ion-row>\r\n      <ion-col size=\"12\" style=\"padding:0 10px\">\r\n        <div class=\"product-description\" [innerHTML]=\"product.products_description\"></div>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n  <ion-list>\r\n    <ion-radio-group *ngFor=\" let att of product.attributes\" [(ngModel)]=\"att.name\"\r\n      (ionChange)=\"fillAttributes(att.name,att.option.id)\">\r\n      <ion-list-header>\r\n        <ion-label>{{att.option.name}}</ion-label>\r\n      </ion-list-header>\r\n      <ion-item *ngFor=\" let val of att.values; let i = index\">\r\n        <ion-label>{{val.value+' '+val.price_prefix+val.price+' '+config.currency}}</ion-label>\r\n        <ion-radio slot=\"start\" [value]=\"val\">\r\n        </ion-radio>\r\n      </ion-item>\r\n    </ion-radio-group>\r\n  </ion-list>\r\n\r\n</ion-content>\r\n<ion-footer class=\"product-detail-footer\">\r\n\r\n  <ion-button expand=\"full\" color=\"secondary\" *ngIf=\"cartButton=='addToCart' && !is_upcomming\"\r\n    (click)=\"addToCartProduct()\">\r\n    {{'Add to Cart'|translate}}</ion-button>\r\n  <ion-button expand=\"full\" color=\"danger\" *ngIf=\"cartButton=='outOfStock' && !is_upcomming\">\r\n    {{'OUT OF STOCK'|translate}}</ion-button>\r\n  <ion-button expand=\"full\" color=\"secondary\" *ngIf=\"cartButton=='external' && !is_upcomming\"\r\n    (click)=\"openProductUrl()\">\r\n    {{'VIEW PRODUCT'|translate}}</ion-button>\r\n\r\n  <ion-button expand=\"full\" color=\"danger\" *ngIf=\"is_upcomming\">{{'Up Coming'|translate}}</ion-button>\r\n</ion-footer>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/components/menu-component/menu-component.component.html":
  /*!***********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/components/menu-component/menu-component.component.html ***!
    \***********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcComponentsMenuComponentMenuComponentComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div #expandWrapper class='expand-wrapper' [class.collapsed]=\"!expanded\">\r\n    <ng-content></ng-content>\r\n  </div>\r\n  ";
    /***/
  },

  /***/
  "./node_modules/tslib/tslib.es6.js":
  /*!*****************************************!*\
    !*** ./node_modules/tslib/tslib.es6.js ***!
    \*****************************************/

  /*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */

  /***/
  function node_modulesTslibTslibEs6Js(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__extends", function () {
      return __extends;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__assign", function () {
      return _assign;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__rest", function () {
      return __rest;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__decorate", function () {
      return __decorate;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__param", function () {
      return __param;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__metadata", function () {
      return __metadata;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__awaiter", function () {
      return __awaiter;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__generator", function () {
      return __generator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__exportStar", function () {
      return __exportStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__values", function () {
      return __values;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__read", function () {
      return __read;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spread", function () {
      return __spread;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spreadArrays", function () {
      return __spreadArrays;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__await", function () {
      return __await;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function () {
      return __asyncGenerator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function () {
      return __asyncDelegator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncValues", function () {
      return __asyncValues;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function () {
      return __makeTemplateObject;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importStar", function () {
      return __importStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importDefault", function () {
      return __importDefault;
    });
    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0
    
    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.
    
    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    /* global Reflect, Promise */


    var _extendStatics = function extendStatics(d, b) {
      _extendStatics = Object.setPrototypeOf || {
        __proto__: []
      } instanceof Array && function (d, b) {
        d.__proto__ = b;
      } || function (d, b) {
        for (var p in b) {
          if (b.hasOwnProperty(p)) d[p] = b[p];
        }
      };

      return _extendStatics(d, b);
    };

    function __extends(d, b) {
      _extendStatics(d, b);

      function __() {
        this.constructor = d;
      }

      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var _assign = function __assign() {
      _assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];

          for (var p in s) {
            if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
          }
        }

        return t;
      };

      return _assign.apply(this, arguments);
    };

    function __rest(s, e) {
      var t = {};

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
      }

      if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
      }
      return t;
    }

    function __decorate(decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    }

    function __metadata(metadataKey, metadataValue) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }

        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }

        function step(result) {
          result.done ? resolve(result.value) : new P(function (resolve) {
            resolve(result.value);
          }).then(fulfilled, rejected);
        }

        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    }

    function __generator(thisArg, body) {
      var _ = {
        label: 0,
        sent: function sent() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
          f,
          y,
          t,
          g;
      return g = {
        next: verb(0),
        "throw": verb(1),
        "return": verb(2)
      }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
        return this;
      }), g;

      function verb(n) {
        return function (v) {
          return step([n, v]);
        };
      }

      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");

        while (_) {
          try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];

            switch (op[0]) {
              case 0:
              case 1:
                t = op;
                break;

              case 4:
                _.label++;
                return {
                  value: op[1],
                  done: false
                };

              case 5:
                _.label++;
                y = op[1];
                op = [0];
                continue;

              case 7:
                op = _.ops.pop();

                _.trys.pop();

                continue;

              default:
                if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                  _ = 0;
                  continue;
                }

                if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
                  _.label = op[1];
                  break;
                }

                if (op[0] === 6 && _.label < t[1]) {
                  _.label = t[1];
                  t = op;
                  break;
                }

                if (t && _.label < t[2]) {
                  _.label = t[2];

                  _.ops.push(op);

                  break;
                }

                if (t[2]) _.ops.pop();

                _.trys.pop();

                continue;
            }

            op = body.call(thisArg, _);
          } catch (e) {
            op = [6, e];
            y = 0;
          } finally {
            f = t = 0;
          }
        }

        if (op[0] & 5) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    }

    function __exportStar(m, exports) {
      for (var p in m) {
        if (!exports.hasOwnProperty(p)) exports[p] = m[p];
      }
    }

    function __values(o) {
      var m = typeof Symbol === "function" && o[Symbol.iterator],
          i = 0;
      if (m) return m.call(o);
      return {
        next: function next() {
          if (o && i >= o.length) o = void 0;
          return {
            value: o && o[i++],
            done: !o
          };
        }
      };
    }

    function __read(o, n) {
      var m = typeof Symbol === "function" && o[Symbol.iterator];
      if (!m) return o;
      var i = m.call(o),
          r,
          ar = [],
          e;

      try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) {
          ar.push(r.value);
        }
      } catch (error) {
        e = {
          error: error
        };
      } finally {
        try {
          if (r && !r.done && (m = i["return"])) m.call(i);
        } finally {
          if (e) throw e.error;
        }
      }

      return ar;
    }

    function __spread() {
      for (var ar = [], i = 0; i < arguments.length; i++) {
        ar = ar.concat(__read(arguments[i]));
      }

      return ar;
    }

    function __spreadArrays() {
      for (var s = 0, i = 0, il = arguments.length; i < il; i++) {
        s += arguments[i].length;
      }

      for (var r = Array(s), k = 0, i = 0; i < il; i++) {
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++) {
          r[k] = a[j];
        }
      }

      return r;
    }

    ;

    function __await(v) {
      return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var g = generator.apply(thisArg, _arguments || []),
          i,
          q = [];
      return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i;

      function verb(n) {
        if (g[n]) i[n] = function (v) {
          return new Promise(function (a, b) {
            q.push([n, v, a, b]) > 1 || resume(n, v);
          });
        };
      }

      function resume(n, v) {
        try {
          step(g[n](v));
        } catch (e) {
          settle(q[0][3], e);
        }
      }

      function step(r) {
        r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);
      }

      function fulfill(value) {
        resume("next", value);
      }

      function reject(value) {
        resume("throw", value);
      }

      function settle(f, v) {
        if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]);
      }
    }

    function __asyncDelegator(o) {
      var i, p;
      return i = {}, verb("next"), verb("throw", function (e) {
        throw e;
      }), verb("return"), i[Symbol.iterator] = function () {
        return this;
      }, i;

      function verb(n, f) {
        i[n] = o[n] ? function (v) {
          return (p = !p) ? {
            value: __await(o[n](v)),
            done: n === "return"
          } : f ? f(v) : v;
        } : f;
      }
    }

    function __asyncValues(o) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var m = o[Symbol.asyncIterator],
          i;
      return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i);

      function verb(n) {
        i[n] = o[n] && function (v) {
          return new Promise(function (resolve, reject) {
            v = o[n](v), settle(resolve, reject, v.done, v.value);
          });
        };
      }

      function settle(resolve, reject, d, v) {
        Promise.resolve(v).then(function (v) {
          resolve({
            value: v,
            done: d
          });
        }, reject);
      }
    }

    function __makeTemplateObject(cooked, raw) {
      if (Object.defineProperty) {
        Object.defineProperty(cooked, "raw", {
          value: raw
        });
      } else {
        cooked.raw = raw;
      }

      return cooked;
    }

    ;

    function __importStar(mod) {
      if (mod && mod.__esModule) return mod;
      var result = {};
      if (mod != null) for (var k in mod) {
        if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
      }
      result["default"] = mod;
      return result;
    }

    function __importDefault(mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    }
    /***/

  },

  /***/
  "./src/app/app-routing.module.ts":
  /*!***************************************!*\
    !*** ./src/app/app-routing.module.ts ***!
    \***************************************/

  /*! exports provided: AppRoutingModule */

  /***/
  function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
      return AppRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _providers_auth_guard_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../providers/auth-guard/auth-guard.service */
    "./src/providers/auth-guard/auth-guard.service.ts");

    var routes = [//{
    // path: '',
    //redirectTo: 'home',
    // pathMatch: 'full'
    //},
    {
      path: '',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | tabs-tabs-module */
        "tabs-tabs-module").then(__webpack_require__.bind(null,
        /*! ./tabs/tabs.module */
        "./src/app/tabs/tabs.module.ts")).then(function (m) {
          return m.TabsPageModule;
        });
      }
    }, {
      path: 'about-us',
      loadChildren: './about-us/about-us.module#AboutUsPageModule'
    }, {
      path: 'cart',
      loadChildren: './cart/cart.module#CartPageModule'
    }, {
      path: 'contact-us',
      loadChildren: './contact-us/contact-us.module#ContactUsPageModule'
    }, {
      path: 'intro',
      loadChildren: './intro/intro.module#IntroPageModule'
    }, {
      path: 'my-account',
      loadChildren: './my-account/my-account.module#MyAccountPageModule',
      canActivate: [_providers_auth_guard_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]]
    }, {
      path: 'my-orders',
      loadChildren: './my-orders/my-orders.module#MyOrdersPageModule',
      canActivate: [_providers_auth_guard_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]]
    }, {
      path: 'news',
      loadChildren: './news/news.module#NewsPageModule'
    }, {
      path: 'news-detail',
      loadChildren: './news-detail/news-detail.module#NewsDetailPageModule'
    }, {
      path: 'news-list/:id/:name',
      loadChildren: './news-list/news-list.module#NewsListPageModule'
    }, {
      path: 'order',
      loadChildren: './order/order.module#OrderPageModule',
      canActivate: [_providers_auth_guard_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]],
      data: {
        hideGuestLogin: false
      }
    }, {
      path: 'product-detail/:id',
      loadChildren: './product-detail/product-detail.module#ProductDetailPageModule'
    }, {
      path: 'products/:id/:name/:type',
      loadChildren: './products/products.module#ProductsPageModule'
    }, {
      path: 'search',
      loadChildren: './search/search.module#SearchPageModule'
    }, {
      path: 'settings',
      loadChildren: './settings/settings.module#SettingsPageModule'
    }, {
      path: 'shipping-method',
      loadChildren: './shipping-method/shipping-method.module#ShippingMethodPageModule',
      canActivate: [_providers_auth_guard_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]],
      data: {
        hideGuestLogin: false
      }
    }, {
      path: 'thank-you',
      loadChildren: './thank-you/thank-you.module#ThankYouPageModule',
      canActivate: [_providers_auth_guard_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]],
      data: {
        hideGuestLogin: false
      }
    }, {
      path: 'wish-list',
      loadChildren: './wish-list/wish-list.module#WishListPageModule',
      canActivate: [_providers_auth_guard_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]]
    }, {
      path: 'addresses',
      loadChildren: './address-pages/addresses/addresses.module#AddressesPageModule',
      canActivate: [_providers_auth_guard_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]]
    }, {
      path: 'billing-address',
      loadChildren: './address-pages/billing-address/billing-address.module#BillingAddressPageModule',
      canActivate: [_providers_auth_guard_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]],
      data: {
        hideGuestLogin: false
      }
    }, {
      path: 'shipping-address',
      loadChildren: './address-pages/shipping-address/shipping-address.module#ShippingAddressPageModule',
      canActivate: [_providers_auth_guard_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]],
      data: {
        hideGuestLogin: false
      }
    }, {
      path: 'categories/:parent/:name',
      loadChildren: './categorie-pages/categories/categories.module#CategoriesPageModule'
    }, {
      path: 'categories2/:parent/:name',
      loadChildren: './categorie-pages/categories2/categories2.module#Categories2PageModule'
    }, {
      path: 'categories3/:parent/:name',
      loadChildren: './categorie-pages/categories3/categories3.module#Categories3PageModule'
    }, {
      path: 'categories4/:parent/:name',
      loadChildren: './categorie-pages/categories4/categories4.module#Categories4PageModule'
    }, {
      path: 'categories5/:parent/:name',
      loadChildren: './categorie-pages/categories5/categories5.module#Categories5PageModule'
    }, {
      path: 'categories6/:parent/:name',
      loadChildren: './categorie-pages/categories6/categories6.module#Categories6PageModule'
    }, {
      path: 'home',
      loadChildren: './home-pages/home/home.module#HomePageModule'
    }, {
      path: 'home2',
      loadChildren: './home-pages/home2/home2.module#Home2PageModule'
    }, {
      path: 'home3',
      loadChildren: './home-pages/home3/home3.module#Home3PageModule'
    }, {
      path: 'home4',
      loadChildren: './home-pages/home4/home4.module#Home4PageModule'
    }, {
      path: 'home5',
      loadChildren: './home-pages/home5/home5.module#Home5PageModule'
    }, {
      path: 'home6',
      loadChildren: './home-pages/home6/home6.module#Home6PageModule'
    }, {
      path: 'home7',
      loadChildren: './home-pages/home7/home7.module#Home7PageModule'
    }, {
      path: 'home8',
      loadChildren: './home-pages/home8/home8.module#Home8PageModule'
    }, {
      path: 'home9',
      loadChildren: './home-pages/home9/home9.module#Home9PageModule'
    }, {
      path: 'home10',
      loadChildren: './home-pages/home10/home10.module#Home10PageModule'
    }, {
      path: 'my-order-detail',
      loadChildren: './my-order-detail/my-order-detail.module#MyOrderDetailPageModule',
      canActivate: [_providers_auth_guard_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]]
    }, {
      path: 'add-review/:id',
      loadChildren: './add-review/add-review.module#AddReviewPageModule',
      canActivate: [_providers_auth_guard_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]]
    }, {
      path: 'reviews/:id',
      loadChildren: './reviews/reviews.module#ReviewsPageModule'
    }];

    var AppRoutingModule = function AppRoutingModule() {
      _classCallCheck(this, AppRoutingModule);
    };

    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, {
        preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"]
      })],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AppRoutingModule);
    /***/
  },

  /***/
  "./src/app/app.component.scss":
  /*!************************************!*\
    !*** ./src/app/app.component.scss ***!
    \************************************/

  /*! exports provided: default */

  /***/
  function srcAppAppComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content .nameTextAvatar {\n  width: auto;\n  height: auto;\n}\nion-content ion-list ion-item {\n  --inner-padding-end: 0;\n}\nion-content ion-list ion-item ion-menu-toggle {\n  width: 100%;\n}\nion-content ion-list ion-item:first-child {\n  --inner-padding-end: 0;\n  --padding-start: 0;\n  height: 100px;\n}\nion-content ion-list ion-item:first-child #backimage {\n  position: fixed;\n  opacity: 0.3;\n}\nion-content ion-list ion-item:first-child div {\n  background: var(--ion-color-primary-shade);\n  width: 100%;\n  height: 100px;\n}\nion-content ion-list ion-item:first-child ion-item {\n  position: absolute;\n  --background: transparent;\n  --color: var(--ion-color-primary-contrast);\n}\nion-content ion-list ion-item:first-child ion-item ion-label h2 {\n  font-weight: bold;\n}\nion-content ion-list ion-item:first-child ion-item ion-label p {\n  color: var(--ion-color-primary-contrast);\n  white-space: normal;\n}\nion-content ion-list ion-item ion-grid ion-row {\n  border-bottom: solid var(--ion-color-light-shade);\n  border-width: 0.4px;\n  height: 47px;\n}\nion-content ion-list ion-item ion-grid ion-row ion-icon {\n  zoom: 1.2;\n}\nion-content ion-list ion-item ion-grid ion-row img {\n  max-width: 60%;\n}\nion-content ion-list ion-item ion-grid ion-row ion-col p {\n  margin-top: 6px !important;\n  margin-bottom: 8px !important;\n}\nion-content ion-list ion-item ion-grid ion-row ion-col:first-child {\n  text-align: center;\n}\n.item-inner-list {\n  height: 52px !important;\n  margin-left: -3px !important;\n  --inner-padding-end: 0 !important;\n  --padding-start: 0 !important;\n}\n.item-inner-list ion-row {\n  padding-left: 0 !important;\n}\n.item-inner-list:last-child {\n  border-bottom: none;\n}\n.col-text {\n  padding-left: 16px !important;\n}\n.primary-Color {\n  color: var(--ion-color-primary);\n  display: none;\n}\n#nametext {\n  font-size: 25px;\n  text-transform: capitalize;\n}\n.rotation-animation {\n  -webkit-animation-name: rotate;\n          animation-name: rotate;\n  -webkit-animation-duration: 6s;\n          animation-duration: 6s;\n  -webkit-animation-iteration-count: infinite;\n          animation-iteration-count: infinite;\n  -webkit-animation-timing-function: linear;\n          animation-timing-function: linear;\n}\n@-webkit-keyframes rotate {\n  from {\n    transform: rotate(0deg);\n  }\n  to {\n    transform: rotate(360deg);\n  }\n}\n@keyframes rotate {\n  from {\n    transform: rotate(0deg);\n  }\n  to {\n    transform: rotate(360deg);\n  }\n}\n.margin50bottom {\n  margin-bottom: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDQUo7QURHSTtFQUNFLHNCQUFBO0FDRE47QURFTTtFQUNFLFdBQUE7QUNBUjtBREVNO0VBQ0Usc0JBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7QUNBUjtBRENRO0VBQ0UsZUFBQTtFQUNBLFlBQUE7QUNDVjtBRENRO0VBQ0UsMENBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtBQ0NWO0FEQ1E7RUFDRSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsMENBQUE7QUNDVjtBRENZO0VBQ0UsaUJBQUE7QUNDZDtBRENZO0VBQ0Usd0NBQUE7RUFDQSxtQkFBQTtBQ0NkO0FES1E7RUFDRSxpREFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ0hWO0FESVU7RUFDRSxTQUFBO0FDRlo7QURJVTtFQUNFLGNBQUE7QUNGWjtBREtZO0VBQ0UsMEJBQUE7RUFDQSw2QkFBQTtBQ0hkO0FES1k7RUFDRSxrQkFBQTtBQ0hkO0FEV0E7RUFDRSx1QkFBQTtFQUNBLDRCQUFBO0VBQ0EsaUNBQUE7RUFDQSw2QkFBQTtBQ1JGO0FEU0U7RUFDRSwwQkFBQTtBQ1BKO0FEU0U7RUFDRSxtQkFBQTtBQ1BKO0FEVUE7RUFDRSw2QkFBQTtBQ1BGO0FEVUE7RUFDRSwrQkFBQTtFQUNBLGFBQUE7QUNQRjtBRFNBO0VBQ0UsZUFBQTtFQUNBLDBCQUFBO0FDTkY7QURTQTtFQUNFLDhCQUFBO1VBQUEsc0JBQUE7RUFDQSw4QkFBQTtVQUFBLHNCQUFBO0VBQ0EsMkNBQUE7VUFBQSxtQ0FBQTtFQUNBLHlDQUFBO1VBQUEsaUNBQUE7QUNORjtBRFNBO0VBQ0U7SUFDRSx1QkFBQTtFQ05GO0VEUUE7SUFDRSx5QkFBQTtFQ05GO0FBQ0Y7QURBQTtFQUNFO0lBQ0UsdUJBQUE7RUNORjtFRFFBO0lBQ0UseUJBQUE7RUNORjtBQUNGO0FEU0E7RUFDRSxtQkFBQTtBQ1BGIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gIC5uYW1lVGV4dEF2YXRhciB7XHJcbiAgICB3aWR0aDogYXV0bztcclxuICAgIGhlaWdodDogYXV0bztcclxuICB9XHJcbiAgaW9uLWxpc3Qge1xyXG4gICAgaW9uLWl0ZW0ge1xyXG4gICAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwO1xyXG4gICAgICBpb24tbWVudS10b2dnbGUge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICB9XHJcbiAgICAgICY6Zmlyc3QtY2hpbGQge1xyXG4gICAgICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XHJcbiAgICAgICAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xyXG4gICAgICAgIGhlaWdodDogMTAwcHg7XHJcbiAgICAgICAgI2JhY2tpbWFnZSB7XHJcbiAgICAgICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAgICAgICBvcGFjaXR5OiAwLjM7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRpdiB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeS1zaGFkZSk7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgIGhlaWdodDogMTAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlvbi1pdGVtIHtcclxuICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeS1jb250cmFzdCk7XHJcbiAgICAgICAgICBpb24tbGFiZWwge1xyXG4gICAgICAgICAgICBoMiB7XHJcbiAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcCB7XHJcbiAgICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LWNvbnRyYXN0KTtcclxuICAgICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIGlvbi1ncmlkIHtcclxuICAgICAgICBpb24tcm93IHtcclxuICAgICAgICAgIGJvcmRlci1ib3R0b206IHNvbGlkIHZhcigtLWlvbi1jb2xvci1saWdodC1zaGFkZSk7XHJcbiAgICAgICAgICBib3JkZXItd2lkdGg6IDAuNHB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiA0N3B4O1xyXG4gICAgICAgICAgaW9uLWljb24ge1xyXG4gICAgICAgICAgICB6b29tOiAxLjI7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICBtYXgtd2lkdGg6IDYwJTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGlvbi1jb2wge1xyXG4gICAgICAgICAgICBwIHtcclxuICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA2cHggIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA4cHggIWltcG9ydGFudDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAmOmZpcnN0LWNoaWxkIHtcclxuICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuLml0ZW0taW5uZXItbGlzdCB7XHJcbiAgaGVpZ2h0OiA1MnB4ICFpbXBvcnRhbnQ7XHJcbiAgbWFyZ2luLWxlZnQ6IC0zcHggIWltcG9ydGFudDtcclxuICAtLWlubmVyLXBhZGRpbmctZW5kOiAwICFpbXBvcnRhbnQ7XHJcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwICFpbXBvcnRhbnQ7XHJcbiAgaW9uLXJvdyB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDAgIWltcG9ydGFudDtcclxuICB9XHJcbiAgJjpsYXN0LWNoaWxkIHtcclxuICAgIGJvcmRlci1ib3R0b206IG5vbmU7XHJcbiAgfVxyXG59XHJcbi5jb2wtdGV4dCB7XHJcbiAgcGFkZGluZy1sZWZ0OiAxNnB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5wcmltYXJ5LUNvbG9yIHtcclxuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuI25hbWV0ZXh0IHtcclxuICBmb250LXNpemU6IDI1cHg7XHJcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbn1cclxuXHJcbi5yb3RhdGlvbi1hbmltYXRpb24ge1xyXG4gIGFuaW1hdGlvbi1uYW1lOiByb3RhdGU7XHJcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiA2cztcclxuICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiBpbmZpbml0ZTtcclxuICBhbmltYXRpb24tdGltaW5nLWZ1bmN0aW9uOiBsaW5lYXI7XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgcm90YXRlIHtcclxuICBmcm9tIHtcclxuICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gIH1cclxuICB0byB7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpO1xyXG4gIH1cclxufVxyXG5cclxuLm1hcmdpbjUwYm90dG9tIHtcclxuICBtYXJnaW4tYm90dG9tOiA1MHB4O1xyXG59XHJcbiIsImlvbi1jb250ZW50IC5uYW1lVGV4dEF2YXRhciB7XG4gIHdpZHRoOiBhdXRvO1xuICBoZWlnaHQ6IGF1dG87XG59XG5pb24tY29udGVudCBpb24tbGlzdCBpb24taXRlbSB7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XG59XG5pb24tY29udGVudCBpb24tbGlzdCBpb24taXRlbSBpb24tbWVudS10b2dnbGUge1xuICB3aWR0aDogMTAwJTtcbn1cbmlvbi1jb250ZW50IGlvbi1saXN0IGlvbi1pdGVtOmZpcnN0LWNoaWxkIHtcbiAgLS1pbm5lci1wYWRkaW5nLWVuZDogMDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICBoZWlnaHQ6IDEwMHB4O1xufVxuaW9uLWNvbnRlbnQgaW9uLWxpc3QgaW9uLWl0ZW06Zmlyc3QtY2hpbGQgI2JhY2tpbWFnZSB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgb3BhY2l0eTogMC4zO1xufVxuaW9uLWNvbnRlbnQgaW9uLWxpc3QgaW9uLWl0ZW06Zmlyc3QtY2hpbGQgZGl2IHtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnktc2hhZGUpO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDBweDtcbn1cbmlvbi1jb250ZW50IGlvbi1saXN0IGlvbi1pdGVtOmZpcnN0LWNoaWxkIGlvbi1pdGVtIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeS1jb250cmFzdCk7XG59XG5pb24tY29udGVudCBpb24tbGlzdCBpb24taXRlbTpmaXJzdC1jaGlsZCBpb24taXRlbSBpb24tbGFiZWwgaDIge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbmlvbi1jb250ZW50IGlvbi1saXN0IGlvbi1pdGVtOmZpcnN0LWNoaWxkIGlvbi1pdGVtIGlvbi1sYWJlbCBwIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LWNvbnRyYXN0KTtcbiAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbn1cbmlvbi1jb250ZW50IGlvbi1saXN0IGlvbi1pdGVtIGlvbi1ncmlkIGlvbi1yb3cge1xuICBib3JkZXItYm90dG9tOiBzb2xpZCB2YXIoLS1pb24tY29sb3ItbGlnaHQtc2hhZGUpO1xuICBib3JkZXItd2lkdGg6IDAuNHB4O1xuICBoZWlnaHQ6IDQ3cHg7XG59XG5pb24tY29udGVudCBpb24tbGlzdCBpb24taXRlbSBpb24tZ3JpZCBpb24tcm93IGlvbi1pY29uIHtcbiAgem9vbTogMS4yO1xufVxuaW9uLWNvbnRlbnQgaW9uLWxpc3QgaW9uLWl0ZW0gaW9uLWdyaWQgaW9uLXJvdyBpbWcge1xuICBtYXgtd2lkdGg6IDYwJTtcbn1cbmlvbi1jb250ZW50IGlvbi1saXN0IGlvbi1pdGVtIGlvbi1ncmlkIGlvbi1yb3cgaW9uLWNvbCBwIHtcbiAgbWFyZ2luLXRvcDogNnB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDhweCAhaW1wb3J0YW50O1xufVxuaW9uLWNvbnRlbnQgaW9uLWxpc3QgaW9uLWl0ZW0gaW9uLWdyaWQgaW9uLXJvdyBpb24tY29sOmZpcnN0LWNoaWxkIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uaXRlbS1pbm5lci1saXN0IHtcbiAgaGVpZ2h0OiA1MnB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1sZWZ0OiAtM3B4ICFpbXBvcnRhbnQ7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDAgIWltcG9ydGFudDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwICFpbXBvcnRhbnQ7XG59XG4uaXRlbS1pbm5lci1saXN0IGlvbi1yb3cge1xuICBwYWRkaW5nLWxlZnQ6IDAgIWltcG9ydGFudDtcbn1cbi5pdGVtLWlubmVyLWxpc3Q6bGFzdC1jaGlsZCB7XG4gIGJvcmRlci1ib3R0b206IG5vbmU7XG59XG5cbi5jb2wtdGV4dCB7XG4gIHBhZGRpbmctbGVmdDogMTZweCAhaW1wb3J0YW50O1xufVxuXG4ucHJpbWFyeS1Db2xvciB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbiNuYW1ldGV4dCB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5yb3RhdGlvbi1hbmltYXRpb24ge1xuICBhbmltYXRpb24tbmFtZTogcm90YXRlO1xuICBhbmltYXRpb24tZHVyYXRpb246IDZzO1xuICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiBpbmZpbml0ZTtcbiAgYW5pbWF0aW9uLXRpbWluZy1mdW5jdGlvbjogbGluZWFyO1xufVxuXG5Aa2V5ZnJhbWVzIHJvdGF0ZSB7XG4gIGZyb20ge1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xuICB9XG4gIHRvIHtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpO1xuICB9XG59XG4ubWFyZ2luNTBib3R0b20ge1xuICBtYXJnaW4tYm90dG9tOiA1MHB4O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/app.component.ts":
  /*!**********************************!*\
    !*** ./src/app/app.component.ts ***!
    \**********************************/

  /*! exports provided: AppComponent */

  /***/
  function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic-native/status-bar/ngx */
    "./node_modules/@ionic-native/status-bar/ngx/index.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic-native/app-version/ngx */
    "./node_modules/@ionic-native/app-version/ngx/index.js");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var _ionic_native_admob_free_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic-native/admob-free/ngx */
    "./node_modules/@ionic-native/admob-free/ngx/index.js");
    /* harmony import */


    var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/social-sharing/ngx */
    "./node_modules/@ionic-native/social-sharing/ngx/index.js");
    /* harmony import */


    var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic-native/in-app-browser/ngx */
    "./node_modules/@ionic-native/in-app-browser/ngx/index.js");
    /* harmony import */


    var _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @ionic-native/network/ngx */
    "./node_modules/@ionic-native/network/ngx/index.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _modals_login_login_page__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ./modals/login/login.page */
    "./src/app/modals/login/login.page.ts");
    /* harmony import */


    var _modals_sign_up_sign_up_page__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ./modals/sign-up/sign-up.page */
    "./src/app/modals/sign-up/sign-up.page.ts");
    /* harmony import */


    var _modals_language_language_page__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ./modals/language/language.page */
    "./src/app/modals/language/language.page.ts");
    /* harmony import */


    var _modals_currency_list_currency_list_page__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ./modals/currency-list/currency-list.page */
    "./src/app/modals/currency-list/currency-list.page.ts");
    /* harmony import */


    var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! src/providers/app-events/app-events.service */
    "./src/providers/app-events/app-events.service.ts");
    /* harmony import */


    var src_providers_deeplinking_deep_linking_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! src/providers/deeplinking/deep-linking.service */
    "./src/providers/deeplinking/deep-linking.service.ts");
    /* harmony import */


    var firebase_app__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! firebase/app */
    "./node_modules/firebase/app/dist/index.cjs.js");
    /* harmony import */


    var firebase_app__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_20__);
    /* harmony import */


    var src_providers_back_button_exit_back_button_exit_app_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! src/providers/back-button-exit/back-button-exit-app.service */
    "./src/providers/back-button-exit/back-button-exit-app.service.ts");

    var AppComponent = /*#__PURE__*/function () {
      function AppComponent(shared, config, router, navCtrl, modalCtrl, statusBar, storage, network, loading, admobFree, appEventsService, plt, zone, appVersion, iab, socialSharing, deepLinking, menuCtrl, backButtonExit) {
        var _this = this;

        _classCallCheck(this, AppComponent);

        this.shared = shared;
        this.config = config;
        this.router = router;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.statusBar = statusBar;
        this.storage = storage;
        this.network = network;
        this.loading = loading;
        this.admobFree = admobFree;
        this.appEventsService = appEventsService;
        this.plt = plt;
        this.zone = zone;
        this.appVersion = appVersion;
        this.iab = iab;
        this.socialSharing = socialSharing;
        this.deepLinking = deepLinking;
        this.menuCtrl = menuCtrl;
        this.backButtonExit = backButtonExit;
        this.appPages = []; // For all pages

        this.a1 = [{
          name: 'Home',
          icon: 'home',
          url: 'home',
          img: 'assets/left-menu-icon/home.png',
          items: [{
            name: "1",
            url: '/home'
          }, {
            name: "2",
            url: '/home2'
          }, {
            name: "3",
            url: '/home3'
          }, {
            name: "4",
            url: '/home4'
          }, {
            name: "5",
            url: '/home5'
          }, {
            name: "6",
            url: '/home6'
          }, {
            name: "7",
            url: '/home7'
          }, {
            name: "8",
            url: '/home8'
          }, {
            name: "9",
            url: '/home9'
          }, {
            name: "10",
            url: '/home10'
          }],
          expanded: false
        }, {
          name: 'Categories',
          icon: 'apps',
          url: 'categories/0/0',
          img: 'assets/left-menu-icon/category.png',
          items: [{
            name: "1",
            url: '/categories/0/0'
          }, {
            name: "2",
            url: '/categories2/0/0'
          }, {
            name: "3",
            url: '/categories3/0/0'
          }, {
            name: "4",
            url: '/categories4/0/0'
          }, {
            name: "5",
            url: '/categories5/0/0'
          }, {
            name: "6",
            url: '/categories6/0/0'
          }],
          expanded: false
        }, {
          name: 'Shop',
          icon: 'cash',
          url: '/products/0/0/newest',
          img: 'assets/left-menu-icon/shop.png',
          items: [{
            name: "Newest",
            url: '/products/0/0/newest'
          }, {
            name: "Top Seller",
            url: '/products/0/0/top seller'
          }, {
            name: "Deals",
            url: '/products/0/0/special'
          }, {
            name: "Most Liked",
            url: '/products/0/0/most liked'
          }],
          expanded: false
        }];
        this.a2 = [{
          name: 'Home',
          icon: 'home',
          url: 'home',
          img: 'assets/left-menu-icon/home.png'
        }, {
          name: 'Categories',
          icon: 'apps',
          url: 'categories',
          img: 'assets/left-menu-icon/category.png'
        }, {
          name: 'Shop',
          icon: 'cash',
          url: '/products',
          img: 'assets/left-menu-icon/shop.png',
          items: [{
            name: "Newest",
            url: '/products/0/0/newest'
          }, {
            name: "Top Seller",
            url: '/products/0/0/top seller'
          }, {
            name: "Deals",
            url: '/products/0/0/special'
          }, {
            name: "Most Liked",
            url: '/products/0/0/most liked'
          }],
          expanded: false
        }];
        this.a3 = [{
          name: 'My Wish List',
          icon: 'heart',
          img: 'assets/left-menu-icon/wishlist.png',
          url: '/wish-list',
          value: 'wishListPage'
        }, {
          name: 'Contact Us',
          icon: 'call',
          img: 'assets/left-menu-icon/phone.png',
          url: '/contact-us',
          value: 'contactPage'
        }, {
          name: 'About Us',
          icon: 'information-circle',
          img: 'assets/left-menu-icon/about.png',
          url: '/about-us',
          value: 'aboutUsPage'
        }, {
          name: 'News',
          icon: 'newspaper',
          img: 'assets/left-menu-icon/news.png',
          url: '/news',
          value: 'newsPage'
        }, {
          name: 'Intro',
          icon: 'logo-ionic',
          img: 'assets/left-menu-icon/intro.png',
          url: '/intro',
          value: 'introPage'
        }, {
          name: 'Share',
          icon: 'share',
          img: 'assets/left-menu-icon/share.png',
          url: 'share',
          value: 'sharePage'
        }, {
          name: 'Rate Us',
          icon: 'star-half',
          img: 'assets/left-menu-icon/rating.png',
          url: 'rateUs',
          value: 'ratePage'
        }, {
          name: 'Settings',
          icon: 'settings',
          img: 'assets/left-menu-icon/setting.png',
          url: '/settings',
          value: 'settingsPage'
        }];
        this.a4 = [{
          name: 'My Wish List',
          icon: 'heart',
          img: 'assets/left-menu-icon/wishlist.png',
          url: '/wish-list',
          value: 'wishListPage'
        }, {
          name: 'Edit Profile',
          icon: 'lock-closed',
          img: 'assets/left-menu-icon/locked.png',
          url: '/my-account',
          login: true,
          value: 'editPage'
        }, {
          name: 'Address',
          icon: 'locate',
          img: 'assets/left-menu-icon/map.png',
          url: '/addresses',
          login: true,
          value: 'addressesPage'
        }, {
          name: 'My Orders',
          icon: 'cart',
          img: 'assets/left-menu-icon/orders.png',
          url: '/my-orders',
          login: true,
          value: 'myOrdersPage'
        }, {
          name: 'Contact Us',
          icon: 'call',
          img: 'assets/left-menu-icon/phone.png',
          url: '/contact-us',
          value: 'contactPage'
        }, {
          name: 'About Us',
          icon: 'information-circle',
          img: 'assets/left-menu-icon/about.png',
          url: '/about-us',
          value: 'aboutUsPage'
        }, {
          name: 'News',
          icon: 'newspaper',
          img: 'assets/left-menu-icon/news.png',
          url: '/news',
          value: 'newsPage'
        }, {
          name: 'Intro',
          icon: 'logo-ionic',
          img: 'assets/left-menu-icon/intro.png',
          url: '/intro',
          value: 'introPage'
        }, {
          name: 'Share',
          icon: 'share',
          img: 'assets/left-menu-icon/share.png',
          url: 'share',
          value: 'sharePage'
        }, {
          name: 'Rate Us',
          icon: 'star-half',
          img: 'assets/left-menu-icon/rating.png',
          url: 'rateUs',
          value: 'ratePage'
        }, {
          name: 'Settings',
          icon: 'settings',
          img: 'assets/left-menu-icon/setting.png',
          url: '/settings',
          value: 'settingsPage'
        }];
        this.plt.ready().then(function () {
          _this.statusBar.styleDefault();
        });
        var connectedToInternet = true;
        network.onDisconnect().subscribe(function () {
          connectedToInternet = false;

          _this.shared.showAlertWithTitle("Please Connect to the Internet", "Disconnected");
        });
        network.onConnect().subscribe(function () {
          if (!connectedToInternet) {
            window.location.reload();

            _this.shared.showAlertWithTitle("Network connected Reloading Data" + '...', "Connected");
          }
        });
        document.documentElement.dir = localStorage.direction;
        shared.dir = localStorage.direction;
        this.initializeApp();
        var showAd = this.appEventsService.subscribe("showAd");
        showAd.subscriptions.add(showAd.event.subscribe(function (data) {
          _this.showInterstitial();
        }));
        var openCategoryPage = this.appEventsService.subscribe("openCategoryPage");
        openCategoryPage.subscriptions.add(openCategoryPage.event.subscribe(function (data) {
          _this.openCategoryPage();
        }));
        var openHomePage = this.appEventsService.subscribe("openHomePage");
        openHomePage.subscriptions.add(openHomePage.event.subscribe(function (data) {
          _this.zone.run(function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      this.openHomePage();

                    case 1:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          });
        }));
        var openShippingAddressPage = this.appEventsService.subscribe("openShippingAddressPage");
        openShippingAddressPage.subscriptions.add(openShippingAddressPage.event.subscribe(function (data) {
          _this.zone.run(function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (config.appNavigationTabs) this.navCtrl.navigateForward("tabs/cart/shipping-address");else this.navCtrl.navigateForward("/shipping-address");

                    case 1:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          });
        }));
      }

      _createClass(AppComponent, [{
        key: "click",
        value: function click() {
          console.log(this.shared.missingValues);
        }
      }, {
        key: "initializeApp",
        value: function initializeApp() {
          var _this2 = this;

          this.plt.ready().then(function () {
            _this2.runAdmob();

            _this2.config.siteSetting().then(function (value) {
              firebase_app__WEBPACK_IMPORTED_MODULE_20__["initializeApp"](_this2.config.firebaseConfig);

              _this2.loadHomePage();

              _this2.getLeftItems(); //subscribe for push notifiation


              _this2.shared.subscribePush();
            });

            if (_this2.plt.is('cordova')) {
              _this2.deepLinking.initializeDeepLinks();
            }

            _this2.statusBar.styleLightContent();
          });
        } // loading home page =========================================================================

      }, {
        key: "loadHomePage",
        value: function loadHomePage() {
          var _this3 = this;

          this.storage.get('firsttimeApp').then(function (val) {
            var value = val;
            if (_this3.config.showIntroPage == 0) value = 'firstTime';

            if (value == 'firstTime') {
              _this3.openHomePage();

              _this3.config.checkingNewSettingsFromServer();
            } else {
              _this3.navCtrl.navigateRoot("intro");
            }

            _this3.storage.set('firsttimeApp', 'firstTime');
          });
        } // starting admob =========================================================================

      }, {
        key: "runAdmob",
        value: function runAdmob() {
          if (this.plt.is('ios')) {
            if (this.config.admobIos == 1) this.initializeAdmob(this.config.admobBanneridIos, this.config.admobIntidIos);
            this.config.admob = this.config.admobIos;
          } else if (this.plt.is('android')) {
            if (this.config.admob == 1) this.initializeAdmob(this.config.admobBannerid, this.config.admobIntid);
          }
        } // preparing admob =========================================================================

      }, {
        key: "initializeAdmob",
        value: function initializeAdmob(bannerId, intId) {
          if (this.plt.is('cordova')) {
            var bannerConfig = {
              id: bannerId,
              isTesting: false,
              autoShow: true
            };
            this.admobFree.banner.config(bannerConfig);
            this.admobFree.banner.prepare().then(function () {//alert("loaded" +bannerId);
              //this.admobFree.banner.show();
            })["catch"](function (e) {
              return console.log(e);
            });
            var interstitialConfig = {
              id: intId,
              isTesting: false,
              autoShow: false
            };
            this.admobFree.interstitial.config(interstitialConfig);
            this.admobFree.interstitial.prepare();
          }
        } //=========================================================================

      }, {
        key: "showInterstitial",
        value: function showInterstitial() {
          if (this.plt.is('cordova')) {
            this.admobFree.interstitial.show(); //this.admobFree.interstitial.isReady().then(() => { });

            this.admobFree.interstitial.prepare();
          }
        } //=========================================================================

      }, {
        key: "openPage",
        value: function openPage(page) {
          console.log(page); //this.router.navigateByUrl(page);

          if (page == 'home') this.openHomePage();else if (page == 'categories') this.openCategoryPage();else if (page == '/products/0/0/newest') this.navCtrl.navigateForward(page);else if (page == '/products/0/0/top seller') this.navCtrl.navigateForward(page);else if (page == '/products/0/0/special') this.navCtrl.navigateForward(page);else if (page == '/products/0/0/most liked') this.navCtrl.navigateForward(page);else if (page == 'share') this.share();else if (page == 'rateUs') this.rateUs();else this.navCtrl.navigateRoot(page);
          this.menuCtrl.toggle();
        }
      }, {
        key: "openHomePage",
        value: function openHomePage() {
          if (this.config.appNavigationTabs) {
            if (this.config.homePage == 1) {
              this.navCtrl.navigateForward("/tabs/home");
            }

            if (this.config.homePage == 2) {
              this.navCtrl.navigateForward("/tabs/home2");
            }

            if (this.config.homePage == 3) {
              this.navCtrl.navigateForward("/tabs/home3");
            }

            if (this.config.homePage == 4) {
              this.navCtrl.navigateForward("/tabs/home4");
            }

            if (this.config.homePage == 5) {
              this.navCtrl.navigateForward("/tabs/home5");
            }

            if (this.config.homePage == 6) this.navCtrl.navigateForward("/tabs/home6");
            if (this.config.homePage == 7) this.navCtrl.navigateForward("/tabs/home7");
            if (this.config.homePage == 8) this.navCtrl.navigateForward("/tabs/home8");
            if (this.config.homePage == 9) this.navCtrl.navigateForward("/tabs/home9");
            if (this.config.homePage == 10) this.navCtrl.navigateForward("/tabs/home10");
          } else {
            if (this.config.homePage == 1) {
              this.navCtrl.navigateRoot("/home");
            }

            if (this.config.homePage == 2) {
              this.navCtrl.navigateRoot("/home2");
            }

            if (this.config.homePage == 3) {
              this.navCtrl.navigateRoot("/home3");
            }

            if (this.config.homePage == 4) {
              this.navCtrl.navigateRoot("/home4");
            }

            if (this.config.homePage == 5) {
              this.navCtrl.navigateRoot("/home5");
            }

            if (this.config.homePage == 6) this.navCtrl.navigateRoot("/home6");
            if (this.config.homePage == 7) this.navCtrl.navigateRoot("/home7");
            if (this.config.homePage == 8) this.navCtrl.navigateRoot("/home8");
            if (this.config.homePage == 9) this.navCtrl.navigateRoot("/home9");
            if (this.config.homePage == 10) this.navCtrl.navigateRoot("/home10");
          }
        }
      }, {
        key: "openCategoryPage",
        value: function openCategoryPage() {
          if (this.config.appNavigationTabs) {
            if (this.config.categoryPage == 1) {
              this.navCtrl.navigateForward("categories/0/0");
            }

            if (this.config.categoryPage == 2) {
              this.navCtrl.navigateForward("categories2/0/0");
            }

            if (this.config.categoryPage == 3) {
              this.navCtrl.navigateForward("categories3/0/0");
            }

            if (this.config.categoryPage == 4) {
              this.navCtrl.navigateForward("categories4/0/0");
            }

            if (this.config.categoryPage == 5) {
              this.navCtrl.navigateForward("categories5/0/0");
            }

            if (this.config.categoryPage == 6) {
              this.navCtrl.navigateForward("categories6/0/0");
            }
          } else {
            if (this.config.categoryPage == 1) {
              this.navCtrl.navigateRoot("categories/0/0");
            }

            if (this.config.categoryPage == 2) {
              this.navCtrl.navigateRoot("categories2/0/0");
            }

            if (this.config.categoryPage == 3) {
              this.navCtrl.navigateRoot("categories3/0/0");
            }

            if (this.config.categoryPage == 4) {
              this.navCtrl.navigateRoot("categories4/0/0");
            }

            if (this.config.categoryPage == 5) {
              this.navCtrl.navigateRoot("categories5/0/0");
            }

            if (this.config.categoryPage == 6) {
              this.navCtrl.navigateRoot("categories6/0/0");
            }
          }
        }
      }, {
        key: "openSubcategoryPage",
        value: function openSubcategoryPage(parent) {
          var i = "/" + parent.id + "/" + parent.name;

          if (this.config.appNavigationTabs) {
            if (this.config.categoryPage == 1) {
              this.navCtrl.navigateForward(this.config.currentRoute + "/categories" + i);
            }

            if (this.config.categoryPage == 2) {
              this.navCtrl.navigateForward(this.config.currentRoute + "/categories2" + i);
            }

            if (this.config.categoryPage == 3) {
              this.navCtrl.navigateForward(this.config.currentRoute + "/categories3" + i);
            }

            if (this.config.categoryPage == 4) {
              this.navCtrl.navigateForward(this.config.currentRoute + "/categories4" + i);
            }

            if (this.config.categoryPage == 5) {
              this.navCtrl.navigateForward(this.config.currentRoute + "/categories5" + i);
            }

            if (this.config.categoryPage == 6) {
              this.navCtrl.navigateForward(this.config.currentRoute + "/categories6" + i);
            }
          } else {
            if (this.config.categoryPage == 1) {
              this.navCtrl.navigateForward("categories" + i);
            }

            if (this.config.categoryPage == 2) {
              this.navCtrl.navigateForward("categories2" + i);
            }

            if (this.config.categoryPage == 3) {
              this.navCtrl.navigateForward("categories3" + i);
            }

            if (this.config.categoryPage == 4) {
              this.navCtrl.navigateForward("categories4" + i);
            }

            if (this.config.categoryPage == 5) {
              this.navCtrl.navigateForward("categories5" + i);
            }

            if (this.config.categoryPage == 6) {
              this.navCtrl.navigateForward("categories6" + i);
            }
          }
        }
      }, {
        key: "openLoginPage",
        value: function openLoginPage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var modal;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.modalCtrl.create({
                      component: _modals_login_login_page__WEBPACK_IMPORTED_MODULE_14__["LoginPage"]
                    });

                  case 2:
                    modal = _context3.sent;
                    _context3.next = 5;
                    return modal.present();

                  case 5:
                    return _context3.abrupt("return", _context3.sent);

                  case 6:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "openSignUpPage",
        value: function openSignUpPage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var modal;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.modalCtrl.create({
                      component: _modals_sign_up_sign_up_page__WEBPACK_IMPORTED_MODULE_15__["SignUpPage"]
                    });

                  case 2:
                    modal = _context4.sent;
                    _context4.next = 5;
                    return modal.present();

                  case 5:
                    return _context4.abrupt("return", _context4.sent);

                  case 6:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "logOut",
        value: function logOut() {
          this.shared.logOut();
        }
      }, {
        key: "rateUs",
        value: function rateUs() {
          var _this4 = this;

          this.loading.autoHide(2000);

          if (this.plt.is('ios')) {
            this.iab.create(this.config.packgeName.toString(), "_system");
          } else if (this.plt.is('android')) {
            this.appVersion.getPackageName().then(function (val) {
              _this4.iab.create("https://play.google.com/store/apps/details?id=" + val, "_system");
            });
          }
        }
      }, {
        key: "share",
        value: function share() {
          var _this5 = this;

          this.loading.autoHide(2000);

          if (this.plt.is('ios')) {
            this.socialSharing.share("Nice Application", this.config.appName, "assets/logo_header.png", this.config.packgeName.toString()).then(function () {})["catch"](function () {});
          } else if (this.plt.is('android')) {
            this.appVersion.getPackageName().then(function (val) {
              _this5.socialSharing.share("Nice Application", _this5.config.appName, "assets/logo_header.png", "https://play.google.com/store/apps/details?id=" + val).then(function () {})["catch"](function () {});
            });
          }
        }
      }, {
        key: "openLanguagePage",
        value: function openLanguagePage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var modal;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    _context5.next = 2;
                    return this.modalCtrl.create({
                      component: _modals_language_language_page__WEBPACK_IMPORTED_MODULE_16__["LanguagePage"]
                    });

                  case 2:
                    modal = _context5.sent;
                    _context5.next = 5;
                    return modal.present();

                  case 5:
                    return _context5.abrupt("return", _context5.sent);

                  case 6:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "openCurrencyPage",
        value: function openCurrencyPage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var modal;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.modalCtrl.create({
                      component: _modals_currency_list_currency_list_page__WEBPACK_IMPORTED_MODULE_17__["CurrencyListPage"]
                    });

                  case 2:
                    modal = _context6.sent;
                    _context6.next = 5;
                    return modal.present();

                  case 5:
                    return _context6.abrupt("return", _context6.sent);

                  case 6:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        } //==============================================================
        //for

      }, {
        key: "expandItem",
        value: function expandItem(i) {
          if (i.expanded == false) i.expanded = true;else i.expanded = false;
        }
      }, {
        key: "showImg",
        value: function showImg() {
          return !this.config.defaultIcons;
        }
      }, {
        key: "getLeftItems",
        value: function getLeftItems() {
          var _this6 = this;

          var tempArray = new Array();

          if (!this.config.appInProduction) {
            this.a1.forEach(function (v, index) {
              tempArray.push(v);
            });
          } else {
            this.a2.forEach(function (v, index) {
              tempArray.push(v);
            });
          }

          if (this.shared.customerData.customers_id == null) {
            this.a3.forEach(function (v, index) {
              tempArray.push(v);
            });
          } else {
            this.a4.forEach(function (v, index) {
              tempArray.push(v);
            });
          }

          tempArray.forEach(function (v, index) {
            if (_this6.config.wishListPage == 0 && v.value == "wishListPage") {
              tempArray.splice(index, 1);
            }

            if (_this6.config.editProfilePage == 0 && v.value == "editPage") {
              tempArray.splice(index, 1);
            }

            if (_this6.config.contactUsPage == 0 && v.value == "contactPage") {
              tempArray.splice(index, 1);
            }

            if (_this6.config.aboutUsPage == 0 && v.value == "aboutUsPage") {
              tempArray.splice(index, 1);
            }

            if (_this6.config.newsPage == 0 && v.value == "newsPage") {
              tempArray.splice(index, 1);
            }

            if (_this6.config.introPage == 0 && v.value == "introPage") {
              tempArray.splice(index, 1);
            }

            if (_this6.config.shareApp == 0 && v.value == "sharePage") {
              tempArray.splice(index, 1);
            }

            if (_this6.config.rateApp == 0 && v.value == "ratePage") {
              tempArray.splice(index, 1);
            }

            if (_this6.config.settingPage == 0 && v.value == "settingsPage") {
              tempArray.splice(index, 1);
            }

            if (_this6.config.myOrdersPage == 0 && v.value == "myOrdersPage") {
              tempArray.splice(index, 1);
            }
          });
          this.appPages = tempArray;
          return tempArray;
        }
      }, {
        key: "getNameFirstLetter",
        value: function getNameFirstLetter() {
          return this.shared.getNameFirstLetter();
        }
      }, {
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {
          this.backButtonExit.routerOutlets = this.routerOutlets;
          this.backButtonExit.backButtonEvent();
        }
      }]);

      return AppComponent;
    }();

    AppComponent.ctorParameters = function () {
      return [{
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_13__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_3__["StatusBar"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_12__["Storage"]
      }, {
        type: _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_11__["Network"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_7__["LoadingService"]
      }, {
        type: _ionic_native_admob_free_ngx__WEBPACK_IMPORTED_MODULE_8__["AdMobFree"]
      }, {
        type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_18__["AppEventsService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]
      }, {
        type: _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_6__["AppVersion"]
      }, {
        type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_10__["InAppBrowser"]
      }, {
        type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_9__["SocialSharing"]
      }, {
        type: src_providers_deeplinking_deep_linking_service__WEBPACK_IMPORTED_MODULE_19__["DeepLinkingService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"]
      }, {
        type: src_providers_back_button_exit_back_button_exit_app_service__WEBPACK_IMPORTED_MODULE_21__["BackButtonExitAppService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChildren"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonRouterOutlet"]), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["QueryList"])], AppComponent.prototype, "routerOutlets", void 0);
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-root',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./app.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./app.component.scss */
      "./src/app/app.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"], _angular_router__WEBPACK_IMPORTED_MODULE_13__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_3__["StatusBar"], _ionic_storage__WEBPACK_IMPORTED_MODULE_12__["Storage"], _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_11__["Network"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_7__["LoadingService"], _ionic_native_admob_free_ngx__WEBPACK_IMPORTED_MODULE_8__["AdMobFree"], src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_18__["AppEventsService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"], _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_6__["AppVersion"], _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_10__["InAppBrowser"], _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_9__["SocialSharing"], src_providers_deeplinking_deep_linking_service__WEBPACK_IMPORTED_MODULE_19__["DeepLinkingService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"], src_providers_back_button_exit_back_button_exit_app_service__WEBPACK_IMPORTED_MODULE_21__["BackButtonExitAppService"]])], AppComponent);
    /***/
  },

  /***/
  "./src/app/app.module.ts":
  /*!*******************************!*\
    !*** ./src/app/app.module.ts ***!
    \*******************************/

  /*! exports provided: AppModule */

  /***/
  function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic-native/splash-screen/ngx */
    "./node_modules/@ionic-native/splash-screen/ngx/index.js");
    /* harmony import */


    var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic-native/status-bar/ngx */
    "./node_modules/@ionic-native/status-bar/ngx/index.js");
    /* harmony import */


    var _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic-native/app-version/ngx */
    "./node_modules/@ionic-native/app-version/ngx/index.js");
    /* harmony import */


    var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic-native/onesignal/ngx */
    "./node_modules/@ionic-native/onesignal/ngx/index.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/geolocation/ngx */
    "./node_modules/@ionic-native/geolocation/ngx/index.js");
    /* harmony import */


    var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic-native/native-geocoder/ngx */
    "./node_modules/@ionic-native/native-geocoder/ngx/index.js");
    /* harmony import */


    var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @ionic-native/social-sharing/ngx */
    "./node_modules/@ionic-native/social-sharing/ngx/index.js");
    /* harmony import */


    var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @ionic-native/in-app-browser/ngx */
    "./node_modules/@ionic-native/in-app-browser/ngx/index.js");
    /* harmony import */


    var _ionic_native_admob_free_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @ionic-native/admob-free/ngx */
    "./node_modules/@ionic-native/admob-free/ngx/index.js");
    /* harmony import */


    var _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @ionic-native/network/ngx */
    "./node_modules/@ionic-native/network/ngx/index.js");
    /* harmony import */


    var _ionic_native_deeplinks_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @ionic-native/deeplinks/ngx */
    "./node_modules/@ionic-native/deeplinks/ngx/index.js");
    /* harmony import */


    var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! @ionic-native/http/ngx */
    "./node_modules/@ionic-native/http/ngx/index.js");
    /* harmony import */


    var _ionic_native_email_composer_ngx__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! @ionic-native/email-composer/ngx */
    "./node_modules/@ionic-native/email-composer/ngx/index.js");
    /* harmony import */


    var _providers_config_config_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ../providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! ./app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _app_routing_module__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! ./app-routing.module */
    "./src/app/app-routing.module.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _components_menu_component_menu_component_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
    /*! ../components/menu-component/menu-component.component */
    "./src/components/menu-component/menu-component.component.ts");
    /* harmony import */


    var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(
    /*! @ionic-native/facebook/ngx */
    "./node_modules/@ionic-native/facebook/ngx/index.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");
    /* harmony import */


    var _modals_refund_policy_refund_policy_module__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(
    /*! ./modals/refund-policy/refund-policy.module */
    "./src/app/modals/refund-policy/refund-policy.module.ts");
    /* harmony import */


    var _modals_currency_list_currency_list_module__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(
    /*! ./modals/currency-list/currency-list.module */
    "./src/app/modals/currency-list/currency-list.module.ts");
    /* harmony import */


    var _modals_login_login_module__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(
    /*! ./modals/login/login.module */
    "./src/app/modals/login/login.module.ts");
    /* harmony import */


    var _modals_sign_up_sign_up_module__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(
    /*! ./modals/sign-up/sign-up.module */
    "./src/app/modals/sign-up/sign-up.module.ts");
    /* harmony import */


    var _modals_forgot_password_forgot_password_module__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(
    /*! ./modals/forgot-password/forgot-password.module */
    "./src/app/modals/forgot-password/forgot-password.module.ts");
    /* harmony import */


    var _modals_privacy_policy_privacy_policy_module__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(
    /*! ./modals/privacy-policy/privacy-policy.module */
    "./src/app/modals/privacy-policy/privacy-policy.module.ts");
    /* harmony import */


    var _modals_select_country_select_country_module__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(
    /*! ./modals/select-country/select-country.module */
    "./src/app/modals/select-country/select-country.module.ts");
    /* harmony import */


    var _modals_select_zones_select_zones_module__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(
    /*! ./modals/select-zones/select-zones.module */
    "./src/app/modals/select-zones/select-zones.module.ts");
    /* harmony import */


    var _modals_term_services_term_services_module__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(
    /*! ./modals/term-services/term-services.module */
    "./src/app/modals/term-services/term-services.module.ts");
    /* harmony import */


    var _modals_language_language_module__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(
    /*! ./modals/language/language.module */
    "./src/app/modals/language/language.module.ts");
    /* harmony import */


    var _modals_blank_modal_blank_modal_module__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(
    /*! ./modals/blank-modal/blank-modal.module */
    "./src/app/modals/blank-modal/blank-modal.module.ts");
    /* harmony import */


    var _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(
    /*! @ionic-native/photo-viewer/ngx */
    "./node_modules/@ionic-native/photo-viewer/ngx/index.js");
    /* harmony import */


    var ts_md5_dist_md5__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(
    /*! ts-md5/dist/md5 */
    "./node_modules/ts-md5/dist/md5.js");
    /* harmony import */


    var ts_md5_dist_md5__WEBPACK_IMPORTED_MODULE_39___default = /*#__PURE__*/__webpack_require__.n(ts_md5_dist_md5__WEBPACK_IMPORTED_MODULE_39__);
    /* harmony import */


    var _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(
    /*! @ionic-native/local-notifications/ngx */
    "./node_modules/@ionic-native/local-notifications/ngx/index.js");
    /* harmony import */


    var _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(
    /*! @ionic-native/fcm/ngx */
    "./node_modules/@ionic-native/fcm/ngx/index.js");
    /* harmony import */


    var _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(
    /*! @ionic-native/device/ngx */
    "./node_modules/@ionic-native/device/ngx/index.js");
    /* harmony import */


    var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(
    /*! @ionic-native/google-plus/ngx */
    "./node_modules/@ionic-native/google-plus/ngx/index.js");
    /* harmony import */


    var _modals_edit_address_edit_address_module__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(
    /*! ./modals/edit-address/edit-address.module */
    "./src/app/modals/edit-address/edit-address.module.ts");
    /* harmony import */


    var _ionic_native_stripe_ngx__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(
    /*! @ionic-native/stripe/ngx */
    "./node_modules/@ionic-native/stripe/ngx/index.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(
    /*! @ionic-native/google-maps */
    "./node_modules/@ionic-native/google-maps/index.js");
    /* harmony import */


    var src_providers_deeplinking_deep_linking_service__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(
    /*! src/providers/deeplinking/deep-linking.service */
    "./src/providers/deeplinking/deep-linking.service.ts");
    /* harmony import */


    var src_providers_get_device_id_get_device_id_service__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(
    /*! src/providers/get-device-id/get-device-id.service */
    "./src/providers/get-device-id/get-device-id.service.ts");
    /* harmony import */


    var src_providers_get_ip_Address_get_ip_address_service__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(
    /*! src/providers/get-ip-Address/get-ip-address.service */
    "./src/providers/get-ip-Address/get-ip-address.service.ts");
    /* harmony import */


    var _ionic_native_network_interface_ngx__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(
    /*! @ionic-native/network-interface/ngx */
    "./node_modules/@ionic-native/network-interface/ngx/index.js");
    /* harmony import */


    var _ionic_native_unique_device_id_ngx__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(
    /*! @ionic-native/unique-device-id/ngx */
    "./node_modules/@ionic-native/unique-device-id/ngx/index.js");
    /* harmony import */


    var src_providers_auth_guard_auth_guard_service__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(
    /*! src/providers/auth-guard/auth-guard.service */
    "./src/providers/auth-guard/auth-guard.service.ts");
    /* harmony import */


    var src_providers_back_button_exit_back_button_exit_app_service__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(
    /*! src/providers/back-button-exit/back-button-exit-app.service */
    "./src/providers/back-button-exit/back-button-exit-app.service.ts"); // Providers Import
    // For Translation
    //for side menu expandable


    var AppModule = function AppModule() {
      _classCallCheck(this, AppModule);
    };

    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_app_component__WEBPACK_IMPORTED_MODULE_20__["AppComponent"], _components_menu_component_menu_component_component__WEBPACK_IMPORTED_MODULE_23__["MenuComponentComponent"]],
      entryComponents: [],
      imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"].forRoot({
        mode: 'md'
      }), _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["IonicStorageModule"].forRoot(), _app_routing_module__WEBPACK_IMPORTED_MODULE_21__["AppRoutingModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_22__["HttpClientModule"], src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_26__["PipesModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_25__["FormsModule"], _modals_blank_modal_blank_modal_module__WEBPACK_IMPORTED_MODULE_37__["BlankModalPageModule"], _modals_language_language_module__WEBPACK_IMPORTED_MODULE_36__["LanguagePageModule"], _modals_refund_policy_refund_policy_module__WEBPACK_IMPORTED_MODULE_27__["RefundPolicyPageModule"], _modals_currency_list_currency_list_module__WEBPACK_IMPORTED_MODULE_28__["CurrencyListPageModule"], _modals_login_login_module__WEBPACK_IMPORTED_MODULE_29__["LoginPageModule"], _modals_sign_up_sign_up_module__WEBPACK_IMPORTED_MODULE_30__["SignUpPageModule"], _modals_forgot_password_forgot_password_module__WEBPACK_IMPORTED_MODULE_31__["ForgotPasswordPageModule"], _modals_privacy_policy_privacy_policy_module__WEBPACK_IMPORTED_MODULE_32__["PrivacyPolicyPageModule"], _modals_term_services_term_services_module__WEBPACK_IMPORTED_MODULE_35__["TermServicesPageModule"], _modals_select_country_select_country_module__WEBPACK_IMPORTED_MODULE_33__["SelectCountryPageModule"], _modals_select_zones_select_zones_module__WEBPACK_IMPORTED_MODULE_34__["SelectZonesPageModule"], _modals_edit_address_edit_address_module__WEBPACK_IMPORTED_MODULE_44__["EditAddressPageModule"]],
      providers: [_ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_5__["StatusBar"], _providers_config_config_service__WEBPACK_IMPORTED_MODULE_18__["ConfigService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_19__["SharedDataService"], _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_4__["SplashScreen"], _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_6__["AppVersion"], _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_7__["OneSignal"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_9__["Geolocation"], _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_10__["NativeGeocoder"], _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_11__["SocialSharing"], _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_12__["InAppBrowser"], _ionic_native_admob_free_ngx__WEBPACK_IMPORTED_MODULE_13__["AdMobFree"], _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_14__["Network"], _ionic_native_deeplinks_ngx__WEBPACK_IMPORTED_MODULE_15__["Deeplinks"], _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_16__["HTTP"], _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_24__["Facebook"], _ionic_native_email_composer_ngx__WEBPACK_IMPORTED_MODULE_17__["EmailComposer"], _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_38__["PhotoViewer"], ts_md5_dist_md5__WEBPACK_IMPORTED_MODULE_39__["Md5"], _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_40__["LocalNotifications"], _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_41__["FCM"], _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_42__["Device"], _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_43__["GooglePlus"], _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_47__["GoogleMaps"], _ionic_native_stripe_ngx__WEBPACK_IMPORTED_MODULE_45__["Stripe"], src_providers_deeplinking_deep_linking_service__WEBPACK_IMPORTED_MODULE_48__["DeepLinkingService"], src_providers_get_device_id_get_device_id_service__WEBPACK_IMPORTED_MODULE_49__["GetDeviceIdService"], src_providers_get_ip_Address_get_ip_address_service__WEBPACK_IMPORTED_MODULE_50__["GetIpAddressService"], _ionic_native_network_interface_ngx__WEBPACK_IMPORTED_MODULE_51__["NetworkInterface"], _ionic_native_unique_device_id_ngx__WEBPACK_IMPORTED_MODULE_52__["UniqueDeviceID"], src_providers_auth_guard_auth_guard_service__WEBPACK_IMPORTED_MODULE_53__["AuthGuardService"], src_providers_back_button_exit_back_button_exit_app_service__WEBPACK_IMPORTED_MODULE_54__["BackButtonExitAppService"], {
        provide: _angular_router__WEBPACK_IMPORTED_MODULE_46__["RouteReuseStrategy"],
        useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicRouteStrategy"]
      }],
      bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_20__["AppComponent"]]
    })], AppModule);
    /***/
  },

  /***/
  "./src/app/modals/blank-modal/blank-modal.module.ts":
  /*!**********************************************************!*\
    !*** ./src/app/modals/blank-modal/blank-modal.module.ts ***!
    \**********************************************************/

  /*! exports provided: BlankModalPageModule */

  /***/
  function srcAppModalsBlankModalBlankModalModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BlankModalPageModule", function () {
      return BlankModalPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _blank_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./blank-modal.page */
    "./src/app/modals/blank-modal/blank-modal.page.ts");

    var routes = [{
      path: '',
      component: _blank_modal_page__WEBPACK_IMPORTED_MODULE_6__["BlankModalPage"]
    }];

    var BlankModalPageModule = function BlankModalPageModule() {
      _classCallCheck(this, BlankModalPageModule);
    };

    BlankModalPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_blank_modal_page__WEBPACK_IMPORTED_MODULE_6__["BlankModalPage"]],
      entryComponents: [_blank_modal_page__WEBPACK_IMPORTED_MODULE_6__["BlankModalPage"]]
    })], BlankModalPageModule);
    /***/
  },

  /***/
  "./src/app/modals/blank-modal/blank-modal.page.scss":
  /*!**********************************************************!*\
    !*** ./src/app/modals/blank-modal/blank-modal.page.scss ***!
    \**********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppModalsBlankModalBlankModalPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZGFscy9ibGFuay1tb2RhbC9ibGFuay1tb2RhbC5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/modals/blank-modal/blank-modal.page.ts":
  /*!********************************************************!*\
    !*** ./src/app/modals/blank-modal/blank-modal.page.ts ***!
    \********************************************************/

  /*! exports provided: BlankModalPage */

  /***/
  function srcAppModalsBlankModalBlankModalPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BlankModalPage", function () {
      return BlankModalPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

    var BlankModalPage = /*#__PURE__*/function () {
      function BlankModalPage(modalCont) {
        _classCallCheck(this, BlankModalPage);

        this.modalCont = modalCont;
      }

      _createClass(BlankModalPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {//this.modalCont.dismiss();
        }
      }]);

      return BlankModalPage;
    }();

    BlankModalPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }];
    };

    BlankModalPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-blank-modal',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./blank-modal.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/blank-modal/blank-modal.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./blank-modal.page.scss */
      "./src/app/modals/blank-modal/blank-modal.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])], BlankModalPage);
    /***/
  },

  /***/
  "./src/app/modals/currency-list/currency-list.module.ts":
  /*!**************************************************************!*\
    !*** ./src/app/modals/currency-list/currency-list.module.ts ***!
    \**************************************************************/

  /*! exports provided: CurrencyListPageModule */

  /***/
  function srcAppModalsCurrencyListCurrencyListModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CurrencyListPageModule", function () {
      return CurrencyListPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _currency_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./currency-list.page */
    "./src/app/modals/currency-list/currency-list.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _currency_list_page__WEBPACK_IMPORTED_MODULE_6__["CurrencyListPage"]
    }];

    var CurrencyListPageModule = function CurrencyListPageModule() {
      _classCallCheck(this, CurrencyListPageModule);
    };

    CurrencyListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]],
      declarations: [_currency_list_page__WEBPACK_IMPORTED_MODULE_6__["CurrencyListPage"]],
      entryComponents: [_currency_list_page__WEBPACK_IMPORTED_MODULE_6__["CurrencyListPage"]]
    })], CurrencyListPageModule);
    /***/
  },

  /***/
  "./src/app/modals/currency-list/currency-list.page.scss":
  /*!**************************************************************!*\
    !*** ./src/app/modals/currency-list/currency-list.page.scss ***!
    \**************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppModalsCurrencyListCurrencyListPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZGFscy9jdXJyZW5jeS1saXN0L2N1cnJlbmN5LWxpc3QucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/modals/currency-list/currency-list.page.ts":
  /*!************************************************************!*\
    !*** ./src/app/modals/currency-list/currency-list.page.ts ***!
    \************************************************************/

  /*! exports provided: CurrencyListPage */

  /***/
  function srcAppModalsCurrencyListCurrencyListPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CurrencyListPage", function () {
      return CurrencyListPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

    var CurrencyListPage = /*#__PURE__*/function () {
      function CurrencyListPage(loading, config, shared, modalCtrl, http) {
        _classCallCheck(this, CurrencyListPage);

        this.loading = loading;
        this.config = config;
        this.shared = shared;
        this.modalCtrl = modalCtrl;
        this.http = http;
        this.currencyList = [];
        this.currentCurrencySymbol = localStorage.currency;
        this.getListOfCurrency();
      }

      _createClass(CurrencyListPage, [{
        key: "getListOfCurrency",
        value: function getListOfCurrency() {
          var _this7 = this;

          this.loading.show();
          this.config.getHttp('getcurrencies').then(function (data) {
            _this7.loading.hide();

            _this7.currencyList = data.data;

            _this7.currencyList.forEach(function (val) {
              if (localStorage.currencyCode == val.code) _this7.currency = val;
            });
          });
        }
      }, {
        key: "updateCurrency",
        value: function updateCurrency() {
          if (this.currency == undefined) return;
          console.log(localStorage.currencyCode + "  " + this.currency.code);

          if (localStorage.currencyCode != this.currency.code) {
            this.loading.autoHide(1000);
            localStorage.currencyCode = this.currency.code;

            if (this.currency.symbol_left != null) {
              localStorage.currencyPos = "left";
              localStorage.currency = this.currency.symbol_left;
            }

            if (this.currency.symbol_right != null) {
              localStorage.currencyPos = "right";
              localStorage.currency = this.currency.symbol_right;
            }

            localStorage.decimals = this.currency.decimal_places;
            this.shared.emptyCart();
            this.shared.emptyRecentViewed();
            setTimeout(function () {
              window.location.reload();
            }, 1000);
          }
        } //close modal

      }, {
        key: "dismiss",
        value: function dismiss() {
          this.modalCtrl.dismiss();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return CurrencyListPage;
    }();

    CurrencyListPage.ctorParameters = function () {
      return [{
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_2__["LoadingService"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]
      }];
    };

    CurrencyListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-currency-list',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./currency-list.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/currency-list/currency-list.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./currency-list.page.scss */
      "./src/app/modals/currency-list/currency-list.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_2__["LoadingService"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]])], CurrencyListPage);
    /***/
  },

  /***/
  "./src/app/modals/edit-address/edit-address.module.ts":
  /*!************************************************************!*\
    !*** ./src/app/modals/edit-address/edit-address.module.ts ***!
    \************************************************************/

  /*! exports provided: EditAddressPageModule */

  /***/
  function srcAppModalsEditAddressEditAddressModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditAddressPageModule", function () {
      return EditAddressPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _edit_address_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./edit-address.page */
    "./src/app/modals/edit-address/edit-address.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _edit_address_page__WEBPACK_IMPORTED_MODULE_6__["EditAddressPage"]
    }];

    var EditAddressPageModule = function EditAddressPageModule() {
      _classCallCheck(this, EditAddressPageModule);
    };

    EditAddressPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]],
      declarations: [_edit_address_page__WEBPACK_IMPORTED_MODULE_6__["EditAddressPage"]]
    })], EditAddressPageModule);
    /***/
  },

  /***/
  "./src/app/modals/edit-address/edit-address.page.scss":
  /*!************************************************************!*\
    !*** ./src/app/modals/edit-address/edit-address.page.scss ***!
    \************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppModalsEditAddressEditAddressPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZGFscy9lZGl0LWFkZHJlc3MvZWRpdC1hZGRyZXNzLnBhZ2Uuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/modals/edit-address/edit-address.page.ts":
  /*!**********************************************************!*\
    !*** ./src/app/modals/edit-address/edit-address.page.ts ***!
    \**********************************************************/

  /*! exports provided: EditAddressPage */

  /***/
  function srcAppModalsEditAddressEditAddressPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditAddressPage", function () {
      return EditAddressPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _select_zones_select_zones_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../select-zones/select-zones.page */
    "./src/app/modals/select-zones/select-zones.page.ts");
    /* harmony import */


    var _select_country_select_country_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../select-country/select-country.page */
    "./src/app/modals/select-country/select-country.page.ts");
    /* harmony import */


    var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/providers/app-events/app-events.service */
    "./src/providers/app-events/app-events.service.ts");

    var EditAddressPage = /*#__PURE__*/function () {
      function EditAddressPage(appEventsService, config, modalCtrl, loading, shared, navParams) {
        _classCallCheck(this, EditAddressPage);

        this.appEventsService = appEventsService;
        this.config = config;
        this.modalCtrl = modalCtrl;
        this.loading = loading;
        this.shared = shared;
        this.navParams = navParams;
        this.shippingData = {};
        this.type = 'update'; //============================================================================================  
        //adding shipping address of the user

        this.addShippingAddress = function (form) {
          var _this8 = this;

          this.loading.show();
          this.shippingData.customers_id = this.shared.customerData.customers_id;
          var dat = this.shippingData; //dat.entry_state = dat.delivery_zone;
          // dat.entry_zone = dat.delivery_zone;

          dat.is_default = 0;
          this.config.postHttp('addshippingaddress', dat).then(function (data) {
            _this8.loading.hide();

            _this8.dismiss();

            _this8.shared.toast(data.message);
          }, function (response) {
            this.loading.hide();
            console.log(response);
          });
        }; //============================================================================================  
        //updating shipping address of the user


        this.updateShippingAddress = function (form, id) {
          var _this9 = this;

          this.loading.show();
          this.shippingData.customers_id = this.shared.customerData.customers_id;
          var dat = this.shippingData; //dat.entry_state = dat.delivery_zone;
          //dat.entry_zone = dat.delivery_zone;

          dat.is_default = 0;
          this.config.postHttp('updateshippingaddress', dat).then(function (data) {
            _this9.loading.hide();

            if (data.success == 1) {
              _this9.dismiss();

              _this9.shared.toast(data.message);
            }
          }, function (response) {
            this.loading.hide();
            console.log(response);
          });
        };

        this.data = navParams.get('data');
        this.type = navParams.get('type');

        if (this.type != 'add') {
          this.shippingData.entry_firstname = this.data.firstname;
          this.shippingData.entry_lastname = this.data.lastname;
          this.shippingData.entry_street_address = this.data.street;
          this.shippingData.entry_country_name = this.data.country_name;
          this.shippingData.entry_zone = this.data.zone_name;
          this.shippingData.entry_postcode = this.data.postcode;
          this.shippingData.entry_country_id = this.data.countries_id;
          this.shippingData.entry_address_id = this.data.address_id;
          this.shippingData.entry_city = this.data.city;
          this.shippingData.entry_zone_id = this.data.zone_id;
          this.shippingData.entry_state = this.data.state;
          this.shippingData.suburb = this.data.suburb;
          this.shippingData.address_id = this.data.address_id;
          if (this.data.zone_name == null) this.shippingData.entry_zone = "other";
        }
      }

      _createClass(EditAddressPage, [{
        key: "selectCountryPage",
        value: function selectCountryPage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
            var _this10 = this;

            var modal;
            return regeneratorRuntime.wrap(function _callee7$(_context7) {
              while (1) {
                switch (_context7.prev = _context7.next) {
                  case 0:
                    _context7.next = 2;
                    return this.modalCtrl.create({
                      component: _select_country_select_country_page__WEBPACK_IMPORTED_MODULE_7__["SelectCountryPage"],
                      componentProps: {
                        page: 'editShipping'
                      }
                    });

                  case 2:
                    modal = _context7.sent;
                    modal.onDidDismiss().then(function () {
                      _this10.updateCountryZone();
                    });
                    _context7.next = 6;
                    return modal.present();

                  case 6:
                    return _context7.abrupt("return", _context7.sent);

                  case 7:
                  case "end":
                    return _context7.stop();
                }
              }
            }, _callee7, this);
          }));
        }
      }, {
        key: "selectZonePage",
        value: function selectZonePage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
            var _this11 = this;

            var modal;
            return regeneratorRuntime.wrap(function _callee8$(_context8) {
              while (1) {
                switch (_context8.prev = _context8.next) {
                  case 0:
                    _context8.next = 2;
                    return this.modalCtrl.create({
                      component: _select_zones_select_zones_page__WEBPACK_IMPORTED_MODULE_6__["SelectZonesPage"],
                      componentProps: {
                        page: 'editShipping',
                        id: this.shippingData.entry_country_id
                      }
                    });

                  case 2:
                    modal = _context8.sent;
                    modal.onDidDismiss().then(function () {
                      _this11.updateCountryZone();
                    });
                    _context8.next = 6;
                    return modal.present();

                  case 6:
                    return _context8.abrupt("return", _context8.sent);

                  case 7:
                  case "end":
                    return _context8.stop();
                }
              }
            }, _callee8, this);
          }));
        } //close modal

      }, {
        key: "dismiss",
        value: function dismiss() {
          this.modalCtrl.dismiss();
        }
      }, {
        key: "updateCountryZone",
        value: function updateCountryZone() {
          console.log(this.shared.tempdata.entry_country_id);

          if (this.shared.tempdata.entry_country_id != undefined) {
            this.shippingData.entry_country_id = this.shared.tempdata.entry_country_id;
            this.shippingData.entry_country_name = this.shared.tempdata.entry_country_name;
            this.shippingData.entry_country_code = this.shared.tempdata.entry_country_code;
            this.shippingData.entry_zone = this.shared.tempdata.entry_zone;
          }

          if (this.shared.tempdata.entry_zone != undefined) {
            this.shippingData.entry_zone = this.shared.tempdata.entry_zone;
            this.shippingData.entry_zone_id = this.shared.tempdata.entry_zone_id;
          }
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return EditAddressPage;
    }();

    EditAddressPage.ctorParameters = function () {
      return [{
        type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_8__["AppEventsService"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]
      }];
    };

    EditAddressPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-edit-address',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./edit-address.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/edit-address/edit-address.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./edit-address.page.scss */
      "./src/app/modals/edit-address/edit-address.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_8__["AppEventsService"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]])], EditAddressPage);
    /***/
  },

  /***/
  "./src/app/modals/forgot-password/forgot-password.module.ts":
  /*!******************************************************************!*\
    !*** ./src/app/modals/forgot-password/forgot-password.module.ts ***!
    \******************************************************************/

  /*! exports provided: ForgotPasswordPageModule */

  /***/
  function srcAppModalsForgotPasswordForgotPasswordModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ForgotPasswordPageModule", function () {
      return ForgotPasswordPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _forgot_password_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./forgot-password.page */
    "./src/app/modals/forgot-password/forgot-password.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _forgot_password_page__WEBPACK_IMPORTED_MODULE_6__["ForgotPasswordPage"]
    }];

    var ForgotPasswordPageModule = function ForgotPasswordPageModule() {
      _classCallCheck(this, ForgotPasswordPageModule);
    };

    ForgotPasswordPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_forgot_password_page__WEBPACK_IMPORTED_MODULE_6__["ForgotPasswordPage"]],
      entryComponents: [_forgot_password_page__WEBPACK_IMPORTED_MODULE_6__["ForgotPasswordPage"]]
    })], ForgotPasswordPageModule);
    /***/
  },

  /***/
  "./src/app/modals/forgot-password/forgot-password.page.scss":
  /*!******************************************************************!*\
    !*** ./src/app/modals/forgot-password/forgot-password.page.scss ***!
    \******************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppModalsForgotPasswordForgotPasswordPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-img {\n  height: 150px;\n  opacity: 0.1;\n}\n\nform ion-item {\n  --padding-start: 0;\n  --background: var(--ion-background-color);\n}\n\nform ion-item ion-label {\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n}\n\nform ion-item:nth-child(2) {\n  height: 33px;\n}\n\nform ion-item:nth-child(2) ion-label {\n  color: red;\n}\n\nform ion-button {\n  margin-top: 10px;\n  text-transform: uppercase;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9tb2RhbHMvZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5wYWdlLnNjc3MiLCJzcmMvYXBwL21vZGFscy9mb3Jnb3QtcGFzc3dvcmQvZm9yZ290LXBhc3N3b3JkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLGFBQUE7RUFDQSxZQUFBO0FDQUo7O0FES0k7RUFDSSxrQkFBQTtFQUNBLHlDQUFBO0FDRlI7O0FER1E7RUFDSSwyQ0FBQTtBQ0RaOztBREdRO0VBQ0ksWUFBQTtBQ0RaOztBREVZO0VBQ0ksVUFBQTtBQ0FoQjs7QURJSTtFQUNJLGdCQUFBO0VBQ0EseUJBQUE7QUNGUiIsImZpbGUiOiJzcmMvYXBwL21vZGFscy9mb3Jnb3QtcGFzc3dvcmQvZm9yZ290LXBhc3N3b3JkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIEZvciBpbWFnZSBkZXNpZ25cclxuaW9uLWltZyB7XHJcbiAgICBoZWlnaHQ6IDE1MHB4O1xyXG4gICAgb3BhY2l0eTogMC4xO1xyXG59XHJcblxyXG4vLyBGb3IgRm9ybVxyXG5mb3JtIHtcclxuICAgIGlvbi1pdGVtIHtcclxuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XHJcbiAgICAgICAgaW9uLWxhYmVsIHtcclxuICAgICAgICAgICAgY29sb3I6IHJnYmEodmFyKC0taW9uLXRleHQtY29sb3ItcmdiKSwgMC41KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgJjpudGgtY2hpbGQoMikge1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMzcHg7XHJcbiAgICAgICAgICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogcmVkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgaW9uLWJ1dHRvbiB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgfVxyXG59XHJcbiIsImlvbi1pbWcge1xuICBoZWlnaHQ6IDE1MHB4O1xuICBvcGFjaXR5OiAwLjE7XG59XG5cbmZvcm0gaW9uLWl0ZW0ge1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IpO1xufVxuZm9ybSBpb24taXRlbSBpb24tbGFiZWwge1xuICBjb2xvcjogcmdiYSh2YXIoLS1pb24tdGV4dC1jb2xvci1yZ2IpLCAwLjUpO1xufVxuZm9ybSBpb24taXRlbTpudGgtY2hpbGQoMikge1xuICBoZWlnaHQ6IDMzcHg7XG59XG5mb3JtIGlvbi1pdGVtOm50aC1jaGlsZCgyKSBpb24tbGFiZWwge1xuICBjb2xvcjogcmVkO1xufVxuZm9ybSBpb24tYnV0dG9uIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/modals/forgot-password/forgot-password.page.ts":
  /*!****************************************************************!*\
    !*** ./src/app/modals/forgot-password/forgot-password.page.ts ***!
    \****************************************************************/

  /*! exports provided: ForgotPasswordPage */

  /***/
  function srcAppModalsForgotPasswordForgotPasswordPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ForgotPasswordPage", function () {
      return ForgotPasswordPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");

    var ForgotPasswordPage = /*#__PURE__*/function () {
      function ForgotPasswordPage(loading, config, shared, modalCtrl) {
        _classCallCheck(this, ForgotPasswordPage);

        this.loading = loading;
        this.config = config;
        this.shared = shared;
        this.modalCtrl = modalCtrl;
        this.formData = {
          email: ''
        };
        this.errorMessage = '';
      }

      _createClass(ForgotPasswordPage, [{
        key: "forgetPassword",
        value: function forgetPassword() {
          var _this12 = this;

          this.loading.show();
          this.errorMessage = '';
          this.config.postHttp('processforgotpassword', this.formData).then(function (data) {
            _this12.loading.hide();

            if (data.success == 1) {
              _this12.shared.toast(data.message);

              _this12.dismiss();
            }

            if (data.success == 0) {
              _this12.errorMessage = data.message;

              _this12.shared.toast(data.message);
            }
          });
        }
      }, {
        key: "dismiss",
        value: function dismiss() {
          this.modalCtrl.dismiss();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ForgotPasswordPage;
    }();

    ForgotPasswordPage.ctorParameters = function () {
      return [{
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_3__["LoadingService"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
      }];
    };

    ForgotPasswordPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-forgot-password',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./forgot-password.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/forgot-password/forgot-password.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./forgot-password.page.scss */
      "./src/app/modals/forgot-password/forgot-password.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_3__["LoadingService"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]])], ForgotPasswordPage);
    /***/
  },

  /***/
  "./src/app/modals/language/language.module.ts":
  /*!****************************************************!*\
    !*** ./src/app/modals/language/language.module.ts ***!
    \****************************************************/

  /*! exports provided: LanguagePageModule */

  /***/
  function srcAppModalsLanguageLanguageModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LanguagePageModule", function () {
      return LanguagePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _language_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./language.page */
    "./src/app/modals/language/language.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _language_page__WEBPACK_IMPORTED_MODULE_6__["LanguagePage"]
    }];

    var LanguagePageModule = function LanguagePageModule() {
      _classCallCheck(this, LanguagePageModule);
    };

    LanguagePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]],
      exports: [_language_page__WEBPACK_IMPORTED_MODULE_6__["LanguagePage"]],
      declarations: [_language_page__WEBPACK_IMPORTED_MODULE_6__["LanguagePage"]],
      entryComponents: [_language_page__WEBPACK_IMPORTED_MODULE_6__["LanguagePage"]]
    })], LanguagePageModule);
    /***/
  },

  /***/
  "./src/app/modals/language/language.page.scss":
  /*!****************************************************!*\
    !*** ./src/app/modals/language/language.page.scss ***!
    \****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppModalsLanguageLanguagePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZGFscy9sYW5ndWFnZS9sYW5ndWFnZS5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/modals/language/language.page.ts":
  /*!**************************************************!*\
    !*** ./src/app/modals/language/language.page.ts ***!
    \**************************************************/

  /*! exports provided: LanguagePage */

  /***/
  function srcAppModalsLanguageLanguagePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LanguagePage", function () {
      return LanguagePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");

    var LanguagePage = /*#__PURE__*/function () {
      function LanguagePage(loading, modalCont, config, shared) {
        var _this13 = this;

        _classCallCheck(this, LanguagePage);

        this.loading = loading;
        this.modalCont = modalCont;
        this.config = config;
        this.shared = shared;
        this.prviousLanguageId = localStorage.langId; //getting all languages

        this.loading.show();
        this.config.getHttp('getlanguages').then(function (data) {
          _this13.loading.hide();

          _this13.languages = data.languages;

          var _iterator = _createForOfIteratorHelper(_this13.languages),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var _data = _step.value;

              if (_data.languages_id == _this13.prviousLanguageId) {
                _this13.selectedLanguage = _data;
              }
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }
        });
      }

      _createClass(LanguagePage, [{
        key: "updateLanguage",
        value: function updateLanguage(lang) {
          if (lang != undefined && this.prviousLanguageId != lang.languages_id) {
            this.loading.show();
            localStorage.langId = lang.languages_id;
            localStorage.direction = lang.direction;
            this.shared.emptyCart();
            this.shared.emptyRecentViewed();
            setTimeout(function () {
              window.location.reload();
            }, 900);
          }
        } //close modal

      }, {
        key: "dismiss",
        value: function dismiss() {
          this.modalCont.dismiss();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return LanguagePage;
    }();

    LanguagePage.ctorParameters = function () {
      return [{
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_3__["LoadingService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]
      }];
    };

    LanguagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-language',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./language.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/language/language.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./language.page.scss */
      "./src/app/modals/language/language.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_3__["LoadingService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]])], LanguagePage);
    /***/
  },

  /***/
  "./src/app/modals/login/login.module.ts":
  /*!**********************************************!*\
    !*** ./src/app/modals/login/login.module.ts ***!
    \**********************************************/

  /*! exports provided: LoginPageModule */

  /***/
  function srcAppModalsLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
      return LoginPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./login.page */
    "./src/app/modals/login/login.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }];

    var LoginPageModule = function LoginPageModule() {
      _classCallCheck(this, LoginPageModule);
    };

    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]],
      entryComponents: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })], LoginPageModule);
    /***/
  },

  /***/
  "./src/app/modals/login/login.page.scss":
  /*!**********************************************!*\
    !*** ./src/app/modals/login/login.page.scss ***!
    \**********************************************/

  /*! exports provided: default */

  /***/
  function srcAppModalsLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-img {\n  height: 150px;\n  opacity: 0.1;\n}\n\nform {\n  padding-top: 28px;\n}\n\nform ion-item {\n  --padding-start: 0;\n  --background: var(--ion-background-color);\n}\n\nform ion-item ion-label {\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n}\n\nform ion-item:nth-child(3) {\n  height: 33px;\n}\n\nform ion-item:nth-child(3) ion-label {\n  color: red;\n}\n\nform ion-button {\n  text-transform: uppercase;\n}\n\nion-button:nth-child(3) {\n  margin-top: 15px;\n}\n\nion-button:nth-child(4) {\n  margin-top: 15px;\n  --background: #3b5998;\n  color: white;\n}\n\nion-button:nth-child(5) {\n  color: black;\n  margin-top: 20px;\n  --border-color: black;\n  --border-width: thin;\n}\n\n.fb-button {\n  margin-top: 15px;\n  --background: #3b5998;\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9tb2RhbHMvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic3JjL2FwcC9tb2RhbHMvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsYUFBQTtFQUNBLFlBQUE7QUNBRjs7QURJQTtFQUNFLGlCQUFBO0FDREY7O0FERUU7RUFDRSxrQkFBQTtFQUNBLHlDQUFBO0FDQUo7O0FEQ0k7RUFDRSwyQ0FBQTtBQ0NOOztBRENJO0VBQ0UsWUFBQTtBQ0NOOztBREFNO0VBQ0UsVUFBQTtBQ0VSOztBREVFO0VBQ0UseUJBQUE7QUNBSjs7QURNRTtFQUNFLGdCQUFBO0FDSEo7O0FES0U7RUFDRSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtBQ0hKOztBREtFO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtBQ0hKOztBRE9BO0VBQ0UsZ0JBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7QUNKRiIsImZpbGUiOiJzcmMvYXBwL21vZGFscy9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBGb3IgaW1hZ2UgZGVzaWduXHJcbmlvbi1pbWcge1xyXG4gIGhlaWdodDogMTUwcHg7XHJcbiAgb3BhY2l0eTogMC4xO1xyXG59XHJcblxyXG4vLyBGb3IgRm9ybVxyXG5mb3JtIHtcclxuICBwYWRkaW5nLXRvcDogMjhweDtcclxuICBpb24taXRlbSB7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XHJcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcclxuICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgIGNvbG9yOiByZ2JhKHZhcigtLWlvbi10ZXh0LWNvbG9yLXJnYiksIDAuNSk7XHJcbiAgICB9XHJcbiAgICAmOm50aC1jaGlsZCgzKSB7XHJcbiAgICAgIGhlaWdodDogMzNweDtcclxuICAgICAgaW9uLWxhYmVsIHtcclxuICAgICAgICBjb2xvcjogcmVkO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIGlvbi1idXR0b24ge1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICB9XHJcbn1cclxuXHJcbi8vIEZvciBMb2dpbiwgRmFjZWJvb2ssIFJlZ2lzdGVyIEJ1dHRvbnNcclxuaW9uLWJ1dHRvbiB7XHJcbiAgJjpudGgtY2hpbGQoMykge1xyXG4gICAgbWFyZ2luLXRvcDogMTVweDtcclxuICB9XHJcbiAgJjpudGgtY2hpbGQoNCkge1xyXG4gICAgbWFyZ2luLXRvcDogMTVweDtcclxuICAgIC0tYmFja2dyb3VuZDogIzNiNTk5ODtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcbiAgJjpudGgtY2hpbGQoNSkge1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIC0tYm9yZGVyLWNvbG9yOiBibGFjaztcclxuICAgIC0tYm9yZGVyLXdpZHRoOiB0aGluO1xyXG4gIH1cclxufVxyXG5cclxuLmZiLWJ1dHRvbiB7XHJcbiAgbWFyZ2luLXRvcDogMTVweDtcclxuICAtLWJhY2tncm91bmQ6ICMzYjU5OTg7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcbiIsImlvbi1pbWcge1xuICBoZWlnaHQ6IDE1MHB4O1xuICBvcGFjaXR5OiAwLjE7XG59XG5cbmZvcm0ge1xuICBwYWRkaW5nLXRvcDogMjhweDtcbn1cbmZvcm0gaW9uLWl0ZW0ge1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IpO1xufVxuZm9ybSBpb24taXRlbSBpb24tbGFiZWwge1xuICBjb2xvcjogcmdiYSh2YXIoLS1pb24tdGV4dC1jb2xvci1yZ2IpLCAwLjUpO1xufVxuZm9ybSBpb24taXRlbTpudGgtY2hpbGQoMykge1xuICBoZWlnaHQ6IDMzcHg7XG59XG5mb3JtIGlvbi1pdGVtOm50aC1jaGlsZCgzKSBpb24tbGFiZWwge1xuICBjb2xvcjogcmVkO1xufVxuZm9ybSBpb24tYnV0dG9uIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuaW9uLWJ1dHRvbjpudGgtY2hpbGQoMykge1xuICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuaW9uLWJ1dHRvbjpudGgtY2hpbGQoNCkge1xuICBtYXJnaW4tdG9wOiAxNXB4O1xuICAtLWJhY2tncm91bmQ6ICMzYjU5OTg7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbmlvbi1idXR0b246bnRoLWNoaWxkKDUpIHtcbiAgY29sb3I6IGJsYWNrO1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICAtLWJvcmRlci1jb2xvcjogYmxhY2s7XG4gIC0tYm9yZGVyLXdpZHRoOiB0aGluO1xufVxuXG4uZmItYnV0dG9uIHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbiAgLS1iYWNrZ3JvdW5kOiAjM2I1OTk4O1xuICBjb2xvcjogd2hpdGU7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/modals/login/login.page.ts":
  /*!********************************************!*\
    !*** ./src/app/modals/login/login.page.ts ***!
    \********************************************/

  /*! exports provided: LoginPage */

  /***/
  function srcAppModalsLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
      return LoginPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _sign_up_sign_up_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../sign-up/sign-up.page */
    "./src/app/modals/sign-up/sign-up.page.ts");
    /* harmony import */


    var _forgot_password_forgot_password_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../forgot-password/forgot-password.page */
    "./src/app/modals/forgot-password/forgot-password.page.ts");
    /* harmony import */


    var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic-native/facebook/ngx */
    "./node_modules/@ionic-native/facebook/ngx/index.js");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/google-plus/ngx */
    "./node_modules/@ionic-native/google-plus/ngx/index.js");
    /* harmony import */


    var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! src/providers/app-events/app-events.service */
    "./src/providers/app-events/app-events.service.ts");

    var LoginPage = /*#__PURE__*/function () {
      function LoginPage(config, modalCtrl, loading, shared, fb, applicationRef, navCtrl, appEventsService, navParams, googlePlus) {
        _classCallCheck(this, LoginPage);

        this.config = config;
        this.modalCtrl = modalCtrl;
        this.loading = loading;
        this.shared = shared;
        this.fb = fb;
        this.applicationRef = applicationRef;
        this.navCtrl = navCtrl;
        this.appEventsService = appEventsService;
        this.navParams = navParams;
        this.googlePlus = googlePlus;
        this.formData = {
          email: '',
          password: ''
        };
        this.errorMessage = '';
        this.hideGuestLogin = navParams.get('hideGuestLogin');
        this.shared.currentOpenedModel = this;
      }

      _createClass(LoginPage, [{
        key: "login",
        value: function login() {
          var _this14 = this;

          this.loading.show();
          this.errorMessage = '';
          this.config.postHttp('processlogin', this.formData).then(function (data) {
            _this14.loading.hide();

            if (data.success == 1) {
              _this14.shared.login(data.data[0]);

              _this14.dismiss();
            }

            if (data.success == 0) {
              _this14.errorMessage = data.message;
            }
          });
        }
      }, {
        key: "openSignUpPage",
        value: function openSignUpPage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
            var modal;
            return regeneratorRuntime.wrap(function _callee9$(_context9) {
              while (1) {
                switch (_context9.prev = _context9.next) {
                  case 0:
                    this.dismiss();
                    _context9.next = 3;
                    return this.modalCtrl.create({
                      component: _sign_up_sign_up_page__WEBPACK_IMPORTED_MODULE_5__["SignUpPage"]
                    });

                  case 3:
                    modal = _context9.sent;
                    _context9.next = 6;
                    return modal.present();

                  case 6:
                    return _context9.abrupt("return", _context9.sent);

                  case 7:
                  case "end":
                    return _context9.stop();
                }
              }
            }, _callee9, this);
          }));
        }
      }, {
        key: "openForgetPasswordPage",
        value: function openForgetPasswordPage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
            var modal;
            return regeneratorRuntime.wrap(function _callee10$(_context10) {
              while (1) {
                switch (_context10.prev = _context10.next) {
                  case 0:
                    _context10.next = 2;
                    return this.modalCtrl.create({
                      component: _forgot_password_forgot_password_page__WEBPACK_IMPORTED_MODULE_6__["ForgotPasswordPage"]
                    });

                  case 2:
                    modal = _context10.sent;
                    _context10.next = 5;
                    return modal.present();

                  case 5:
                    return _context10.abrupt("return", _context10.sent);

                  case 6:
                  case "end":
                    return _context10.stop();
                }
              }
            }, _callee10, this);
          }));
        }
      }, {
        key: "facebookLogin",
        value: function facebookLogin() {
          var _this15 = this;

          this.fb.getLoginStatus().then(function (res) {
            if (res.status == 'connected') {
              console.log("user connected already" + res.authResponse.accessToken);

              _this15.createAccount(res.authResponse.accessToken, 'fb');
            } else {
              console.log("USer Not login ");

              _this15.fb.login(['public_profile', 'email']).then(function (res) {
                // this.alert.show('Logged into Facebook!' + JSON.stringify(res));
                console.log("successfully login ");

                _this15.createAccount(res.authResponse.accessToken, 'fb');
              })["catch"](function (e) {
                return _this15.shared.showAlert('Error logging into Facebook' + JSON.stringify(e));
              });
            }
          })["catch"](function (e) {
            return _this15.shared.showAlert('Error Check Login Status Facebook' + JSON.stringify(e));
          });
        }
      }, {
        key: "googleLogin",
        value: function googleLogin() {
          var _this16 = this;

          this.loading.autoHide(500);
          this.googlePlus.login({}).then(function (res) {
            //  alert(JSON.stringify(res))
            _this16.createAccount(res, 'google');
          })["catch"](function (err) {
            return _this16.shared.showAlert(JSON.stringify(err));
          });
        } //============================================================================================  
        //creating new account using function facebook or google details 

      }, {
        key: "createAccount",
        value: function createAccount(info, type) {
          var _this17 = this;

          // alert(info);
          this.loading.show();
          var dat = {};
          var url = '';

          if (type == 'fb') {
            url = 'facebookregistration';
            dat.access_token = info;
          } else {
            url = 'googleregistration';
            dat = info;
          }

          this.config.postHttp(url, dat).then(function (data) {
            _this17.loading.hide(); // alert("data get");


            if (data.success == 1) {
              _this17.shared.login(data.data[0]); //alert('login');


              _this17.shared.showAlertWithTitle("<h3>Your Account has been created successfully !</h3><ul><li>Your Email: " + "<span>" + _this17.shared.customerData.email + "</span>" + "</li><li>Your Password: " + "<span>" + _this17.shared.customerData.password + "</span>" + " </li></ul><p>You can login using this Email and Password.<br>You can change your password in Menu -> My Account</p>", "Account Information"); //  $ionicSideMenuDelegate.toggleLeft();


              _this17.dismiss();
            } else if (data.success == 2) {
              //  alert("login with alreday");
              _this17.dismiss();

              _this17.shared.login(data.data[0]);
            }
          }, function (error) {
            _this17.loading.hide();

            _this17.shared.showAlert("error " + JSON.stringify(error)); // console.log("error " + JSON.stringify(error));

          });
        }
      }, {
        key: "dismiss",
        //close modal
        value: function dismiss() {
          this.modalCtrl.dismiss();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "guestLogin",
        value: function guestLogin() {
          this.shared.orderDetails.guest_status = 1;
          this.appEventsService.publish('openShippingAddressPage', "");
          this.dismiss();
        }
      }]);

      return LoginPage;
    }();

    LoginPage.ctorParameters = function () {
      return [{
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_8__["LoadingService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"]
      }, {
        type: _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_7__["Facebook"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_10__["AppEventsService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"]
      }, {
        type: _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_9__["GooglePlus"]
      }];
    };

    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-login',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./login.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/login/login.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./login.page.scss */
      "./src/app/modals/login/login.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_8__["LoadingService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"], _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_7__["Facebook"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_10__["AppEventsService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"], _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_9__["GooglePlus"]])], LoginPage);
    /***/
  },

  /***/
  "./src/app/modals/privacy-policy/privacy-policy.module.ts":
  /*!****************************************************************!*\
    !*** ./src/app/modals/privacy-policy/privacy-policy.module.ts ***!
    \****************************************************************/

  /*! exports provided: PrivacyPolicyPageModule */

  /***/
  function srcAppModalsPrivacyPolicyPrivacyPolicyModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PrivacyPolicyPageModule", function () {
      return PrivacyPolicyPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _privacy_policy_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./privacy-policy.page */
    "./src/app/modals/privacy-policy/privacy-policy.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _privacy_policy_page__WEBPACK_IMPORTED_MODULE_6__["PrivacyPolicyPage"]
    }];

    var PrivacyPolicyPageModule = function PrivacyPolicyPageModule() {
      _classCallCheck(this, PrivacyPolicyPageModule);
    };

    PrivacyPolicyPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]],
      declarations: [_privacy_policy_page__WEBPACK_IMPORTED_MODULE_6__["PrivacyPolicyPage"]],
      entryComponents: [_privacy_policy_page__WEBPACK_IMPORTED_MODULE_6__["PrivacyPolicyPage"]]
    })], PrivacyPolicyPageModule);
    /***/
  },

  /***/
  "./src/app/modals/privacy-policy/privacy-policy.page.scss":
  /*!****************************************************************!*\
    !*** ./src/app/modals/privacy-policy/privacy-policy.page.scss ***!
    \****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppModalsPrivacyPolicyPrivacyPolicyPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content ion-text p {\n  padding-left: 10px;\n  padding-right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9tb2RhbHMvcHJpdmFjeS1wb2xpY3kvcHJpdmFjeS1wb2xpY3kucGFnZS5zY3NzIiwic3JjL2FwcC9tb2RhbHMvcHJpdmFjeS1wb2xpY3kvcHJpdmFjeS1wb2xpY3kucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdJO0VBQ0Usa0JBQUE7RUFDQSxtQkFBQTtBQ0ZOIiwiZmlsZSI6InNyYy9hcHAvbW9kYWxzL3ByaXZhY3ktcG9saWN5L3ByaXZhY3ktcG9saWN5LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5pb24tY29udGVudCB7XHJcbiAgaW9uLXRleHQge1xyXG4gICAgcCB7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiaW9uLWNvbnRlbnQgaW9uLXRleHQgcCB7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/modals/privacy-policy/privacy-policy.page.ts":
  /*!**************************************************************!*\
    !*** ./src/app/modals/privacy-policy/privacy-policy.page.ts ***!
    \**************************************************************/

  /*! exports provided: PrivacyPolicyPage */

  /***/
  function srcAppModalsPrivacyPolicyPrivacyPolicyPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PrivacyPolicyPage", function () {
      return PrivacyPolicyPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");

    var PrivacyPolicyPage = /*#__PURE__*/function () {
      function PrivacyPolicyPage(mdCtrl, shared) {
        _classCallCheck(this, PrivacyPolicyPage);

        this.mdCtrl = mdCtrl;
        this.shared = shared;
        this.shared.currentOpenedModel = this;
      }

      _createClass(PrivacyPolicyPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {} // For Modal Dismiss

      }, {
        key: "dismiss",
        value: function dismiss() {
          this.shared.currentOpenedModel = null;
          this.mdCtrl.dismiss();
        }
      }]);

      return PrivacyPolicyPage;
    }();

    PrivacyPolicyPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]
      }];
    };

    PrivacyPolicyPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-privacy-policy',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./privacy-policy.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/privacy-policy/privacy-policy.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./privacy-policy.page.scss */
      "./src/app/modals/privacy-policy/privacy-policy.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]])], PrivacyPolicyPage);
    /***/
  },

  /***/
  "./src/app/modals/refund-policy/refund-policy.module.ts":
  /*!**************************************************************!*\
    !*** ./src/app/modals/refund-policy/refund-policy.module.ts ***!
    \**************************************************************/

  /*! exports provided: RefundPolicyPageModule */

  /***/
  function srcAppModalsRefundPolicyRefundPolicyModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RefundPolicyPageModule", function () {
      return RefundPolicyPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _refund_policy_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./refund-policy.page */
    "./src/app/modals/refund-policy/refund-policy.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _refund_policy_page__WEBPACK_IMPORTED_MODULE_6__["RefundPolicyPage"]
    }];

    var RefundPolicyPageModule = function RefundPolicyPageModule() {
      _classCallCheck(this, RefundPolicyPageModule);
    };

    RefundPolicyPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]],
      declarations: [_refund_policy_page__WEBPACK_IMPORTED_MODULE_6__["RefundPolicyPage"]],
      entryComponents: [_refund_policy_page__WEBPACK_IMPORTED_MODULE_6__["RefundPolicyPage"]]
    })], RefundPolicyPageModule);
    /***/
  },

  /***/
  "./src/app/modals/refund-policy/refund-policy.page.scss":
  /*!**************************************************************!*\
    !*** ./src/app/modals/refund-policy/refund-policy.page.scss ***!
    \**************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppModalsRefundPolicyRefundPolicyPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content ion-text p {\n  padding-left: 10px;\n  padding-right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9tb2RhbHMvcmVmdW5kLXBvbGljeS9yZWZ1bmQtcG9saWN5LnBhZ2Uuc2NzcyIsInNyYy9hcHAvbW9kYWxzL3JlZnVuZC1wb2xpY3kvcmVmdW5kLXBvbGljeS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBR0k7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0FDRk4iLCJmaWxlIjoic3JjL2FwcC9tb2RhbHMvcmVmdW5kLXBvbGljeS9yZWZ1bmQtcG9saWN5LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5pb24tY29udGVudCB7XHJcbiAgaW9uLXRleHQge1xyXG4gICAgcCB7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiaW9uLWNvbnRlbnQgaW9uLXRleHQgcCB7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/modals/refund-policy/refund-policy.page.ts":
  /*!************************************************************!*\
    !*** ./src/app/modals/refund-policy/refund-policy.page.ts ***!
    \************************************************************/

  /*! exports provided: RefundPolicyPage */

  /***/
  function srcAppModalsRefundPolicyRefundPolicyPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RefundPolicyPage", function () {
      return RefundPolicyPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");

    var RefundPolicyPage = /*#__PURE__*/function () {
      function RefundPolicyPage(mdCtrl, shared) {
        _classCallCheck(this, RefundPolicyPage);

        this.mdCtrl = mdCtrl;
        this.shared = shared;
        this.shared.currentOpenedModel = this;
      }

      _createClass(RefundPolicyPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {} // For Modal Dismiss

      }, {
        key: "dismiss",
        value: function dismiss() {
          this.shared.currentOpenedModel = null;
          this.mdCtrl.dismiss();
        }
      }]);

      return RefundPolicyPage;
    }();

    RefundPolicyPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]
      }];
    };

    RefundPolicyPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-refund-policy',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./refund-policy.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/refund-policy/refund-policy.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./refund-policy.page.scss */
      "./src/app/modals/refund-policy/refund-policy.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]])], RefundPolicyPage);
    /***/
  },

  /***/
  "./src/app/modals/select-country/select-country.module.ts":
  /*!****************************************************************!*\
    !*** ./src/app/modals/select-country/select-country.module.ts ***!
    \****************************************************************/

  /*! exports provided: SelectCountryPageModule */

  /***/
  function srcAppModalsSelectCountrySelectCountryModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SelectCountryPageModule", function () {
      return SelectCountryPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _select_country_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./select-country.page */
    "./src/app/modals/select-country/select-country.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _select_country_page__WEBPACK_IMPORTED_MODULE_6__["SelectCountryPage"]
    }];

    var SelectCountryPageModule = function SelectCountryPageModule() {
      _classCallCheck(this, SelectCountryPageModule);
    };

    SelectCountryPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_select_country_page__WEBPACK_IMPORTED_MODULE_6__["SelectCountryPage"]]
    })], SelectCountryPageModule);
    /***/
  },

  /***/
  "./src/app/modals/select-country/select-country.page.scss":
  /*!****************************************************************!*\
    !*** ./src/app/modals/select-country/select-country.page.scss ***!
    \****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppModalsSelectCountrySelectCountryPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZGFscy9zZWxlY3QtY291bnRyeS9zZWxlY3QtY291bnRyeS5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/modals/select-country/select-country.page.ts":
  /*!**************************************************************!*\
    !*** ./src/app/modals/select-country/select-country.page.ts ***!
    \**************************************************************/

  /*! exports provided: SelectCountryPage */

  /***/
  function srcAppModalsSelectCountrySelectCountryPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SelectCountryPage", function () {
      return SelectCountryPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/providers/app-events/app-events.service */
    "./src/providers/app-events/app-events.service.ts");

    var SelectCountryPage = /*#__PURE__*/function () {
      function SelectCountryPage(http, appEventsService, config, modalCtrl, loading, shared, navParams) {
        var _this18 = this;

        _classCallCheck(this, SelectCountryPage);

        this.http = http;
        this.appEventsService = appEventsService;
        this.config = config;
        this.modalCtrl = modalCtrl;
        this.loading = loading;
        this.shared = shared;
        this.navParams = navParams;
        this.searchQuery = '';
        this.countries = new Array();
        this.shared.currentOpenedModel = this;
        loading.show();
        var dat = {
          type: 'null'
        };
        config.postHttp('getcountries', dat).then(function (data) {
          loading.hide();
          _this18.items = _this18.countries = data.data;
          setTimeout(function () {
            _this18.searchBar.setFocus();
          }, 250);
        });
      }

      _createClass(SelectCountryPage, [{
        key: "initializeItems",
        value: function initializeItems() {
          this.items = this.countries;
        }
      }, {
        key: "getItems",
        value: function getItems(ev) {
          // Reset items back to all of the items
          this.initializeItems(); // set val to the value of the searchbar

          var val = ev.target.value; // if the value is an empty string don't filter the items

          if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
              return item.countries_name.toLowerCase().indexOf(val.toLowerCase()) > -1;
            });
          }
        } //close modal

      }, {
        key: "dismiss",
        value: function dismiss() {
          this.modalCtrl.dismiss();
          this.shared.currentOpenedModel = null;
        }
      }, {
        key: "selectCountry",
        value: function selectCountry(c) {
          if (this.navParams.get('page') == 'shipping') {
            this.shared.orderDetails.delivery_country = c.countries_name;
            this.shared.orderDetails.delivery_country_code = c.countries_id;
            this.shared.orderDetails.delivery_country_id = c.countries_id;
            this.shared.orderDetails.delivery_zone = null;
            this.shared.orderDetails.delivery_state = null;
          } else if (this.navParams.get('page') == 'editShipping') {
            this.shared.tempdata.entry_country_id = c.countries_id;
            this.shared.tempdata.entry_country_name = c.countries_name;
            this.shared.tempdata.entry_country_code = c.countries_id;
            this.shared.tempdata.entry_zone = null;
          } else {
            this.shared.orderDetails.billing_country = c.countries_name;
            this.shared.orderDetails.billing_country_code = c.countries_id;
            this.shared.orderDetails.billing_country_id = c.countries_id;
            this.shared.orderDetails.billing_zone = null;
            this.shared.orderDetails.billing_state = null;
          }

          this.dismiss();
          console.log(this.navParams.get('page'));
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return SelectCountryPage;
    }();

    SelectCountryPage.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]
      }, {
        type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_7__["AppEventsService"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('Searchbar', {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonSearchbar"])], SelectCountryPage.prototype, "searchBar", void 0);
    SelectCountryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-select-country',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./select-country.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/select-country/select-country.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./select-country.page.scss */
      "./src/app/modals/select-country/select-country.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_7__["AppEventsService"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"]])], SelectCountryPage);
    /***/
  },

  /***/
  "./src/app/modals/select-zones/select-zones.module.ts":
  /*!************************************************************!*\
    !*** ./src/app/modals/select-zones/select-zones.module.ts ***!
    \************************************************************/

  /*! exports provided: SelectZonesPageModule */

  /***/
  function srcAppModalsSelectZonesSelectZonesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SelectZonesPageModule", function () {
      return SelectZonesPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _select_zones_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./select-zones.page */
    "./src/app/modals/select-zones/select-zones.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _select_zones_page__WEBPACK_IMPORTED_MODULE_6__["SelectZonesPage"]
    }];

    var SelectZonesPageModule = function SelectZonesPageModule() {
      _classCallCheck(this, SelectZonesPageModule);
    };

    SelectZonesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_select_zones_page__WEBPACK_IMPORTED_MODULE_6__["SelectZonesPage"]]
    })], SelectZonesPageModule);
    /***/
  },

  /***/
  "./src/app/modals/select-zones/select-zones.page.scss":
  /*!************************************************************!*\
    !*** ./src/app/modals/select-zones/select-zones.page.scss ***!
    \************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppModalsSelectZonesSelectZonesPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZGFscy9zZWxlY3Qtem9uZXMvc2VsZWN0LXpvbmVzLnBhZ2Uuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/modals/select-zones/select-zones.page.ts":
  /*!**********************************************************!*\
    !*** ./src/app/modals/select-zones/select-zones.page.ts ***!
    \**********************************************************/

  /*! exports provided: SelectZonesPage */

  /***/
  function srcAppModalsSelectZonesSelectZonesPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SelectZonesPage", function () {
      return SelectZonesPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/providers/app-events/app-events.service */
    "./src/providers/app-events/app-events.service.ts");

    var SelectZonesPage = /*#__PURE__*/function () {
      function SelectZonesPage(navCtrl, navParams, http, appEventsService, config, modalCtrl, loading, shared) {
        var _this19 = this;

        _classCallCheck(this, SelectZonesPage);

        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.appEventsService = appEventsService;
        this.config = config;
        this.modalCtrl = modalCtrl;
        this.loading = loading;
        this.shared = shared;
        this.searchQuery = '';
        this.zones = new Array();
        this.shared.currentOpenedModel = this;
        loading.show();
        var dat = {
          zone_country_id: this.navParams.get('id')
        };
        config.postHttp('getzones', dat).then(function (data) {
          loading.hide();
          _this19.items = _this19.zones = data.data;
        });
      }

      _createClass(SelectZonesPage, [{
        key: "initializeItems",
        value: function initializeItems() {
          this.items = this.zones;
        }
      }, {
        key: "getItems",
        value: function getItems(ev) {
          // Reset items back to all of the items
          this.initializeItems(); // set val to the value of the searchbar

          var val = ev.target.value; // if the value is an empty string don't filter the items

          if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
              return item.zone_name.toLowerCase().indexOf(val.toLowerCase()) > -1;
            });
          }
        } //close modal

      }, {
        key: "dismiss",
        value: function dismiss() {
          this.modalCtrl.dismiss();
          this.shared.currentOpenedModel = null;
        }
      }, {
        key: "selectZone",
        value: function selectZone(c) {
          if (this.navParams.get('page') == 'shipping') {
            if (c == 'other') {
              //  console.log(c);
              this.shared.orderDetails.delivery_zone = 'other';
              this.shared.orderDetails.delivery_state = 'other';
              this.shared.orderDetails.tax_zone_id = null;
            } else {
              this.shared.orderDetails.delivery_zone = c.zone_name;
              this.shared.orderDetails.delivery_state = c.zone_name;
              this.shared.orderDetails.tax_zone_id = c.zone_id;
            }
          } else if (this.navParams.get('page') == 'editShipping') {
            if (c == 'other') {
              this.shared.tempdata.entry_zone = 'other';
              this.shared.tempdata.entry_zone_id = 0;
            } else {
              this.shared.tempdata.entry_zone = c.zone_name;
              this.shared.tempdata.entry_zone_id = c.zone_id;
            }
          } else {
            if (c == 'other') {
              this.shared.orderDetails.billing_zone = 'other';
              this.shared.orderDetails.billing_state = 'other';
            } else {
              this.shared.orderDetails.billing_zone = c.zone_name;
              this.shared.orderDetails.billing_state = c.zone_name;
            }
          }

          this.dismiss();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return SelectZonesPage;
    }();

    SelectZonesPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]
      }, {
        type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_7__["AppEventsService"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_6__["SharedDataService"]
      }];
    };

    SelectZonesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-select-zones',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./select-zones.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/select-zones/select-zones.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./select-zones.page.scss */
      "./src/app/modals/select-zones/select-zones.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"], src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_7__["AppEventsService"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_6__["SharedDataService"]])], SelectZonesPage);
    /***/
  },

  /***/
  "./src/app/modals/sign-up/sign-up.module.ts":
  /*!**************************************************!*\
    !*** ./src/app/modals/sign-up/sign-up.module.ts ***!
    \**************************************************/

  /*! exports provided: SignUpPageModule */

  /***/
  function srcAppModalsSignUpSignUpModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SignUpPageModule", function () {
      return SignUpPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _sign_up_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./sign-up.page */
    "./src/app/modals/sign-up/sign-up.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _sign_up_page__WEBPACK_IMPORTED_MODULE_6__["SignUpPage"]
    }];

    var SignUpPageModule = function SignUpPageModule() {
      _classCallCheck(this, SignUpPageModule);
    };

    SignUpPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_sign_up_page__WEBPACK_IMPORTED_MODULE_6__["SignUpPage"]],
      entryComponents: [_sign_up_page__WEBPACK_IMPORTED_MODULE_6__["SignUpPage"]]
    })], SignUpPageModule);
    /***/
  },

  /***/
  "./src/app/modals/sign-up/sign-up.page.scss":
  /*!**************************************************!*\
    !*** ./src/app/modals/sign-up/sign-up.page.scss ***!
    \**************************************************/

  /*! exports provided: default */

  /***/
  function srcAppModalsSignUpSignUpPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content ion-avatar {\n  text-align: center;\n  margin-left: auto;\n  margin-right: auto;\n  height: 100px;\n  width: 100px;\n}\nion-content form ion-item {\n  --padding-start: 0;\n  --background: var(--ion-background-color);\n}\nion-content form ion-item ion-label {\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n}\nion-content form ion-item:nth-child(6) {\n  height: 33px;\n}\nion-content form ion-item:nth-child(6) ion-label {\n  color: red;\n}\nion-content form p {\n  font-size: 14px;\n}\nion-content form ion-button {\n  text-transform: uppercase;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9tb2RhbHMvc2lnbi11cC9zaWduLXVwLnBhZ2Uuc2NzcyIsInNyYy9hcHAvbW9kYWxzL3NpZ24tdXAvc2lnbi11cC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUU7RUFDRSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQ0RKO0FETUk7RUFDRSxrQkFBQTtFQUNBLHlDQUFBO0FDSk47QURLTTtFQUNFLDJDQUFBO0FDSFI7QURLTTtFQUNFLFlBQUE7QUNIUjtBRElRO0VBQ0UsVUFBQTtBQ0ZWO0FETUk7RUFDRSxlQUFBO0FDSk47QURNSTtFQUNFLHlCQUFBO0FDSk4iLCJmaWxlIjoic3JjL2FwcC9tb2RhbHMvc2lnbi11cC9zaWduLXVwLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICAvLyBGb3IgaW1hZ2UgZGVzaWduXHJcbiAgaW9uLWF2YXRhciB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgIGhlaWdodDogMTAwcHg7XHJcbiAgICB3aWR0aDogMTAwcHg7XHJcbiAgfVxyXG5cclxuICAvLyBGb3IgRm9ybVxyXG4gIGZvcm0ge1xyXG4gICAgaW9uLWl0ZW0ge1xyXG4gICAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XHJcbiAgICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IpO1xyXG4gICAgICBpb24tbGFiZWwge1xyXG4gICAgICAgIGNvbG9yOiByZ2JhKHZhcigtLWlvbi10ZXh0LWNvbG9yLXJnYiksIDAuNSk7XHJcbiAgICAgIH1cclxuICAgICAgJjpudGgtY2hpbGQoNikge1xyXG4gICAgICAgIGhlaWdodDogMzNweDtcclxuICAgICAgICBpb24tbGFiZWwge1xyXG4gICAgICAgICAgY29sb3I6IHJlZDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHAge1xyXG4gICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICB9XHJcbiAgICBpb24tYnV0dG9uIHtcclxuICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiaW9uLWNvbnRlbnQgaW9uLWF2YXRhciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgaGVpZ2h0OiAxMDBweDtcbiAgd2lkdGg6IDEwMHB4O1xufVxuaW9uLWNvbnRlbnQgZm9ybSBpb24taXRlbSB7XG4gIC0tcGFkZGluZy1zdGFydDogMDtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XG59XG5pb24tY29udGVudCBmb3JtIGlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gIGNvbG9yOiByZ2JhKHZhcigtLWlvbi10ZXh0LWNvbG9yLXJnYiksIDAuNSk7XG59XG5pb24tY29udGVudCBmb3JtIGlvbi1pdGVtOm50aC1jaGlsZCg2KSB7XG4gIGhlaWdodDogMzNweDtcbn1cbmlvbi1jb250ZW50IGZvcm0gaW9uLWl0ZW06bnRoLWNoaWxkKDYpIGlvbi1sYWJlbCB7XG4gIGNvbG9yOiByZWQ7XG59XG5pb24tY29udGVudCBmb3JtIHAge1xuICBmb250LXNpemU6IDE0cHg7XG59XG5pb24tY29udGVudCBmb3JtIGlvbi1idXR0b24ge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/modals/sign-up/sign-up.page.ts":
  /*!************************************************!*\
    !*** ./src/app/modals/sign-up/sign-up.page.ts ***!
    \************************************************/

  /*! exports provided: SignUpPage */

  /***/
  function srcAppModalsSignUpSignUpPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SignUpPage", function () {
      return SignUpPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _term_services_term_services_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../term-services/term-services.page */
    "./src/app/modals/term-services/term-services.page.ts");
    /* harmony import */


    var _refund_policy_refund_policy_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../refund-policy/refund-policy.page */
    "./src/app/modals/refund-policy/refund-policy.page.ts");
    /* harmony import */


    var _privacy_policy_privacy_policy_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../privacy-policy/privacy-policy.page */
    "./src/app/modals/privacy-policy/privacy-policy.page.ts");

    var SignUpPage = /*#__PURE__*/function () {
      function SignUpPage(http, config, modalCtrl, loading, shared, platform) {
        _classCallCheck(this, SignUpPage);

        this.http = http;
        this.config = config;
        this.modalCtrl = modalCtrl;
        this.loading = loading;
        this.shared = shared;
        this.platform = platform;
        this.formData = {
          customers_firstname: '',
          customers_lastname: '',
          email: '',
          password: '',
          customers_telephone: ''
        };
        this.image = "";
        this.errorMessage = '';
        this.shared.currentOpenedModel = this;
      }

      _createClass(SignUpPage, [{
        key: "registerUser",
        value: function registerUser() {
          var _this20 = this;

          this.errorMessage = '';
          this.loading.show();
          this.config.postHttp('processregistration', this.formData).then(function (data) {
            _this20.loading.hide();

            if (data.success == 1) {
              _this20.shared.login(data.data[0]);

              _this20.dismiss();
            }

            if (data.success == 0) {
              _this20.errorMessage = data.message;
            }
          });
        }
      }, {
        key: "openPrivacyPolicyPage",
        value: function openPrivacyPolicyPage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
            var modal;
            return regeneratorRuntime.wrap(function _callee11$(_context11) {
              while (1) {
                switch (_context11.prev = _context11.next) {
                  case 0:
                    _context11.next = 2;
                    return this.modalCtrl.create({
                      component: _privacy_policy_privacy_policy_page__WEBPACK_IMPORTED_MODULE_9__["PrivacyPolicyPage"]
                    });

                  case 2:
                    modal = _context11.sent;
                    _context11.next = 5;
                    return modal.present();

                  case 5:
                    return _context11.abrupt("return", _context11.sent);

                  case 6:
                  case "end":
                    return _context11.stop();
                }
              }
            }, _callee11, this);
          }));
        }
      }, {
        key: "openTermServicesPage",
        value: function openTermServicesPage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
            var modal;
            return regeneratorRuntime.wrap(function _callee12$(_context12) {
              while (1) {
                switch (_context12.prev = _context12.next) {
                  case 0:
                    _context12.next = 2;
                    return this.modalCtrl.create({
                      component: _term_services_term_services_page__WEBPACK_IMPORTED_MODULE_7__["TermServicesPage"]
                    });

                  case 2:
                    modal = _context12.sent;
                    _context12.next = 5;
                    return modal.present();

                  case 5:
                    return _context12.abrupt("return", _context12.sent);

                  case 6:
                  case "end":
                    return _context12.stop();
                }
              }
            }, _callee12, this);
          }));
        }
      }, {
        key: "openRefundPolicyPage",
        value: function openRefundPolicyPage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee13() {
            var modal;
            return regeneratorRuntime.wrap(function _callee13$(_context13) {
              while (1) {
                switch (_context13.prev = _context13.next) {
                  case 0:
                    _context13.next = 2;
                    return this.modalCtrl.create({
                      component: _refund_policy_refund_policy_page__WEBPACK_IMPORTED_MODULE_8__["RefundPolicyPage"]
                    });

                  case 2:
                    modal = _context13.sent;
                    _context13.next = 5;
                    return modal.present();

                  case 5:
                    return _context13.abrupt("return", _context13.sent);

                  case 6:
                  case "end":
                    return _context13.stop();
                }
              }
            }, _callee13, this);
          }));
        }
      }, {
        key: "dismiss",
        value: function dismiss() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee14() {
            return regeneratorRuntime.wrap(function _callee14$(_context14) {
              while (1) {
                switch (_context14.prev = _context14.next) {
                  case 0:
                    this.modalCtrl.dismiss();

                  case 1:
                  case "end":
                    return _context14.stop();
                }
              }
            }, _callee14, this);
          }));
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return SignUpPage;
    }();

    SignUpPage.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_6__["SharedDataService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"]
      }];
    };

    SignUpPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-sign-up',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./sign-up.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/sign-up/sign-up.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./sign-up.page.scss */
      "./src/app/modals/sign-up/sign-up.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_6__["SharedDataService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"]])], SignUpPage);
    /***/
  },

  /***/
  "./src/app/modals/term-services/term-services.module.ts":
  /*!**************************************************************!*\
    !*** ./src/app/modals/term-services/term-services.module.ts ***!
    \**************************************************************/

  /*! exports provided: TermServicesPageModule */

  /***/
  function srcAppModalsTermServicesTermServicesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TermServicesPageModule", function () {
      return TermServicesPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _term_services_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./term-services.page */
    "./src/app/modals/term-services/term-services.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _term_services_page__WEBPACK_IMPORTED_MODULE_6__["TermServicesPage"]
    }];

    var TermServicesPageModule = function TermServicesPageModule() {
      _classCallCheck(this, TermServicesPageModule);
    };

    TermServicesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]],
      declarations: [_term_services_page__WEBPACK_IMPORTED_MODULE_6__["TermServicesPage"]],
      entryComponents: [_term_services_page__WEBPACK_IMPORTED_MODULE_6__["TermServicesPage"]]
    })], TermServicesPageModule);
    /***/
  },

  /***/
  "./src/app/modals/term-services/term-services.page.scss":
  /*!**************************************************************!*\
    !*** ./src/app/modals/term-services/term-services.page.scss ***!
    \**************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppModalsTermServicesTermServicesPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content ion-text p {\n  padding-left: 10px;\n  padding-right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9tb2RhbHMvdGVybS1zZXJ2aWNlcy90ZXJtLXNlcnZpY2VzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvbW9kYWxzL3Rlcm0tc2VydmljZXMvdGVybS1zZXJ2aWNlcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBR1E7RUFDSSxrQkFBQTtFQUNBLG1CQUFBO0FDRloiLCJmaWxlIjoic3JjL2FwcC9tb2RhbHMvdGVybS1zZXJ2aWNlcy90ZXJtLXNlcnZpY2VzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5pb24tY29udGVudHtcclxuICAgIGlvbi10ZXh0e1xyXG4gICAgICAgIHB7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCJpb24tY29udGVudCBpb24tdGV4dCBwIHtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/modals/term-services/term-services.page.ts":
  /*!************************************************************!*\
    !*** ./src/app/modals/term-services/term-services.page.ts ***!
    \************************************************************/

  /*! exports provided: TermServicesPage */

  /***/
  function srcAppModalsTermServicesTermServicesPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TermServicesPage", function () {
      return TermServicesPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");

    var TermServicesPage = /*#__PURE__*/function () {
      function TermServicesPage(mdCtrl, shared) {
        _classCallCheck(this, TermServicesPage);

        this.mdCtrl = mdCtrl;
        this.shared = shared;
        this.shared.currentOpenedModel = this;
      }

      _createClass(TermServicesPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {} // For Modal Dismiss

      }, {
        key: "dismiss",
        value: function dismiss() {
          this.shared.currentOpenedModel = null;
          this.mdCtrl.dismiss();
        }
      }]);

      return TermServicesPage;
    }();

    TermServicesPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]
      }];
    };

    TermServicesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-term-services',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./term-services.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/modals/term-services/term-services.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./term-services.page.scss */
      "./src/app/modals/term-services/term-services.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]])], TermServicesPage);
    /***/
  },

  /***/
  "./src/app/product-detail/product-detail.page.scss":
  /*!*********************************************************!*\
    !*** ./src/app/product-detail/product-detail.page.scss ***!
    \*********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppProductDetailProductDetailPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "@charset \"UTF-8\";\n.div-time {\n  display: inline-flex;\n  vertical-align: middle;\n  font-size: 17px;\n}\n.div-time ion-icon {\n  padding: 2px 0;\n}\n.product-detail-page .product-tags-top {\n  position: absolute;\n  top: 10px;\n  left: 0;\n  z-index: 9;\n}\n.product-detail-page .product-tags-top .product-tag-new {\n  color: white;\n  background-color: red;\n  padding: 5px 8px;\n  display: inline-block;\n  margin-bottom: 4px;\n}\n.product-detail-page .product-tags-top .product-tag-featured {\n  background-color: var(--ion-color-secondary);\n  color: white;\n  padding: 5px 8px;\n}\n.product-detail-page .icons {\n  position: absolute;\n  top: 10px;\n  right: 0;\n  background-color: #eee;\n  z-index: 9;\n  direction: ltr;\n}\n.product-detail-page .icons ion-icon {\n  padding-top: 5px;\n  font-size: 22px;\n  padding-left: 5px;\n  padding-right: 5px;\n}\n.product-detail-page .icons ion-icon[name=share] {\n  color: #bed13c;\n}\n.product-detail-page .icons ion-icon[aria-label=heart],\n.product-detail-page .icons .icon[aria-label=\"heart outline\"] {\n  color: var(--ion-color-secondary);\n}\n.product-detail-page .product-slides .swiper-wrapper .swiper-slide {\n  background-color: #ececec;\n}\n.product-detail-page .product-slides img {\n  -webkit-filter: brightness(0.9);\n          filter: brightness(0.9);\n}\n.product-detail-page .product-detail-title {\n  position: relative;\n  padding-bottom: 0;\n}\n.product-detail-page .product-detail-title .product-tags {\n  position: absolute;\n  left: 10px;\n  top: -15px;\n  z-index: 9;\n}\n.product-detail-page .product-detail-title .product-tags .product-tag-off {\n  background-color: var(--ion-color-secondary);\n  color: white;\n  padding: 5px 8px;\n  margin-right: 4px;\n  float: left;\n  font-size: 14px;\n  text-transform: uppercase;\n}\n.product-detail-page .product-detail-title .product-tags .product-tag-featured {\n  background-color: var(--ion-color-secondary);\n  color: white;\n  padding: 5px 8px;\n  float: left;\n}\n.product-detail-page .product-detail-title .price-group {\n  padding-top: 15px;\n  padding-bottom: 0;\n}\n.product-detail-page .product-detail-title .price-group .product-price-normal {\n  padding-right: 5px;\n  color: var(--ion-color-danger);\n}\n.product-detail-page .product-detail-title .price-group .product-price-normal-through {\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n  text-decoration: line-through;\n  padding-right: 5px;\n}\n.product-detail-page .product-detail-title .price-group .product-instock,\n.product-detail-page .product-detail-title .price-group .product-outstock {\n  float: right;\n  color: red;\n  padding-top: 0;\n  font-size: 14px;\n}\n.product-detail-page .product-detail-title .price-group .product-instock {\n  color: var(--ion-text-color);\n}\n.product-detail-page .product-detail-title .price-group .product-outstock {\n  color: var(--ion-text-color);\n}\n.product-detail-page .product-detail-title .product-title {\n  padding-bottom: 0;\n}\n.product-detail-page .product-detail-title .product-title h3 {\n  color: var(--ion-text-color);\n  margin-top: 0;\n  margin-bottom: 0;\n  font-size: 14px;\n}\n.product-detail-page .product-detail-title .product-title h3 small {\n  font-size: 12px;\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n}\n.product-detail-page .product-ratings .stars-outer {\n  display: inline-block;\n  position: relative;\n  font-size: 25px;\n}\n.product-detail-page .product-ratings .stars-outer::before {\n  content: \"☆☆☆☆☆\";\n  color: #ccc;\n}\n.product-detail-page .product-ratings .stars-outer .stars-inner {\n  font-size: 25px;\n  position: absolute;\n  top: 0;\n  left: 0;\n  white-space: nowrap;\n  overflow: hidden;\n}\n.product-detail-page .product-ratings .stars-outer .stars-inner::before {\n  content: \"★★★★★\";\n  color: #f8ce0b;\n}\n.product-detail-page .product-ratings ion-icon {\n  font-size: 28px;\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n}\n.product-detail-page .product-ratings h6 {\n  margin-bottom: 6px;\n  text-align: start;\n  color: var(--ion-text-color);\n  font-size: 14px;\n  margin-top: 6px;\n}\n.product-detail-page .product-detail-header {\n  background-color: var(--ion-color-light);\n  position: relative;\n}\n.product-detail-page .product-detail-header .left,\n.product-detail-page .product-detail-header .right {\n  padding-bottom: 0;\n}\n.product-detail-page .product-detail-header .left .col,\n.product-detail-page .product-detail-header .right .col {\n  display: flex;\n  align-items: center;\n}\n.product-detail-page .product-detail-header .left .col {\n  justify-content: flex-start;\n}\n.product-detail-page .product-detail-header .right .col {\n  justify-content: flex-end;\n}\n.product-detail-page .product-detail-header .qty-name {\n  padding-top: 0;\n  font-size: 12px;\n}\n.product-detail-page .product-detail-header .qty-name,\n.product-detail-page .product-detail-header .ttl-name {\n  padding-left: 0;\n  padding-bottom: 0;\n  text-transform: uppercase;\n  font-size: 12px;\n}\n.product-detail-page .product-detail-header .ttl-vlue {\n  font-weight: bold;\n  font-size: 20px;\n}\n.product-detail-page .product-detail-header .qty-vlue,\n.product-detail-page .product-detail-header .ttl-vlue {\n  padding-left: 0;\n  padding-right: 0;\n}\n.product-detail-page .product-detail-header .total-para {\n  font-size: 12px;\n}\n.product-detail-page .product-detail-header .qty-vlue {\n  padding-left: 0;\n  padding-right: 0;\n}\n.product-detail-page .product-detail-header .qty-vlue ion-button {\n  color: white;\n  border-width: 0;\n  margin: 0;\n  height: 30px;\n}\n.product-detail-page .product-detail-header .qty-vlue .dgi {\n  min-width: 35px;\n  max-width: 50px;\n  background-color: white;\n  height: 30px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  box-shadow: inset 0 0 2px 0 rgba(0, 0, 0, 0.2);\n}\n.product-detail-page .product-detail-header .qty-vlue .dgi input {\n  text-align: center;\n  padding-left: 0px;\n}\n.product-detail-page .product-detail-content h3 {\n  margin-top: 0;\n  font-weight: 400;\n  font-size: 14px;\n  font-weight: 400;\n}\n.product-detail-page .product-detail-content p {\n  margin-bottom: 0;\n}\n.product-detail-page .product-detail-content.bing-fo {\n  background-color: var(ion-color-light);\n}\n.product-detail-page .group-product ion-label {\n  margin-left: 10px;\n}\n.product-detail-page .group-product ion-thumbnail {\n  margin: 0;\n  width: 115px;\n  height: 115px;\n}\n.product-detail-page .group-product ion-item {\n  --padding-start: 0;\n  align-items: flex-start;\n  border-bottom: 1px solid #dedede;\n  background-color: var(--ion-color-dark);\n}\n.product-detail-page .group-product ion-item .item-inner {\n  border-bottom: none;\n}\n.product-detail-page .group-product .item:last-child {\n  border-bottom: none;\n}\n.product-detail-page .group-product h3 {\n  color: var(--ion-color-dark);\n  margin-top: 0;\n  margin-bottom: 5px;\n  font-weight: 400;\n}\n.product-detail-page .group-product .woo-price {\n  float: left;\n  font-size: 12px;\n}\n.product-detail-page .group-product .woo-price del .woocommerce-Price-amount {\n  font-size: 16px;\n  color: var(--ion-color-dark);\n  text-decoration: line-through;\n}\n.product-detail-page .group-product .woo-price ins {\n  text-decoration: none;\n}\n.product-detail-page .group-product .woo-price ins .woocommerce-Price-amount {\n  margin-left: 5px;\n}\n.product-detail-page .group-product .qty-box-total {\n  float: left;\n}\n.product-detail-page .group-product .qty-box-total .left,\n.product-detail-page .group-product .qty-box-total .right {\n  padding-bottom: 0;\n}\n.product-detail-page .group-product .qty-box-total .left .col,\n.product-detail-page .group-product .qty-box-total .right .col {\n  display: flex;\n  align-items: center;\n}\n.product-detail-page .group-product .qty-box-total .left {\n  padding-left: 0;\n}\n.product-detail-page .group-product .qty-box-total .left .col {\n  padding-left: 0;\n  justify-content: flex-start;\n}\n.product-detail-page .group-product .qty-box-total .right {\n  padding-right: 0;\n}\n.product-detail-page .group-product .qty-box-total .right .col {\n  padding-right: 0;\n  justify-content: flex-end;\n}\n.product-detail-page .group-product .qty-box-total .qty-name,\n.product-detail-page .group-product .qty-box-total .ttl-name {\n  font-size: 12px;\n  text-transform: uppercase;\n  padding-bottom: 0;\n  padding-left: 0;\n}\n.product-detail-page .group-product .qty-box-total .ttl-vlue {\n  font-size: 12px;\n  padding-left: 0;\n}\n.product-detail-page .group-product .qty-box-total .qty-vlue {\n  padding-left: 0;\n  padding-right: 0;\n}\n.product-detail-page .group-product .qty-box-total .qty-vlue .button {\n  color: white;\n  border-width: 0;\n  margin: 0;\n  width: 45px;\n}\n.product-detail-page .group-product .qty-box-total .qty-vlue .dgi {\n  min-width: 50px;\n  background-color: white;\n  height: 30px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  box-shadow: inset 0 0 2px 0 rgba(0, 0, 0, 0.2);\n  text-align: center;\n  padding-left: 0;\n  --padding-start: 0;\n  padding-right: 0;\n}\n.product-detail-page .product-detail-content h3 {\n  margin-top: 0;\n}\n.product-detail-page .product-detail-content p {\n  margin-bottom: 0;\n}\n.product-detail-page .product-detail-content.bing-fo {\n  background-color: var(--ion-color-light);\n}\n.product-detail-page .product-description p {\n  font-size: 14px;\n  padding-left: 0px;\n  margin-top: 5px;\n}\n.product-detail-page .product-tags-top {\n  font-size: 14px;\n  position: absolute;\n  top: 10px;\n  left: 0;\n  z-index: 9;\n}\n.product-detail-page .product-tags-top .product-tag-new {\n  color: white;\n  background-color: red;\n  padding: 5px 8px;\n  display: inline-block;\n  margin-bottom: 4px;\n  text-transform: uppercase;\n}\n.product-detail-page .product-tags-top .product-tag-featured {\n  background-color: var(--ion-color-secondary);\n  color: white;\n  padding: 5px 8px;\n  text-transform: uppercase;\n}\n.product-detail-page ion-slides ion-slide:last-child {\n  height: auto;\n}\n.product-detail-page app-product {\n  width: 100%;\n}\n.product-detail-page ion-row ion-col h5 {\n  margin-top: 7px;\n  margin-bottom: 8px;\n}\n.product-detail-page ion-row ion-col ion-icon {\n  zoom: 0.8;\n  border-color: black;\n  border-width: 2px;\n}\n.product-detail-page .select-col {\n  font-size: 12px;\n}\n.product-detail-page .related-item {\n  padding-left: 10px;\n}\n.product-detail-page [dir=rtl] .product-tags-top {\n  right: 0;\n  left: auto;\n}\n.product-detail-page [dir=rtl] .share-like {\n  left: 0;\n  right: auto;\n}\n.product-detail-footer ion-button {\n  width: 100%;\n  margin: 0;\n  --color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjdC1kZXRhaWwvcHJvZHVjdC1kZXRhaWwucGFnZS5zY3NzIiwiL1VzZXJzL3NhdG8vU3R1ZGlvUHJvamVjdHMvZGVsaXZlcnljdXN0b21lci9zcmMvYXBwL3Byb2R1Y3QtZGV0YWlsL3Byb2R1Y3QtZGV0YWlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUNBaEI7RUFJRSxvQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtBRERGO0FDSkU7RUFDRSxjQUFBO0FETUo7QUNDRTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxVQUFBO0FERUo7QUNBSTtFQUNFLFlBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtBREVOO0FDQUk7RUFDRSw0Q0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBREVOO0FDQ0U7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0Esc0JBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtBRENKO0FDQUk7RUFDRSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FERU47QUNBSTtFQUNFLGNBQUE7QURFTjtBQ0FJOztFQUVFLGlDQUFBO0FERU47QUNHTTtFQUNFLHlCQUFBO0FERFI7QUNJSTtFQUNFLCtCQUFBO1VBQUEsdUJBQUE7QURGTjtBQ0tFO0VBQ0Usa0JBQUE7RUFDQSxpQkFBQTtBREhKO0FDS0k7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtBREhOO0FDS007RUFDRSw0Q0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSx5QkFBQTtBREhSO0FDS007RUFDRSw0Q0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QURIUjtBQ09JO0VBQ0UsaUJBQUE7RUFDQSxpQkFBQTtBRExOO0FDT007RUFDRSxrQkFBQTtFQUNBLDhCQUFBO0FETFI7QUNPTTtFQUNFLDJDQUFBO0VBQ0EsNkJBQUE7RUFDQSxrQkFBQTtBRExSO0FDUU07O0VBRUUsWUFBQTtFQUNBLFVBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBRE5SO0FDUU07RUFDRSw0QkFBQTtBRE5SO0FDUU07RUFDRSw0QkFBQTtBRE5SO0FDU0k7RUFDRSxpQkFBQTtBRFBOO0FDUU07RUFDRSw0QkFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUROUjtBQ09RO0VBQ0UsZUFBQTtFQUNBLDJDQUFBO0FETFY7QUNXSTtFQUNFLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FEVE47QUNVTTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtBRFJSO0FDVU07RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QURSUjtBQ1VRO0VBQ0UsZ0JBQUE7RUFDQSxjQUFBO0FEUlY7QUNZSTtFQUNFLGVBQUE7RUFDQSwyQ0FBQTtBRFZOO0FDWUk7RUFDRSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBRFZOO0FDYUU7RUFDRSx3Q0FBQTtFQUNBLGtCQUFBO0FEWEo7QUNZSTs7RUFFRSxpQkFBQTtBRFZOO0FDV007O0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0FEUlI7QUNZTTtFQUNFLDJCQUFBO0FEVlI7QUNjTTtFQUNFLHlCQUFBO0FEWlI7QUNnQkk7RUFDRSxjQUFBO0VBQ0EsZUFBQTtBRGROO0FDZ0JJOztFQUVFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtBRGROO0FDZ0JJO0VBQ0UsaUJBQUE7RUFDQSxlQUFBO0FEZE47QUNnQkk7O0VBRUUsZUFBQTtFQUNBLGdCQUFBO0FEZE47QUNnQkk7RUFDRSxlQUFBO0FEZE47QUNnQkk7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7QURkTjtBQ2VNO0VBQ0UsWUFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtBRGJSO0FDZU07RUFDRSxlQUFBO0VBQ0EsZUFBQTtFQUNBLHVCQUFBO0VBRUEsWUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsOENBQUE7QURkUjtBQ2dCUTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7QURkVjtBQ29CSTtFQUNFLGFBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBRGxCTjtBQ29CSTtFQUNFLGdCQUFBO0FEbEJOO0FDb0JJO0VBQ0Usc0NBQUE7QURsQk47QUNzQkk7RUFDRSxpQkFBQTtBRHBCTjtBQ3NCSTtFQUNFLFNBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtBRHBCTjtBQ3NCSTtFQUNFLGtCQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQ0FBQTtFQUNBLHVDQUFBO0FEcEJOO0FDcUJNO0VBQ0UsbUJBQUE7QURuQlI7QUNzQkk7RUFDRSxtQkFBQTtBRHBCTjtBQ3NCSTtFQUNFLDRCQUFBO0VBRUEsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QURyQk47QUN1Qkk7RUFDRSxXQUFBO0VBQ0EsZUFBQTtBRHJCTjtBQ3VCUTtFQUNFLGVBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0FEckJWO0FDd0JNO0VBQ0UscUJBQUE7QUR0QlI7QUN1QlE7RUFDRSxnQkFBQTtBRHJCVjtBQzBCSTtFQUNFLFdBQUE7QUR4Qk47QUMwQk07O0VBRUUsaUJBQUE7QUR4QlI7QUN5QlE7O0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0FEdEJWO0FDeUJNO0VBQ0UsZUFBQTtBRHZCUjtBQ3dCUTtFQUNFLGVBQUE7RUFDQSwyQkFBQTtBRHRCVjtBQ3lCTTtFQUNFLGdCQUFBO0FEdkJSO0FDd0JRO0VBQ0UsZ0JBQUE7RUFDQSx5QkFBQTtBRHRCVjtBQ3lCTTs7RUFFRSxlQUFBO0VBQ0EseUJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUR2QlI7QUN5Qk07RUFDRSxlQUFBO0VBQ0EsZUFBQTtBRHZCUjtBQ3lCTTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtBRHZCUjtBQ3dCUTtFQUNFLFlBQUE7RUFDQSxlQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7QUR0QlY7QUN3QlE7RUFDRSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSw4Q0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUR0QlY7QUM0Qkk7RUFDRSxhQUFBO0FEMUJOO0FDNkJJO0VBQ0UsZ0JBQUE7QUQzQk47QUM2Qkk7RUFDRSx3Q0FBQTtBRDNCTjtBQytCSTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUQ3Qk47QUNnQ0U7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFVBQUE7QUQ5Qko7QUNnQ0k7RUFDRSxZQUFBO0VBQ0EscUJBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtBRDlCTjtBQ2dDSTtFQUNFLDRDQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7QUQ5Qk47QUNtQ007RUFDRSxZQUFBO0FEakNSO0FDcUNFO0VBQ0UsV0FBQTtBRG5DSjtBQ3dDTTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBRHRDUjtBQ3dDTTtFQUNFLFNBQUE7RUFDQSxtQkFBQTtFQUVBLGlCQUFBO0FEdkNSO0FDMkNFO0VBQ0UsZUFBQTtBRHpDSjtBQzJDRTtFQUNFLGtCQUFBO0FEekNKO0FDNkNJO0VBQ0UsUUFBQTtFQUNBLFVBQUE7QUQzQ047QUM2Q0k7RUFDRSxPQUFBO0VBQ0EsV0FBQTtBRDNDTjtBQ2dERTtFQUNFLFdBQUE7RUFDQSxTQUFBO0VBQ0EsY0FBQTtBRDdDSiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3QtZGV0YWlsL3Byb2R1Y3QtZGV0YWlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbi5kaXYtdGltZSB7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICBmb250LXNpemU6IDE3cHg7XG59XG4uZGl2LXRpbWUgaW9uLWljb24ge1xuICBwYWRkaW5nOiAycHggMDtcbn1cblxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLnByb2R1Y3QtdGFncy10b3Age1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMTBweDtcbiAgbGVmdDogMDtcbiAgei1pbmRleDogOTtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LXRhZ3MtdG9wIC5wcm9kdWN0LXRhZy1uZXcge1xuICBjb2xvcjogd2hpdGU7XG4gIGJhY2tncm91bmQtY29sb3I6IHJlZDtcbiAgcGFkZGluZzogNXB4IDhweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtYXJnaW4tYm90dG9tOiA0cHg7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC10YWdzLXRvcCAucHJvZHVjdC10YWctZmVhdHVyZWQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiA1cHggOHB4O1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLmljb25zIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDEwcHg7XG4gIHJpZ2h0OiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlO1xuICB6LWluZGV4OiA5O1xuICBkaXJlY3Rpb246IGx0cjtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5pY29ucyBpb24taWNvbiB7XG4gIHBhZGRpbmctdG9wOiA1cHg7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5pY29ucyBpb24taWNvbltuYW1lPXNoYXJlXSB7XG4gIGNvbG9yOiAjYmVkMTNjO1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLmljb25zIGlvbi1pY29uW2FyaWEtbGFiZWw9aGVhcnRdLFxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLmljb25zIC5pY29uW2FyaWEtbGFiZWw9XCJoZWFydCBvdXRsaW5lXCJdIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLnByb2R1Y3Qtc2xpZGVzIC5zd2lwZXItd3JhcHBlciAuc3dpcGVyLXNsaWRlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VjZWNlYztcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LXNsaWRlcyBpbWcge1xuICBmaWx0ZXI6IGJyaWdodG5lc3MoMC45KTtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC10aXRsZSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgcGFkZGluZy1ib3R0b206IDA7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC1kZXRhaWwtdGl0bGUgLnByb2R1Y3QtdGFncyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMTBweDtcbiAgdG9wOiAtMTVweDtcbiAgei1pbmRleDogOTtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC10aXRsZSAucHJvZHVjdC10YWdzIC5wcm9kdWN0LXRhZy1vZmYge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiA1cHggOHB4O1xuICBtYXJnaW4tcmlnaHQ6IDRweDtcbiAgZmxvYXQ6IGxlZnQ7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC10aXRsZSAucHJvZHVjdC10YWdzIC5wcm9kdWN0LXRhZy1mZWF0dXJlZCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDVweCA4cHg7XG4gIGZsb2F0OiBsZWZ0O1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLnByb2R1Y3QtZGV0YWlsLXRpdGxlIC5wcmljZS1ncm91cCB7XG4gIHBhZGRpbmctdG9wOiAxNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC10aXRsZSAucHJpY2UtZ3JvdXAgLnByb2R1Y3QtcHJpY2Utbm9ybWFsIHtcbiAgcGFkZGluZy1yaWdodDogNXB4O1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhbmdlcik7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC1kZXRhaWwtdGl0bGUgLnByaWNlLWdyb3VwIC5wcm9kdWN0LXByaWNlLW5vcm1hbC10aHJvdWdoIHtcbiAgY29sb3I6IHJnYmEodmFyKC0taW9uLXRleHQtY29sb3ItcmdiKSwgMC41KTtcbiAgdGV4dC1kZWNvcmF0aW9uOiBsaW5lLXRocm91Z2g7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC10aXRsZSAucHJpY2UtZ3JvdXAgLnByb2R1Y3QtaW5zdG9jayxcbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC10aXRsZSAucHJpY2UtZ3JvdXAgLnByb2R1Y3Qtb3V0c3RvY2sge1xuICBmbG9hdDogcmlnaHQ7XG4gIGNvbG9yOiByZWQ7XG4gIHBhZGRpbmctdG9wOiAwO1xuICBmb250LXNpemU6IDE0cHg7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC1kZXRhaWwtdGl0bGUgLnByaWNlLWdyb3VwIC5wcm9kdWN0LWluc3RvY2sge1xuICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLnByb2R1Y3QtZGV0YWlsLXRpdGxlIC5wcmljZS1ncm91cCAucHJvZHVjdC1vdXRzdG9jayB7XG4gIGNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvcik7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC1kZXRhaWwtdGl0bGUgLnByb2R1Y3QtdGl0bGUge1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC10aXRsZSAucHJvZHVjdC10aXRsZSBoMyB7XG4gIGNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvcik7XG4gIG1hcmdpbi10b3A6IDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC10aXRsZSAucHJvZHVjdC10aXRsZSBoMyBzbWFsbCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6IHJnYmEodmFyKC0taW9uLXRleHQtY29sb3ItcmdiKSwgMC41KTtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LXJhdGluZ3MgLnN0YXJzLW91dGVyIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LXJhdGluZ3MgLnN0YXJzLW91dGVyOjpiZWZvcmUge1xuICBjb250ZW50OiBcIuKYhuKYhuKYhuKYhuKYhlwiO1xuICBjb2xvcjogI2NjYztcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LXJhdGluZ3MgLnN0YXJzLW91dGVyIC5zdGFycy1pbm5lciB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC1yYXRpbmdzIC5zdGFycy1vdXRlciAuc3RhcnMtaW5uZXI6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwi4piF4piF4piF4piF4piFXCI7XG4gIGNvbG9yOiAjZjhjZTBiO1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLnByb2R1Y3QtcmF0aW5ncyBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgY29sb3I6IHJnYmEodmFyKC0taW9uLXRleHQtY29sb3ItcmdiKSwgMC41KTtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LXJhdGluZ3MgaDYge1xuICBtYXJnaW4tYm90dG9tOiA2cHg7XG4gIHRleHQtYWxpZ246IHN0YXJ0O1xuICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi10b3A6IDZweDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC1kZXRhaWwtaGVhZGVyIC5sZWZ0LFxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLnByb2R1Y3QtZGV0YWlsLWhlYWRlciAucmlnaHQge1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC1oZWFkZXIgLmxlZnQgLmNvbCxcbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC1oZWFkZXIgLnJpZ2h0IC5jb2wge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLnByb2R1Y3QtZGV0YWlsLWhlYWRlciAubGVmdCAuY29sIHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLnByb2R1Y3QtZGV0YWlsLWhlYWRlciAucmlnaHQgLmNvbCB7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC1kZXRhaWwtaGVhZGVyIC5xdHktbmFtZSB7XG4gIHBhZGRpbmctdG9wOiAwO1xuICBmb250LXNpemU6IDEycHg7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC1kZXRhaWwtaGVhZGVyIC5xdHktbmFtZSxcbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC1oZWFkZXIgLnR0bC1uYW1lIHtcbiAgcGFkZGluZy1sZWZ0OiAwO1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLnByb2R1Y3QtZGV0YWlsLWhlYWRlciAudHRsLXZsdWUge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLnByb2R1Y3QtZGV0YWlsLWhlYWRlciAucXR5LXZsdWUsXG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC1kZXRhaWwtaGVhZGVyIC50dGwtdmx1ZSB7XG4gIHBhZGRpbmctbGVmdDogMDtcbiAgcGFkZGluZy1yaWdodDogMDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC1oZWFkZXIgLnRvdGFsLXBhcmEge1xuICBmb250LXNpemU6IDEycHg7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC1kZXRhaWwtaGVhZGVyIC5xdHktdmx1ZSB7XG4gIHBhZGRpbmctbGVmdDogMDtcbiAgcGFkZGluZy1yaWdodDogMDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC1oZWFkZXIgLnF0eS12bHVlIGlvbi1idXR0b24ge1xuICBjb2xvcjogd2hpdGU7XG4gIGJvcmRlci13aWR0aDogMDtcbiAgbWFyZ2luOiAwO1xuICBoZWlnaHQ6IDMwcHg7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC1kZXRhaWwtaGVhZGVyIC5xdHktdmx1ZSAuZGdpIHtcbiAgbWluLXdpZHRoOiAzNXB4O1xuICBtYXgtd2lkdGg6IDUwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBoZWlnaHQ6IDMwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3gtc2hhZG93OiBpbnNldCAwIDAgMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLnByb2R1Y3QtZGV0YWlsLWhlYWRlciAucXR5LXZsdWUgLmRnaSBpbnB1dCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC1kZXRhaWwtY29udGVudCBoMyB7XG4gIG1hcmdpbi10b3A6IDA7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC1jb250ZW50IHAge1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLnByb2R1Y3QtZGV0YWlsLWNvbnRlbnQuYmluZy1mbyB7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcihpb24tY29sb3ItbGlnaHQpO1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLmdyb3VwLXByb2R1Y3QgaW9uLWxhYmVsIHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAuZ3JvdXAtcHJvZHVjdCBpb24tdGh1bWJuYWlsIHtcbiAgbWFyZ2luOiAwO1xuICB3aWR0aDogMTE1cHg7XG4gIGhlaWdodDogMTE1cHg7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAuZ3JvdXAtcHJvZHVjdCBpb24taXRlbSB7XG4gIC0tcGFkZGluZy1zdGFydDogMDtcbiAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGVkZWRlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyayk7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAuZ3JvdXAtcHJvZHVjdCBpb24taXRlbSAuaXRlbS1pbm5lciB7XG4gIGJvcmRlci1ib3R0b206IG5vbmU7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAuZ3JvdXAtcHJvZHVjdCAuaXRlbTpsYXN0LWNoaWxkIHtcbiAgYm9yZGVyLWJvdHRvbTogbm9uZTtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5ncm91cC1wcm9kdWN0IGgzIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYXJrKTtcbiAgbWFyZ2luLXRvcDogMDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICBmb250LXdlaWdodDogNDAwO1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLmdyb3VwLXByb2R1Y3QgLndvby1wcmljZSB7XG4gIGZsb2F0OiBsZWZ0O1xuICBmb250LXNpemU6IDEycHg7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAuZ3JvdXAtcHJvZHVjdCAud29vLXByaWNlIGRlbCAud29vY29tbWVyY2UtUHJpY2UtYW1vdW50IHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmspO1xuICB0ZXh0LWRlY29yYXRpb246IGxpbmUtdGhyb3VnaDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5ncm91cC1wcm9kdWN0IC53b28tcHJpY2UgaW5zIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLmdyb3VwLXByb2R1Y3QgLndvby1wcmljZSBpbnMgLndvb2NvbW1lcmNlLVByaWNlLWFtb3VudCB7XG4gIG1hcmdpbi1sZWZ0OiA1cHg7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAuZ3JvdXAtcHJvZHVjdCAucXR5LWJveC10b3RhbCB7XG4gIGZsb2F0OiBsZWZ0O1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLmdyb3VwLXByb2R1Y3QgLnF0eS1ib3gtdG90YWwgLmxlZnQsXG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAuZ3JvdXAtcHJvZHVjdCAucXR5LWJveC10b3RhbCAucmlnaHQge1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5ncm91cC1wcm9kdWN0IC5xdHktYm94LXRvdGFsIC5sZWZ0IC5jb2wsXG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAuZ3JvdXAtcHJvZHVjdCAucXR5LWJveC10b3RhbCAucmlnaHQgLmNvbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAuZ3JvdXAtcHJvZHVjdCAucXR5LWJveC10b3RhbCAubGVmdCB7XG4gIHBhZGRpbmctbGVmdDogMDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5ncm91cC1wcm9kdWN0IC5xdHktYm94LXRvdGFsIC5sZWZ0IC5jb2wge1xuICBwYWRkaW5nLWxlZnQ6IDA7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5ncm91cC1wcm9kdWN0IC5xdHktYm94LXRvdGFsIC5yaWdodCB7XG4gIHBhZGRpbmctcmlnaHQ6IDA7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAuZ3JvdXAtcHJvZHVjdCAucXR5LWJveC10b3RhbCAucmlnaHQgLmNvbCB7XG4gIHBhZGRpbmctcmlnaHQ6IDA7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAuZ3JvdXAtcHJvZHVjdCAucXR5LWJveC10b3RhbCAucXR5LW5hbWUsXG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAuZ3JvdXAtcHJvZHVjdCAucXR5LWJveC10b3RhbCAudHRsLW5hbWUge1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBwYWRkaW5nLWxlZnQ6IDA7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAuZ3JvdXAtcHJvZHVjdCAucXR5LWJveC10b3RhbCAudHRsLXZsdWUge1xuICBmb250LXNpemU6IDEycHg7XG4gIHBhZGRpbmctbGVmdDogMDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5ncm91cC1wcm9kdWN0IC5xdHktYm94LXRvdGFsIC5xdHktdmx1ZSB7XG4gIHBhZGRpbmctbGVmdDogMDtcbiAgcGFkZGluZy1yaWdodDogMDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5ncm91cC1wcm9kdWN0IC5xdHktYm94LXRvdGFsIC5xdHktdmx1ZSAuYnV0dG9uIHtcbiAgY29sb3I6IHdoaXRlO1xuICBib3JkZXItd2lkdGg6IDA7XG4gIG1hcmdpbjogMDtcbiAgd2lkdGg6IDQ1cHg7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAuZ3JvdXAtcHJvZHVjdCAucXR5LWJveC10b3RhbCAucXR5LXZsdWUgLmRnaSB7XG4gIG1pbi13aWR0aDogNTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGhlaWdodDogMzBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJveC1zaGFkb3c6IGluc2V0IDAgMCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMik7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy1sZWZ0OiAwO1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gIHBhZGRpbmctcmlnaHQ6IDA7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC1kZXRhaWwtY29udGVudCBoMyB7XG4gIG1hcmdpbi10b3A6IDA7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC1kZXRhaWwtY29udGVudCBwIHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LWRldGFpbC1jb250ZW50LmJpbmctZm8ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgLnByb2R1Y3QtZGVzY3JpcHRpb24gcCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LXRhZ3MtdG9wIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMTBweDtcbiAgbGVmdDogMDtcbiAgei1pbmRleDogOTtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5wcm9kdWN0LXRhZ3MtdG9wIC5wcm9kdWN0LXRhZy1uZXcge1xuICBjb2xvcjogd2hpdGU7XG4gIGJhY2tncm91bmQtY29sb3I6IHJlZDtcbiAgcGFkZGluZzogNXB4IDhweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtYXJnaW4tYm90dG9tOiA0cHg7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAucHJvZHVjdC10YWdzLXRvcCAucHJvZHVjdC10YWctZmVhdHVyZWQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiA1cHggOHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuLnByb2R1Y3QtZGV0YWlsLXBhZ2UgaW9uLXNsaWRlcyBpb24tc2xpZGU6bGFzdC1jaGlsZCB7XG4gIGhlaWdodDogYXV0bztcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIGFwcC1wcm9kdWN0IHtcbiAgd2lkdGg6IDEwMCU7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSBpb24tcm93IGlvbi1jb2wgaDUge1xuICBtYXJnaW4tdG9wOiA3cHg7XG4gIG1hcmdpbi1ib3R0b206IDhweDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIGlvbi1yb3cgaW9uLWNvbCBpb24taWNvbiB7XG4gIHpvb206IDAuODtcbiAgYm9yZGVyLWNvbG9yOiBibGFjaztcbiAgYm9yZGVyLXdpZHRoOiAycHg7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSAuc2VsZWN0LWNvbCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIC5yZWxhdGVkLWl0ZW0ge1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG59XG4ucHJvZHVjdC1kZXRhaWwtcGFnZSBbZGlyPXJ0bF0gLnByb2R1Y3QtdGFncy10b3Age1xuICByaWdodDogMDtcbiAgbGVmdDogYXV0bztcbn1cbi5wcm9kdWN0LWRldGFpbC1wYWdlIFtkaXI9cnRsXSAuc2hhcmUtbGlrZSB7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiBhdXRvO1xufVxuXG4ucHJvZHVjdC1kZXRhaWwtZm9vdGVyIGlvbi1idXR0b24ge1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luOiAwO1xuICAtLWNvbG9yOiB3aGl0ZTtcbn0iLCIuZGl2LXRpbWUge1xyXG4gIGlvbi1pY29uIHtcclxuICAgIHBhZGRpbmc6IDJweCAwO1xyXG4gIH1cclxuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcclxuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gIGZvbnQtc2l6ZTogMTdweDtcclxufVxyXG4ucHJvZHVjdC1kZXRhaWwtcGFnZSB7XHJcbiAgLnByb2R1Y3QtdGFncy10b3Age1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAxMHB4O1xyXG4gICAgbGVmdDogMDtcclxuICAgIHotaW5kZXg6IDk7XHJcblxyXG4gICAgLnByb2R1Y3QtdGFnLW5ldyB7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xyXG4gICAgICBwYWRkaW5nOiA1cHggOHB4O1xyXG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDRweDtcclxuICAgIH1cclxuICAgIC5wcm9kdWN0LXRhZy1mZWF0dXJlZCB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xyXG4gICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgIHBhZGRpbmc6IDVweCA4cHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5pY29ucyB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDEwcHg7XHJcbiAgICByaWdodDogMDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNlZWU7XHJcbiAgICB6LWluZGV4OiA5O1xyXG4gICAgZGlyZWN0aW9uOiBsdHI7XHJcbiAgICBpb24taWNvbiB7XHJcbiAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcclxuICAgIH1cclxuICAgIGlvbi1pY29uW25hbWU9XCJzaGFyZVwiXSB7XHJcbiAgICAgIGNvbG9yOiAjYmVkMTNjO1xyXG4gICAgfVxyXG4gICAgaW9uLWljb25bYXJpYS1sYWJlbD1cImhlYXJ0XCJdLFxyXG4gICAgLmljb25bYXJpYS1sYWJlbD1cImhlYXJ0IG91dGxpbmVcIl0ge1xyXG4gICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5wcm9kdWN0LXNsaWRlcyB7XHJcbiAgICAuc3dpcGVyLXdyYXBwZXIge1xyXG4gICAgICAuc3dpcGVyLXNsaWRlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjM2LCAyMzYsIDIzNik7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGltZyB7XHJcbiAgICAgIGZpbHRlcjogYnJpZ2h0bmVzcygwLjkpO1xyXG4gICAgfVxyXG4gIH1cclxuICAucHJvZHVjdC1kZXRhaWwtdGl0bGUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDA7XHJcblxyXG4gICAgLnByb2R1Y3QtdGFncyB7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgbGVmdDogMTBweDtcclxuICAgICAgdG9wOiAtMTVweDtcclxuICAgICAgei1pbmRleDogOTtcclxuXHJcbiAgICAgIC5wcm9kdWN0LXRhZy1vZmYge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBwYWRkaW5nOiA1cHggOHB4O1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogNHB4O1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICB9XHJcbiAgICAgIC5wcm9kdWN0LXRhZy1mZWF0dXJlZCB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIHBhZGRpbmc6IDVweCA4cHg7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAucHJpY2UtZ3JvdXAge1xyXG4gICAgICBwYWRkaW5nLXRvcDogMTVweDtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDA7XHJcblxyXG4gICAgICAucHJvZHVjdC1wcmljZS1ub3JtYWwge1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcclxuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhbmdlcik7XHJcbiAgICAgIH1cclxuICAgICAgLnByb2R1Y3QtcHJpY2Utbm9ybWFsLXRocm91Z2gge1xyXG4gICAgICAgIGNvbG9yOiByZ2JhKHZhcigtLWlvbi10ZXh0LWNvbG9yLXJnYiksIDAuNSk7XHJcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBsaW5lLXRocm91Z2g7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogNXB4O1xyXG4gICAgICB9XHJcblxyXG4gICAgICAucHJvZHVjdC1pbnN0b2NrLFxyXG4gICAgICAucHJvZHVjdC1vdXRzdG9jayB7XHJcbiAgICAgICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgICAgIGNvbG9yOiByZWQ7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDA7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5wcm9kdWN0LWluc3RvY2sge1xyXG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvcik7XHJcbiAgICAgIH1cclxuICAgICAgLnByb2R1Y3Qtb3V0c3RvY2sge1xyXG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvcik7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5wcm9kdWN0LXRpdGxlIHtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDA7XHJcbiAgICAgIGgzIHtcclxuICAgICAgICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDA7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgc21hbGwge1xyXG4gICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgY29sb3I6IHJnYmEodmFyKC0taW9uLXRleHQtY29sb3ItcmdiKSwgMC41KTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLnByb2R1Y3QtcmF0aW5ncyB7XHJcbiAgICAuc3RhcnMtb3V0ZXIge1xyXG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgICAmOjpiZWZvcmUge1xyXG4gICAgICAgIGNvbnRlbnQ6IFwiXFwyNjA2XFwyNjA2XFwyNjA2XFwyNjA2XFwyNjA2XCI7XHJcbiAgICAgICAgY29sb3I6ICNjY2M7XHJcbiAgICAgIH1cclxuICAgICAgLnN0YXJzLWlubmVyIHtcclxuICAgICAgICBmb250LXNpemU6IDI1cHg7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHRvcDogMDtcclxuICAgICAgICBsZWZ0OiAwO1xyXG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuXHJcbiAgICAgICAgJjo6YmVmb3JlIHtcclxuICAgICAgICAgIGNvbnRlbnQ6IFwiXFwyNjA1XFwyNjA1XFwyNjA1XFwyNjA1XFwyNjA1XCI7XHJcbiAgICAgICAgICBjb2xvcjogI2Y4Y2UwYjtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgZm9udC1zaXplOiAyOHB4O1xyXG4gICAgICBjb2xvcjogcmdiYSh2YXIoLS1pb24tdGV4dC1jb2xvci1yZ2IpLCAwLjUpO1xyXG4gICAgfVxyXG4gICAgaDYge1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiA2cHg7XHJcbiAgICAgIHRleHQtYWxpZ246IHN0YXJ0O1xyXG4gICAgICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xyXG4gICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgIG1hcmdpbi10b3A6IDZweDtcclxuICAgIH1cclxuICB9XHJcbiAgLnByb2R1Y3QtZGV0YWlsLWhlYWRlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgLmxlZnQsXHJcbiAgICAucmlnaHQge1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMDtcclxuICAgICAgLmNvbCB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAubGVmdCB7XHJcbiAgICAgIC5jb2wge1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLnJpZ2h0IHtcclxuICAgICAgLmNvbCB7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5xdHktbmFtZSB7XHJcbiAgICAgIHBhZGRpbmctdG9wOiAwO1xyXG4gICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICB9XHJcbiAgICAucXR5LW5hbWUsXHJcbiAgICAudHRsLW5hbWUge1xyXG4gICAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAwO1xyXG4gICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICB9XHJcbiAgICAudHRsLXZsdWUge1xyXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgfVxyXG4gICAgLnF0eS12bHVlLFxyXG4gICAgLnR0bC12bHVlIHtcclxuICAgICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiAwO1xyXG4gICAgfVxyXG4gICAgLnRvdGFsLXBhcmEge1xyXG4gICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICB9XHJcbiAgICAucXR5LXZsdWUge1xyXG4gICAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDA7XHJcbiAgICAgIGlvbi1idXR0b24ge1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBib3JkZXItd2lkdGg6IDA7XHJcbiAgICAgICAgbWFyZ2luOiAwO1xyXG4gICAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgfVxyXG4gICAgICAuZGdpIHtcclxuICAgICAgICBtaW4td2lkdGg6IDM1cHg7XHJcbiAgICAgICAgbWF4LXdpZHRoOiA1MHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG5cclxuICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGJveC1zaGFkb3c6IGluc2V0IDAgMCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMik7XHJcblxyXG4gICAgICAgIGlucHV0IHtcclxuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAucHJvZHVjdC1kZXRhaWwtY29udGVudCB7XHJcbiAgICBoMyB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDA7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIH1cclxuICAgIHAge1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgfVxyXG4gICAgJi5iaW5nLWZvIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKGlvbi1jb2xvci1saWdodCk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5ncm91cC1wcm9kdWN0IHtcclxuICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgfVxyXG4gICAgaW9uLXRodW1ibmFpbCB7XHJcbiAgICAgIG1hcmdpbjogMDtcclxuICAgICAgd2lkdGg6IDExNXB4O1xyXG4gICAgICBoZWlnaHQ6IDExNXB4O1xyXG4gICAgfVxyXG4gICAgaW9uLWl0ZW0ge1xyXG4gICAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RlZGVkZTtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmspO1xyXG4gICAgICAuaXRlbS1pbm5lciB7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogbm9uZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLml0ZW06bGFzdC1jaGlsZCB7XHJcbiAgICAgIGJvcmRlci1ib3R0b206IG5vbmU7XHJcbiAgICB9XHJcbiAgICBoMyB7XHJcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyayk7XHJcbiAgICAgIC8vZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICBtYXJnaW4tdG9wOiAwO1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICB9XHJcbiAgICAud29vLXByaWNlIHtcclxuICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgZGVsIHtcclxuICAgICAgICAud29vY29tbWVyY2UtUHJpY2UtYW1vdW50IHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyayk7XHJcbiAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IGxpbmUtdGhyb3VnaDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgaW5zIHtcclxuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgLndvb2NvbW1lcmNlLVByaWNlLWFtb3VudCB7XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5xdHktYm94LXRvdGFsIHtcclxuICAgICAgZmxvYXQ6IGxlZnQ7XHJcblxyXG4gICAgICAubGVmdCxcclxuICAgICAgLnJpZ2h0IHtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMDtcclxuICAgICAgICAuY29sIHtcclxuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAubGVmdCB7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gICAgICAgIC5jb2wge1xyXG4gICAgICAgICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAucmlnaHQge1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDA7XHJcbiAgICAgICAgLmNvbCB7XHJcbiAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwO1xyXG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgLnF0eS1uYW1lLFxyXG4gICAgICAudHRsLW5hbWUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAwO1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogMDtcclxuICAgICAgfVxyXG4gICAgICAudHRsLXZsdWUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICAgIH1cclxuICAgICAgLnF0eS12bHVlIHtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogMDtcclxuICAgICAgICAuYnV0dG9uIHtcclxuICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICAgIGJvcmRlci13aWR0aDogMDtcclxuICAgICAgICAgIG1hcmdpbjogMDtcclxuICAgICAgICAgIHdpZHRoOiA0NXB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuZGdpIHtcclxuICAgICAgICAgIG1pbi13aWR0aDogNTBweDtcclxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgIGJveC1zaGFkb3c6IGluc2V0IDAgMCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMik7XHJcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XHJcbiAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAucHJvZHVjdC1kZXRhaWwtY29udGVudCB7XHJcbiAgICBoMyB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDA7XHJcbiAgICAgIC8vZm9udC1zaXplOiAxN3B4O1xyXG4gICAgfVxyXG4gICAgcCB7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICB9XHJcbiAgICAmLmJpbmctZm8ge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG4gICAgfVxyXG4gIH1cclxuICAucHJvZHVjdC1kZXNjcmlwdGlvbiB7XHJcbiAgICBwIHtcclxuICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAucHJvZHVjdC10YWdzLXRvcCB7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDEwcHg7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgei1pbmRleDogOTtcclxuXHJcbiAgICAucHJvZHVjdC10YWctbmV3IHtcclxuICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XHJcbiAgICAgIHBhZGRpbmc6IDVweCA4cHg7XHJcbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgbWFyZ2luLWJvdHRvbTogNHB4O1xyXG4gICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgfVxyXG4gICAgLnByb2R1Y3QtdGFnLWZlYXR1cmVkIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgcGFkZGluZzogNXB4IDhweDtcclxuICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIH1cclxuICB9XHJcbiAgaW9uLXNsaWRlcyB7XHJcbiAgICBpb24tc2xpZGUge1xyXG4gICAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICBhcHAtcHJvZHVjdCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgaW9uLXJvdyB7XHJcbiAgICAvL21hcmdpbi1yaWdodDogMTBweDtcclxuICAgIGlvbi1jb2wge1xyXG4gICAgICBoNSB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogN3B4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDhweDtcclxuICAgICAgfVxyXG4gICAgICBpb24taWNvbiB7XHJcbiAgICAgICAgem9vbTogMC44O1xyXG4gICAgICAgIGJvcmRlci1jb2xvcjogYmxhY2s7XHJcbiAgICAgICAgLy8gY29sb3I6ICNmZmQ3MDA7XHJcbiAgICAgICAgYm9yZGVyLXdpZHRoOiAycHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLnNlbGVjdC1jb2wge1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gIH1cclxuICAucmVsYXRlZC1pdGVtIHtcclxuICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICB9XHJcblxyXG4gIFtkaXI9XCJydGxcIl0ge1xyXG4gICAgLnByb2R1Y3QtdGFncy10b3Age1xyXG4gICAgICByaWdodDogMDtcclxuICAgICAgbGVmdDogYXV0bztcclxuICAgIH1cclxuICAgIC5zaGFyZS1saWtlIHtcclxuICAgICAgbGVmdDogMDtcclxuICAgICAgcmlnaHQ6IGF1dG87XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbi5wcm9kdWN0LWRldGFpbC1mb290ZXIge1xyXG4gIGlvbi1idXR0b24ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICAtLWNvbG9yOiB3aGl0ZTtcclxuICB9XHJcbn1cclxuIl19 */";
    /***/
  },

  /***/
  "./src/app/product-detail/product-detail.page.ts":
  /*!*******************************************************!*\
    !*** ./src/app/product-detail/product-detail.page.ts ***!
    \*******************************************************/

  /*! exports provided: ProductDetailPage */

  /***/
  function srcAppProductDetailProductDetailPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductDetailPage", function () {
      return ProductDetailPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic-native/in-app-browser/ngx */
    "./node_modules/@ionic-native/in-app-browser/ngx/index.js");
    /* harmony import */


    var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic-native/social-sharing/ngx */
    "./node_modules/@ionic-native/social-sharing/ngx/index.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic-native/photo-viewer/ngx */
    "./node_modules/@ionic-native/photo-viewer/ngx/index.js");
    /* harmony import */


    var _modals_login_login_page__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../modals/login/login.page */
    "./src/app/modals/login/login.page.ts");
    /* harmony import */


    var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! src/providers/app-events/app-events.service */
    "./src/providers/app-events/app-events.service.ts");

    var ProductDetailPage = /*#__PURE__*/function () {
      function ProductDetailPage(navCtrl, config, shared, modalCtrl, loading, iab, appEventsService, storage, photoViewer, socialSharing, activatedRoute, router) {
        var _this22 = this;

        _classCallCheck(this, ProductDetailPage);

        this.navCtrl = navCtrl;
        this.config = config;
        this.shared = shared;
        this.modalCtrl = modalCtrl;
        this.loading = loading;
        this.iab = iab;
        this.appEventsService = appEventsService;
        this.storage = storage;
        this.photoViewer = photoViewer;
        this.socialSharing = socialSharing;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.product = {};
        this.attributes = [];
        this.selectAttribute = true;
        this.cartButton = "addToCart";
        this.is_upcomming = false;
        this.isLiked = 0;
        this.wishArray = [];
        this.disableCartButton = false;
        this.variations = new Array();
        this.groupProducts = new Array();
        this.variationPrice = null;
        this.loaderWcVendorInfo = false;
        this.loaderProductVariations = false;
        this.sliderConfigReleatedItems = {
          slidesPerView: this.config.productSlidesPerPage,
          spaceBetween: 0
        };
        this.sliderConfig = {
          zoom: true
        };
        this.ratingStarsValue = 1; //============================================================================================  
        //function adding attibute into array

        this.fillAttributes = function (val, optionID) {
          var _this21 = this;

          //console.log(val);
          //  console.log(this.attributes);
          this.attributes.forEach(function (value, index) {
            if (optionID == value.products_options_id) {
              value.products_options_values_id = val.id;
              value.options_values_price = val.price;
              value.price_prefix = val.price_prefix;
              value.attribute_id = val.products_attributes_id;
              value.products_options_values = val.value;
              value.name = val.value + ' ' + val.price_prefix + val.price + " " + _this21.config.currency;
            }
          });
          console.log(this.attributes); //calculating total price 

          this.calculatingTotalPrice();
          this.checkAvailability();
        }; //============================================================================================  
        //calculating total price  


        this.calculatingTotalPrice = function () {
          var price = parseFloat(this.product.products_price.toString());
          if (this.product.discount_price != null || this.product.discount_price != undefined) price = this.product.discount_price;
          var totalPrice = this.shared.calculateFinalPriceService(this.attributes) + parseFloat(price.toString());
          if (this.product.discount_price != null || this.product.discount_price != undefined) this.discount_price = totalPrice;else this.product_price = totalPrice; //  console.log(totalPrice);
        };

        this.pId = this.activatedRoute.snapshot.paramMap.get('id');
        this.product = JSON.parse(JSON.stringify(this.getProductData(this.pId)));
        this.product.cartQuantity = 1; // console.log(this.product);

        this.discount_price = this.product.discount_price;
        this.product_price = this.product.products_price;
        this.flash_price = this.product.flash_price;
        if (this.product.discount_price == null) this.current_price = this.product.products_price;
        if (this.product.discount_price != null) this.current_price = this.product.discount_price;
        if (this.product.flash_start_date) this.current_price = this.product.flash_price;
        if (this.product.products_type == 0 && this.product.defaultStock <= 0) this.cartButton = "outOfStock";
        if (this.product.products_type == 1) this.cartButton = "addToCart";
        if (this.product.products_type == 2) this.cartButton = "external";

        if (this.product.attributes != null && this.product.attributes != undefined && this.product.attributes.length != 0) {
          //this.selectAttribute = this.product.attributes[0].values[0];
          // console.log(this.selectAttribute);
          this.product.attributes.forEach(function (value, index) {
            var att = {
              products_options_id: value.option.id,
              products_options: value.option.name,
              products_options_values_id: value.values[0].id,
              options_values_price: value.values[0].price,
              price_prefix: value.values[0].price_prefix,
              products_options_values: value.values[0].value,
              attribute_id: value.values[0].products_attributes_id,
              name: value.values[0].value + ' ' + value.values[0].price_prefix + value.values[0].price + " " + _this22.config.currency
            };
            value.name = value.values[0];

            _this22.attributes.push(att);
          });
          this.checkAvailability();
          console.log(this.attributes);
        }
      } //============================================================================================  


      _createClass(ProductDetailPage, [{
        key: "qunatityPlus",
        value: function qunatityPlus(q) {
          this.product.cartQuantity++;
          if (this.product.products_max_stock == null) return 0;

          if (this.product.cartQuantity > q.products_max_stock) {
            this.product.cartQuantity--;
            this.shared.toast('Product Quantity is Limited!');
          }
        } //============================================================================================  
        //function decreasing the quantity

      }, {
        key: "qunatityMinus",
        value: function qunatityMinus(q) {
          if (this.product.cartQuantity == 1) {
            return 0;
          }

          this.product.cartQuantity--;
        }
      }, {
        key: "quantityChange",
        value: function quantityChange() {
          var _this23 = this;

          if (this.product.products_max_stock == null) {
            console.log("quantity is unlimited");
          } else if (this.product.cartQuantity > this.product.products_max_stock) {
            console.log("quantity is less than stock quantity");
          } else if (this.product.cartQuantity < this.product.products_max_stock) {
            this.shared.translateString("Product Quantity is Limited!").then(function (res) {
              if (_this23.product.cartQuantity == null) _this23.product.cartQuantity = 1;else _this23.product.cartQuantity = parseInt(_this23.product.cartQuantity);

              _this23.shared.showAlert(res);
            });
          }

          if (this.product.cartQuantity == null || this.product.cartQuantity == 0 || this.product.cartQuantity < 0) {
            this.product.cartQuantity = 1;
          }
        }
      }, {
        key: "zoomImage",
        value: function zoomImage(img) {
          this.photoViewer.show(img);
        }
      }, {
        key: "getProductData",
        value: function getProductData(id) {
          var p;
          this.shared.singleProductPageData.forEach(function (element) {
            if (element.products_id == id) {
              p = element;
            }
          });
          return p;
        }
      }, {
        key: "checkAvailability",
        value: function checkAvailability() {
          var _this24 = this;

          this.loading.show();
          var att = [];

          var _iterator2 = _createForOfIteratorHelper(this.attributes),
              _step2;

          try {
            for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
              var a = _step2.value;
              att.push(a.attribute_id.toString());
            }
          } catch (err) {
            _iterator2.e(err);
          } finally {
            _iterator2.f();
          }

          var data = {
            products_id: this.product.products_id.toString(),
            attributes: att
          };
          this.config.postHttp('getquantity', data).then(function (data) {
            _this24.loading.hide();

            if (data.success == 1) {
              if (data.stock > 0) {
                _this24.cartButton = "addToCart";
              } else {
                _this24.cartButton = "outOfStock";

                _this24.shared.toast("Product Not Available With these Attributes!");
              }

              console.log(data.stock);
            }
          }, function (error) {
            _this24.loading.hide();
          });
        }
      }, {
        key: "openProductUrl",
        value: function openProductUrl() {
          this.loading.autoHide(2000);
          this.iab.create(this.product.products_url, "blank");
        }
      }, {
        key: "addToCartProduct",
        value: function addToCartProduct() {
          this.loading.autoHide(500); // console.log(this.product);

          this.shared.addToCart(this.product, this.attributes);
          this.navCtrl.pop();
        }
      }, {
        key: "checkProductNew",
        value: function checkProductNew() {
          var pDate = new Date(this.product.products_date_added);
          var date = pDate.getTime() + this.config.newProductDuration * 86400000;
          var todayDate = new Date().getTime();
          if (date > todayDate) return true;else return false;
        }
      }, {
        key: "pDiscount",
        value: function pDiscount() {
          var rtn = "";
          var p1 = parseInt(this.product.products_price);
          var p2 = parseInt(this.product.discount_price);

          if (p1 == 0 || p2 == null || p2 == undefined || p2 == 0) {
            rtn = "";
          }

          var result = Math.abs((p1 - p2) / p1 * 100);
          result = parseInt(result.toString());

          if (result == 0) {
            rtn = "";
          }

          rtn = result + '%';
          return rtn;
        }
      }, {
        key: "share",
        value: function share() {
          this.loading.autoHide(1000); // Share via email

          this.socialSharing.share(this.product.products_name, this.product.products_name, this.config.url + this.product.products_image, this.config.yourSiteUrl + "/product-detail/" + this.product.products_slug).then(function () {// Success!
          })["catch"](function () {// Error!
          });
        }
      }, {
        key: "clickWishList",
        value: function clickWishList() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee15() {
            var modal;
            return regeneratorRuntime.wrap(function _callee15$(_context15) {
              while (1) {
                switch (_context15.prev = _context15.next) {
                  case 0:
                    if (!(this.shared.customerData.customers_id == null || this.shared.customerData.customers_id == undefined)) {
                      _context15.next = 9;
                      break;
                    }

                    _context15.next = 3;
                    return this.modalCtrl.create({
                      component: _modals_login_login_page__WEBPACK_IMPORTED_MODULE_11__["LoginPage"],
                      componentProps: {
                        'hideGuestLogin': true
                      }
                    });

                  case 3:
                    modal = _context15.sent;
                    _context15.next = 6;
                    return modal.present();

                  case 6:
                    return _context15.abrupt("return", _context15.sent);

                  case 9:
                    if (this.product.isLiked == '0') {
                      this.addWishList();
                    } else this.removeWishList();

                  case 10:
                  case "end":
                    return _context15.stop();
                }
              }
            }, _callee15, this);
          }));
        }
      }, {
        key: "addWishList",
        value: function addWishList() {
          this.shared.addWishList(this.product);
        }
      }, {
        key: "removeWishList",
        value: function removeWishList() {
          this.shared.removeWishList(this.product);
        } //===============================================================================================================================
        // <!-- 2.0 updates -->

      }, {
        key: "openReviewsPage",
        value: function openReviewsPage() {
          this.navCtrl.navigateForward("/reviews/" + this.product.id);
        }
      }, {
        key: "ratingPercentage",
        value: function ratingPercentage() {
          return this.shared.getProductRatingPercentage(this.product.rating);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          if (this.product.flash_start_date) {
            if (this.product.server_time < this.product.flash_start_date) this.is_upcomming = true;
            console.log("server time less than " + (this.product.server_time - this.product.flash_start_date));
          }
        } //===============================================================================================================================
        // getProductReviews() {
        //   this.config.getHttp("getreviews?languages_id=" + this.config.langId + "&products_id=" + this.product.id).then((data: any) => {
        //     this.product.reviewed_customers = data.data
        //   });
        // }

      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {// this.getProductReviews();
        }
      }]);

      return ProductDetailPage;
    }();

    ProductDetailPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"]
      }, {
        type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__["InAppBrowser"]
      }, {
        type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_12__["AppEventsService"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["Storage"]
      }, {
        type: _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_10__["PhotoViewer"]
      }, {
        type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_7__["SocialSharing"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_9__["ActivatedRoute"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_9__["Router"]
      }];
    };

    ProductDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-product-detail',
      encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./product-detail.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/product-detail/product-detail.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./product-detail.page.scss */
      "./src/app/product-detail/product-detail.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"], _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__["InAppBrowser"], src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_12__["AppEventsService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["Storage"], _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_10__["PhotoViewer"], _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_7__["SocialSharing"], _angular_router__WEBPACK_IMPORTED_MODULE_9__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_9__["Router"]])], ProductDetailPage);
    /***/
  },

  /***/
  "./src/components/menu-component/menu-component.component.scss":
  /*!*********************************************************************!*\
    !*** ./src/components/menu-component/menu-component.component.scss ***!
    \*********************************************************************/

  /*! exports provided: default */

  /***/
  function srcComponentsMenuComponentMenuComponentComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".expand-wrapper {\n  transition: max-height 0.4s ease-in-out;\n  overflow: hidden;\n  height: auto;\n  width: 100%;\n}\n\n.collapsed {\n  max-height: 0 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2NvbXBvbmVudHMvbWVudS1jb21wb25lbnQvbWVudS1jb21wb25lbnQuY29tcG9uZW50LnNjc3MiLCJzcmMvY29tcG9uZW50cy9tZW51LWNvbXBvbmVudC9tZW51LWNvbXBvbmVudC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHVDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ0NKOztBREVJO0VBQ0Usd0JBQUE7QUNDTiIsImZpbGUiOiJzcmMvY29tcG9uZW50cy9tZW51LWNvbXBvbmVudC9tZW51LWNvbXBvbmVudC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5leHBhbmQtd3JhcHBlciB7XHJcbiAgICB0cmFuc2l0aW9uOiBtYXgtaGVpZ2h0IDAuNHMgZWFzZS1pbi1vdXQ7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5jb2xsYXBzZWQge1xyXG4gICAgICBtYXgtaGVpZ2h0OiAwICFpbXBvcnRhbnQ7XHJcbiAgICB9IiwiLmV4cGFuZC13cmFwcGVyIHtcbiAgdHJhbnNpdGlvbjogbWF4LWhlaWdodCAwLjRzIGVhc2UtaW4tb3V0O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBoZWlnaHQ6IGF1dG87XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uY29sbGFwc2VkIHtcbiAgbWF4LWhlaWdodDogMCAhaW1wb3J0YW50O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/components/menu-component/menu-component.component.ts":
  /*!*******************************************************************!*\
    !*** ./src/components/menu-component/menu-component.component.ts ***!
    \*******************************************************************/

  /*! exports provided: MenuComponentComponent */

  /***/
  function srcComponentsMenuComponentMenuComponentComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MenuComponentComponent", function () {
      return MenuComponentComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var MenuComponentComponent = /*#__PURE__*/function () {
      function MenuComponentComponent(renderer) {
        _classCallCheck(this, MenuComponentComponent);

        this.renderer = renderer;
        this.expanded = false;
        this.expandHeight = "150px";
      }

      _createClass(MenuComponentComponent, [{
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {
          this.renderer.setStyle(this.expandWrapper.nativeElement, "max-height", this.expandHeight);
        }
      }]);

      return MenuComponentComponent;
    }();

    MenuComponentComponent.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("expandWrapper", {
      read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])], MenuComponentComponent.prototype, "expandWrapper", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])("expanded"), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)], MenuComponentComponent.prototype, "expanded", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])("expandHeight"), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)], MenuComponentComponent.prototype, "expandHeight", void 0);
    MenuComponentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-menu-component',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./menu-component.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/components/menu-component/menu-component.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./menu-component.component.scss */
      "./src/components/menu-component/menu-component.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]])], MenuComponentComponent);
    /***/
  },

  /***/
  "./src/environments/environment.ts":
  /*!*****************************************!*\
    !*** ./src/environments/environment.ts ***!
    \*****************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js"); // This file can be replaced during build by using the `fileReplacements` array.
    // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
    // The list of file replacements can be found in `angular.json`.


    var environment = {
      production: true
    };
    /*
     * For easier debugging in development mode, you can import the following file
     * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
     *
     * This import should be commented out in production mode because it will have a negative impact
     * on performance if an error is thrown.
     */
    //import 'zone.js/dist/zone-error';  // Included with Angular CLI.

    /***/
  },

  /***/
  "./src/main.ts":
  /*!*********************!*\
    !*** ./src/main.ts ***!
    \*********************/

  /*! no exports provided */

  /***/
  function srcMainTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/platform-browser-dynamic */
    "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
    /* harmony import */


    var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./app/app.module */
    "./src/app/app.module.ts");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./environments/environment */
    "./src/environments/environment.ts");

    if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
      Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
    }

    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])["catch"](function (err) {
      return console.log(err);
    });
    /***/
  },

  /***/
  "./src/pipes/curency.pipe.ts":
  /*!***********************************!*\
    !*** ./src/pipes/curency.pipe.ts ***!
    \***********************************/

  /*! exports provided: CurencyPipe */

  /***/
  function srcPipesCurencyPipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CurencyPipe", function () {
      return CurencyPipe;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var CurencyPipe = /*#__PURE__*/function () {
      function CurencyPipe() {
        _classCallCheck(this, CurencyPipe);
      }

      _createClass(CurencyPipe, [{
        key: "transform",
        value: function transform(value) {
          var currency = localStorage.currency;
          var decimals = localStorage.decimals;
          var currecnyPos = localStorage.currencyPos;
          var priceFixed = parseFloat(value).toFixed(decimals);

          if (priceFixed.toString() == 'NaN') {
            if (currecnyPos == 'left') return currency + "" + value;else return value + " " + currency;
          } else {
            if (currecnyPos == 'left') return currency + "" + priceFixed;else return priceFixed + "" + currency;
          }
        }
      }]);

      return CurencyPipe;
    }();

    CurencyPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
      name: 'curency'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], CurencyPipe);
    /***/
  },

  /***/
  "./src/pipes/pipes.module.ts":
  /*!***********************************!*\
    !*** ./src/pipes/pipes.module.ts ***!
    \***********************************/

  /*! exports provided: PipesModule */

  /***/
  function srcPipesPipesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PipesModule", function () {
      return PipesModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _curency_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./curency.pipe */
    "./src/pipes/curency.pipe.ts");
    /* harmony import */


    var _translate_app_translate_app_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./translate-app/translate-app.pipe */
    "./src/pipes/translate-app/translate-app.pipe.ts");

    var PipesModule = function PipesModule() {
      _classCallCheck(this, PipesModule);
    };

    PipesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_curency_pipe__WEBPACK_IMPORTED_MODULE_2__["CurencyPipe"], _translate_app_translate_app_pipe__WEBPACK_IMPORTED_MODULE_3__["TranslateAppPipe"]],
      imports: [],
      exports: [_curency_pipe__WEBPACK_IMPORTED_MODULE_2__["CurencyPipe"], _translate_app_translate_app_pipe__WEBPACK_IMPORTED_MODULE_3__["TranslateAppPipe"]]
    })], PipesModule);
    /***/
  },

  /***/
  "./src/pipes/translate-app/translate-app.pipe.ts":
  /*!*******************************************************!*\
    !*** ./src/pipes/translate-app/translate-app.pipe.ts ***!
    \*******************************************************/

  /*! exports provided: TranslateAppPipe */

  /***/
  function srcPipesTranslateAppTranslateAppPipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TranslateAppPipe", function () {
      return TranslateAppPipe;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");

    var TranslateAppPipe = /*#__PURE__*/function () {
      function TranslateAppPipe(shared) {
        _classCallCheck(this, TranslateAppPipe);

        this.shared = shared;
      }

      _createClass(TranslateAppPipe, [{
        key: "transform",
        value: function transform(value) {
          //console.log(value + " " + this.shared.translationListArray[value.toString()]);
          if (this.shared.translationListArray[value] == undefined) {
            if (this.shared.lab) this.shared.missingValues[value] = value;
            return value;
          }

          return this.shared.translationListArray[value];
        }
      }]);

      return TranslateAppPipe;
    }();

    TranslateAppPipe.ctorParameters = function () {
      return [{
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__["SharedDataService"]
      }];
    };

    TranslateAppPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
      name: 'translate',
      pure: false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__["SharedDataService"]])], TranslateAppPipe);
    /***/
  },

  /***/
  "./src/providers/app-events/app-events.service.ts":
  /*!********************************************************!*\
    !*** ./src/providers/app-events/app-events.service.ts ***!
    \********************************************************/

  /*! exports provided: AppEventsService */

  /***/
  function srcProvidersAppEventsAppEventsServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppEventsService", function () {
      return AppEventsService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/internal/Subject */
    "./node_modules/rxjs/internal/Subject.js");
    /* harmony import */


    var rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__);
    /* harmony import */


    var rxjs_internal_Subscription__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/internal/Subscription */
    "./node_modules/rxjs/internal/Subscription.js");
    /* harmony import */


    var rxjs_internal_Subscription__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_Subscription__WEBPACK_IMPORTED_MODULE_3__);

    var AppEventsService = /*#__PURE__*/function () {
      function AppEventsService() {
        _classCallCheck(this, AppEventsService);

        this.openCategoryPage = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.openDeepLink = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.openHomePage = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.openShippingAddressPage = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.setting = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.showAd = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.settingsLoaded = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.recentDeleted = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.cartChange = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.wishListUpdate = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.openThankYouPage = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.openSubcategoryPage = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.countryChange = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.stateChange = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.cardScratched = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.updateSideMenu = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.productExpired = new rxjs_internal_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.subscriptions = new rxjs_internal_Subscription__WEBPACK_IMPORTED_MODULE_3__["Subscription"]();
        this.openCategoryPage$ = this.openCategoryPage.asObservable();
        this.openDeepLink$ = this.openDeepLink.asObservable();
        this.openHomePage$ = this.openHomePage.asObservable();
        this.openShippingAddressPage$ = this.openShippingAddressPage.asObservable();
        this.setting$ = this.setting.asObservable();
        this.recentDeleted$ = this.recentDeleted.asObservable();
        this.settingsLoaded$ = this.settingsLoaded.asObservable();
        this.cartChange$ = this.cartChange.asObservable();
        this.wishListUpdate$ = this.wishListUpdate.asObservable();
        this.showAd$ = this.showAd.asObservable();
        this.openThankYouPage$ = this.openThankYouPage.asObservable();
        this.openSubcategoryPage$ = this.openSubcategoryPage.asObservable();
        this.countryChange$ = this.countryChange.asObservable();
        this.stateChange$ = this.stateChange.asObservable();
        this.cardScratched$ = this.cardScratched.asObservable();
        this.updateSideMenu$ = this.updateSideMenu.asObservable();
        this.productExpired$ = this.productExpired.asObservable();
      }

      _createClass(AppEventsService, [{
        key: "publish",
        value: function publish(eventName, eventData) {
          if (eventName == "openCategoryPage") this.openCategoryPage.next(eventData);
          if (eventName == "openDeepLink") this.openDeepLink.next(eventData);
          if (eventName == "openHomePage") this.openHomePage.next(eventData);
          if (eventName == "openShippingAddressPage") this.openShippingAddressPage.next(eventData);
          if (eventName == "setting") this.setting.next(eventData);
          if (eventName == "settingsLoaded") this.settingsLoaded.next(eventData);
          if (eventName == "recentDeleted") this.recentDeleted.next(eventData);
          if (eventName == "cartChange") this.cartChange.next(eventData);
          if (eventName == "wishListUpdate") this.wishListUpdate.next(eventData);
          if (eventName == "showAd") this.showAd.next(eventData);
          if (eventName == "openThankYouPage") this.openThankYouPage.next(eventData);
          if (eventName == "openSubcategoryPage") this.openSubcategoryPage.next(eventData);
          if (eventName == "countryChange") this.countryChange.next(eventData);
          if (eventName == "stateChange") this.stateChange.next(eventData);
          if (eventName == "cardScratched") this.cardScratched.next(eventData);
          if (eventName == "updateSideMenu") this.updateSideMenu.next(eventData);
          if (eventName == "productExpired") this.productExpired.next(eventData);
        }
      }, {
        key: "subscribe",
        value: function subscribe(eventName) {
          if (eventName == "openCategoryPage") return {
            subscriptions: this.subscriptions,
            event: this.openCategoryPage$
          };
          if (eventName == "openDeepLink") return {
            subscriptions: this.subscriptions,
            event: this.openDeepLink$
          };
          if (eventName == "openHomePage") return {
            subscriptions: this.subscriptions,
            event: this.openHomePage$
          };
          if (eventName == "setting") return {
            subscriptions: this.subscriptions,
            event: this.setting$
          };
          if (eventName == "settingsLoaded") return {
            subscriptions: this.subscriptions,
            event: this.settingsLoaded$
          };
          if (eventName == "recentDeleted") return {
            subscriptions: this.subscriptions,
            event: this.recentDeleted$
          };
          if (eventName == "cartChange") return {
            subscriptions: this.subscriptions,
            event: this.cartChange$
          };
          if (eventName == "wishListUpdate") return {
            subscriptions: this.subscriptions,
            event: this.wishListUpdate$
          };
          if (eventName == "showAd") return {
            subscriptions: this.subscriptions,
            event: this.showAd$
          };
          if (eventName == "openShippingAddressPage") return {
            subscriptions: this.subscriptions,
            event: this.openShippingAddressPage$
          };
          if (eventName == "openThankYouPage") return {
            subscriptions: this.subscriptions,
            event: this.openThankYouPage$
          };
          if (eventName == "openSubcategoryPage") return {
            subscriptions: this.subscriptions,
            event: this.openSubcategoryPage$
          };
          if (eventName == "countryChange") return {
            subscriptions: this.subscriptions,
            event: this.countryChange$
          };
          if (eventName == "stateChange") return {
            subscriptions: this.subscriptions,
            event: this.stateChange$
          };
          if (eventName == "cardScratched") return {
            subscriptions: this.subscriptions,
            event: this.cardScratched$
          };
          if (eventName == "updateSideMenu") return {
            subscriptions: this.subscriptions,
            event: this.updateSideMenu$
          };
          if (eventName == "productExpired") return {
            subscriptions: this.subscriptions,
            event: this.productExpired$
          };
        }
      }]);

      return AppEventsService;
    }();

    AppEventsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], AppEventsService);
    /***/
  },

  /***/
  "./src/providers/auth-guard/auth-guard.service.ts":
  /*!********************************************************!*\
    !*** ./src/providers/auth-guard/auth-guard.service.ts ***!
    \********************************************************/

  /*! exports provided: AuthGuardService */

  /***/
  function srcProvidersAuthGuardAuthGuardServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AuthGuardService", function () {
      return AuthGuardService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_app_modals_login_login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/modals/login/login.page */
    "./src/app/modals/login/login.page.ts");
    /* harmony import */


    var _shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");

    var AuthGuardService = /*#__PURE__*/function () {
      function AuthGuardService(modalCtrl, shared) {
        _classCallCheck(this, AuthGuardService);

        this.modalCtrl = modalCtrl;
        this.shared = shared;
      }

      _createClass(AuthGuardService, [{
        key: "canActivate",
        value: function canActivate(route) {
          console.log(this.shared.orderDetails.guest_status);
          console.log(this.shared.orderDetails);

          if (this.shared.customerData.customers_id == null) {
            if (this.shared.orderDetails.guest_status == 0) {
              this.openLoginPage(route.data.hideGuestLogin);
              return false;
            } else if (this.shared.orderDetails.guest_status == 1 && route.data.hideGuestLogin == false) {
              return true;
            } else {
              this.openLoginPage(route.data.hideGuestLogin);
            }
          } else return true;
        }
      }, {
        key: "openLoginPage",
        value: function openLoginPage(value) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee16() {
            var val, modal;
            return regeneratorRuntime.wrap(function _callee16$(_context16) {
              while (1) {
                switch (_context16.prev = _context16.next) {
                  case 0:
                    console.log(value);
                    val = value;
                    if (value == undefined) val = true;
                    _context16.next = 5;
                    return this.modalCtrl.create({
                      component: src_app_modals_login_login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"],
                      componentProps: {
                        'hideGuestLogin': val
                      }
                    });

                  case 5:
                    modal = _context16.sent;
                    _context16.next = 8;
                    return modal.present();

                  case 8:
                    return _context16.abrupt("return", _context16.sent);

                  case 9:
                  case "end":
                    return _context16.stop();
                }
              }
            }, _callee16, this);
          }));
        }
      }]);

      return AuthGuardService;
    }();

    AuthGuardService.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"]
      }];
    };

    AuthGuardService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"]])], AuthGuardService);
    /***/
  },

  /***/
  "./src/providers/back-button-exit/back-button-exit-app.service.ts":
  /*!************************************************************************!*\
    !*** ./src/providers/back-button-exit/back-button-exit-app.service.ts ***!
    \************************************************************************/

  /*! exports provided: BackButtonExitAppService */

  /***/
  function srcProvidersBackButtonExitBackButtonExitAppServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BackButtonExitAppService", function () {
      return BackButtonExitAppService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _config_config_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../config/config.service */
    "./src/providers/config/config.service.ts");

    var BackButtonExitAppService = /*#__PURE__*/function () {
      function BackButtonExitAppService(router, shared, plt, config, navCtrl, modalCtrl) {
        _classCallCheck(this, BackButtonExitAppService);

        this.router = router;
        this.shared = shared;
        this.plt = plt;
        this.config = config;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl; // set up hardware back button event.

        this.currentUrl = "";
        this.lastTimeBackPress = 0;
        this.timePeriodToExit = 2000;
      } // active hardware back button


      _createClass(BackButtonExitAppService, [{
        key: "backButtonEvent",
        value: function backButtonEvent() {
          var _this25 = this;

          this.plt.backButton.subscribe(function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this25, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee18() {
              var _this26 = this;

              return regeneratorRuntime.wrap(function _callee18$(_context18) {
                while (1) {
                  switch (_context18.prev = _context18.next) {
                    case 0:
                      this.currentUrl = this.router.url;
                      this.routerOutlets.forEach(function (outlet) {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this26, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee17() {
                          return regeneratorRuntime.wrap(function _callee17$(_context17) {
                            while (1) {
                              switch (_context17.prev = _context17.next) {
                                case 0:
                                  if (outlet && outlet.canGoBack()) {
                                    outlet.pop();
                                  } else {
                                    this.checkIfModalIsOpen();
                                  }

                                case 1:
                                case "end":
                                  return _context17.stop();
                              }
                            }
                          }, _callee17, this);
                        }));
                      });

                    case 2:
                    case "end":
                      return _context18.stop();
                  }
                }
              }, _callee18, this);
            }));
          });
        }
      }, {
        key: "checkIfModalIsOpen",
        value: function checkIfModalIsOpen() {
          var _this27 = this;

          this.modalCtrl.getTop().then(function (data) {
            if (data == undefined) _this27.tryToCloseTheApp();
          });
        }
      }, {
        key: "tryToCloseTheApp",
        value: function tryToCloseTheApp() {
          if (this.getCurrentTime() - this.lastTimeBackPress < this.timePeriodToExit) {
            navigator['app'].exitApp();
          } else if (this.isRootUrl()) {
            this.shared.toast('Press back again to exit App.', this.timePeriodToExit);
            this.lastTimeBackPress = this.getCurrentTime();
          }
        }
      }, {
        key: "isRootUrl",
        value: function isRootUrl() {
          console.log(this.currentUrl);
          if (this.config.appNavigationTabs == false) return true;else {
            if (this.currentUrl == '/tabs/home') return true;else if (this.currentUrl == '/tabs/home2') return true;else if (this.currentUrl == '/tabs/home3') return true;else if (this.currentUrl == '/tabs/home4') return true;else if (this.currentUrl == '/tabs/home5') return true;else if (this.currentUrl == '/tabs/home6') return true;else if (this.currentUrl == '/tabs/home7') return true;else if (this.currentUrl == '/tabs/home8') return true;else if (this.currentUrl == '/tabs/home9') return true;else if (this.currentUrl == '/tabs/home10') return true;else if (this.currentUrl == '/tabs/cart') return true;else if (this.currentUrl == '/tabs/search') return true;else if (this.currentUrl == '/tabs/settings') return true;else if (this.currentUrl == '/tabs/categories') return true;else if (this.currentUrl == '/tabs/categories2') return true;else if (this.currentUrl == '/tabs/categories3') return true;else if (this.currentUrl == '/tabs/categories4') return true;else if (this.currentUrl == '/tabs/categories5') return true;else if (this.currentUrl == '/tabs/categories6') return true;
          }
        }
      }, {
        key: "getCurrentTime",
        value: function getCurrentTime() {
          return new Date().getTime();
        }
      }]);

      return BackButtonExitAppService;
    }();

    BackButtonExitAppService.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
      }, {
        type: _config_config_service__WEBPACK_IMPORTED_MODULE_5__["ConfigService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }];
    };

    BackButtonExitAppService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _config_config_service__WEBPACK_IMPORTED_MODULE_5__["ConfigService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])], BackButtonExitAppService);
    /***/
  },

  /***/
  "./src/providers/config/config.service.ts":
  /*!************************************************!*\
    !*** ./src/providers/config/config.service.ts ***!
    \************************************************/

  /*! exports provided: ConfigService */

  /***/
  function srcProvidersConfigConfigServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ConfigService", function () {
      return ConfigService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var ts_md5_dist_md5__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ts-md5/dist/md5 */
    "./node_modules/ts-md5/dist/md5.js");
    /* harmony import */


    var ts_md5_dist_md5__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ts_md5_dist_md5__WEBPACK_IMPORTED_MODULE_5__);
    /* harmony import */


    var _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic-native/local-notifications/ngx */
    "./node_modules/@ionic-native/local-notifications/ngx/index.js");
    /* harmony import */


    var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic-native/http/ngx */
    "./node_modules/@ionic-native/http/ngx/index.js");
    /* harmony import */


    var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/providers/app-events/app-events.service */
    "./src/providers/app-events/app-events.service.ts");
    /* harmony import */


    var _get_ip_Address_get_ip_address_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../get-ip-Address/get-ip-address.service */
    "./src/providers/get-ip-Address/get-ip-address.service.ts");
    /* harmony import */


    var _get_device_id_get_device_id_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../get-device-id/get-device-id.service */
    "./src/providers/get-device-id/get-device-id.service.ts");

    if (localStorage.langId == undefined) {
      localStorage.langId = '1'; //default language id

      localStorage.languageCode = "es"; //default language code

      localStorage.direction = "ltr"; //default language direction of app

      localStorage.currency = "€"; //default currecny html code to show in app.
      // Please visit this link to get your html code  https://html-css-js.com/html/character-codes/currency/

      localStorage.currencyCode = "EUR"; //default currency code

      localStorage.currencyPos = "right"; //default currency position

      localStorage.decimals = 2; //default currecny decimal

      localStorage.appNavigation = "tabs"; //default app naviagtion. tabs, sidemenu
    }

    var ConfigService = /*#__PURE__*/function () {
      function ConfigService(storage, platform, md5, localNotifications, http, appEventsService, httpNative, getIpAddressService, getDeviceIdService) {
        _classCallCheck(this, ConfigService);

        this.storage = storage;
        this.platform = platform;
        this.md5 = md5;
        this.localNotifications = localNotifications;
        this.http = http;
        this.appEventsService = appEventsService;
        this.httpNative = httpNative;
        this.getIpAddressService = getIpAddressService;
        this.getDeviceIdService = getDeviceIdService;
        this.yourSiteUrl = 'https://appu.es';
        this.consumerKey = "54f1998a1594406259010e9e1e";
        this.consumerSecret = "1efba47d159440625961f43559";
        this.showIntroPage = 0; //  0 to hide and 1 to show intro page

        this.appInProduction = true; //  0 to hide and 1 to show intro page

        this.defaultIcons = true; //  0 to hide and 1 to show intro page

        this.appNavigationTabs = localStorage.appNavigation == "tabs" ? true : false; //  true for tabs layout and false for sidemenu layout

        this.appTheme = 'default';
        this.darkMode = false;
        this.bannerAnimationEffect = "default"; // fade, coverFlow, flip, cube, default

        this.bannerStyle = "default"; // default, squareBullets, numberBullets, bottomBulletsWhiteBackground, progressBar, verticalRightBullets, verticalLeftBullets

        this.productCardStyle = "1";
        this.productSlidesPerPage = 2.5;
        this.url = this.yourSiteUrl + '/api/';
        this.imgUrl = this.yourSiteUrl + "/";
        this.langId = localStorage.langId;
        this.currecnyCode = localStorage.currencyCode;
        this.loader = 'dots';
        this.newProductDuration = 10;
        this.cartButton = 1; //1 = show and 0 = hide

        this.currency = localStorage.currency;
        this.currencyPos = localStorage.currencyPos;
        this.paypalCurrencySymbol = localStorage.currency;
        this.homePage = 1;
        this.categoryPage = 1;
        this.siteUrl = '';
        this.appName = '';
        this.packgeName = "";
        this.introPage = 1;
        this.myOrdersPage = 1;
        this.newsPage = 1;
        this.wishListPage = 1;
        this.shippingAddressPage = 1;
        this.aboutUsPage = 1;
        this.contactUsPage = 1;
        this.editProfilePage = 1;
        this.settingPage = 1;
        this.admob = 1;
        this.admobBannerid = '';
        this.admobIntid = '';
        this.admobIos = 1;
        this.admobBanneridIos = '';
        this.admobIntidIos = '';
        this.googleAnalaytics = "";
        this.rateApp = 1;
        this.shareApp = 1;
        this.fbButton = 1;
        this.googleButton = 1;
        this.notificationType = "onesignal";
        this.onesignalAppId = "f534497e-f305-4a9b-b89c-d07c94386a34";
        this.onesignalSenderId = "61123581233";
        this.appSettings = {};
        this.currentRoute = "tabs/home";
        this.enableAddressMap = true; // Initialize Firebase

        this.firebaseConfig = {};
        this.setUserSettings();
        this.consumerKey = ts_md5_dist_md5__WEBPACK_IMPORTED_MODULE_5__["Md5"].hashStr(this.consumerKey).toString();
        this.consumerSecret = ts_md5_dist_md5__WEBPACK_IMPORTED_MODULE_5__["Md5"].hashStr(this.consumerSecret).toString();
        if (this.appNavigationTabs == false) this.currentRoute = "";
      }

      _createClass(ConfigService, [{
        key: "getHeadersForHttp",
        value: function getHeadersForHttp() {
          var d = new Date();
          var nonce = d.getTime().toString();
          var headers = {
            'consumer-key': this.consumerKey,
            'consumer-secret': this.consumerSecret,
            'consumer-nonce': nonce,
            'consumer-device-id': this.getDeviceIdService.getDeviceId(),
            'consumer-ip': this.getIpAddressService.getIpAddress(),
            'Content-Type': 'application/json'
          };
          return headers;
        }
      }, {
        key: "getHttp",
        value: function getHttp(req) {
          var _this28 = this;

          var customHeaders = this.getHeadersForHttp();
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"](customHeaders)
          };
          return new Promise(function (resolve) {
            if (_this28.platform.is('cordova')) {
              _this28.httpNative.get(_this28.url + req, {}, customHeaders).then(function (data) {
                var d = JSON.parse(data.data); //this.storeHttpData(request, d);

                resolve(d); //console.log(data.status);
                //console.log(data.data); // data received by server
                //console.log(data.headers);
              })["catch"](function (error) {// console.log("Error : " + req);
                // console.log(error);
                // console.log(error.error); // error message as string
                // console.log(error.headers);
              });
            } else {
              _this28.http.get(_this28.url + req, httpOptions).subscribe(function (data) {
                resolve(data);
              }, function (err) {
                console.log("Error : " + req);
                console.log(err);
              });
            }
          });
        }
      }, {
        key: "postHttp2",
        value: function postHttp2(req, data) {
          var customHeaders = this.getHeadersForHttp();
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"](customHeaders)
          };

          if (this.platform.is('cordova')) {
            return this.httpNative.post(this.url + req, data, customHeaders);
          }
        }
      }, {
        key: "postHttp",
        value: function postHttp(req, data) {
          var _this29 = this;

          var customHeaders = this.getHeadersForHttp();
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"](customHeaders)
          };
          return new Promise(function (resolve, reject) {
            if (_this29.platform.is('cordova')) {
              _this29.httpNative.setDataSerializer("json");

              _this29.httpNative.post(_this29.url + req, data, customHeaders).then(function (data) {
                var d = JSON.parse(data.data); //alert(JSON.stringify(data.data));
                //console.log(this.url + req, d);
                //this.storeHttpData(request, d);

                resolve(d); //console.log(data.status);
                //console.log(data.data); // data received by server
                //console.log(data.headers);
              })["catch"](function (error) {
                reject(JSON.parse(error)); //alert(error)
                // console.log("Error : " + req);
                // console.log(error);
                // console.log(error.error); // error message as string
                // console.log(error.headers);
              });
            } else {
              _this29.http.post(_this29.url + req, data, httpOptions).subscribe(function (data) {
                resolve(data);
              }, function (err) {
                reject(JSON.parse(err));
                console.log("Error : " + req); //console.log(err);
              });
            }
          });
        }
      }, {
        key: "siteSetting",
        value: function siteSetting() {
          var _this30 = this;

          return new Promise(function (resolve) {
            _this30.storage.get('appSettings').then(function (val) {
              if (val == null) {
                _this30.getSettingsFromServer().then(function (data) {
                  if (data.success == "1") {
                    _this30.appSettings = data.data;

                    _this30.storage.set("appSettings", _this30.appSettings);

                    _this30.defaultSettings();

                    _this30.appEventsService.publish('settingsLoaded', "");
                  }

                  resolve();
                });
              } else {
                _this30.appSettings = val;

                _this30.defaultSettings();

                _this30.appEventsService.publish('settingsLoaded', "");

                resolve();
              }
            });
          });
        }
      }, {
        key: "defaultSettings",
        value: function defaultSettings() {
          this.fbId = this.appSettings.facebook_app_id;
          this.address = this.appSettings.address + ', ' + this.appSettings.city + ', ' + this.appSettings.state + ' ' + this.appSettings.zip + ', ' + this.appSettings.country;
          this.email = this.appSettings.contact_us_email;
          this.latitude = this.appSettings.latitude;
          this.longitude = this.appSettings.longitude;
          this.phoneNo = this.appSettings.phone_no;
          this.pushNotificationSenderId = this.appSettings.fcm_android_sender_id;
          this.lazyLoadingGif = this.appSettings.lazzy_loading_effect;
          this.newProductDuration = this.appSettings.new_product_duration;
          this.notifText = this.appSettings.notification_text;
          this.notifTitle = this.appSettings.notification_title;
          this.notifDuration = this.appSettings.notification_duration;
          this.currency = this.appSettings.currency_symbol;
          this.cartButton = this.appSettings.cart_button;
          this.footerShowHide = this.appSettings.footer_button;
          this.setLocalNotification();
          this.appName = this.appSettings.app_name;
          this.homePage = this.appSettings.home_style;
          this.categoryPage = this.appSettings.category_style;
          if (this.appSettings.card_style) this.productCardStyle = this.appSettings.card_style;
          if (this.appSettings.banner_style) this.setBannerStyle(this.appSettings.banner_style);
          this.siteUrl = this.appSettings.site_url;
          this.introPage = this.appSettings.intro_page;
          this.myOrdersPage = this.appSettings.my_orders_page;
          this.newsPage = this.appSettings.news_page;
          this.wishListPage = this.appSettings.wish_list_page;
          this.shippingAddressPage = this.appSettings.shipping_address_page;
          this.aboutUsPage = this.appSettings.about_us_page;
          this.contactUsPage = this.appSettings.contact_us_page;
          this.editProfilePage = this.appSettings.edit_profile_page;
          this.packgeName = this.appSettings.package_name;
          this.settingPage = this.appSettings.setting_page;
          this.admob = this.appSettings.admob;
          this.admobBannerid = this.appSettings.ad_unit_id_banner;
          this.admobIntid = this.appSettings.ad_unit_id_interstitial;
          this.googleAnalaytics = this.appSettings.google_analytic_id;
          this.rateApp = this.appSettings.rate_app;
          this.shareApp = this.appSettings.share_app;
          this.fbButton = this.appSettings.facebook_login;
          this.googleButton = this.appSettings.google_login;
          this.notificationType = this.appSettings.default_notification;
          this.onesignalAppId = this.appSettings.onesignal_app_id;
          this.onesignalSenderId = this.appSettings.onesignal_sender_id;
          this.admobIos = this.appSettings.ios_admob;
          this.admobBanneridIos = this.appSettings.ios_ad_unit_id_banner;
          this.admobIntidIos = this.appSettings.ios_ad_unit_id_interstitial;
          this.defaultIcons = this.appSettings.app_icon_image == "icon" ? true : false;
          this.enableAddressMap = this.appSettings.is_enable_location == "1" ? true : false;
          if (this.appNavigationTabs) if (this.homePage != 1) this.currentRoute = "tabs/home" + this.homePage;
          this.firebaseConfig = {
            apiKey: this.appSettings.firebase_apikey,
            authDomain: this.appSettings.auth_domain,
            databaseURL: this.appSettings.database_URL,
            projectId: this.appSettings.projectId,
            storageBucket: this.appSettings.storage_bucket,
            messagingSenderId: this.appSettings.messaging_senderid
          };
        }
      }, {
        key: "getCurrentHomePage",
        value: function getCurrentHomePage() {
          if (this.homePage == 1) return "home";else return "home" + this.homePage;
        }
      }, {
        key: "getCurrentCategoriesPage",
        value: function getCurrentCategoriesPage() {
          if (this.categoryPage == 1) return "categories";else return "categories" + this.categoryPage;
        }
      }, {
        key: "checkingNewSettingsFromServer",
        value: function checkingNewSettingsFromServer() {
          var _this31 = this;

          this.getSettingsFromServer().then(function (data) {
            if (data.success == "1") {
              var settings = data.data;

              _this31.storage.set("appSettings", settings).then(function () {});
            }
          });
        } //Subscribe for local notification when application is start for the first time

      }, {
        key: "setLocalNotification",
        value: function setLocalNotification() {
          var _this32 = this;

          this.platform.ready().then(function () {
            _this32.storage.get('localNotification').then(function (val) {
              if (val == undefined) {
                _this32.storage.set('localNotification', 'localNotification');

                _this32.localNotifications.schedule({
                  id: 1,
                  title: _this32.notifTitle,
                  text: _this32.notifText,
                  every: _this32.notifDuration
                });
              }
            });
          });
        }
      }, {
        key: "getSettingsFromServer",
        value: function getSettingsFromServer() {
          return this.getHttp('sitesetting');
        }
      }, {
        key: "setBannerStyle",
        value: function setBannerStyle(s) {
          switch (parseInt(s)) {
            case 4:
              this.bannerStyle = "squareBullets";
              break;

            case 5:
              this.bannerStyle = "numberBullets";
              break;

            case 3:
              this.bannerStyle = "bottomBulletsWhiteBackground";
              break;

            case 2:
              this.bannerStyle = "progressBar";
              break;

            case 6:
              this.bannerStyle = "verticalRightBullets";
              break;

            case 1:
              this.bannerStyle = "default";
              break;

            default:
              this.bannerStyle = "default";
              break;
          }
        }
      }, {
        key: "setCardStyle",
        value: function setCardStyle(value) {
          if (!this.appInProduction) {
            this.productCardStyle = value;
          }

          console.log(value);
        }
      }, {
        key: "setUserSettings",
        value: function setUserSettings() {
          var _this33 = this;

          this.storage.get('setting').then(function (val) {
            if (val != null || val != undefined) {
              _this33.darkMode = val.darkMode;
            }

            console.log(val, _this33.darkMode);
          });
        }
      }]);

      return ConfigService;
    }();

    ConfigService.ctorParameters = function () {
      return [{
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]
      }, {
        type: ts_md5_dist_md5__WEBPACK_IMPORTED_MODULE_5__["Md5"]
      }, {
        type: _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_6__["LocalNotifications"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }, {
        type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_8__["AppEventsService"]
      }, {
        type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_7__["HTTP"]
      }, {
        type: _get_ip_Address_get_ip_address_service__WEBPACK_IMPORTED_MODULE_9__["GetIpAddressService"]
      }, {
        type: _get_device_id_get_device_id_service__WEBPACK_IMPORTED_MODULE_10__["GetDeviceIdService"]
      }];
    };

    ConfigService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"], ts_md5_dist_md5__WEBPACK_IMPORTED_MODULE_5__["Md5"], _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_6__["LocalNotifications"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_8__["AppEventsService"], _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_7__["HTTP"], _get_ip_Address_get_ip_address_service__WEBPACK_IMPORTED_MODULE_9__["GetIpAddressService"], _get_device_id_get_device_id_service__WEBPACK_IMPORTED_MODULE_10__["GetDeviceIdService"]])], ConfigService);
    /***/
  },

  /***/
  "./src/providers/deeplinking/deep-linking.service.ts":
  /*!***********************************************************!*\
    !*** ./src/providers/deeplinking/deep-linking.service.ts ***!
    \***********************************************************/

  /*! exports provided: DeepLinkingService */

  /***/
  function srcProvidersDeeplinkingDeepLinkingServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DeepLinkingService", function () {
      return DeepLinkingService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_native_deeplinks_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic-native/deeplinks/ngx */
    "./node_modules/@ionic-native/deeplinks/ngx/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _config_config_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var _loading_loading_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_app_product_detail_product_detail_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/product-detail/product-detail.page */
    "./src/app/product-detail/product-detail.page.ts");

    var DeepLinkingService = /*#__PURE__*/function () {
      function DeepLinkingService(navCtrl, shared, config, loading, deeplinks) {
        _classCallCheck(this, DeepLinkingService);

        this.navCtrl = navCtrl;
        this.shared = shared;
        this.config = config;
        this.loading = loading;
        this.deeplinks = deeplinks;
        this.link = "empty";
        this.deepUrl = "";
      }

      _createClass(DeepLinkingService, [{
        key: "initializeDeepLinks",
        value: function initializeDeepLinks() {
          var _this34 = this;

          //this.deeplinks.routeWithNavController(this.nav, {
          this.deeplinks.route({
            '/product-detail/:productSlug': src_app_product_detail_product_detail_page__WEBPACK_IMPORTED_MODULE_7__["ProductDetailPage"]
          }).subscribe(function (match) {
            // match.$route - the route we matched, which is the matched entry from the arguments to route()
            // match.$args - the args passed in the link
            // match.$link - the full link data
            //this.deepUrl = match.$link.url;
            var linkPath = match.$link.path;

            if (linkPath.indexOf('product-detail') != -1) {
              _this34.getSingleProductDetail(match.$args.productSlug);
            }

            console.log('Successfully matched route', match); //if (this.rootPage != undefined) this.naviagateDeeplink();
          }, function (nomatch) {
            // nomatch.$link - the full link data
            _this34.deepUrl = nomatch.$link.url; //if (this.rootPage != undefined) this.naviagateDeeplink();

            console.error('Got a deeplink that didn\'t match', nomatch);
          });
        }
      }, {
        key: "getSingleProductDetail",
        value: function getSingleProductDetail(id) {
          var _this35 = this;

          this.loading.show();
          var dat = {};
          if (this.shared.customerData.customers_id) dat.customers_id = this.shared.customerData.customers_id;else dat.customers_id = null;
          dat.page_number = 0;
          dat.products_slug = id;
          dat.language_id = this.config.langId;
          dat.currency_code = this.config.currecnyCode;
          this.config.postHttp('getallproducts', dat).then(function (data) {
            _this35.loading.hide();

            if (data.success == 1) {
              _this35.shared.singleProductPageData.push(data.product_data[0]);

              _this35.navCtrl.navigateForward(_this35.config.currentRoute + "/product-detail/" + data.product_data[0].products_id);
            }

            console.log("getSingleProductDetail deeplink", dat, data);
          });
        }
      }]);

      return DeepLinkingService;
    }();

    DeepLinkingService.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"]
      }, {
        type: _config_config_service__WEBPACK_IMPORTED_MODULE_5__["ConfigService"]
      }, {
        type: _loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"]
      }, {
        type: _ionic_native_deeplinks_ngx__WEBPACK_IMPORTED_MODULE_2__["Deeplinks"]
      }];
    };

    DeepLinkingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"], _config_config_service__WEBPACK_IMPORTED_MODULE_5__["ConfigService"], _loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"], _ionic_native_deeplinks_ngx__WEBPACK_IMPORTED_MODULE_2__["Deeplinks"]])], DeepLinkingService);
    /***/
  },

  /***/
  "./src/providers/get-device-id/get-device-id.service.ts":
  /*!**************************************************************!*\
    !*** ./src/providers/get-device-id/get-device-id.service.ts ***!
    \**************************************************************/

  /*! exports provided: GetDeviceIdService */

  /***/
  function srcProvidersGetDeviceIdGetDeviceIdServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GetDeviceIdService", function () {
      return GetDeviceIdService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_native_unique_device_id_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic-native/unique-device-id/ngx */
    "./node_modules/@ionic-native/unique-device-id/ngx/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

    var GetDeviceIdService = /*#__PURE__*/function () {
      function GetDeviceIdService(uniqueDeviceID, platform) {
        var _this36 = this;

        _classCallCheck(this, GetDeviceIdService);

        this.uniqueDeviceID = uniqueDeviceID;
        this.platform = platform;
        this.deviceId = "";
        this.realDeviceIdUpdatedCounter = 0;
        this.platform.ready().then(function () {
          if (_this36.platform.is('cordova')) {
            _this36.setRealDeviceId();
          }
        });
      }

      _createClass(GetDeviceIdService, [{
        key: "setRealDeviceId",
        value: function setRealDeviceId() {
          var _this37 = this;

          this.uniqueDeviceID.get().then(function (uuid) {
            _this37.deviceId = uuid;
            _this37.realDeviceIdUpdatedCounter++;
          })["catch"](function (error) {
            console.log(error);
          });
        }
      }, {
        key: "getDeviceId",
        value: function getDeviceId() {
          var id = "";

          if (this.realDeviceIdUpdatedCounter == 0) {
            var d = new Date();
            id = d.getTime().toString();
          } else {
            id = this.deviceId;
          }

          return id;
        }
      }]);

      return GetDeviceIdService;
    }();

    GetDeviceIdService.ctorParameters = function () {
      return [{
        type: _ionic_native_unique_device_id_ngx__WEBPACK_IMPORTED_MODULE_2__["UniqueDeviceID"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]
      }];
    };

    GetDeviceIdService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_unique_device_id_ngx__WEBPACK_IMPORTED_MODULE_2__["UniqueDeviceID"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]])], GetDeviceIdService);
    /***/
  },

  /***/
  "./src/providers/get-ip-Address/get-ip-address.service.ts":
  /*!****************************************************************!*\
    !*** ./src/providers/get-ip-Address/get-ip-address.service.ts ***!
    \****************************************************************/

  /*! exports provided: GetIpAddressService */

  /***/
  function srcProvidersGetIpAddressGetIpAddressServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GetIpAddressService", function () {
      return GetIpAddressService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_native_network_interface_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic-native/network-interface/ngx */
    "./node_modules/@ionic-native/network-interface/ngx/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

    var GetIpAddressService = /*#__PURE__*/function () {
      function GetIpAddressService(platform, networkInterface) {
        var _this38 = this;

        _classCallCheck(this, GetIpAddressService);

        this.platform = platform;
        this.networkInterface = networkInterface;
        this.ipAddress = "";
        this.realIpAddressUpdatedCounter = 0;
        this.platform.ready().then(function () {
          if (_this38.platform.is('cordova')) {
            _this38.setRealIpAddress();
          }
        });
      }

      _createClass(GetIpAddressService, [{
        key: "getIpAddress",
        value: function getIpAddress() {
          var ip;

          if (this.realIpAddressUpdatedCounter == 0) {
            var p1 = Math.floor(Math.random() * 255) + 1;
            var p2 = Math.floor(Math.random() * 255);
            var p3 = Math.floor(Math.random() * 255);
            var p4 = Math.floor(Math.random() * 255);
            ip = p1 + "." + p2 + "." + p3 + "." + p4;
          } else {
            ip = this.ipAddress;
          }

          return ip;
        }
      }, {
        key: "setRealIpAddress",
        value: function setRealIpAddress() {
          var _this39 = this;

          this.networkInterface.getWiFiIPAddress().then(function (address) {
            if (address.ip != undefined) {
              _this39.ipAddress = address.ip;
              _this39.realIpAddressUpdatedCounter++;
            }
          })["catch"](function (error) {
            console.error("Unable to get IP: ".concat(error));
          });
          this.networkInterface.getCarrierIPAddress().then(function (address) {
            if (address.ip != undefined) {
              _this39.ipAddress = address.ip;
              _this39.realIpAddressUpdatedCounter++;
            }
          })["catch"](function (error) {
            console.error("Unable to get IP: ".concat(error));
          });
        }
      }]);

      return GetIpAddressService;
    }();

    GetIpAddressService.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]
      }, {
        type: _ionic_native_network_interface_ngx__WEBPACK_IMPORTED_MODULE_2__["NetworkInterface"]
      }];
    };

    GetIpAddressService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"], _ionic_native_network_interface_ngx__WEBPACK_IMPORTED_MODULE_2__["NetworkInterface"]])], GetIpAddressService);
    /***/
  },

  /***/
  "./src/providers/loading/loading.service.ts":
  /*!**************************************************!*\
    !*** ./src/providers/loading/loading.service.ts ***!
    \**************************************************/

  /*! exports provided: LoadingService */

  /***/
  function srcProvidersLoadingLoadingServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoadingService", function () {
      return LoadingService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");

    var LoadingService = /*#__PURE__*/function () {
      function LoadingService(loadingCtrl) {
        _classCallCheck(this, LoadingService);

        this.loadingCtrl = loadingCtrl;
      }

      _createClass(LoadingService, [{
        key: "show",
        value: function show() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee19() {
            return regeneratorRuntime.wrap(function _callee19$(_context19) {
              while (1) {
                switch (_context19.prev = _context19.next) {
                  case 0:
                    _context19.next = 2;
                    return this.loadingCtrl.create({
                      duration: 7000
                    });

                  case 2:
                    this.loading = _context19.sent;
                    this.loading.present();

                  case 4:
                  case "end":
                    return _context19.stop();
                }
              }
            }, _callee19, this);
          }));
        }
      }, {
        key: "hide",
        value: function hide() {
          try {
            this.loading.dismiss();
          } catch (error) {}
        }
      }, {
        key: "autoHide",
        value: function autoHide(time) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee20() {
            return regeneratorRuntime.wrap(function _callee20$(_context20) {
              while (1) {
                switch (_context20.prev = _context20.next) {
                  case 0:
                    _context20.next = 2;
                    return this.loadingCtrl.create({
                      duration: time
                    });

                  case 2:
                    this.loading = _context20.sent;
                    this.loading.present();

                  case 4:
                  case "end":
                    return _context20.stop();
                }
              }
            }, _callee20, this);
          }));
        }
      }]);

      return LoadingService;
    }();

    LoadingService.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }];
    };

    LoadingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])], LoadingService);
    /***/
  },

  /***/
  "./src/providers/shared-data/shared-data.service.ts":
  /*!**********************************************************!*\
    !*** ./src/providers/shared-data/shared-data.service.ts ***!
    \**********************************************************/

  /*! exports provided: SharedDataService */

  /***/
  function srcProvidersSharedDataSharedDataServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SharedDataService", function () {
      return SharedDataService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _config_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _loading_loading_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic-native/onesignal/ngx */
    "./node_modules/@ionic-native/onesignal/ngx/index.js");
    /* harmony import */


    var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic-native/splash-screen/ngx */
    "./node_modules/@ionic-native/splash-screen/ngx/index.js");
    /* harmony import */


    var _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/app-version/ngx */
    "./node_modules/@ionic-native/app-version/ngx/index.js");
    /* harmony import */


    var _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic-native/fcm/ngx */
    "./node_modules/@ionic-native/fcm/ngx/index.js");
    /* harmony import */


    var _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @ionic-native/device/ngx */
    "./node_modules/@ionic-native/device/ngx/index.js");
    /* harmony import */


    var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! src/providers/app-events/app-events.service */
    "./src/providers/app-events/app-events.service.ts");

    var SharedDataService = /*#__PURE__*/function () {
      function SharedDataService(config, httpClient, storage, loading, appEventsService, platform, device, fcm, alertCtrl, appVersion, oneSignal, toastCtrl, splashScreen) {
        var _this40 = this;

        _classCallCheck(this, SharedDataService);

        this.config = config;
        this.httpClient = httpClient;
        this.storage = storage;
        this.loading = loading;
        this.appEventsService = appEventsService;
        this.platform = platform;
        this.device = device;
        this.fcm = fcm;
        this.alertCtrl = alertCtrl;
        this.appVersion = appVersion;
        this.oneSignal = oneSignal;
        this.toastCtrl = toastCtrl;
        this.splashScreen = splashScreen;
        this.banners = [];
        this.tab1 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        this.tab2 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        this.tab3 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        this.flashSaleProducts = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        this.allCategories = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        this.categories = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        this.subCategories = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        this.customerData = {};
        this.recentViewedProducts = new Array();
        this.cartProducts = new Array();
        this.privacyPolicy = "";
        this.termServices = "";
        this.refundPolicy = "";
        this.aboutUs = "";
        this.cartquantity = 0;
        this.wishList = new Array();
        this.tempdata = {};
        this.dir = "ltr";
        this.selectedFooterPage = "HomePage";
        this.currentOpenedModel = null;
        this.orderDetails = {
          guest_status: 0,
          email: "",
          tax_zone_id: "",
          delivery_firstname: "",
          delivery_lastname: "",
          delivery_state: "",
          delivery_city: "",
          delivery_postcode: "",
          delivery_zone: "139",
          delivery_country: "ESPAÑA",
          delivery_country_id: "195",
          delivery_street_address: "",
          delivery_country_code: "",
          delivery_phone: "",
          delivery_location: "",
          delivery_lat: "",
          delivery_long: "",
          billing_firstname: "",
          billing_lastname: "",
          billing_state: "",
          billing_city: "",
          billing_postcode: "",
          billing_zone: "139",
          billing_country: "ESPAÑA",
          billing_country_id: "195",
          billing_street_address: "",
          billing_country_code: "",
          billing_phone: "",
          billing_location: "",
          billing_lat: "",
          billing_long: "",
          total_tax: '',
          shipping_cost: '',
          shipping_method: '',
          payment_method: '',
          comments: ''
        };
        this.translationListArray = [];
        this.singleProductPageData = [];
        this.lab = false;
        this.missingValues = [];
        this.primaryHexColor = "#90bd30";
        this.splashScreenHide = false;
        setTimeout(function () {
          _this40.lab = true;
        }, 10000);
        this.platform.ready().then(function () {
          _this40.config.getHttp("applabels3?lang=" + _this40.config.langId).then(function (data) {
            _this40.translationListArray = data;
          });
        });
        var settingsLoaded = this.appEventsService.subscribe("settingsLoaded");
        settingsLoaded.subscriptions.add(settingsLoaded.event.subscribe(function (data) {
          _this40.onStart();
        })); //getting recent viewed items from local storage

        storage.get('customerData').then(function (val) {
          if (val != null || val != undefined) _this40.customerData = val;
        }); //getting recent viewed items from local storage

        storage.get('recentViewedProducts').then(function (val) {
          if (val != null) _this40.recentViewedProducts = val;
        });

        if (this.platform.is('cordova')) {} //getting recent viewed items from local storage


        storage.get('cartProducts').then(function (val) {
          if (val != null) _this40.cartProducts = val;

          _this40.cartTotalItems(); // console.log(val);

        }); //---------------- end -----------------
      }

      _createClass(SharedDataService, [{
        key: "hideSplashScreen",
        value: function hideSplashScreen() {
          if (this.platform.is('cordova')) {
            if (!this.splashScreenHide) {
              this.splashScreen.hide();
              this.splashScreenHide = true;
            }
          }
        }
      }, {
        key: "onStart",
        value: function onStart() {
          var _this41 = this;

          //getting all banners
          this.config.getHttp('getbanners?languages_id=' + this.config.langId).then(function (data) {
            if (data.success == "1") _this41.banners = data.data;
          }); //getting tab 1

          var data = {};
          if (this.customerData.customers_id != null) data.customers_id = this.customerData.customers_id;
          data.page_number = 0;
          data.language_id = this.config.langId;
          data.currency_code = this.config.currecnyCode;
          data.type = 'flashsale';
          this.config.postHttp('getallproducts', data).then(function (data) {
            _this41.flashSaleProducts = data.product_data;
          });
          data.type = 'top seller';
          this.config.postHttp('getallproducts', data).then(function (data) {
            _this41.tab1 = data.product_data;
          }); //getting tab 2

          data.type = 'special';
          this.config.postHttp('getallproducts', data).then(function (data) {
            _this41.tab2 = data.product_data;
          }); //getting tab 3

          data.type = 'most liked';
          this.config.postHttp('getallproducts', data).then(function (data) {
            _this41.tab3 = data.product_data;
          }); //getting all allCategories

          this.config.postHttp('allcategories', data).then(function (data) {
            if (_this41.allCategories[0] == 1) {
              _this41.allCategories = [];
              _this41.categories = [];
              _this41.subCategories = [];
            }

            var _iterator3 = _createForOfIteratorHelper(data.data),
                _step3;

            try {
              for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
                var value = _step3.value;
                value.id = value.categories_id;
                value.name = value.categories_name;

                _this41.allCategories.push(value);

                if (value.parent_id == 0) _this41.categories.push(value);else _this41.subCategories.push(value);
              }
            } catch (err) {
              _iterator3.e(err);
            } finally {
              _iterator3.f();
            }
          }); //getting allpages from the server

          this.config.postHttp('getallpages', {
            language_id: this.config.langId,
            currency_code: this.config.currecnyCode
          }).then(function (data) {
            if (data.success == 1) {
              var pages = data.pages_data;

              var _iterator4 = _createForOfIteratorHelper(pages),
                  _step4;

              try {
                for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
                  var value = _step4.value;
                  if (value.slug == 'privacy-policy') _this41.privacyPolicy = value.description;
                  if (value.slug == 'term-services') _this41.termServices = value.description;
                  if (value.slug == 'refund-policy') _this41.refundPolicy = value.description;
                  if (value.slug == 'about-us') _this41.aboutUs = value.description;
                }
              } catch (err) {
                _iterator4.e(err);
              } finally {
                _iterator4.f();
              }
            }
          });
        } //adding into recent array products

      }, {
        key: "addToRecent",
        value: function addToRecent(p) {
          var found = false;

          var _iterator5 = _createForOfIteratorHelper(this.recentViewedProducts),
              _step5;

          try {
            for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
              var value = _step5.value;

              if (value.products_id == p.products_id) {
                found = true;
              }
            }
          } catch (err) {
            _iterator5.e(err);
          } finally {
            _iterator5.f();
          }

          if (found == false) {
            this.recentViewedProducts.push(p);
            this.storage.set('recentViewedProducts', this.recentViewedProducts);
          }
        } //removing from recent array products

      }, {
        key: "removeRecent",
        value: function removeRecent(p) {
          var _this42 = this;

          this.recentViewedProducts.forEach(function (value, index) {
            if (value.products_id == p.products_id) {
              _this42.recentViewedProducts.splice(index, 1);

              _this42.storage.set('recentViewedProducts', _this42.recentViewedProducts);
            }
          });
          this.appEventsService.publish('recentDeleted', "");
        } //adding into cart array products

      }, {
        key: "addToCart",
        value: function addToCart(product, attArray) {
          var _this43 = this;

          // console.log(this.cartProducts);
          var attributesArray = attArray;

          if (attArray.length == 0 || attArray == null) {
            //console.log("filling attirbutes");
            attributesArray = [];

            if (product.attributes != undefined) {
              // console.log("filling product default attibutes");
              product.attributes.forEach(function (value, index) {
                var att = {
                  products_options_id: value.option.id,
                  products_options: value.option.name,
                  products_options_values_id: value.values[0].id,
                  options_values_price: value.values[0].price,
                  price_prefix: value.values[0].price_prefix,
                  products_options_values: value.values[0].value,
                  name: value.values[0].value + ' ' + value.values[0].price_prefix + value.values[0].price + " " + _this43.config.currency
                };
                attributesArray.push(att);
              });
            }
          } //  if(checkDublicateService(product.products_id,$rootScope.cartProducts)==false){


          var pprice = product.products_price;
          var on_sale = false;

          if (product.discount_price != null) {
            pprice = product.discount_price;
            on_sale = true;
          }

          if (product.flash_price != null) {
            pprice = product.flash_price;
          } // console.log("in side producs detail");
          // console.log(attributesArray);
          // console.log(this.cartProducts);


          var finalPrice = this.calculateFinalPriceService(attributesArray) + parseFloat(pprice);
          var cartQuantiyToAdd = 1;
          if (product.cartQuantity) cartQuantiyToAdd = product.cartQuantity;
          var obj = {
            cart_id: product.products_id + this.cartProducts.length,
            products_id: product.products_id,
            manufacture: product.manufacturers_name,
            customers_basket_quantity: cartQuantiyToAdd,
            final_price: finalPrice,
            model: product.products_model,
            categories: product.categories,
            // categories_id: product.categories_id,
            // categories_name: product.categories_name,
            quantity: product.products_max_stock,
            weight: product.products_weight,
            on_sale: on_sale,
            unit: product.products_weight_unit,
            image: product.products_image,
            attributes: attributesArray,
            products_name: product.products_name,
            price: pprice,
            subtotal: cartQuantiyToAdd * finalPrice,
            total: cartQuantiyToAdd * finalPrice
          };
          this.cartProducts.push(obj);
          this.storage.set('cartProducts', this.cartProducts);
          this.cartTotalItems(); // console.log(this.cartProducts);
          //console.log(this.cartProducts);
        } //removing from recent array products

      }, {
        key: "removeCart",
        value: function removeCart(p) {
          var _this44 = this;

          this.cartProducts.forEach(function (value, index) {
            if (value.cart_id == p) {
              _this44.cartProducts.splice(index, 1);

              _this44.storage.set('cartProducts', _this44.cartProducts);
            }
          });
          this.cartTotalItems();
        }
      }, {
        key: "emptyCart",
        value: function emptyCart() {
          this.orderDetails.guest_status = 0;
          this.cartProducts = [];
          this.storage.set('cartProducts', this.cartProducts);
          this.cartTotalItems();
        }
      }, {
        key: "emptyRecentViewed",
        value: function emptyRecentViewed() {
          this.recentViewedProducts = [];
          this.storage.set('recentViewedProducts', this.recentViewedProducts);
        }
      }, {
        key: "calculateFinalPriceService",
        value: function calculateFinalPriceService(attArray) {
          var total = 0;
          attArray.forEach(function (value, index) {
            var attPrice = parseFloat(value.options_values_price);

            if (value.price_prefix == '+') {
              //  console.log('+');
              total += attPrice;
            } else {
              //  console.log('-');
              total -= attPrice;
            }
          }); // console.log("max "+total);

          return total;
        } //Function calcualte the total items of cart

      }, {
        key: "cartTotalItems",
        value: function cartTotalItems() {
          var total = 0;

          var _iterator6 = _createForOfIteratorHelper(this.cartProducts),
              _step6;

          try {
            for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
              var value = _step6.value;
              total += parseInt(value.customers_basket_quantity);
            }
          } catch (err) {
            _iterator6.e(err);
          } finally {
            _iterator6.f();
          }

          this.cartquantity = total; //console.log("updated", this.cartquantity);

          return total;
        }
      }, {
        key: "removeWishList",
        value: function removeWishList(p) {
          var _this45 = this;

          this.loading.show();
          var data = {};
          data.liked_customers_id = this.customerData.customers_id;
          data.liked_products_id = p.products_id;
          this.config.postHttp('unlikeproduct', data).then(function (data) {
            _this45.loading.hide();

            if (data.success == 1) {
              _this45.appEventsService.publish('wishListUpdate', {
                id: p.products_id,
                value: 0
              });

              p.isLiked = 0;

              _this45.wishList.forEach(function (value, index) {
                if (value.products_id == p.products_id) _this45.wishList.splice(index, 1);
              });
            }

            if (data.success == 0) {}
          });
        }
      }, {
        key: "addWishList",
        value: function addWishList(p) {
          var _this46 = this;

          this.loading.show();
          var data = {};
          data.liked_customers_id = this.customerData.customers_id;
          data.liked_products_id = p.products_id;
          this.config.postHttp('likeproduct', data).then(function (data) {
            _this46.loading.hide();

            if (data.success == 1) {
              _this46.appEventsService.publish('wishListUpdate', {
                id: p.products_id,
                value: 1
              });

              p.isLiked = 1;
            }

            if (data.success == 0) {}
          });
        }
      }, {
        key: "login",
        value: function login(data) {
          this.customerData = data;
          this.customerData.customers_telephone = data.phone;
          this.customerData.phone = data.phone;
          this.customerData.customers_id = data.id;
          this.customerData.customers_firstname = data.first_name;
          this.customerData.customers_lastname = data.last_name;
          this.customerData.phone = data.phone;
          this.customerData.avatar = data.avatar;
          this.customerData.image_id = data.image_id;
          this.customerData.customers_dob = data.dob;
          this.storage.set('customerData', this.customerData);
          this.subscribePush();
          console.log(this.customerData);
        }
      }, {
        key: "logOut",
        value: function logOut() {
          this.loading.autoHide(500);
          this.customerData = {};
          this.storage.set('customerData', this.customerData); // this.fb.logout();
        } //============================================================================================
        //getting token and passing to server

      }, {
        key: "subscribePush",
        value: function subscribePush() {
          var _this47 = this;

          if (this.platform.is('cordova')) {
            // pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
            if (this.config.notificationType == "fcm") {
              try {
                this.fcm.subscribeToTopic('marketing');
                this.fcm.getToken().then(function (token) {
                  //alert("registration" + token);
                  console.log(token); //this.storage.set('registrationId', token);

                  _this47.registerDevice(token);
                });
                this.fcm.onNotification().subscribe(function (data) {
                  if (data.wasTapped) {
                    console.log("Received in background");
                  } else {
                    console.log("Received in foreground");
                  }

                  ;
                });
                this.fcm.onTokenRefresh().subscribe(function (token) {
                  // this.storage.set('registrationId', token);
                  _this47.registerDevice(token);
                });
              } catch (error) {}
            } else if (this.config.notificationType == "onesignal") {
              this.oneSignal.startInit(this.config.onesignalAppId, this.config.onesignalSenderId);
              this.oneSignal.endInit();
              this.oneSignal.handleNotificationReceived().subscribe(function () {// do something when notification is received
              });
              this.oneSignal.getIds().then(function (data) {
                _this47.registerDevice(data.userId); //alert(data.userId);

              });
            }
          }
        } //============================================================================================
        //registering device for push notification function

      }, {
        key: "registerDevice",
        value: function registerDevice(registrationId) {
          //this.storage.get('registrationId').then((registrationId) => {
          if (registrationId === null || registrationId === "") return;
          console.log(registrationId);
          var data = {};
          if (this.customerData.customers_id == null) data.customers_id = null;else data.customers_id = this.customerData.customers_id; //	alert("device ready fired");

          var deviceInfo = this.device;
          data.device_model = deviceInfo.model;
          data.device_type = deviceInfo.platform;
          data.device_id = registrationId;
          data.device_os = deviceInfo.version;
          data.manufacturer = deviceInfo.manufacturer;
          data.ram = '2gb';
          data.processor = 'mediatek';
          data.location = 'empty'; //alert(JSON.stringify(data));
          //alert(data);

          this.config.postHttp("registerdevices", data).then(function (data) {//alert(registrationId + " " + JSON.stringify(data));
          })["catch"](); //  });
        }
      }, {
        key: "showAd",
        value: function showAd() {
          //this.loading.autoHide(2000);
          this.appEventsService.publish('showAd', "");
        }
      }, {
        key: "toast",
        value: function toast(msg) {
          var _this48 = this;

          var time = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 3500;
          this.translateString(msg).then(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this48, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee21() {
              var toast;
              return regeneratorRuntime.wrap(function _callee21$(_context21) {
                while (1) {
                  switch (_context21.prev = _context21.next) {
                    case 0:
                      _context21.next = 2;
                      return this.toastCtrl.create({
                        message: res,
                        duration: time,
                        position: 'bottom'
                      });

                    case 2:
                      toast = _context21.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context21.stop();
                  }
                }
              }, _callee21, this);
            }));
          });
        }
      }, {
        key: "toastMiddle",
        value: function toastMiddle(msg) {
          var _this49 = this;

          this.translateString(msg).then(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this49, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee22() {
              var toast;
              return regeneratorRuntime.wrap(function _callee22$(_context22) {
                while (1) {
                  switch (_context22.prev = _context22.next) {
                    case 0:
                      _context22.next = 2;
                      return this.toastCtrl.create({
                        message: res,
                        duration: 3500,
                        position: 'middle'
                      });

                    case 2:
                      toast = _context22.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context22.stop();
                  }
                }
              }, _callee22, this);
            }));
          });
        }
      }, {
        key: "toastWithCloseButton",
        value: function toastWithCloseButton(msg) {
          var _this50 = this;

          this.translateString(msg).then(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this50, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee23() {
              var toast;
              return regeneratorRuntime.wrap(function _callee23$(_context23) {
                while (1) {
                  switch (_context23.prev = _context23.next) {
                    case 0:
                      _context23.next = 2;
                      return this.toastCtrl.create({
                        message: res,
                        keyboardClose: true,
                        position: 'middle'
                      });

                    case 2:
                      toast = _context23.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context23.stop();
                  }
                }
              }, _callee23, this);
            }));
          });
        } //categories page

      }, {
        key: "getCategoriesPageItems",
        value: function getCategoriesPageItems(parent) {
          var c = [];
          if (parent == undefined) c = this.categories;else {
            var _iterator7 = _createForOfIteratorHelper(this.allCategories),
                _step7;

            try {
              for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                var v = _step7.value;

                if (v.parent == parent) {
                  c.push(v);
                }
              }
            } catch (err) {
              _iterator7.e(err);
            } finally {
              _iterator7.f();
            }
          }
          return c;
        } // translation services

      }, {
        key: "translateString",
        value: function translateString(value) {
          var _this51 = this;

          return new Promise(function (resolve) {
            var v = _this51.translationListArray[value];
            console.log(v);

            if (v == undefined) {
              _this51.missingValues[value] = value;
              v = value;
            }

            resolve(v);
          });
        }
      }, {
        key: "translateArray",
        value: function translateArray(value) {
          var _this52 = this;

          return new Promise(function (resolve) {
            var tempArray = [];
            value.forEach(function (element) {
              if (_this52.translationListArray[element] != undefined) {
                tempArray[element] = _this52.translationListArray[element];
              } else {
                tempArray[element] = element;
                _this52.missingValues[value] = value;
              }
            });
            resolve(tempArray);
          });
        } //=================================================

      }, {
        key: "showAlert",
        value: function showAlert(text) {
          var _this53 = this;

          this.translateArray([text, "ok", "Alert"]).then(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this53, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee24() {
              var alert;
              return regeneratorRuntime.wrap(function _callee24$(_context24) {
                while (1) {
                  switch (_context24.prev = _context24.next) {
                    case 0:
                      console.log(res);
                      _context24.next = 3;
                      return this.alertCtrl.create({
                        header: res["Alert"],
                        message: res[text],
                        buttons: [res["ok"]]
                      });

                    case 3:
                      alert = _context24.sent;
                      _context24.next = 6;
                      return alert.present();

                    case 6:
                    case "end":
                      return _context24.stop();
                  }
                }
              }, _callee24, this);
            }));
          });
        }
      }, {
        key: "showAlertWithTitle",
        value: function showAlertWithTitle(text, title) {
          var _this54 = this;

          this.translateArray([text, "ok", title]).then(function (res) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this54, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee25() {
              var alert;
              return regeneratorRuntime.wrap(function _callee25$(_context25) {
                while (1) {
                  switch (_context25.prev = _context25.next) {
                    case 0:
                      _context25.next = 2;
                      return this.alertCtrl.create({
                        header: res[title],
                        message: res[text],
                        buttons: [res["ok"]]
                      });

                    case 2:
                      alert = _context25.sent;
                      _context25.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context25.stop();
                  }
                }
              }, _callee25, this);
            }));
          });
        }
      }, {
        key: "getNameFirstLetter",
        value: function getNameFirstLetter() {
          return this.customerData.first_name.charAt(0);
        }
      }, {
        key: "getProductRatingPercentage",
        value: function getProductRatingPercentage(rating) {
          var val = parseFloat(rating) * 100 / 5;
          return val + '%';
        }
      }]);

      return SharedDataService;
    }();

    SharedDataService.ctorParameters = function () {
      return [{
        type: _config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }, {
        type: _loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"]
      }, {
        type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_12__["AppEventsService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"]
      }, {
        type: _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_11__["Device"]
      }, {
        type: _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_10__["FCM"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"]
      }, {
        type: _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_9__["AppVersion"]
      }, {
        type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_7__["OneSignal"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"]
      }, {
        type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_8__["SplashScreen"]
      }];
    };

    SharedDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"], src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_12__["AppEventsService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"], _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_11__["Device"], _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_10__["FCM"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_9__["AppVersion"], _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_7__["OneSignal"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"], _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_8__["SplashScreen"]])], SharedDataService);
    /***/
  },

  /***/
  0:
  /*!***************************!*\
    !*** multi ./src/main.ts ***!
    \***************************/

  /*! no static exports found */

  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__(
    /*! /Users/sato/StudioProjects/deliverycustomer/src/main.ts */
    "./src/main.ts");
    /***/
  }
}, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es5.js.map