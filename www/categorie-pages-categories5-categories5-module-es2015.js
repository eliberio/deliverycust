(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["categorie-pages-categories5-categories5-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/categorie-pages/categories5/categories5.page.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/categorie-pages/categories5/categories5.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-title>\r\n      {{\"Categories\"| translate }}\r\n    </ion-title>\r\n\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-item-group *ngFor=\"let c of shared.categories\" class=\"animated fadeIn\">\r\n    <ion-item-divider (click)=\"viewAll(c)\">\r\n      <ion-avatar slot=\"end\">\r\n        <img *ngIf=\"c.image\" src=\"{{config.imgUrl+c.image}}\" />\r\n      </ion-avatar>\r\n      <ion-label class=\"ion-padding\">\r\n        <ion-text>\r\n          <h2>{{c.name}}</h2>\r\n        </ion-text>\r\n      </ion-label>\r\n    </ion-item-divider>\r\n    <div *ngFor=\"let s of shared.subCategories\">\r\n      <ion-item *ngIf=\"c.id==s.parent_id\" (click)=\"viewAll(s)\">\r\n        <ion-avatar slot=\"end\">\r\n          <img *ngIf=\"s.image\" src=\"{{config.imgUrl+s.image}}\" />\r\n        </ion-avatar>\r\n        <ion-label class=\"ion-padding\">\r\n          <ion-text>\r\n            <h2>{{s.name|translate}}</h2>\r\n            <p>{{s.total_products}} {{'Products'| translate }}</p>\r\n          </ion-text>\r\n        </ion-label>\r\n      </ion-item>\r\n    </div>\r\n  </ion-item-group>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/categorie-pages/categories5/categories5.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/categorie-pages/categories5/categories5.module.ts ***!
  \*******************************************************************/
/*! exports provided: Categories5PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Categories5PageModule", function() { return Categories5PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _categories5_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./categories5.page */ "./src/app/categorie-pages/categories5/categories5.page.ts");
/* harmony import */ var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/pipes/pipes.module */ "./src/pipes/pipes.module.ts");








const routes = [
    {
        path: '',
        component: _categories5_page__WEBPACK_IMPORTED_MODULE_6__["Categories5Page"]
    }
];
let Categories5PageModule = class Categories5PageModule {
};
Categories5PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]
        ],
        declarations: [_categories5_page__WEBPACK_IMPORTED_MODULE_6__["Categories5Page"]]
    })
], Categories5PageModule);



/***/ }),

/***/ "./src/app/categorie-pages/categories5/categories5.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/categorie-pages/categories5/categories5.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content p {\n  font-size: var(--sub-heading-font-size);\n}\nion-content ion-item-divider {\n  --background: var(--ion-color-light-shade);\n}\nion-content ion-item-divider ion-avatar {\n  margin-right: 16px;\n}\nion-content ion-item-divider ion-label {\n  padding-bottom: 0;\n  padding-top: 0;\n}\nion-content ion-item-divider ion-label h2 {\n  font-weight: bold;\n  font-size: var(--heading-font-size);\n  color: var(--ion-color-light-contrast);\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\nion-content ion-item ion-label {\n  padding-bottom: 0;\n  padding-top: 0;\n}\nion-content ion-item ion-label h2 {\n  font-size: 14px;\n  font-weight: 400;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9jYXRlZ29yaWUtcGFnZXMvY2F0ZWdvcmllczUvY2F0ZWdvcmllczUucGFnZS5zY3NzIiwic3JjL2FwcC9jYXRlZ29yaWUtcGFnZXMvY2F0ZWdvcmllczUvY2F0ZWdvcmllczUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsdUNBQUE7QUNBSjtBREVFO0VBQ0UsMENBQUE7QUNBSjtBRENJO0VBQ0Usa0JBQUE7QUNDTjtBRENJO0VBQ0UsaUJBQUE7RUFDQSxjQUFBO0FDQ047QURBTTtFQUNFLGlCQUFBO0VBQ0EsbUNBQUE7RUFDQSxzQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtBQ0VSO0FER0k7RUFDRSxpQkFBQTtFQUNBLGNBQUE7QUNETjtBREVNO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0FDQVIiLCJmaWxlIjoic3JjL2FwcC9jYXRlZ29yaWUtcGFnZXMvY2F0ZWdvcmllczUvY2F0ZWdvcmllczUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gIHAge1xyXG4gICAgZm9udC1zaXplOiB2YXIoLS1zdWItaGVhZGluZy1mb250LXNpemUpO1xyXG4gIH1cclxuICBpb24taXRlbS1kaXZpZGVyIHtcclxuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0LXNoYWRlKTtcclxuICAgIGlvbi1hdmF0YXIge1xyXG4gICAgICBtYXJnaW4tcmlnaHQ6IDE2cHg7XHJcbiAgICB9XHJcbiAgICBpb24tbGFiZWwge1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMDtcclxuICAgICAgcGFkZGluZy10b3A6IDA7XHJcbiAgICAgIGgyIHtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBmb250LXNpemU6IHZhcigtLWhlYWRpbmctZm9udC1zaXplKTtcclxuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0LWNvbnRyYXN0KTtcclxuICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgaW9uLWl0ZW0ge1xyXG4gICAgaW9uLWxhYmVsIHtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDA7XHJcbiAgICAgIHBhZGRpbmctdG9wOiAwO1xyXG4gICAgICBoMiB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiaW9uLWNvbnRlbnQgcCB7XG4gIGZvbnQtc2l6ZTogdmFyKC0tc3ViLWhlYWRpbmctZm9udC1zaXplKTtcbn1cbmlvbi1jb250ZW50IGlvbi1pdGVtLWRpdmlkZXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodC1zaGFkZSk7XG59XG5pb24tY29udGVudCBpb24taXRlbS1kaXZpZGVyIGlvbi1hdmF0YXIge1xuICBtYXJnaW4tcmlnaHQ6IDE2cHg7XG59XG5pb24tY29udGVudCBpb24taXRlbS1kaXZpZGVyIGlvbi1sYWJlbCB7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBwYWRkaW5nLXRvcDogMDtcbn1cbmlvbi1jb250ZW50IGlvbi1pdGVtLWRpdmlkZXIgaW9uLWxhYmVsIGgyIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogdmFyKC0taGVhZGluZy1mb250LXNpemUpO1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0LWNvbnRyYXN0KTtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG59XG5pb24tY29udGVudCBpb24taXRlbSBpb24tbGFiZWwge1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgcGFkZGluZy10b3A6IDA7XG59XG5pb24tY29udGVudCBpb24taXRlbSBpb24tbGFiZWwgaDIge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG59Il19 */");

/***/ }),

/***/ "./src/app/categorie-pages/categories5/categories5.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/categorie-pages/categories5/categories5.page.ts ***!
  \*****************************************************************/
/*! exports provided: Categories5Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Categories5Page", function() { return Categories5Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/providers/shared-data/shared-data.service */ "./src/providers/shared-data/shared-data.service.ts");
/* harmony import */ var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/providers/config/config.service */ "./src/providers/config/config.service.ts");





let Categories5Page = class Categories5Page {
    constructor(shared, config, router) {
        this.shared = shared;
        this.config = config;
        this.router = router;
    }
    ngOnInit() {
    }
    viewAll(c) {
        this.router.navigateByUrl(this.config.currentRoute + "/products/" + c.id + "/" + c.name + "/newest");
    }
};
Categories5Page.ctorParameters = () => [
    { type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"] },
    { type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
Categories5Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-categories5',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./categories5.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/categorie-pages/categories5/categories5.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./categories5.page.scss */ "./src/app/categorie-pages/categories5/categories5.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"],
        src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], Categories5Page);



/***/ })

}]);
//# sourceMappingURL=categorie-pages-categories5-categories5-module-es2015.js.map