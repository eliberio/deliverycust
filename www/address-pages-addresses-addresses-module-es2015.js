(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["address-pages-addresses-addresses-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/address-pages/addresses/addresses.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/address-pages/addresses/addresses.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title> {{'Address'| translate }} </ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button fill=\"clear\" routerLink=\"/cart\" routerDirection=\"forward\">\r\n        <ion-icon name=\"basket\"></ion-icon>\r\n        <ion-badge>{{shared.cartTotalItems()}}</ion-badge>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <div *ngIf=\"allShippingAddress.length==0\" class=\"ion-text-center ion-padding\">\r\n    {{'Please add your new shipping address for the futher processing of the your order'|translate}}\r\n  </div>\r\n  <ion-list>\r\n    <ion-item *ngFor=\"let address of allShippingAddress\">\r\n\r\n      <ion-icon name=\"create\" slot=\"start\" (click)=\"openEditShippingPage(address)\"></ion-icon>\r\n      {{address.street+', '+ address.city+' '+address.postcode+', '+address.country_name}}\r\n\r\n      <ion-icon name=\"radio-button-off\" *ngIf=\"address.default_address==0\" (click)=\"defaultAddress(address.address_id)\"\r\n        slot=\"end\"></ion-icon>\r\n      <ion-icon name=\"radio-button-on\" *ngIf=\"address.default_address==1\" slot=\"end\"></ion-icon>\r\n\r\n    </ion-item>\r\n  </ion-list>\r\n  <ion-button expand=\"full\" color=\"secondary\" (click)=\"addShippingAddress()\">{{'Add New Address'|translate}}\r\n  </ion-button>\r\n\r\n</ion-content>");

/***/ }),

/***/ "./src/app/address-pages/addresses/addresses.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/address-pages/addresses/addresses.module.ts ***!
  \*************************************************************/
/*! exports provided: AddressesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressesPageModule", function() { return AddressesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _addresses_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./addresses.page */ "./src/app/address-pages/addresses/addresses.page.ts");
/* harmony import */ var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/pipes/pipes.module */ "./src/pipes/pipes.module.ts");







// For Translation Language

const routes = [
    {
        path: '',
        component: _addresses_page__WEBPACK_IMPORTED_MODULE_6__["AddressesPage"]
    }
];
let AddressesPageModule = class AddressesPageModule {
};
AddressesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]
        ],
        declarations: [_addresses_page__WEBPACK_IMPORTED_MODULE_6__["AddressesPage"]]
    })
], AddressesPageModule);



/***/ }),

/***/ "./src/app/address-pages/addresses/addresses.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/address-pages/addresses/addresses.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content p {\n  font-size: 20px;\n  text-align: center;\n}\nion-content form ion-item {\n  --background: var(--ion-background-color);\n}\nion-content form ion-item ion-label {\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9hZGRyZXNzLXBhZ2VzL2FkZHJlc3Nlcy9hZGRyZXNzZXMucGFnZS5zY3NzIiwic3JjL2FwcC9hZGRyZXNzLXBhZ2VzL2FkZHJlc3Nlcy9hZGRyZXNzZXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0FDQVI7QURHUTtFQUNJLHlDQUFBO0FDRFo7QURFWTtFQUNJLDJDQUFBO0FDQWhCIiwiZmlsZSI6InNyYy9hcHAvYWRkcmVzcy1wYWdlcy9hZGRyZXNzZXMvYWRkcmVzc2VzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICAgIHAge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICBmb3JtIHtcclxuICAgICAgICBpb24taXRlbSB7XHJcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IpO1xyXG4gICAgICAgICAgICBpb24tbGFiZWwge1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHJnYmEodmFyKC0taW9uLXRleHQtY29sb3ItcmdiKSwgMC41KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iLCJpb24tY29udGVudCBwIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5pb24tY29udGVudCBmb3JtIGlvbi1pdGVtIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XG59XG5pb24tY29udGVudCBmb3JtIGlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gIGNvbG9yOiByZ2JhKHZhcigtLWlvbi10ZXh0LWNvbG9yLXJnYiksIDAuNSk7XG59Il19 */");

/***/ }),

/***/ "./src/app/address-pages/addresses/addresses.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/address-pages/addresses/addresses.page.ts ***!
  \***********************************************************/
/*! exports provided: AddressesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressesPage", function() { return AddressesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/providers/config/config.service */ "./src/providers/config/config.service.ts");
/* harmony import */ var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/providers/shared-data/shared-data.service */ "./src/providers/shared-data/shared-data.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/providers/loading/loading.service */ "./src/providers/loading/loading.service.ts");
/* harmony import */ var src_app_modals_edit_address_edit_address_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/modals/edit-address/edit-address.page */ "./src/app/modals/edit-address/edit-address.page.ts");
/* harmony import */ var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/providers/app-events/app-events.service */ "./src/providers/app-events/app-events.service.ts");









let AddressesPage = class AddressesPage {
    constructor(navCtrl, shared, modalCtrl, config, storage, appEventsService, loading) {
        this.navCtrl = navCtrl;
        this.shared = shared;
        this.modalCtrl = modalCtrl;
        this.config = config;
        this.storage = storage;
        this.appEventsService = appEventsService;
        this.loading = loading;
        this.allShippingAddress = new Array;
        //============================================================================================  
        // delete shipping address
        this.deleteAddress = function (id) {
            this.loading.show();
            var dat = {
                customers_id: this.shared.customerData.customers_id,
                address_book_id: id
            };
            this.config.postHttp('deleteshippingaddress', dat).then((data) => {
                this.loading.hide();
                if (data.success == 1) {
                    this.getAllAddress();
                }
            }, function (response) {
                this.loading.hide();
                this.shared.toast("Error server not reponding");
            });
        };
        //============================================================================================  
        // default shipping address
        this.defaultAddress = function (id) {
            this.loading.show();
            var dat = {
                customers_id: this.shared.customerData.customers_id,
                address_book_id: id
            };
            this.config.postHttp('updatedefaultaddress', dat).then((data) => {
                this.loading.hide();
                if (data.success == 1) {
                }
                this.getAllAddress();
            }, function (response) {
                this.loading.hide();
                this.shared.toast("Error server not reponding");
            });
        };
    }
    getAllAddress() {
        this.loading.show();
        var dat = { customers_id: this.shared.customerData.customers_id };
        this.config.postHttp('getalladdress', dat).then((data) => {
            this.loading.hide();
            if (data.success == 1) {
                this.allShippingAddress = data.data;
            }
        });
    }
    openEditShippingPage(data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let modal = yield this.modalCtrl.create({
                component: src_app_modals_edit_address_edit_address_page__WEBPACK_IMPORTED_MODULE_7__["EditAddressPage"],
                componentProps: { data: data, type: 'update' }
            });
            modal.onDidDismiss().then(() => {
                this.getAllAddress();
            });
            return yield modal.present();
        });
    }
    addShippingAddress() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let modal = yield this.modalCtrl.create({
                component: src_app_modals_edit_address_edit_address_page__WEBPACK_IMPORTED_MODULE_7__["EditAddressPage"],
                componentProps: { type: 'add' }
            });
            modal.onDidDismiss().then(() => {
                this.getAllAddress();
            });
            return yield modal.present();
        });
    }
    ionViewWillEnter() {
        this.getAllAddress();
    }
    ngOnInit() {
    }
};
AddressesPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] },
    { type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"] },
    { type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_8__["AppEventsService"] },
    { type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"] }
];
AddressesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-addresses',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./addresses.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/address-pages/addresses/addresses.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./addresses.page.scss */ "./src/app/address-pages/addresses/addresses.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"],
        src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"],
        src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"],
        src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_8__["AppEventsService"],
        src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"]])
], AddressesPage);



/***/ })

}]);
//# sourceMappingURL=address-pages-addresses-addresses-module-es2015.js.map