(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["categorie-pages-categories2-categories2-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/categorie-pages/categories2/categories2.page.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/categorie-pages/categories2/categories2.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" *ngIf=\"parent.name!='Categories'\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-menu-button slot=\"start\" *ngIf=\"parent.name=='Categories' && !config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-title>\r\n      {{parent.name| translate }}\r\n    </ion-title>\r\n    \r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <ion-item lines=\"full\" *ngFor=\"let c of getCategories()\" (click)=\"openSubCategories(c)\" class=\"animate-item\">\r\n    <ion-thumbnail slot=\"start\" class=\"animate-item delay-500ms\">\r\n      <img *ngIf=\"c.image\" src=\"{{config.imgUrl+c.image}}\" />\r\n    </ion-thumbnail>\r\n    <ion-label class=\"ion-padding\">\r\n      <ion-text>\r\n        <h2>{{c.name|translate}}</h2>\r\n        <p>{{c.total_products}} {{'Products'| translate }}</p>\r\n      </ion-text>\r\n    </ion-label>\r\n  </ion-item>\r\n  <div class=\"ion-text-center\">\r\n    <ion-button *ngIf=\"parent.id!=0\" icon-end clear color=\"secondary\" (click)=\"viewAll()\">\r\n      {{ 'View All' | translate }}\r\n      <ion-icon name=\"caret-forward\"></ion-icon>\r\n    </ion-button>\r\n  </div>\r\n\r\n</ion-content>");

/***/ }),

/***/ "./src/app/categorie-pages/categories2/categories2.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/categorie-pages/categories2/categories2.module.ts ***!
  \*******************************************************************/
/*! exports provided: Categories2PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Categories2PageModule", function() { return Categories2PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _categories2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./categories2.page */ "./src/app/categorie-pages/categories2/categories2.page.ts");
/* harmony import */ var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/pipes/pipes.module */ "./src/pipes/pipes.module.ts");








const routes = [
    {
        path: '',
        component: _categories2_page__WEBPACK_IMPORTED_MODULE_6__["Categories2Page"]
    }
];
let Categories2PageModule = class Categories2PageModule {
};
Categories2PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]
        ],
        declarations: [_categories2_page__WEBPACK_IMPORTED_MODULE_6__["Categories2Page"]]
    })
], Categories2PageModule);



/***/ }),

/***/ "./src/app/categorie-pages/categories2/categories2.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/categorie-pages/categories2/categories2.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content ion-item {\n  --padding-start: 0;\n}\nion-content ion-item ion-thumbnail {\n  height: 140px;\n  width: 25%;\n  margin: 0;\n}\nion-content ion-item ion-label h2 {\n  color: var(--ion-text-color);\n  font-size: var(--heading-font-size);\n  font-weight: 400;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\nion-content ion-item ion-label p {\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n  font-size: var(--sub-heading-font-size);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9jYXRlZ29yaWUtcGFnZXMvY2F0ZWdvcmllczIvY2F0ZWdvcmllczIucGFnZS5zY3NzIiwic3JjL2FwcC9jYXRlZ29yaWUtcGFnZXMvY2F0ZWdvcmllczIvY2F0ZWdvcmllczIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0Usa0JBQUE7QUNBSjtBRENJO0VBQ0UsYUFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0FDQ047QURFTTtFQUNFLDRCQUFBO0VBQ0EsbUNBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtBQ0FSO0FERU07RUFDRSwyQ0FBQTtFQUNBLHVDQUFBO0FDQVIiLCJmaWxlIjoic3JjL2FwcC9jYXRlZ29yaWUtcGFnZXMvY2F0ZWdvcmllczIvY2F0ZWdvcmllczIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gIGlvbi1pdGVtIHtcclxuICAgIC0tcGFkZGluZy1zdGFydDogMDtcclxuICAgIGlvbi10aHVtYm5haWwge1xyXG4gICAgICBoZWlnaHQ6IDE0MHB4O1xyXG4gICAgICB3aWR0aDogMjUlO1xyXG4gICAgICBtYXJnaW46IDA7XHJcbiAgICB9XHJcbiAgICBpb24tbGFiZWwge1xyXG4gICAgICBoMiB7XHJcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi10ZXh0LWNvbG9yKTtcclxuICAgICAgICBmb250LXNpemU6IHZhcigtLWhlYWRpbmctZm9udC1zaXplKTtcclxuICAgICAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgICAgfVxyXG4gICAgICBwIHtcclxuICAgICAgICBjb2xvcjogcmdiYSh2YXIoLS1pb24tdGV4dC1jb2xvci1yZ2IpLCAwLjUpO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogdmFyKC0tc3ViLWhlYWRpbmctZm9udC1zaXplKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iLCJpb24tY29udGVudCBpb24taXRlbSB7XG4gIC0tcGFkZGluZy1zdGFydDogMDtcbn1cbmlvbi1jb250ZW50IGlvbi1pdGVtIGlvbi10aHVtYm5haWwge1xuICBoZWlnaHQ6IDE0MHB4O1xuICB3aWR0aDogMjUlO1xuICBtYXJnaW46IDA7XG59XG5pb24tY29udGVudCBpb24taXRlbSBpb24tbGFiZWwgaDIge1xuICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xuICBmb250LXNpemU6IHZhcigtLWhlYWRpbmctZm9udC1zaXplKTtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG59XG5pb24tY29udGVudCBpb24taXRlbSBpb24tbGFiZWwgcCB7XG4gIGNvbG9yOiByZ2JhKHZhcigtLWlvbi10ZXh0LWNvbG9yLXJnYiksIDAuNSk7XG4gIGZvbnQtc2l6ZTogdmFyKC0tc3ViLWhlYWRpbmctZm9udC1zaXplKTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/categorie-pages/categories2/categories2.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/categorie-pages/categories2/categories2.page.ts ***!
  \*****************************************************************/
/*! exports provided: Categories2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Categories2Page", function() { return Categories2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/providers/shared-data/shared-data.service */ "./src/providers/shared-data/shared-data.service.ts");
/* harmony import */ var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/providers/config/config.service */ "./src/providers/config/config.service.ts");





let Categories2Page = class Categories2Page {
    constructor(shared, config, router, activatedRoute) {
        this.shared = shared;
        this.config = config;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.categories = [];
        this.parent = {};
        this.parent.id = this.activatedRoute.snapshot.paramMap.get('parent');
        this.parent.name = this.activatedRoute.snapshot.paramMap.get('name');
        if (this.parent.id == undefined)
            this.parent.id = 0;
        if (this.parent.name == undefined)
            this.parent.name = 0;
        if (this.parent.name == 0)
            this.parent.name = "Categories";
    }
    getCategories() {
        let cat = [];
        for (let value of this.shared.allCategories) {
            if (value.parent_id == this.parent.id) {
                cat.push(value);
            }
        }
        return cat;
    }
    openSubCategories(parent) {
        let count = 0;
        for (let value of this.shared.allCategories) {
            if (parent.id == value.parent_id)
                count++;
        }
        if (count != 0)
            this.router.navigateByUrl(this.config.currentRoute + "/categories2/" + parent.id + "/" + parent.name);
        else
            this.router.navigateByUrl(this.config.currentRoute + "/products/" + parent.id + "/" + parent.name + "/newest");
    }
    viewAll() {
        this.router.navigateByUrl(this.config.currentRoute + "/products/" + this.parent.id + "/" + this.parent.name + "/newest");
    }
    ngOnInit() {
    }
};
Categories2Page.ctorParameters = () => [
    { type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"] },
    { type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
Categories2Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-categories2',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./categories2.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/categorie-pages/categories2/categories2.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./categories2.page.scss */ "./src/app/categorie-pages/categories2/categories2.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"],
        src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
], Categories2Page);



/***/ })

}]);
//# sourceMappingURL=categorie-pages-categories2-categories2-module-es2015.js.map