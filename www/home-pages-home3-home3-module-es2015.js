(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-pages-home3-home3-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home3/home3.page.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home3/home3.page.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n   \r\n        <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-title mode=ios>\r\n      {{config.appName|translate}}\r\n      <!-- <ion-img src=\"assets/logo_header.png\" alt=\"logo\"></ion-img> -->\r\n    </ion-title>\r\n    <ion-buttons slot=\"end\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-button fill=\"clear\" routerLink=\"/search\" routerDirection=\"forward\">\r\n        <ion-icon slot=\"icon-only\" name=\"search\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button fill=\"clear\" routerLink=\"/cart\" routerDirection=\"forward\">\r\n        <ion-icon name=\"basket\"></ion-icon>\r\n        <ion-badge color=\"secondary\">{{shared.cartTotalItems()}}</ion-badge>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    \r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content class=\"ion-padding-bottom\">\r\n\r\n  <app-banner></app-banner>\r\n\r\n  <!-- Flash sale items products -->\r\n\r\n  <div class=\"module\" *ngIf=\"shared.flashSaleProducts.length!=0\">\r\n    <ion-row class=\"top-icon-header\">\r\n      <ion-button fill=\"clear\">\r\n        <ion-icon slot=\"start\" name=\"time\"></ion-icon>\r\n        {{'Flash Sale'|translate}}\r\n      </ion-button>\r\n    </ion-row>\r\n    <ion-slides [options]=\"sliderConfig\">\r\n      <ion-slide *ngFor=\"let p of shared.flashSaleProducts\">\r\n        <app-product [data]=\"p\" [type]=\"'flash'\"></app-product>\r\n      </ion-slide>\r\n    </ion-slides>\r\n  </div>\r\n  <ion-row class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"albums\"></ion-icon>\r\n      {{'Newest Products'|translate}}\r\n    </ion-button>\r\n  </ion-row>\r\n\r\n  <ion-slides [options]=\"sliderConfig\">\r\n    <ion-slide *ngFor=\"let p of shared.tab1\">\r\n      <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n    </ion-slide>\r\n    <ion-slide>\r\n      <ion-button fill=\"clear\" (click)=\"openProducts('latest')\"> {{'Shop More'| translate}}\r\n        <ion-icon name=\"caret-forward\"></ion-icon>\r\n      </ion-button>\r\n    </ion-slide>\r\n  </ion-slides>\r\n\r\n  <ion-row class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"bookmark\"></ion-icon>\r\n      {{ 'On Sale Products' | translate }}\r\n    </ion-button>\r\n  </ion-row>\r\n\r\n  <ion-slides [options]=\"sliderConfig\">\r\n    <ion-slide *ngFor=\"let p of shared.tab2\">\r\n      <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n    </ion-slide>\r\n    <ion-slide>\r\n      <ion-button fill=\"clear\" (click)=\"openProducts('special')\"> {{'Shop More'| translate}}\r\n        <ion-icon name=\"caret-forward\"></ion-icon>\r\n      </ion-button>\r\n    </ion-slide>\r\n  </ion-slides>\r\n  <ion-row class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"list\"></ion-icon>\r\n      {{ 'Featured Products' | translate }}\r\n    </ion-button>\r\n  </ion-row>\r\n\r\n  <ion-slides [options]=\"sliderConfig\">\r\n    <ion-slide *ngFor=\"let p of shared.tab3\">\r\n      <app-product [data]=\"p\" [type]=\"'normal'\"></app-product>\r\n    </ion-slide>\r\n    <ion-slide>\r\n      <ion-button fill=\"clear\" (click)=\"openProducts('most liked')\"> {{'Shop More'| translate}}\r\n        <ion-icon name=\"caret-forward\"></ion-icon>\r\n      </ion-button>\r\n    </ion-slide>\r\n  </ion-slides>\r\n  <!-- For Vendor List -->\r\n  <!--<app-vendor-list></app-vendor-list>-->\r\n  <!-- recently view Heading -->\r\n  <ion-row *ngIf=\"shared.recentViewedProducts.length!=0\" class=\"top-icon-header\">\r\n    <ion-button fill=\"clear\">\r\n      <ion-icon slot=\"start\" name=\"list\"></ion-icon>\r\n      {{'Recently Viewed'|translate}}\r\n    </ion-button>\r\n  </ion-row>\r\n  <!-- recently viewed swipe slider -->\r\n  <ion-slides #recentSlider [options]=\"sliderConfig\">\r\n    <ion-slide *ngFor=\"let p of shared.recentViewedProducts\">\r\n      <app-product [data]=\"p\" [type]=\"'recent'\"></app-product>\r\n    </ion-slide>\r\n  </ion-slides>\r\n\r\n\r\n</ion-content>");

/***/ }),

/***/ "./src/app/home-pages/home3/home3.module.ts":
/*!**************************************************!*\
  !*** ./src/app/home-pages/home3/home3.module.ts ***!
  \**************************************************/
/*! exports provided: Home3PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Home3PageModule", function() { return Home3PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _home3_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home3.page */ "./src/app/home-pages/home3/home3.page.ts");
/* harmony import */ var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/pipes/pipes.module */ "./src/pipes/pipes.module.ts");
/* harmony import */ var src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/components/share/share.module */ "./src/components/share/share.module.ts");









const routes = [
    {
        path: '',
        component: _home3_page__WEBPACK_IMPORTED_MODULE_6__["Home3Page"]
    }
];
let Home3PageModule = class Home3PageModule {
};
Home3PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
            src_components_share_share_module__WEBPACK_IMPORTED_MODULE_8__["ShareModule"],
        ],
        declarations: [_home3_page__WEBPACK_IMPORTED_MODULE_6__["Home3Page"]]
    })
], Home3PageModule);



/***/ }),

/***/ "./src/app/home-pages/home3/home3.page.scss":
/*!**************************************************!*\
  !*** ./src/app/home-pages/home3/home3.page.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content .swiper-slide {\n  max-width: 40%;\n}\nion-content ion-slides ion-slide:last-child {\n  height: auto;\n}\nion-content app-product {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9ob21lLXBhZ2VzL2hvbWUzL2hvbWUzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvaG9tZS1wYWdlcy9ob21lMy9ob21lMy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFDRSxjQUFBO0FDQUo7QURJTTtFQUNFLFlBQUE7QUNGUjtBRE9FO0VBQ0UsV0FBQTtBQ0xKIiwiZmlsZSI6InNyYy9hcHAvaG9tZS1wYWdlcy9ob21lMy9ob21lMy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgLnN3aXBlci1zbGlkZSB7XHJcbiAgICBtYXgtd2lkdGg6IDQwJTtcclxuICB9XHJcbiAgaW9uLXNsaWRlcyB7XHJcbiAgICBpb24tc2xpZGUge1xyXG4gICAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgYXBwLXByb2R1Y3Qge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG59XHJcbiIsImlvbi1jb250ZW50IC5zd2lwZXItc2xpZGUge1xuICBtYXgtd2lkdGg6IDQwJTtcbn1cbmlvbi1jb250ZW50IGlvbi1zbGlkZXMgaW9uLXNsaWRlOmxhc3QtY2hpbGQge1xuICBoZWlnaHQ6IGF1dG87XG59XG5pb24tY29udGVudCBhcHAtcHJvZHVjdCB7XG4gIHdpZHRoOiAxMDAlO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/home-pages/home3/home3.page.ts":
/*!************************************************!*\
  !*** ./src/app/home-pages/home3/home3.page.ts ***!
  \************************************************/
/*! exports provided: Home3Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Home3Page", function() { return Home3Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/providers/shared-data/shared-data.service */ "./src/providers/shared-data/shared-data.service.ts");
/* harmony import */ var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/providers/config/config.service */ "./src/providers/config/config.service.ts");
/* harmony import */ var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/providers/app-events/app-events.service */ "./src/providers/app-events/app-events.service.ts");






let Home3Page = class Home3Page {
    constructor(nav, config, appEventsService, shared) {
        this.nav = nav;
        this.config = config;
        this.appEventsService = appEventsService;
        this.shared = shared;
        this.sliderConfig = {
            slidesPerView: this.config.productSlidesPerPage,
            spaceBetween: 0
        };
    }
    openProducts(value) {
        this.nav.navigateForward(this.config.currentRoute + "/products/0/0/" + value);
    }
    ngOnInit() {
    }
    ionViewDidEnter() {
        this.shared.hideSplashScreen();
    }
    ionViewWillEnter() {
        if (!this.config.appInProduction) {
            this.config.productCardStyle = "7";
        }
    }
};
Home3Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"] },
    { type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__["AppEventsService"] },
    { type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"] }
];
Home3Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home3',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home3.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-pages/home3/home3.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home3.page.scss */ "./src/app/home-pages/home3/home3.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"],
        src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_5__["AppEventsService"],
        src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]])
], Home3Page);



/***/ })

}]);
//# sourceMappingURL=home-pages-home3-home3-module-es2015.js.map