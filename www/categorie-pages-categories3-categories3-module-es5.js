function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["categorie-pages-categories3-categories3-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/categorie-pages/categories3/categories3.page.html":
  /*!*********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/categorie-pages/categories3/categories3.page.html ***!
    \*********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCategoriePagesCategories3Categories3PageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n    <ion-toolbar>\r\n      <ion-buttons slot=\"start\" *ngIf=\"parent.name!='Categories'\">\r\n        <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n      </ion-buttons>\r\n      <ion-menu-button slot=\"start\" *ngIf=\"parent.name=='Categories' && !config.appNavigationTabs\">\r\n        <ion-icon name=\"menu\"></ion-icon>\r\n      </ion-menu-button>\r\n      <ion-title>\r\n        {{parent.name| translate }}\r\n      </ion-title>\r\n      <ion-buttons slot=\"end\">\r\n        <ion-button fill=\"clear\" routerLink=\"/search\" routerDirection=\"forward\">\r\n          <ion-icon slot=\"icon-only\" name=\"search\"></ion-icon>\r\n        </ion-button>\r\n        <ion-button fill=\"clear\" routerLink=\"/cart\" routerDirection=\"forward\">\r\n          <ion-icon name=\"basket\"></ion-icon>\r\n          <ion-badge color=\"secondary\">{{shared.cartTotalItems()}}</ion-badge>\r\n        </ion-button>\r\n      </ion-buttons>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n\r\n<ion-content>\r\n  <ion-item *ngFor=\"let c of getCategories()\" (click)=\"openSubCategories(c)\"\r\n    class=\"animate-item\">\r\n    <ion-avatar slot=\"start\">\r\n      <img *ngIf=\"c.image\" src=\"{{config.imgUrl+c.image}}\" />\r\n    </ion-avatar>\r\n    <ion-label class=\"ion-padding\">\r\n      <ion-text>\r\n        <h2>{{c.name|translate}}</h2>\r\n        <p>{{c.total_products}} {{'Products'| translate }}</p>\r\n      </ion-text>\r\n    </ion-label>\r\n  </ion-item>\r\n\r\n  <div class=\"ion-text-center\">\r\n      <ion-button *ngIf=\"parent.id!=0\" icon-end clear color=\"secondary\" (click)=\"viewAll()\">\r\n          {{ 'View All' | translate }}\r\n          <ion-icon name=\"caret-forward\"></ion-icon>\r\n        </ion-button>\r\n  </div>\r\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/categorie-pages/categories3/categories3.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/categorie-pages/categories3/categories3.module.ts ***!
    \*******************************************************************/

  /*! exports provided: Categories3PageModule */

  /***/
  function srcAppCategoriePagesCategories3Categories3ModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Categories3PageModule", function () {
      return Categories3PageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _categories3_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./categories3.page */
    "./src/app/categorie-pages/categories3/categories3.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _categories3_page__WEBPACK_IMPORTED_MODULE_6__["Categories3Page"]
    }];

    var Categories3PageModule = function Categories3PageModule() {
      _classCallCheck(this, Categories3PageModule);
    };

    Categories3PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]],
      declarations: [_categories3_page__WEBPACK_IMPORTED_MODULE_6__["Categories3Page"]]
    })], Categories3PageModule);
    /***/
  },

  /***/
  "./src/app/categorie-pages/categories3/categories3.page.scss":
  /*!*******************************************************************!*\
    !*** ./src/app/categorie-pages/categories3/categories3.page.scss ***!
    \*******************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppCategoriePagesCategories3Categories3PageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content ion-item ion-label {\n  padding-bottom: 0;\n  padding-top: 0;\n}\nion-content ion-item ion-label h2 {\n  font-size: var(--heading-font-size);\n  font-weight: 400;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  color: var(--ion-text-color);\n}\nion-content ion-item ion-label p {\n  font-size: var(--sub-heading-font-size);\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9jYXRlZ29yaWUtcGFnZXMvY2F0ZWdvcmllczMvY2F0ZWdvcmllczMucGFnZS5zY3NzIiwic3JjL2FwcC9jYXRlZ29yaWUtcGFnZXMvY2F0ZWdvcmllczMvY2F0ZWdvcmllczMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVRO0VBQ0ksaUJBQUE7RUFDQSxjQUFBO0FDRFo7QURFWTtFQUNJLG1DQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsdUJBQUE7RUFDQSw0QkFBQTtBQ0FoQjtBREVZO0VBQ0ksdUNBQUE7RUFDQSwyQ0FBQTtBQ0FoQiIsImZpbGUiOiJzcmMvYXBwL2NhdGVnb3JpZS1wYWdlcy9jYXRlZ29yaWVzMy9jYXRlZ29yaWVzMy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudHtcclxuICAgIGlvbi1pdGVte1xyXG4gICAgICAgIGlvbi1sYWJlbHtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDA7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAwO1xyXG4gICAgICAgICAgICBoMntcclxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogdmFyKC0taGVhZGluZy1mb250LXNpemUpO1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgICAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgICAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgICAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHB7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IHZhcigtLXN1Yi1oZWFkaW5nLWZvbnQtc2l6ZSk7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogcmdiYSh2YXIoLS1pb24tdGV4dC1jb2xvci1yZ2IpLCAwLjUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59IiwiaW9uLWNvbnRlbnQgaW9uLWl0ZW0gaW9uLWxhYmVsIHtcbiAgcGFkZGluZy1ib3R0b206IDA7XG4gIHBhZGRpbmctdG9wOiAwO1xufVxuaW9uLWNvbnRlbnQgaW9uLWl0ZW0gaW9uLWxhYmVsIGgyIHtcbiAgZm9udC1zaXplOiB2YXIoLS1oZWFkaW5nLWZvbnQtc2l6ZSk7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xufVxuaW9uLWNvbnRlbnQgaW9uLWl0ZW0gaW9uLWxhYmVsIHAge1xuICBmb250LXNpemU6IHZhcigtLXN1Yi1oZWFkaW5nLWZvbnQtc2l6ZSk7XG4gIGNvbG9yOiByZ2JhKHZhcigtLWlvbi10ZXh0LWNvbG9yLXJnYiksIDAuNSk7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/categorie-pages/categories3/categories3.page.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/categorie-pages/categories3/categories3.page.ts ***!
    \*****************************************************************/

  /*! exports provided: Categories3Page */

  /***/
  function srcAppCategoriePagesCategories3Categories3PageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Categories3Page", function () {
      return Categories3Page;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");

    var Categories3Page = /*#__PURE__*/function () {
      function Categories3Page(shared, config, router, activatedRoute) {
        _classCallCheck(this, Categories3Page);

        this.shared = shared;
        this.config = config;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.categories = [];
        this.parent = {};
        this.parent.id = this.activatedRoute.snapshot.paramMap.get('parent');
        this.parent.name = this.activatedRoute.snapshot.paramMap.get('name');
        if (this.parent.id == undefined) this.parent.id = 0;
        if (this.parent.name == undefined) this.parent.name = 0;
        if (this.parent.name == 0) this.parent.name = "Categories";
      }

      _createClass(Categories3Page, [{
        key: "getCategories",
        value: function getCategories() {
          var cat = [];

          var _iterator = _createForOfIteratorHelper(this.shared.allCategories),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var value = _step.value;

              if (value.parent_id == this.parent.id) {
                cat.push(value);
              }
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }

          return cat;
        }
      }, {
        key: "openSubCategories",
        value: function openSubCategories(parent) {
          var count = 0;

          var _iterator2 = _createForOfIteratorHelper(this.shared.allCategories),
              _step2;

          try {
            for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
              var value = _step2.value;
              if (parent.id == value.parent_id) count++;
            }
          } catch (err) {
            _iterator2.e(err);
          } finally {
            _iterator2.f();
          }

          if (count != 0) this.router.navigateByUrl(this.config.currentRoute + "/categories3/" + parent.id + "/" + parent.name);else this.router.navigateByUrl(this.config.currentRoute + "/products/" + parent.id + "/" + parent.name + "/newest");
        }
      }, {
        key: "viewAll",
        value: function viewAll() {
          this.router.navigateByUrl(this.config.currentRoute + "/products/" + this.parent.id + "/" + this.parent.name + "/newest");
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return Categories3Page;
    }();

    Categories3Page.ctorParameters = function () {
      return [{
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }];
    };

    Categories3Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-categories3',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./categories3.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/categorie-pages/categories3/categories3.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./categories3.page.scss */
      "./src/app/categorie-pages/categories3/categories3.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])], Categories3Page);
    /***/
  }
}]);
//# sourceMappingURL=categorie-pages-categories3-categories3-module-es5.js.map