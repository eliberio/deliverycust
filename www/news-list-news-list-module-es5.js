function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["news-list-news-list-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/news-list/news-list.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/news-list/news-list.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppNewsListNewsListPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      {{name}}\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"news-list-page\">\r\n  <ion-list>\r\n    <ion-item lines=\"full\" *ngFor=\"let post of posts\" (click)=\"showPostDetail(post)\" class=\"animate-item\">\r\n      <ion-thumbnail slot=\"start\">\r\n        <ion-img src=\"{{config.imgUrl+post.news_image}}\"></ion-img>\r\n      </ion-thumbnail>\r\n      <ion-label>\r\n        <h2>{{post.news_name}}\r\n          <br>\r\n          <small>\r\n            <ion-icon name=\"time\"></ion-icon>\r\n            <ion-label>\r\n                {{post.news_date_added}}\r\n            </ion-label>\r\n          </small>\r\n        </h2>\r\n        <p [innerHTML]=\"post.news_description\"></p>\r\n      </ion-label>\r\n    </ion-item>\r\n    <ion-infinite-scroll #infinite (ionInfinite)=\"getPosts()\">\r\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\r\n    </ion-infinite-scroll>\r\n  </ion-list>\r\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/news-list/news-list.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/news-list/news-list.module.ts ***!
    \***********************************************/

  /*! exports provided: NewsListPageModule */

  /***/
  function srcAppNewsListNewsListModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewsListPageModule", function () {
      return NewsListPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _news_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./news-list.page */
    "./src/app/news-list/news-list.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _news_list_page__WEBPACK_IMPORTED_MODULE_6__["NewsListPage"]
    }];

    var NewsListPageModule = function NewsListPageModule() {
      _classCallCheck(this, NewsListPageModule);
    };

    NewsListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_news_list_page__WEBPACK_IMPORTED_MODULE_6__["NewsListPage"]]
    })], NewsListPageModule);
    /***/
  },

  /***/
  "./src/app/news-list/news-list.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/news-list/news-list.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppNewsListNewsListPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".news-list-page ion-list ion-item ion-thumbnail {\n  margin-bottom: auto;\n}\n.news-list-page ion-list ion-item ion-label {\n  margin-top: 4px;\n}\n.news-list-page ion-list ion-item ion-label h2 {\n  font-size: 16px;\n  font-weight: bold;\n  white-space: normal;\n}\n.news-list-page ion-list ion-item ion-label h2 small {\n  display: flex;\n  align-items: center;\n  font-size: 14px;\n  color: #747474;\n  font-weight: normal;\n  margin-top: 5px;\n}\n.news-list-page ion-list ion-item ion-label h2 small ion-label {\n  padding-left: 4px;\n}\n.news-list-page ion-list ion-item ion-label p p:not(:first-child) {\n  display: none;\n}\n.news-list-page ion-list ion-item ion-label p p:first-child {\n  white-space: normal;\n  line-height: 1.4;\n  -webkit-line-clamp: 4;\n  display: -webkit-box;\n  box-sizing: border-box;\n  -webkit-box-orient: vertical;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9uZXdzLWxpc3QvbmV3cy1saXN0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvbmV3cy1saXN0L25ld3MtbGlzdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBR007RUFDRSxtQkFBQTtBQ0ZSO0FESU07RUFDRSxlQUFBO0FDRlI7QURHUTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FDRFY7QURFVTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FDQVo7QURDWTtFQUNFLGlCQUFBO0FDQ2Q7QURJVTtFQUNFLGFBQUE7QUNGWjtBRElVO0VBQ0UsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0Esb0JBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0FDRloiLCJmaWxlIjoic3JjL2FwcC9uZXdzLWxpc3QvbmV3cy1saXN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5uZXdzLWxpc3QtcGFnZSB7XHJcbiAgaW9uLWxpc3Qge1xyXG4gICAgaW9uLWl0ZW0ge1xyXG4gICAgICBpb24tdGh1bWJuYWlsIHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiBhdXRvO1xyXG4gICAgICB9XHJcbiAgICAgIGlvbi1sYWJlbCB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNHB4O1xyXG4gICAgICAgIGgyIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcclxuICAgICAgICAgIHNtYWxsIHtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICBjb2xvcjogIzc0NzQ3NDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICAgICAgICBpb24tbGFiZWwge1xyXG4gICAgICAgICAgICAgIHBhZGRpbmctbGVmdDogNHB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHAge1xyXG4gICAgICAgICAgcDpub3QoOmZpcnN0LWNoaWxkKSB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBwOmZpcnN0LWNoaWxkIHtcclxuICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcclxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDEuNDtcclxuICAgICAgICAgICAgLXdlYmtpdC1saW5lLWNsYW1wOiA0O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICAgICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiLm5ld3MtbGlzdC1wYWdlIGlvbi1saXN0IGlvbi1pdGVtIGlvbi10aHVtYm5haWwge1xuICBtYXJnaW4tYm90dG9tOiBhdXRvO1xufVxuLm5ld3MtbGlzdC1wYWdlIGlvbi1saXN0IGlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gIG1hcmdpbi10b3A6IDRweDtcbn1cbi5uZXdzLWxpc3QtcGFnZSBpb24tbGlzdCBpb24taXRlbSBpb24tbGFiZWwgaDIge1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xufVxuLm5ld3MtbGlzdC1wYWdlIGlvbi1saXN0IGlvbi1pdGVtIGlvbi1sYWJlbCBoMiBzbWFsbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICM3NDc0NzQ7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cbi5uZXdzLWxpc3QtcGFnZSBpb24tbGlzdCBpb24taXRlbSBpb24tbGFiZWwgaDIgc21hbGwgaW9uLWxhYmVsIHtcbiAgcGFkZGluZy1sZWZ0OiA0cHg7XG59XG4ubmV3cy1saXN0LXBhZ2UgaW9uLWxpc3QgaW9uLWl0ZW0gaW9uLWxhYmVsIHAgcDpub3QoOmZpcnN0LWNoaWxkKSB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG4ubmV3cy1saXN0LXBhZ2UgaW9uLWxpc3QgaW9uLWl0ZW0gaW9uLWxhYmVsIHAgcDpmaXJzdC1jaGlsZCB7XG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjQ7XG4gIC13ZWJraXQtbGluZS1jbGFtcDogNDtcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/news-list/news-list.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/news-list/news-list.page.ts ***!
    \*********************************************/

  /*! exports provided: NewsListPage */

  /***/
  function srcAppNewsListNewsListPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewsListPage", function () {
      return NewsListPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var NewsListPage = /*#__PURE__*/function () {
      function NewsListPage(navCtrl, http, shared, config, loading, activatedRoute) {
        _classCallCheck(this, NewsListPage);

        this.navCtrl = navCtrl;
        this.http = http;
        this.shared = shared;
        this.config = config;
        this.loading = loading;
        this.activatedRoute = activatedRoute;
        this.page = 0;
        this.posts = new Array();
        this.httpRunning = true;
        this.name = this.activatedRoute.snapshot.paramMap.get('name');
        this.id = this.activatedRoute.snapshot.paramMap.get('id');
        this.getPosts();
      }

      _createClass(NewsListPage, [{
        key: "showPostDetail",
        value: function showPostDetail(post) {
          this.shared.singlePostData = post;
          this.navCtrl.navigateForward(this.config.currentRoute + "/news-detail");
        }
      }, {
        key: "getPosts",
        //============================================================================================  
        //getting list of posts
        value: function getPosts() {
          var _this = this;

          this.httpRunning = true;
          var dat = {};
          dat.language_id = this.config.langId;
          dat.currency_code = this.config.currecnyCode;
          dat.page_number = this.page;
          dat.categories_id = this.id;
          this.config.postHttp('getallnews', dat).then(function (data) {
            _this.httpRunning = false;

            _this.infinite.complete(); //stopping infinite scroll loader


            if (_this.page == 0) {
              _this.posts = [];
              _this.infinite.disabled == false;
            }

            if (data.success == 1) {
              _this.page++;
              data.news_data.forEach(function (value, index) {
                _this.posts.push(value);
              });
            }

            if (data.news_data.length < 9) {
              // if we get less than 10 products then infinite scroll will de disabled
              //disabling infinite scroll
              _this.infinite.disabled == true;

              if (_this.posts.length != 0) {}
            }
          }, function (response) {// console.log("Error while loading posts from the server");
            // console.log(response);
          });
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return NewsListPage;
    }();

    NewsListPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_5__["ConfigService"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"], {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"])], NewsListPage.prototype, "infinite", void 0);
    NewsListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-news-list',
      encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./news-list.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/news-list/news-list.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./news-list.page.scss */
      "./src/app/news-list/news-list.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["SharedDataService"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_5__["ConfigService"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"]])], NewsListPage);
    /***/
  }
}]);
//# sourceMappingURL=news-list-news-list-module-es5.js.map