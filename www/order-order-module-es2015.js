(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["order-order-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/order/order.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/order/order.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      {{'Order'| translate }}\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content [scrollEvents]=\"true\">\r\n  <ion-card>\r\n    <ion-card-header>\r\n      <h4>{{'Shipping Address'|translate}}</h4>\r\n    </ion-card-header>\r\n    <ion-card-content>\r\n      {{orderDetail.delivery_street_address+', '+orderDetail.delivery_city+', '+orderDetail.delivery_state+'\r\n      '+orderDetail.delivery_postcode+',\r\n      '+orderDetail.delivery_country}}\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n  <ion-card>\r\n    <ion-card-header>\r\n      <h4>{{'Billing Address'|translate}}</h4>\r\n    </ion-card-header>\r\n    <ion-card-content>\r\n      {{orderDetail.billing_street_address+', '+orderDetail.billing_city+', '+orderDetail.billing_state+'\r\n      '+orderDetail.billing_postcode+',\r\n      '+orderDetail.billing_country}}\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n  <ion-card>\r\n    <ion-card-header>\r\n      <h4>{{'Shipping Method'|translate}}</h4>\r\n    </ion-card-header>\r\n    <ion-card-content>\r\n      {{orderDetail.shipping_method}}\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n  <ion-card class=\"order-product\">\r\n    <ion-card-header>\r\n      <h4>{{'Products'|translate}}</h4>\r\n    </ion-card-header>\r\n    <ion-card-content *ngFor=\"let product of products\">\r\n      <ion-row class=\"row-product\">\r\n        <ion-col>\r\n          <h3>{{product.products_name}}\r\n            <br>\r\n            <small>{{product.categories_name}}</small>\r\n          </h3>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-item lines=\"full\">\r\n        <ion-thumbnail slot=\"start\">\r\n          <img src=\"{{config.imgUrl+product.image}}\">\r\n        </ion-thumbnail>\r\n        <ion-label>\r\n          <ion-row>\r\n            <ion-col class=\"ion-text-left\" size=\"6\">{{'Price' |translate}}&nbsp;:&nbsp;</ion-col>\r\n            <ion-col class=\"ion-text-right\" size=\"6\">{{product.price| curency}}</ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row>\r\n            <ion-col class=\"ion-text-left\" size=\"6\">{{'Quantity'|translate}}&nbsp;:&nbsp;</ion-col>\r\n            <ion-col class=\"ion-text-right\" size=\"6\">{{product.customers_basket_quantity}}</ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row *ngFor=\"let att of product.attributes\">\r\n            <ion-col size=\"6\">{{att.products_options_values+'&nbsp;'+att.products_options}}&nbsp;:</ion-col>\r\n            <ion-col size=\"6\">{{att.price_prefix +'&nbsp;'+ att.options_values_price+'&nbsp;'+config.curency}}</ion-col>\r\n          </ion-row>\r\n\r\n          <ion-row>\r\n            <ion-col class=\"ion-text-left\" size=\"6\">\r\n              <strong>{{'Total' |translate}}</strong>&nbsp;:&nbsp;</ion-col>\r\n            <ion-col class=\"ion-text-right\" size=\"6\">\r\n              <strong>{{product.total | curency}}</strong>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-label>\r\n      </ion-item>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n  <ion-card>\r\n    <ion-card-header>\r\n      {{'SubTotal'|translate}}\r\n    </ion-card-header>\r\n    <ion-card-content>\r\n      <ion-row>\r\n        <ion-col size=\"6\">\r\n          {{'Products Price'|translate}}\r\n        </ion-col>\r\n        <ion-col size=\"6\" class=\"ion-text-right\">\r\n          {{productsTotal| curency}}\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col size=\"6\">\r\n          {{'Tax'|translate}}\r\n        </ion-col>\r\n        <ion-col size=\"6\" class=\"ion-text-right\">\r\n          {{orderDetail.total_tax| curency}}\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col size=\"6\">\r\n          {{'Shipping Cost'|translate}}\r\n        </ion-col>\r\n        <ion-col size=\"6\" class=\"ion-text-right\">\r\n          {{orderDetail.shipping_cost| curency}}\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row *ngIf=\"couponApplied == 1\">\r\n        <ion-col size=\"6\">\r\n          {{'Discount'|translate}}\r\n        </ion-col>\r\n        <ion-col size=\"6\" class=\"ion-text-right\">\r\n          {{discount| curency}}\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col size=\"6\">\r\n          <strong>{{'Total'|translate}}</strong>\r\n        </ion-col>\r\n        <ion-col size=\"6\" class=\"ion-text-right\">\r\n          <strong>{{totalAmountWithDisocunt| curency}}</strong>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n  <ion-card *ngFor=\"let coupon of couponArray\">\r\n    <ion-card-content>\r\n      <ion-row>\r\n        <ion-col size=\"6\">\r\n          {{'Coupon Code'|translate}}\r\n        </ion-col>\r\n        <ion-col size=\"6\" class=\"ion-text-right\">\r\n          {{coupon.code}}\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col size=\"6\">\r\n          {{'Coupon Amount'|translate}}\r\n        </ion-col>\r\n        <ion-col size=\"6\" class=\"ion-text-right\">\r\n          {{coupon.amount}}\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col size=\"12\" *ngIf=\"coupon.discount_type == 'percent'\">\r\n          {{'A percentage discount for the entire cart'|translate}}\r\n        </ion-col>\r\n        <ion-col size=\"12\" *ngIf=\"coupon.discount_type == 'fixed_cart'\">\r\n          {{'A fixed total discount for the entire cart'|translate}}\r\n        </ion-col>\r\n        <ion-col size=\"12\" *ngIf=\"coupon.discount_type == 'fixed_product'\">\r\n          {{'A fixed total discount for selected products only'|translate}}\r\n        </ion-col>\r\n        <ion-col size=\"12\" *ngIf=\"coupon.discount_type == 'percent_product'\">\r\n          {{'A percentage discount for selected products only'|translate}}\r\n        </ion-col>\r\n        <ion-col size=\"12\">\r\n          <ion-button size=\"small\" color=\"secondary\" (click)=\"deleteCoupon(coupon.code)\">{{'Remove'|translate}}\r\n          </ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n  <ion-card>\r\n    <ion-card-content>\r\n      <ion-list>\r\n        <ion-item>\r\n          <ion-input type=\"text\" placeholder=\"{{'coupon code'|translate}}\" [(ngModel)]=\"c\"></ion-input>\r\n          <ion-button fill=\"clear\" item-end (click)=\"getCoupon(c)\">{{'Apply'|translate}}</ion-button>\r\n        </ion-item>\r\n      </ion-list>\r\n\r\n    </ion-card-content>\r\n  </ion-card>\r\n  <ion-button *ngIf=\"!config.appInProduction\" size=\"small\" fill=\"clear\" (click)=\"couponslist()\">\r\n    {{'Coupon Codes List'|translate}}</ion-button>\r\n\r\n  <ion-card>\r\n    <ion-card-header>\r\n      {{'Order Notes'|translate}}\r\n    </ion-card-header>\r\n    <ion-card-content>\r\n      <ion-item>\r\n        <ion-textarea rows=\"3\" type=\"text\" placeholder=\"{{'Note to the buyer'|translate}}\" name=\"note\"\r\n          [(ngModel)]=\"orderDetail.comments\"></ion-textarea>\r\n      </ion-item>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n  <ion-card>\r\n    <ion-card-content>\r\n      <ion-list>\r\n        <ion-item lines=\"none\">\r\n          <ion-label color=\"dark\">{{'Payment'|translate}}</ion-label>\r\n          <ion-select interface=\"popover\" [(ngModel)]=\"orderDetail.payment_method\" (ionChange)=\" paymentMehodChanged()\"\r\n            okText=\"{{'ok'|translate}}\" cancelText=\"{{'Cancel'|translate}}\">\r\n            <div *ngFor=\"let p of paymentMethods\">\r\n              <ion-select-option *ngIf=\"p.active==1\" [value]=\"p.method\">{{p.name|translate}}</ion-select-option>\r\n            </div>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n\r\n  <div class=\"braintree-paypal\" [class.hidden]=\"orderDetail.payment_method!='braintree_paypal'\">\r\n    <div id=\"paypal-container\"></div>\r\n  </div>\r\n\r\n  <div class=\"braintree-card\" [class.hidden]=\"orderDetail.payment_method!='braintree_card'\">\r\n    <form id=\"braintree-form\" class=\"form\" #brainForm=\"ngForm\">\r\n      <div id=\"error-message\"></div>\r\n      <label class=\"hosted-fields--label\" for=\"card-number\" translate>{{'Card Number'|translate}}</label>\r\n      <div class=\"hosted-field form-control\" id=\"card-number\" value=\"4111111111111111\"></div>\r\n\r\n      <label class=\"hosted-fields--label\" for=\"cvv\" value=\"123\">CVV</label>\r\n      <div class=\"hosted-field form-control\" id=\"cvv\"></div>\r\n\r\n      <label class=\"hosted-fields--label\" for=\"expiration-date\" translate>{{'Expiration Date'|translate}}</label>\r\n      <div class=\"hosted-field form-control\" id=\"expiration-date\" value=\"10/2019\"></div>\r\n\r\n      <input type=\"hidden\" name=\"payment-method-nonce\">\r\n      <ion-button expand=\"full\" color=\"secondary\" type=\"submit\" id=\"braintreesubmit\" disabled>\r\n        {{'Continue'|translate}}</ion-button>\r\n    </form>\r\n  </div>\r\n\r\n  <div class=\"stripe-card\" *ngIf=\"orderDetail.payment_method=='stripe'\">\r\n    <form #stripeForm=\"ngForm\" (ngSubmit)=\"stripePayment()\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-list>\r\n\r\n            <ion-item>\r\n              <ion-input type=\"number\" name=\"number\" placeholder=\"{{'Card Number'|translate}}\"\r\n                [(ngModel)]=\"stripeCard.number\" required></ion-input>\r\n            </ion-item>\r\n            <ion-item>\r\n              <ion-label>{{'Expire Month'|translate}}</ion-label>\r\n              <ion-select name=\"expMonth\" [(ngModel)]=\"stripeCard.expMonth\" required>\r\n                <ion-select-option *ngFor=\"let n of [1,2,3,4,5,6,7,8,9,10,11,12]\" value=\"{{n}}\">{{n}}\r\n                </ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n\r\n            <ion-item>\r\n              <ion-label>{{'Expire Year'|translate}}</ion-label>\r\n              <ion-select name=\"expYear\" [(ngModel)]=\"stripeCard.expYear\" required>\r\n                <ion-select-option\r\n                  *ngFor=\"let n of [2017,2018,2019,2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,2030,2031,2032,2033]\"\r\n                  value=\"{{n}}\">{{n}}</ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n\r\n            <ion-item>\r\n              <ion-input type=\"number\" name=\"cvc\" placeholder=\"{{'CVC'|translate}}\" [(ngModel)]=\"stripeCard.cvc\"\r\n                required></ion-input>\r\n            </ion-item>\r\n\r\n          </ion-list>\r\n        </ion-col>\r\n      </ion-row>\r\n\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-button expand=\"full\" color=\"secondary\" type=\"submit\" [disabled]=\"!stripeForm.form.valid\">\r\n            {{'Continue'|translate}}</ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </form>\r\n  </div>\r\n  <!-- new payment method hyperpay  -->\r\n  <div class=\"stripe-card\" *ngIf=\"orderDetail.payment_method=='hyperpay' || orderDetail.payment_method=='cybersource'\">\r\n    <form #card=\"ngForm\" (ngSubmit)=\"hyperpayPayment()\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-list>\r\n\r\n            <ion-item>\r\n              <ion-input type=\"number\" name=\"number\" placeholder=\"{{'Card Number'|translate}}\"\r\n                [(ngModel)]=\"orderDetail.account_number\" required></ion-input>\r\n            </ion-item>\r\n            <ion-item>\r\n              <ion-label>{{'Expire Month'|translate}}</ion-label>\r\n              <ion-select name=\"expMonth\" [(ngModel)]=\"orderDetail.expiration_month\" required>\r\n                <ion-select-option *ngFor=\"let n of [1,2,3,4,5,6,7,8,9,10,11,12]\" value=\"{{n}}\">{{n}}\r\n                </ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n\r\n            <ion-item>\r\n              <ion-label>{{'Expire Year'|translate}}</ion-label>\r\n              <ion-select name=\"expYear\" [(ngModel)]=\"orderDetail.expiration_year\" required>\r\n                <ion-select-option\r\n                  *ngFor=\"let n of [2017,2018,2019,2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,2030,2031,2032,2033]\"\r\n                  value=\"{{n}}\">{{n}}</ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n\r\n            <ion-item>\r\n              <ion-input type=\"number\" name=\"cvc\" placeholder=\"{{'CVC'|translate}}\" [(ngModel)]=\"orderDetail.cvv2\"\r\n                required></ion-input>\r\n            </ion-item>\r\n\r\n          </ion-list>\r\n        </ion-col>\r\n      </ion-row>\r\n\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-button expand=\"full\" color=\"secondary\" type=\"submit\" [disabled]=\"!card.form.valid\">\r\n            {{'Continue'|translate}}</ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </form>\r\n  </div>\r\n  <ion-button expand=\"full\" class=\"button-footer\" color=\"secondary\" (click)=\"addOrder()\"\r\n    *ngIf=\"orderDetail.payment_method=='cod'\">{{'Continue'|translate}}</ion-button>\r\n  <ion-button expand=\"full\" *ngIf=\"orderDetail.payment_method=='razorpay'\" class=\"button-footer\" color=\"secondary\"\r\n    (click)=\"razorPay()\">{{'Continue'|translate}}\r\n  </ion-button>\r\n  <ion-button expand=\"full\" *ngIf=\"orderDetail.payment_method=='paytm'\" class=\"button-footer\" color=\"secondary\"\r\n    (click)=\"paytmPayment()\">{{'Continue'|translate}}\r\n  </ion-button>\r\n  <ion-button expand=\"full\" class=\"button-footer\" color=\"secondary\" (click)=\"instamojoPayment()\"\r\n    *ngIf=\"orderDetail.payment_method=='instamojo'\">{{'Continue'|translate}}</ion-button>\r\n\r\n\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/order/order.module.ts":
/*!***************************************!*\
  !*** ./src/app/order/order.module.ts ***!
  \***************************************/
/*! exports provided: OrderPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderPageModule", function() { return OrderPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _order_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./order.page */ "./src/app/order/order.page.ts");
/* harmony import */ var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/pipes/pipes.module */ "./src/pipes/pipes.module.ts");








const routes = [
    {
        path: '',
        component: _order_page__WEBPACK_IMPORTED_MODULE_6__["OrderPage"]
    }
];
let OrderPageModule = class OrderPageModule {
};
OrderPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"],
            src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]
        ],
        declarations: [_order_page__WEBPACK_IMPORTED_MODULE_6__["OrderPage"]]
    })
], OrderPageModule);



/***/ }),

/***/ "./src/app/order/order.page.scss":
/*!***************************************!*\
  !*** ./src/app/order/order.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content ion-col {\n  font-size: 12px;\n}\nion-content ion-card ion-card-header {\n  font-size: 14px;\n  margin-bottom: 0;\n  margin-top: 0;\n  color: var(--ion-color-light-contrast);\n  background-color: var(--ion-color-light-shade);\n  padding-top: 10px !important;\n  padding-left: 10px !important;\n  padding-right: 10px !important;\n  padding-bottom: 10px !important;\n}\nion-content ion-card ion-card-header h4 {\n  margin-top: 0;\n  margin-bottom: 0;\n  font-size: 12px;\n  font-weight: 600;\n}\nion-content ion-card ion-card-content {\n  padding-top: 10px !important;\n  padding-left: 10px !important;\n  padding-right: 10px !important;\n  padding-bottom: 10px !important;\n  font-size: 12px;\n  color: var(--ion-text-color);\n}\nion-content ion-card ion-card-content .card-disable {\n  color: rgba(var(--ion-text-color-rgb), 0.5) !important;\n}\nion-content ion-card ion-card-content h3 {\n  font-size: 16px;\n  margin-bottom: 6px;\n}\nion-content ion-card ion-card-content h3 small {\n  font-size: 80%;\n}\nion-content ion-card ion-card-content .item {\n  --background: transparent;\n}\nion-content .row-product {\n  border-bottom: solid var(--ion-color-light-shade);\n  border-width: 0.2px;\n}\nion-content ion-button {\n  margin: 0;\n}\nion-content .braintree-paypal {\n  margin-top: 15px;\n  margin-bottom: 15px;\n}\nion-content .braintree-paypal #braintree-paypal-button {\n  margin-left: auto;\n  margin-right: auto;\n}\nion-content .braintree-card {\n  background-color: var(--ion-background-color);\n  padding: 10px;\n  margin: 10px;\n}\nion-content .braintree-card .form .hosted-fields--label {\n  color: var(--ion-color-dark);\n  font-size: 14px;\n  margin-bottom: 5px;\n  display: block;\n}\nion-content .braintree-card .form .hosted-field {\n  height: 40px;\n  box-sizing: border-box;\n  width: 100%;\n  padding: 6px 12px;\n  display: inline-block;\n  box-shadow: none;\n  font-weight: 600;\n  font-size: 14px;\n  border-radius: 4px;\n  border: 1px solid var(--ion-color-light);\n  line-height: 20px;\n  background-color: var(--ion-color-light);\n  margin-bottom: 10px;\n  transition: all 300ms ease-in-out;\n}\nion-content .braintree-card .form .braintree-hosted-fields-focused {\n  border: 1px solid #64d18a;\n  background-position: left bottom;\n}\nion-content .braintree-card .form .braintree-hosted-fields-invalid {\n  border: 1px solid #ed574a;\n}\nion-content .braintree-card .form .button {\n  margin-top: 0;\n  margin-bottom: 0;\n}\nion-content .braintree-paypal-button {\n  margin: auto;\n}\nion-content .stripe-card {\n  background-color: var(--ion-background-color);\n  padding: 10px;\n  margin: 10px;\n}\nion-content .stripe-card .item {\n  background-color: var(--ion-color-light);\n  margin-top: 5px;\n}\nion-content .hidden {\n  display: none;\n}\nion-content .button-footer {\n  height: 45px;\n  margin-top: 0;\n  margin-bottom: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9vcmRlci9vcmRlci5wYWdlLnNjc3MiLCJzcmMvYXBwL29yZGVyL29yZGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLGVBQUE7QUNBSjtBREdJO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLHNDQUFBO0VBQ0EsOENBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0VBQ0EsOEJBQUE7RUFDQSwrQkFBQTtBQ0ROO0FERU07RUFDRSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNBUjtBREdJO0VBQ0UsNEJBQUE7RUFDQSw2QkFBQTtFQUNBLDhCQUFBO0VBQ0EsK0JBQUE7RUFDQSxlQUFBO0VBQ0EsNEJBQUE7QUNETjtBREVNO0VBQ0Usc0RBQUE7QUNBUjtBREVNO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0FDQVI7QURFUTtFQUNFLGNBQUE7QUNBVjtBREdNO0VBQ0UseUJBQUE7QUNEUjtBREtFO0VBQ0UsaURBQUE7RUFDQSxtQkFBQTtBQ0hKO0FES0U7RUFDRSxTQUFBO0FDSEo7QURNRTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7QUNKSjtBREtJO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtBQ0hOO0FETUU7RUFDRSw2Q0FBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0FDSko7QURPTTtFQUNFLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQ0xSO0FET007RUFDRSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSx3Q0FBQTtFQUNBLGlCQUFBO0VBQ0Esd0NBQUE7RUFDQSxtQkFBQTtFQUNBLGlDQUFBO0FDTFI7QURPTTtFQUNFLHlCQUFBO0VBQ0EsZ0NBQUE7QUNMUjtBRFFNO0VBQ0UseUJBQUE7QUNOUjtBRFFNO0VBQ0UsYUFBQTtFQUNBLGdCQUFBO0FDTlI7QURXRTtFQUNFLFlBQUE7QUNUSjtBRFlFO0VBQ0UsNkNBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQ1ZKO0FEWUk7RUFDRSx3Q0FBQTtFQUNBLGVBQUE7QUNWTjtBRGNFO0VBQ0UsYUFBQTtBQ1pKO0FEZUU7RUFDRSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0FDYkoiLCJmaWxlIjoic3JjL2FwcC9vcmRlci9vcmRlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgaW9uLWNvbCB7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgfVxyXG4gIGlvbi1jYXJkIHtcclxuICAgIGlvbi1jYXJkLWhlYWRlciB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgbWFyZ2luLXRvcDogMDtcclxuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodC1jb250cmFzdCk7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodC1zaGFkZSk7XHJcbiAgICAgIHBhZGRpbmctdG9wOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgIGg0IHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAwO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGlvbi1jYXJkLWNvbnRlbnQge1xyXG4gICAgICBwYWRkaW5nLXRvcDogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgICBwYWRkaW5nLWxlZnQ6IDEwcHggIWltcG9ydGFudDtcclxuICAgICAgcGFkZGluZy1yaWdodDogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvcik7XHJcbiAgICAgIC5jYXJkLWRpc2FibGUge1xyXG4gICAgICAgIGNvbG9yOiByZ2JhKHZhcigtLWlvbi10ZXh0LWNvbG9yLXJnYiksIDAuNSkgIWltcG9ydGFudDtcclxuICAgICAgfVxyXG4gICAgICBoMyB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDZweDtcclxuXHJcbiAgICAgICAgc21hbGwge1xyXG4gICAgICAgICAgZm9udC1zaXplOiA4MCU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC5pdGVtIHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5yb3ctcHJvZHVjdCB7XHJcbiAgICBib3JkZXItYm90dG9tOiBzb2xpZCB2YXIoLS1pb24tY29sb3ItbGlnaHQtc2hhZGUpO1xyXG4gICAgYm9yZGVyLXdpZHRoOiAwLjJweDtcclxuICB9XHJcbiAgaW9uLWJ1dHRvbiB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgfVxyXG5cclxuICAuYnJhaW50cmVlLXBheXBhbCB7XHJcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgICNicmFpbnRyZWUtcGF5cGFsLWJ1dHRvbiB7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5icmFpbnRyZWUtY2FyZCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgbWFyZ2luOiAxMHB4O1xyXG5cclxuICAgIC5mb3JtIHtcclxuICAgICAgLmhvc3RlZC1maWVsZHMtLWxhYmVsIHtcclxuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmspO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgIH1cclxuICAgICAgLmhvc3RlZC1maWVsZCB7XHJcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgcGFkZGluZzogNnB4IDEycHg7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgICAgIHRyYW5zaXRpb246IGFsbCAzMDBtcyBlYXNlLWluLW91dDtcclxuICAgICAgfVxyXG4gICAgICAuYnJhaW50cmVlLWhvc3RlZC1maWVsZHMtZm9jdXNlZCB7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgIzY0ZDE4YTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBsZWZ0IGJvdHRvbTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmJyYWludHJlZS1ob3N0ZWQtZmllbGRzLWludmFsaWQge1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNlZDU3NGE7XHJcbiAgICAgIH1cclxuICAgICAgLmJ1dHRvbiB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuYnJhaW50cmVlLXBheXBhbC1idXR0b24ge1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gIH1cclxuXHJcbiAgLnN0cmlwZS1jYXJkIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yKTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBtYXJnaW46IDEwcHg7XHJcblxyXG4gICAgLml0ZW0ge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG4gICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICAgIC8vaGVpZ2h0OiAyMHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuaGlkZGVuIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG5cclxuICAuYnV0dG9uLWZvb3RlciB7XHJcbiAgICBoZWlnaHQ6IDQ1cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAwO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICB9XHJcbn1cclxuIiwiaW9uLWNvbnRlbnQgaW9uLWNvbCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cbmlvbi1jb250ZW50IGlvbi1jYXJkIGlvbi1jYXJkLWhlYWRlciB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgbWFyZ2luLXRvcDogMDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodC1jb250cmFzdCk7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodC1zaGFkZSk7XG4gIHBhZGRpbmctdG9wOiAxMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctbGVmdDogMTBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4ICFpbXBvcnRhbnQ7XG59XG5pb24tY29udGVudCBpb24tY2FyZCBpb24tY2FyZC1oZWFkZXIgaDQge1xuICBtYXJnaW4tdG9wOiAwO1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5pb24tY29udGVudCBpb24tY2FyZCBpb24tY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZy10b3A6IDEwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy1ib3R0b206IDEwcHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpO1xufVxuaW9uLWNvbnRlbnQgaW9uLWNhcmQgaW9uLWNhcmQtY29udGVudCAuY2FyZC1kaXNhYmxlIHtcbiAgY29sb3I6IHJnYmEodmFyKC0taW9uLXRleHQtY29sb3ItcmdiKSwgMC41KSAhaW1wb3J0YW50O1xufVxuaW9uLWNvbnRlbnQgaW9uLWNhcmQgaW9uLWNhcmQtY29udGVudCBoMyB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbWFyZ2luLWJvdHRvbTogNnB4O1xufVxuaW9uLWNvbnRlbnQgaW9uLWNhcmQgaW9uLWNhcmQtY29udGVudCBoMyBzbWFsbCB7XG4gIGZvbnQtc2l6ZTogODAlO1xufVxuaW9uLWNvbnRlbnQgaW9uLWNhcmQgaW9uLWNhcmQtY29udGVudCAuaXRlbSB7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG5pb24tY29udGVudCAucm93LXByb2R1Y3Qge1xuICBib3JkZXItYm90dG9tOiBzb2xpZCB2YXIoLS1pb24tY29sb3ItbGlnaHQtc2hhZGUpO1xuICBib3JkZXItd2lkdGg6IDAuMnB4O1xufVxuaW9uLWNvbnRlbnQgaW9uLWJ1dHRvbiB7XG4gIG1hcmdpbjogMDtcbn1cbmlvbi1jb250ZW50IC5icmFpbnRyZWUtcGF5cGFsIHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cbmlvbi1jb250ZW50IC5icmFpbnRyZWUtcGF5cGFsICNicmFpbnRyZWUtcGF5cGFsLWJ1dHRvbiB7XG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICBtYXJnaW4tcmlnaHQ6IGF1dG87XG59XG5pb24tY29udGVudCAuYnJhaW50cmVlLWNhcmQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvcik7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIG1hcmdpbjogMTBweDtcbn1cbmlvbi1jb250ZW50IC5icmFpbnRyZWUtY2FyZCAuZm9ybSAuaG9zdGVkLWZpZWxkcy0tbGFiZWwge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmspO1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5pb24tY29udGVudCAuYnJhaW50cmVlLWNhcmQgLmZvcm0gLmhvc3RlZC1maWVsZCB7XG4gIGhlaWdodDogNDBweDtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDZweCAxMnB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xuICBsaW5lLWhlaWdodDogMjBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgdHJhbnNpdGlvbjogYWxsIDMwMG1zIGVhc2UtaW4tb3V0O1xufVxuaW9uLWNvbnRlbnQgLmJyYWludHJlZS1jYXJkIC5mb3JtIC5icmFpbnRyZWUtaG9zdGVkLWZpZWxkcy1mb2N1c2VkIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzY0ZDE4YTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogbGVmdCBib3R0b207XG59XG5pb24tY29udGVudCAuYnJhaW50cmVlLWNhcmQgLmZvcm0gLmJyYWludHJlZS1ob3N0ZWQtZmllbGRzLWludmFsaWQge1xuICBib3JkZXI6IDFweCBzb2xpZCAjZWQ1NzRhO1xufVxuaW9uLWNvbnRlbnQgLmJyYWludHJlZS1jYXJkIC5mb3JtIC5idXR0b24ge1xuICBtYXJnaW4tdG9wOiAwO1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuaW9uLWNvbnRlbnQgLmJyYWludHJlZS1wYXlwYWwtYnV0dG9uIHtcbiAgbWFyZ2luOiBhdXRvO1xufVxuaW9uLWNvbnRlbnQgLnN0cmlwZS1jYXJkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IpO1xuICBwYWRkaW5nOiAxMHB4O1xuICBtYXJnaW46IDEwcHg7XG59XG5pb24tY29udGVudCAuc3RyaXBlLWNhcmQgLml0ZW0ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xuICBtYXJnaW4tdG9wOiA1cHg7XG59XG5pb24tY29udGVudCAuaGlkZGVuIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cbmlvbi1jb250ZW50IC5idXR0b24tZm9vdGVyIHtcbiAgaGVpZ2h0OiA0NXB4O1xuICBtYXJnaW4tdG9wOiAwO1xuICBtYXJnaW4tYm90dG9tOiAwO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/order/order.page.ts":
/*!*************************************!*\
  !*** ./src/app/order/order.page.ts ***!
  \*************************************/
/*! exports provided: OrderPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderPage", function() { return OrderPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/providers/config/config.service */ "./src/providers/config/config.service.ts");
/* harmony import */ var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/providers/shared-data/shared-data.service */ "./src/providers/shared-data/shared-data.service.ts");
/* harmony import */ var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/providers/loading/loading.service */ "./src/providers/loading/loading.service.ts");
/* harmony import */ var src_providers_coupon_coupon_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/providers/coupon/coupon.service */ "./src/providers/coupon/coupon.service.ts");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/ngx/index.js");
/* harmony import */ var _ionic_native_stripe_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/stripe/ngx */ "./node_modules/@ionic-native/stripe/ngx/index.js");
/* harmony import */ var src_providers_paytm_paytm_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/providers/paytm/paytm.service */ "./src/providers/paytm/paytm.service.ts");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");













let OrderPage = class OrderPage {
    constructor(navCtrl, httpClient, router, config, shared, loading, couponProvider, toastController, actionSheetCtrl, iab, platform, paytmService, httpNative, stripe) {
        this.navCtrl = navCtrl;
        this.httpClient = httpClient;
        this.router = router;
        this.config = config;
        this.shared = shared;
        this.loading = loading;
        this.couponProvider = couponProvider;
        this.toastController = toastController;
        this.actionSheetCtrl = actionSheetCtrl;
        this.iab = iab;
        this.platform = platform;
        this.paytmService = paytmService;
        this.httpNative = httpNative;
        this.stripe = stripe;
        this.orderDetail = {}; //include shipping address, billing address,  shipping methods.
        this.products = [];
        this.couponArray = [];
        this.couponApplied = 0;
        this.tokenFromServer = null;
        this.discount = 0;
        this.toastPay = {
            visible: false,
            message: ""
        };
        this.productsTotal = 0;
        this.totalAmountWithDisocunt = 0;
        this.nonce = '';
        this.stripeCard = {
            number: '',
            expMonth: 1,
            expYear: 2020,
            cvc: ''
        };
        this.paymentMethods = [];
        this.paypalClientId = "";
        this.paypalEnviroment = "";
        this.publicKeyStripe = "";
        //============================================================================================
        //CAlculate Discount total
        this.calculateDiscount = function () {
            var subTotal = 0;
            var total = 0;
            for (let value of this.products) {
                subTotal += parseFloat(value.subtotal);
                total += value.total;
            }
            this.productsTotal = subTotal;
            this.discount = (subTotal - total);
        };
        //============================================================================================
        //CAlculate all total
        this.calculateTotal = function () {
            let a = 0;
            for (let value of this.products) {
                // console.log(value);
                var subtotal = parseFloat(value.total);
                a = a + subtotal;
            }
            let b = parseFloat(this.orderDetail.total_tax.toString());
            let c = parseFloat(this.orderDetail.shipping_cost.toString());
            this.totalAmountWithDisocunt = parseFloat((parseFloat(a.toString()) + b + c).toString());
            // console.log(" all total " + $scope.totalAmountWithDisocunt);
            // console.log("shipping_tax " + $scope.orderDetail.shipping_tax);
            // console.log(" shipping_cost " + $scope.orderDetail.shipping_cost);
            this.calculateDiscount();
        };
        //============================================================================================
        //delete Coupon
        this.deleteCoupon = function (code) {
            this.couponArray.forEach((value, index) => {
                if (value.code == code) {
                    this.couponArray.splice(index, 1);
                    return true;
                }
            });
            this.products = (JSON.parse(JSON.stringify(this.shared.cartProducts)));
            this.orderDetail.shipping_cost = this.shared.orderDetails.shipping_cost;
            this.couponArray.forEach((value) => {
                //checking for free shipping
                if (value.free_shipping == true) {
                    this.orderDetail.shippingName = 'free shipping';
                    this.orderDetail.shippingCost = 0;
                }
                this.products = this.couponProvider.apply(value, this.products);
            });
            this.calculateTotal();
            if (this.couponArray.length == 0) {
                this.couponApplied = 0;
            }
        };
        //========================================================================================
        //============================================================================================
        //getting getMostLikedProducts from the server
        this.getCoupon = function (code) {
            if (code == '' || code == null) {
                this.shared.showAlert('Please enter coupon code!');
                return 0;
            }
            this.loading.show();
            var dat = { 'code': code };
            this.config.postHttp('getcoupon', dat).then((data) => {
                this.loading.hide();
                if (data.success == 1) {
                    let coupon = data.data[0];
                    // console.log($scope.coupon)
                    this.applyCouponCart(coupon);
                }
                if (data.success == 0) {
                    this.shared.showAlert(data.message);
                }
            }, error => {
                this.loading.hide();
                console.log(error);
            });
        };
        //============================================================================================
        //applying coupon on the cart
        this.applyCouponCart = function (coupon) {
            //checking the coupon is valid or not
            if (this.couponProvider.validateCouponService(coupon, this.products, this.couponArray) == false) {
                return 0;
            }
            else {
                if (coupon.individual_use == 1) {
                    this.products = (JSON.parse(JSON.stringify(this.shared.cartProducts)));
                    this.couponArray = [];
                    this.orderDetail.shipping_cost = this.shared.orderDetails.shipping_cost;
                    console.log('individual_use');
                }
                var v = {};
                v.code = coupon.code;
                v.amount = coupon.amount;
                v.product_ids = coupon.product_ids;
                v.exclude_product_ids = coupon.exclude_product_ids;
                v.product_categories = coupon.product_categories;
                v.excluded_product_categories = coupon.excluded_product_categories;
                v.discount = coupon.amount;
                v.individual_use = coupon.individual_use;
                v.free_shipping = coupon.free_shipping;
                v.discount_type = coupon.discount_type;
                //   v.limit_usage_to_x_items = coupon.limit_usage_to_x_items;
                //  v.usage_limit = coupon.usage_limit;
                // v.used_by = coupon.used_by ;
                // v.usage_limit_per_user = coupon.usage_limit_per_user ;
                // v.exclude_sale_items = coupon.exclude_sale_items;
                this.couponArray.push(v);
            }
            //checking for free shipping
            if (coupon.free_shipping == 1) {
                // $scope.orderDetail.shippingName = 'free shipping';
                this.orderDetail.shipping_cost = 0;
                //  console.log('free_shipping');
            }
            //applying coupon service
            this.products = this.couponProvider.apply(coupon, this.products);
            if (this.couponArray.length != 0) {
                this.couponApplied = 1;
            }
            this.calculateTotal();
        };
        //============================================================================================
        //getting token from server
        this.getToken = function () {
            this.loading.autoHide(2000);
            this.config.getHttp('generatebraintreetoken').then((data) => {
                // this.loading.hide();
                if (data.success == 1) {
                    if (this.tokenFromServer == null) {
                        this.tokenFromServer = data.token;
                        this.braintreePaypal(this.tokenFromServer);
                        this.braintreeCreditCard(this.tokenFromServer);
                    }
                }
                if (data.success == 0) {
                }
            }, error => {
                // this.loading.hide();
                if (this.paymentBraintree) {
                    this.shared.showAlert("Server Error" + " " + error.status + " Braintree Token");
                }
            });
        };
        //================================================================================
        // braintree paypal method
        this.braintreePaypal = function (clientToken) {
            this.loading.autoHide(2000);
            var nonce = 0;
            var promise = new Promise((resolve, reject) => {
                braintree.setup(clientToken, "custom", {
                    paypal: {
                        container: "paypal-container",
                        displayName: "Shop"
                    },
                    onReady: function () {
                        // $(document).find('#braintree-paypal-button').attr('href', 'javascript:void(0)');
                    },
                    onPaymentMethodReceived: function (obj) {
                        //   console.log(obj.nonce);
                        // this.nonce = obj.nonce;
                        nonce = obj.nonce;
                        resolve();
                    }
                });
            });
            promise.then((data) => {
                // console.log(nonce);
                this.addOrder(nonce);
            }, (err) => { console.log(err); });
        };
        //================================================================================
        // braintree creditcard method
        this.braintreeCreditCard = function (clientToken) {
            // this.loading.autoHide(2000);
            var nonce = 0;
            var promise = new Promise((resolve, reject) => {
                var braintreeForm = document.querySelector('#braintree-form');
                var braintreeSubmit = document.querySelector('button[id="braintreesubmit"]');
                braintree.client.create({
                    authorization: clientToken
                }, function (clientErr, clientInstance) {
                    if (clientErr) { }
                    braintree.hostedFields.create({
                        client: clientInstance,
                        styles: {},
                        fields: {
                            number: {
                                selector: '#card-number',
                                placeholder: '4111 1111 1111 1111'
                            },
                            cvv: {
                                selector: '#cvv',
                                placeholder: '123'
                            },
                            expirationDate: {
                                selector: '#expiration-date',
                                placeholder: '10/2019'
                            }
                        }
                    }, function (hostedFieldsErr, hostedFieldsInstance) {
                        if (hostedFieldsErr) {
                            // Handle error in Hosted Fields creation
                            //alert("hostedFieldsErr" + hostedFieldsErr);
                            document.getElementById('error-message').innerHTML = "hostedFieldsErr" + hostedFieldsErr;
                            console.log("hostedFieldsErr" + hostedFieldsErr);
                            return;
                        }
                        braintreeSubmit.removeAttribute('disabled');
                        braintreeForm.addEventListener('submit', function (event) {
                            document.getElementById('error-message').innerHTML = null;
                            event.preventDefault();
                            hostedFieldsInstance.tokenize(function (tokenizeErr, payload) {
                                if (tokenizeErr) {
                                    //alert('Error : ' + JSON.stringify(tokenizeErr.message));
                                    // Handle error in Hosted Fields tokenization
                                    document.getElementById('error-message').innerHTML = tokenizeErr.message;
                                    return 0;
                                }
                                // Put `payload.nonce` into the `payment-method-nonce` input, and then
                                // submit the form. Alternatively, you could send the nonce to your server
                                // with AJAX.
                                // document.querySelector('input[name="payment-method-nonce"]').value = payload.nonce;
                                // this.nonce = payload.nonce;
                                // this.addOrder(payload.nonce);
                                nonce = payload.nonce;
                                resolve();
                                //  console.log(payload.nonce);
                            });
                        }, false);
                    });
                });
            });
            promise.then((data) => {
                this.addOrder(nonce);
            }, (err) => { console.log(err); });
        };
    }
    //============================================================================================
    //placing order
    addOrder(nonce) {
        this.loading.autoHide(5000);
        this.orderDetail.customers_id = this.shared.customerData.customers_id;
        this.orderDetail.customers_name = this.shared.orderDetails.delivery_firstname + " " + this.shared.orderDetails.delivery_lastname;
        this.orderDetail.delivery_name = this.shared.orderDetails.billing_firstname + " " + this.shared.orderDetails.billing_lastname;
        if (this.shared.orderDetails.guest_status == 1) {
            this.orderDetail.email = this.shared.orderDetails.email;
            this.orderDetail.customers_telephone = this.shared.orderDetails.delivery_phone;
        }
        else {
            this.orderDetail.email = this.shared.customerData.email;
            this.orderDetail.customers_telephone = this.shared.customerData.customers_telephone;
        }
        this.orderDetail.latitude = this.shared.orderDetails.delivery_lat;
        this.orderDetail.longitude = this.shared.orderDetails.delivery_long;
        this.orderDetail.delivery_suburb = this.shared.orderDetails.delivery_state;
        this.orderDetail.customers_suburb = this.shared.orderDetails.delivery_state;
        this.orderDetail.customers_address_format_id = '1';
        this.orderDetail.delivery_address_format_id = '1';
        this.orderDetail.products = this.products;
        this.orderDetail.is_coupon_applied = this.couponApplied;
        this.orderDetail.coupons = this.couponArray;
        this.orderDetail.coupon_amount = this.discount;
        this.orderDetail.totalPrice = this.totalAmountWithDisocunt;
        this.orderDetail.nonce = nonce;
        this.orderDetail.language_id = this.config.langId;
        this.orderDetail.currency_code = this.config.currecnyCode;
        var dat = this.orderDetail;
        console.log(dat);
        this.config.postHttp2('addtoorder', dat).then((data) => {
            //this.loading.hide();
            let d = JSON.parse(data.data);
            if (d.success == 1) {
                // this.shared.emptyCart();
                this.products = [];
                this.orderDetail = {};
                //this.shared.orderDetails = {};
                this.navCtrl.navigateForward(this.config.currentRoute + "/thank-you").then();
            }
            if (d.success == 0) {
                this.shared.showAlert(d.message);
            }
        }, err => {
            console.log(err);
            let d = JSON.parse(err.error);
            //this.router.navigateByUrl("/tabs/home").then();
            this.shared.showAlert(d.message);
        });
    }
    ;
    initializePaymentMethods() {
        // this.loading.show();
        var dat = {};
        dat.language_id = this.config.langId;
        dat.currency_code = this.config.currecnyCode;
        this.config.postHttp('getpaymentmethods', dat).then((data) => {
            //  this.loading.hide();
            if (data.success == 1) {
                this.paymentMethods = data.data;
                for (let a of data.data) {
                    // if (a.method == "braintree_card" && a.active == '1') { this.getToken(); }
                    // if (a.method == "braintree_paypal" && a.active == '1') { this.getToken(); }
                    if (a.method == "paypal" && a.active == '1') {
                        this.paypalClientId = a.public_key;
                        if (a.environment == "Test")
                            this.paypalEnviroment = "PayPalEnvironmentSandbox";
                        else
                            this.paypalEnviroment = "PayPalEnvironmentProduction";
                    }
                    if (a.method == "stripe" && a.active == '1') {
                        this.publicKeyStripe = a.public_key;
                        this.stripe.setPublishableKey(a.public_key);
                    }
                    if (a.method == "razorpay") {
                        this.keyRazorPay = a.public_key;
                    }
                    // if (a.method == "paytm") {
                    //   this.paytmS = a.public_key;
                    // }
                }
            }
        }, err => {
            this.shared.showAlert("getPaymentMethods Server Error");
        });
    }
    stripePayment() {
        this.loading.show();
        this.stripe.createCardToken(this.stripeCard)
            .then(token => {
            this.loading.hide();
            //this.nonce = token.id
            this.addOrder(token.id);
        })
            .catch(error => {
            this.loading.hide();
            //console.log(error);
            //alert(error)
            this.shared.showAlert(error.message);
        });
    }
    couponslist() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            // + '<li>Cart Percentage <span>(cp9989)</a><p>{{"A percentage discount for the entire cart"|translate}}</p></li>'
            //   + '<li>Cart Fixed <span>(cf9999)</span> <p>{{"A fixed total discount for the entire cart"|translate}}</p></li>'
            //   + '<li>Product Fixed <span>(pf8787)</span></a><p>{{"A fixed total discount for selected products only"|translate}}</p></li>'
            //   + '<li>Product Percentage <span>(pp2233)</span><p>{{"A percentage discount for selected products only"|translate}}</p></li>'
            //   + '</ul>';
            // this.translate.get(array).subscribe((res) => { });
            let actionSheet = this.actionSheetCtrl.create({
                header: 'Coupons List',
                buttons: [
                    {
                        icon: 'arrow-round-forward',
                        text: 'Cart Percentage (cp9989). A percentage discount for selected products only',
                        handler: () => {
                            this.c = 'cp9989';
                        }
                    }, {
                        icon: 'arrow-round-forward',
                        text: 'Product Fixed (pf8787). A fixed total discount for selected products only',
                        handler: () => {
                            this.c = 'pf8787';
                        }
                    },
                    {
                        icon: 'arrow-round-forward',
                        text: 'Cart Fixed (cf9999). A fixed total discount for the entire cart',
                        handler: () => {
                            this.c = 'cf9999';
                        }
                    },
                    {
                        icon: 'arrow-round-forward',
                        text: 'Product Percentage (pp2233). A percentage discount for selected products only',
                        handler: () => {
                            this.c = 'pp2233';
                        }
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                        }
                    }
                ]
            });
            (yield actionSheet).present();
        });
    }
    paymentMehodChanged() {
        if (this.orderDetail.payment_method == "braintree_paypal")
            this.getToken();
        if (this.orderDetail.payment_method == "braintree_card")
            this.getToken();
        //if (this.orderDetail.payment_method == "stripe") this.stripe.setPublishableKey(this.publicKeyStripe);
        this.scrollToBottom();
    }
    scrollToBottom() {
        setTimeout(() => {
            this.content.scrollToBottom();
            console.log("botton");
        }, 300);
    }
    //================================= instamojo ===========================
    instamojoPayment() {
        let instamojoClient = new Instamojo(this.httpNative, this.iab, this.config.url + 'instamojotoken');
        var data = instamojoClient.getPaymentFields();
        data.buyer_name = this.shared.customerData.customers_firstname + " " + this.shared.customerData.customers_lastname;
        data.currency = this.config.currency;
        data.purpose = "Order Payment"; // REQUIRED
        data.amount = this.totalAmountWithDisocunt; // REQUIRED
        //data.phone = this.shared.customerData.customers_telephone
        data.email = this.shared.customerData.email;
        data.send_email = true;
        data.send_sms = false;
        data.allow_repeated_payments = true;
        // do not change this
        data.redirect_url = "http://localhost";
        instamojoClient.payNow(data).then(response => {
            console.log(response);
            this.addOrder(response.id);
            //alert("Payment complete: " + JSON.stringify(response));
            this.toastController.create({
                message: 'Payment complete',
                duration: 2000
            }).then(e => {
                e.present();
            });
            setTimeout(() => {
                this.navCtrl.navigateForward('/home');
            }, 3000);
        }).catch(err => {
            console.log(err);
            this.toastController.create({
                duration: 2000,
                message: 'Payment failed'
            }).then(e => {
                e.present();
            });
            this.shared.showAlert("Payment failed: " + JSON.stringify(err));
            throw err;
        });
    }
    //
    razorPay() {
        let customerName = this.shared.customerData.customers_firstname + " " + this.shared.customerData.customers_lastname;
        let cutomerEmail = this.shared.customerData.email;
        let cutomerPhone = this.shared.customerData.customers_telephone;
        let amount = parseInt((this.totalAmountWithDisocunt * 100).toString());
        //let amount = parseInt((this.totalAmountWithDisocunt.toFixed(2)).toString());
        var options = {
            description: 'Order Payment',
            //image: 'https://i.imgur.com/3g7nmJC.png',
            currency: this.config.currecnyCode,
            key: this.keyRazorPay,
            amount: amount,
            name: customerName,
            prefill: {
                email: cutomerEmail,
                contact: cutomerPhone,
                name: customerName
            },
            theme: {
                color: this.shared.primaryHexColor
            },
            modal: {
                ondismiss: function () {
                    //alert('dismissed')
                }
            }
        };
        let _this = this;
        var successCallback = function (payment_id) {
            _this.addOrder(payment_id);
        };
        var cancelCallback = function (error) {
            //console.log(error);
            //window.alert(error);
            _this.shared.toast(error.description + ' (Error ' + error.code + ')');
        };
        RazorpayCheckout.open(options, successCallback, cancelCallback);
        if (this.platform.is('android')) {
            this.platform.resume.subscribe((event) => {
                // Re-register the payment success and cancel callbacks
                RazorpayCheckout.on('payment.success', successCallback);
                RazorpayCheckout.on('payment.cancel', cancelCallback);
                // Pass on the event to RazorpayCheckout
                RazorpayCheckout.onResume(event);
            });
        }
        ;
    }
    paytmPayment() {
        let mId = "";
        let amount = parseInt((this.totalAmountWithDisocunt.toFixed(2)).toString());
        let production = true;
        let cutomerId = 0;
        if (this.shared.customerData.customers_id)
            cutomerId = this.shared.customerData.customers_id;
        let checkSum = "";
        let orderId = "";
        this.paymentMethods.forEach(element => {
            if (element.method == "paytm") {
                mId = element.public_key;
                if (element.environment == "Test") {
                    production = false;
                }
            }
        });
        this.loading.show();
        this.config.getHttp("generatpaytmhashes").then((data) => {
            this.loading.hide();
            checkSum = data.data.CHECKSUMHASH;
            orderId = data.data.ORDER_ID;
            this.paytmService.paytmpage(checkSum, orderId, mId, cutomerId, amount, production).then((data) => {
                if (data.status == "sucess") {
                    this.addOrder(data.id);
                }
                else {
                    this.shared.toast("Paytm Error");
                }
            });
        });
    }
    ngOnInit() {
        this.orderDetail = (JSON.parse(JSON.stringify(this.shared.orderDetails)));
        this.products = (JSON.parse(JSON.stringify(this.shared.cartProducts)));
        this.calculateTotal();
        this.initializePaymentMethods();
    }
    ionViewDidEnter() {
        var toolbar = document.getElementsByTagName("ion-toolbar");
        var style = getComputedStyle(toolbar[0]);
        var color = style.getPropertyValue("--ion-color-primary") || undefined;
        this.shared.primaryHexColor = color;
        console.log(color);
    }
    openHomePage() {
        this.navCtrl.navigateRoot(this.config.currentRoute + "/cart");
    }
    hyperpayPayment() {
        this.addOrder("null");
    }
};
OrderPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_12__["Router"] },
    { type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"] },
    { type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"] },
    { type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"] },
    { type: src_providers_coupon_coupon_service__WEBPACK_IMPORTED_MODULE_7__["CouponService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_8__["InAppBrowser"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: src_providers_paytm_paytm_service__WEBPACK_IMPORTED_MODULE_10__["PaytmService"] },
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_11__["HTTP"] },
    { type: _ionic_native_stripe_ngx__WEBPACK_IMPORTED_MODULE_9__["Stripe"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"])
], OrderPage.prototype, "content", void 0);
OrderPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./order.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/order/order.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./order.page.scss */ "./src/app/order/order.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
        _angular_router__WEBPACK_IMPORTED_MODULE_12__["Router"],
        src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"],
        src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"],
        src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"],
        src_providers_coupon_coupon_service__WEBPACK_IMPORTED_MODULE_7__["CouponService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"],
        _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_8__["InAppBrowser"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        src_providers_paytm_paytm_service__WEBPACK_IMPORTED_MODULE_10__["PaytmService"],
        _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_11__["HTTP"],
        _ionic_native_stripe_ngx__WEBPACK_IMPORTED_MODULE_9__["Stripe"]])
], OrderPage);



/***/ }),

/***/ "./src/providers/paytm/paytm.service.ts":
/*!**********************************************!*\
  !*** ./src/providers/paytm/paytm.service.ts ***!
  \**********************************************/
/*! exports provided: PaytmService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaytmService", function() { return PaytmService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/ngx/index.js");
/* harmony import */ var _shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared-data/shared-data.service */ "./src/providers/shared-data/shared-data.service.ts");




let PaytmService = class PaytmService {
    constructor(iab, shared) {
        this.iab = iab;
        this.shared = shared;
    }
    paytmpage(chcksum, orderId, mId, customerId, amount, inProduction) {
        return new Promise(resolve => {
            var paytmUrl = 'https://securegw-stage.paytm.in';
            if (inProduction == true)
                paytmUrl = 'https://securegw.paytm.in';
            var callBackUrl = paytmUrl + "/theia/paytmCallback?ORDER_ID=" + orderId;
            var completeUrl = paytmUrl + "/order/process";
            var pageContetn = `<html>
              <head>
                <title></title>
              </head>
              <body>
                <form method="post" action="${completeUrl}" name="paytm">
                    <table border="1">
                      <tbody>
                          <input type="hidden" name="MID" value="${mId}">
                          <input type="hidden" name="WEBSITE" value="WEBSTAGING">
                          <input type="hidden" name="ORDER_ID" value="${orderId}">
                          <input type="hidden" name="CUST_ID" value="${customerId}">
                          <input type="hidden" name="INDUSTRY_TYPE_ID" value="Retail">
                          <input type="hidden" name="CHANNEL_ID" value="WAP">
                          <input type="hidden" name="TXN_AMOUNT" value="${amount}">
                          <input type="hidden" name="CALLBACK_URL" value="${callBackUrl}">
                          <input type="hidden" name="CHECKSUMHASH" value="${chcksum}">
                      </tbody>
                    </table>
                    <script type="text/javascript">
                      document.paytm.submit();
                    </script>
                </form>
              </body>
          </html>`;
            var pageContentUrl = "data:text/html;base64," + btoa(pageContetn);
            console.log(pageContentUrl);
            let options = {
                location: 'yes',
                hidden: 'no',
                clearcache: 'yes',
                clearsessioncache: 'yes',
                zoom: 'no',
                disallowoverscroll: 'no',
                toolbar: 'yes',
                hideurlbar: "yes"
            };
            const bb = this.iab.create(pageContentUrl, "_blank", options);
            bb.on('loadstart').subscribe(res => {
                console.log(res.url);
                if (res.url == callBackUrl) {
                    console.log("---------------- payment sucess ---------------");
                    bb.close();
                    resolve({ status: "sucess", id: orderId });
                }
            }), error => {
                console.log(error);
                resolve({ status: "error", error: error });
            };
        });
    }
};
PaytmService.ctorParameters = () => [
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__["InAppBrowser"] },
    { type: _shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"] }
];
PaytmService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__["InAppBrowser"],
        _shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]])
], PaytmService);



/***/ })

}]);
//# sourceMappingURL=order-order-module-es2015.js.map