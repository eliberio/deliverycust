function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["settings-settings-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/settings/settings.page.html":
  /*!***********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/settings/settings.page.html ***!
    \***********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSettingsSettingsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-menu-button>\r\n    <ion-title>\r\n      {{'Account'| translate }}\r\n    </ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button fill=\"clear\" (click)=\"share()\" *ngIf=\"showOption('sharePage')\">\r\n        <ion-icon name=\"share\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button fill=\"clear\" (click)=\"rateUs()\" *ngIf=\"showOption('ratePage')\">\r\n        <ion-icon slot=\"icon-only\" name=\"star-half\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content class=\"setting-page\">\r\n\r\n  <ion-grid class=\"ion-padding\">\r\n\r\n    <ion-row *ngIf=\"shared.customerData.customers_id==null\" (click)=\"openLoginPage()\">\r\n      <ion-avatar>\r\n        <img src=\"assets/avatar.png\">\r\n      </ion-avatar>\r\n      <ion-col size=\"12\" class=\"ion-no-padding primary-contrat-color\" >\r\n        <h2>{{ 'Login & Register' | translate }}</h2>\r\n        <p>{{ 'Please login or create an account for free' | translate }}</p>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row *ngIf=\"shared.customerData.customers_id!=null\">\r\n      <ion-avatar>\r\n        <ion-fab-button id=\"nametxt\">{{getNameFirstLetter()}}</ion-fab-button>\r\n      </ion-avatar>\r\n      <ion-col size=\"12\" class=\"ion-no-padding primary-contrat-color\">\r\n        <h2>{{shared.customerData.customers_firstname +\"&nbsp;\"+shared.customerData.customers_lastname}}</h2>\r\n        <p>{{shared.customerData.email}}</p>\r\n        <ion-button (click)=\"openAccountPage()\">{{'Edit Profile' | translate }}</ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n    <span class=\"opacity-background\"></span>\r\n  </ion-grid>\r\n  <ion-list class=\"ion-padding\">\r\n\r\n    <ion-item lines=\"full\">\r\n      <ion-icon slot=\"start\" name=\"notifications\"></ion-icon>\r\n      <ion-label>\r\n        <p>{{\"Turn on/off Notifications\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-toggle [(ngModel)]=\"setting.notification\" (ionChange)=\"onOffPushNotification()\">\r\n      </ion-toggle>\r\n    </ion-item>\r\n\r\n    <ion-item lines=\"full\">\r\n      <ion-icon slot=\"start\" name=\"moon\"></ion-icon>\r\n      <ion-label>\r\n        <p>{{\"Dark Mode\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-toggle [(ngModel)]=\"setting.darkMode\" (ionChange)=\"changeDarkMode()\">\r\n      </ion-toggle>\r\n    </ion-item>\r\n  </ion-list>\r\n  <ion-list class=\"ion-padding\">\r\n    <ion-item lines=\"full\" (click)=\"showModal('language')\">\r\n      <ion-label>\r\n        <p>{{\"Change Language\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-icon slot=\"start\" name=\"globe\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon>\r\n    </ion-item>\r\n    <ion-item lines=\"full\" (click)=\"openCurrencyPage()\">\r\n      <ion-label>\r\n        <p>{{\"Select Currency\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-icon slot=\"start\" name=\"logo-usd\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon>\r\n    </ion-item>\r\n    <ion-item lines=\"full\" (click)=\"openPage('wish-list')\" *ngIf=\"showOption('wishListPage')\">\r\n      <ion-label>\r\n        <p>{{\"My Wish List\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-icon slot=\"start\" name=\"heart\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon>\r\n    </ion-item>\r\n    <ion-item lines=\"full\" (click)=\"openPage('my-account')\" *ngIf=\"showOption('editPage')\">\r\n      <ion-label>\r\n        <p>{{\"Edit Profile\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-icon slot=\"start\" name=\"lock-closed\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon>\r\n    </ion-item>\r\n    <ion-item lines=\"full\" (click)=\"openPage('addresses')\" *ngIf=\"showOption('address')\">\r\n      <ion-label>\r\n        <p>{{\"Address\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-icon slot=\"start\" name=\"locate\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon>\r\n    </ion-item>\r\n    <ion-item lines=\"full\" (click)=\"openPage('my-orders')\" *ngIf=\"showOption('myOrdersPage')\">\r\n      <ion-label>\r\n        <p>{{\"My Orders\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-icon slot=\"start\" name=\"cart\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon>\r\n    </ion-item>\r\n    <ion-item lines=\"full\" (click)=\"openPage('contact-us')\" *ngIf=\"showOption('contactPage')\">\r\n      <ion-label>\r\n        <p>{{\"Contact Us\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-icon slot=\"start\" name=\"call\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon>\r\n    </ion-item>\r\n    <ion-item lines=\"full\" (click)=\"openPage('about-us')\" *ngIf=\"showOption('aboutUsPage')\">\r\n      <ion-label>\r\n        <p>{{\"About Us\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-icon slot=\"start\" name=\"information-circle\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon>\r\n    </ion-item>\r\n\r\n    <!-- <ion-item lines=\"full\" (click)=\"openSite()\">\r\n      <ion-label>\r\n        <p>{{\"Official Website\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-icon slot=\"start\" name=\"arrow-forward\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon>\r\n    </ion-item>\r\n    <ion-item lines=\"full\" (click)=\"showModal('privacyPolicy')\">\r\n      <ion-label>\r\n        <p>{{\"Privacy Policy\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-icon slot=\"start\" name=\"arrow-forward\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon>\r\n    </ion-item>\r\n    <ion-item lines=\"full\" (click)=\"showModal('refundPolicy')\">\r\n      <ion-label>\r\n        <p>{{\"Refund Policy\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-icon slot=\"start\" name=\"arrow-forward\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon>\r\n    </ion-item>\r\n    <ion-item lines=\"full\" (click)=\"showModal('termServices')\">\r\n      <ion-label>\r\n        <p>{{\"Term and Services\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-icon slot=\"start\" name=\"arrow-forward\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon>\r\n    </ion-item> -->\r\n    <!-- <ion-item lines=\"full\" (click)=\"rateUs()\" *ngIf=\"showOption('ratePage')\">\r\n      <ion-label>\r\n        <p>{{\"Rate Us\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-icon slot=\"start\" name=\"star-half\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon>\r\n    </ion-item>\r\n    <ion-item lines=\"full\" (click)=\"share()\" *ngIf=\"showOption('sharePage')\">\r\n      <ion-label>\r\n        <p>{{\"Share\"|translate}}</p>\r\n      </ion-label>\r\n      <ion-icon slot=\"start\" name=\"share\"></ion-icon>\r\n      <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon>\r\n    </ion-item> -->\r\n  </ion-list>\r\n</ion-content>\r\n\r\n<ion-footer *ngIf=\"shared.customerData.customers_id!=null\">\r\n  <ion-button expand=\"full\" color=\"secondary\" (click)=\"logOut()\">{{'Log Out' | translate }}</ion-button>\r\n</ion-footer>\r\n";
    /***/
  },

  /***/
  "./src/app/settings/settings.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/settings/settings.module.ts ***!
    \*********************************************/

  /*! exports provided: SettingsPageModule */

  /***/
  function srcAppSettingsSettingsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SettingsPageModule", function () {
      return SettingsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _settings_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./settings.page */
    "./src/app/settings/settings.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _settings_page__WEBPACK_IMPORTED_MODULE_6__["SettingsPage"]
    }];

    var SettingsPageModule = function SettingsPageModule() {
      _classCallCheck(this, SettingsPageModule);
    };

    SettingsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_settings_page__WEBPACK_IMPORTED_MODULE_6__["SettingsPage"]]
    })], SettingsPageModule);
    /***/
  },

  /***/
  "./src/app/settings/settings.page.scss":
  /*!*********************************************!*\
    !*** ./src/app/settings/settings.page.scss ***!
    \*********************************************/

  /*! exports provided: default */

  /***/
  function srcAppSettingsSettingsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".setting-page .primary-contrat-color {\n  color: var(--ion-color-primary-contrast);\n}\n.setting-page ion-grid {\n  position: relative;\n  background-color: var(--ion-color-primary);\n  background-position: center;\n  color: var(--ion-color-light);\n  z-index: 10;\n}\n.setting-page ion-grid .opacity-background {\n  position: absolute;\n  top: 0;\n  left: 0;\n  background-color: var(--ion-color-primary-shade);\n  opacity: 0.7;\n  width: 100%;\n  height: 100%;\n  z-index: -1;\n}\n.setting-page ion-grid ion-row {\n  text-align: center;\n}\n.setting-page ion-grid ion-row ion-avatar {\n  margin: auto;\n}\n.setting-page ion-grid ion-row h2 {\n  margin-top: 10px;\n  font-weight: bold;\n}\n.setting-page ion-grid ion-row p {\n  margin-top: 10px;\n  margin-bottom: 8px;\n}\n.setting-page ion-grid ion-row ion-button {\n  height: 30px;\n  --background: var(--ion-color-light);\n  color: var(--ion-color-primary-shade);\n}\n.setting-page ion-list {\n  padding-bottom: 0;\n}\n.setting-page ion-list ion-item {\n  padding-bottom: 0;\n}\n.setting-page ion-list ion-item ion-toggle {\n  padding-right: 0;\n  --background-checked: var(--ion-color-primary-tint);\n  --handle-background-checked: var(--ion-color-primary);\n}\n.setting-page ion-list ion-item ion-icon {\n  color: var(--ion-text-color);\n}\n.setting-page ion-list ion-item p {\n  font-size: 16px;\n  color: var(--ion-text-color);\n}\n.setting-page ion-list:last-child {\n  padding-top: 0;\n}\n.setting-page ion-item ion-button {\n  width: 100%;\n  margin: 0;\n  height: 30px;\n}\n#nametxt {\n  width: 100%;\n  height: 100%;\n  font-size: 25px;\n  text-transform: capitalize;\n}\n.avatar-icon {\n  font-size: 64px !important;\n  color: var(--ion-color-light-shade);\n}\n.row-img {\n  opacity: 0.1 !important;\n}\nion-footer .toolbar {\n  padding-left: 10px;\n}\nion-footer ion-button {\n  height: 45px;\n  margin: 0;\n  margin-bottom: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYXRvL1N0dWRpb1Byb2plY3RzL2RlbGl2ZXJ5Y3VzdG9tZXIvc3JjL2FwcC9zZXR0aW5ncy9zZXR0aW5ncy5wYWdlLnNjc3MiLCJzcmMvYXBwL3NldHRpbmdzL3NldHRpbmdzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLHdDQUFBO0FDQUo7QURHRTtFQVlFLGtCQUFBO0VBRUEsMENBQUE7RUFDQSwyQkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtBQ2JKO0FESEk7RUFDRSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsZ0RBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDS047QURJSTtFQUNFLGtCQUFBO0FDRk47QURHTTtFQUNFLFlBQUE7QUNEUjtBREdNO0VBQ0UsZ0JBQUE7RUFDQSxpQkFBQTtBQ0RSO0FER007RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0FDRFI7QURHTTtFQUNFLFlBQUE7RUFDQSxvQ0FBQTtFQUNBLHFDQUFBO0FDRFI7QURLRTtFQUNFLGlCQUFBO0FDSEo7QURJSTtFQUNFLGlCQUFBO0FDRk47QURHTTtFQUNFLGdCQUFBO0VBQ0EsbURBQUE7RUFDQSxxREFBQTtBQ0RSO0FER007RUFDRSw0QkFBQTtBQ0RSO0FER007RUFDRSxlQUFBO0VBQ0EsNEJBQUE7QUNEUjtBRElJO0VBQ0UsY0FBQTtBQ0ZOO0FETUk7RUFDRSxXQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7QUNKTjtBRFFBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsMEJBQUE7QUNMRjtBRE9BO0VBQ0UsMEJBQUE7RUFDQSxtQ0FBQTtBQ0pGO0FETUE7RUFDRSx1QkFBQTtBQ0hGO0FET0U7RUFDRSxrQkFBQTtBQ0pKO0FETUU7RUFDRSxZQUFBO0VBQ0EsU0FBQTtFQUNBLGdCQUFBO0FDSkoiLCJmaWxlIjoic3JjL2FwcC9zZXR0aW5ncy9zZXR0aW5ncy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2V0dGluZy1wYWdlIHtcclxuICAucHJpbWFyeS1jb250cmF0LWNvbG9yIHtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeS1jb250cmFzdCk7XHJcbiAgfVxyXG5cclxuICBpb24tZ3JpZCB7XHJcbiAgICAub3BhY2l0eS1iYWNrZ3JvdW5kIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICB0b3A6IDA7XHJcbiAgICAgIGxlZnQ6IDA7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LXNoYWRlKTtcclxuICAgICAgb3BhY2l0eTogMC43O1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICB6LWluZGV4OiAtMTtcclxuICAgIH1cclxuXHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAvL2JhY2tncm91bmQtaW1hZ2U6IHVybChcIi9hc3NldHMvaWNvbnNfc3RyaXBlLnN2Z1wiKTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG4gICAgei1pbmRleDogMTA7XHJcbiAgICBpb24tcm93IHtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBpb24tYXZhdGFyIHtcclxuICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICAgIH1cclxuICAgICAgaDIge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgIH1cclxuICAgICAgcCB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiA4cHg7XHJcbiAgICAgIH1cclxuICAgICAgaW9uLWJ1dHRvbiB7XHJcbiAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnktc2hhZGUpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIGlvbi1saXN0IHtcclxuICAgIHBhZGRpbmctYm90dG9tOiAwO1xyXG4gICAgaW9uLWl0ZW0ge1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMDtcclxuICAgICAgaW9uLXRvZ2dsZSB7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogMDtcclxuICAgICAgICAtLWJhY2tncm91bmQtY2hlY2tlZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnktdGludCk7XHJcbiAgICAgICAgLS1oYW5kbGUtYmFja2dyb3VuZC1jaGVja2VkOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICAgIH1cclxuICAgICAgaW9uLWljb24ge1xyXG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvcik7XHJcbiAgICAgIH1cclxuICAgICAgcCB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvcik7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgIHBhZGRpbmctdG9wOiAwO1xyXG4gICAgfVxyXG4gIH1cclxuICBpb24taXRlbSB7XHJcbiAgICBpb24tYnV0dG9uIHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIG1hcmdpbjogMDtcclxuICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4jbmFtZXR4dCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIGZvbnQtc2l6ZTogMjVweDtcclxuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxufVxyXG4uYXZhdGFyLWljb24ge1xyXG4gIGZvbnQtc2l6ZTogNjRweCAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHQtc2hhZGUpO1xyXG59XHJcbi5yb3ctaW1nIHtcclxuICBvcGFjaXR5OiAwLjEgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWZvb3RlciB7XHJcbiAgLnRvb2xiYXIge1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG4gIH1cclxuICBpb24tYnV0dG9uIHtcclxuICAgIGhlaWdodDogNDVweDtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgfVxyXG59XHJcbiIsIi5zZXR0aW5nLXBhZ2UgLnByaW1hcnktY29udHJhdC1jb2xvciB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeS1jb250cmFzdCk7XG59XG4uc2V0dGluZy1wYWdlIGlvbi1ncmlkIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG4gIHotaW5kZXg6IDEwO1xufVxuLnNldHRpbmctcGFnZSBpb24tZ3JpZCAub3BhY2l0eS1iYWNrZ3JvdW5kIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LXNoYWRlKTtcbiAgb3BhY2l0eTogMC43O1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICB6LWluZGV4OiAtMTtcbn1cbi5zZXR0aW5nLXBhZ2UgaW9uLWdyaWQgaW9uLXJvdyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5zZXR0aW5nLXBhZ2UgaW9uLWdyaWQgaW9uLXJvdyBpb24tYXZhdGFyIHtcbiAgbWFyZ2luOiBhdXRvO1xufVxuLnNldHRpbmctcGFnZSBpb24tZ3JpZCBpb24tcm93IGgyIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uc2V0dGluZy1wYWdlIGlvbi1ncmlkIGlvbi1yb3cgcCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDhweDtcbn1cbi5zZXR0aW5nLXBhZ2UgaW9uLWdyaWQgaW9uLXJvdyBpb24tYnV0dG9uIHtcbiAgaGVpZ2h0OiAzMHB4O1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeS1zaGFkZSk7XG59XG4uc2V0dGluZy1wYWdlIGlvbi1saXN0IHtcbiAgcGFkZGluZy1ib3R0b206IDA7XG59XG4uc2V0dGluZy1wYWdlIGlvbi1saXN0IGlvbi1pdGVtIHtcbiAgcGFkZGluZy1ib3R0b206IDA7XG59XG4uc2V0dGluZy1wYWdlIGlvbi1saXN0IGlvbi1pdGVtIGlvbi10b2dnbGUge1xuICBwYWRkaW5nLXJpZ2h0OiAwO1xuICAtLWJhY2tncm91bmQtY2hlY2tlZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnktdGludCk7XG4gIC0taGFuZGxlLWJhY2tncm91bmQtY2hlY2tlZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xufVxuLnNldHRpbmctcGFnZSBpb24tbGlzdCBpb24taXRlbSBpb24taWNvbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvcik7XG59XG4uc2V0dGluZy1wYWdlIGlvbi1saXN0IGlvbi1pdGVtIHAge1xuICBmb250LXNpemU6IDE2cHg7XG4gIGNvbG9yOiB2YXIoLS1pb24tdGV4dC1jb2xvcik7XG59XG4uc2V0dGluZy1wYWdlIGlvbi1saXN0Omxhc3QtY2hpbGQge1xuICBwYWRkaW5nLXRvcDogMDtcbn1cbi5zZXR0aW5nLXBhZ2UgaW9uLWl0ZW0gaW9uLWJ1dHRvbiB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW46IDA7XG4gIGhlaWdodDogMzBweDtcbn1cblxuI25hbWV0eHQge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBmb250LXNpemU6IDI1cHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4uYXZhdGFyLWljb24ge1xuICBmb250LXNpemU6IDY0cHggIWltcG9ydGFudDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodC1zaGFkZSk7XG59XG5cbi5yb3ctaW1nIHtcbiAgb3BhY2l0eTogMC4xICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1mb290ZXIgLnRvb2xiYXIge1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG59XG5pb24tZm9vdGVyIGlvbi1idXR0b24ge1xuICBoZWlnaHQ6IDQ1cHg7XG4gIG1hcmdpbjogMDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/settings/settings.page.ts":
  /*!*******************************************!*\
    !*** ./src/app/settings/settings.page.ts ***!
    \*******************************************/

  /*! exports provided: SettingsPage */

  /***/
  function srcAppSettingsSettingsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SettingsPage", function () {
      return SettingsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic-native/app-version/ngx */
    "./node_modules/@ionic-native/app-version/ngx/index.js");
    /* harmony import */


    var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic-native/onesignal/ngx */
    "./node_modules/@ionic-native/onesignal/ngx/index.js");
    /* harmony import */


    var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic-native/in-app-browser/ngx */
    "./node_modules/@ionic-native/in-app-browser/ngx/index.js");
    /* harmony import */


    var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/social-sharing/ngx */
    "./node_modules/@ionic-native/social-sharing/ngx/index.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _modals_login_login_page__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../modals/login/login.page */
    "./src/app/modals/login/login.page.ts");
    /* harmony import */


    var _modals_privacy_policy_privacy_policy_page__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../modals/privacy-policy/privacy-policy.page */
    "./src/app/modals/privacy-policy/privacy-policy.page.ts");
    /* harmony import */


    var _modals_term_services_term_services_page__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ../modals/term-services/term-services.page */
    "./src/app/modals/term-services/term-services.page.ts");
    /* harmony import */


    var _modals_refund_policy_refund_policy_page__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ../modals/refund-policy/refund-policy.page */
    "./src/app/modals/refund-policy/refund-policy.page.ts");
    /* harmony import */


    var _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @ionic-native/local-notifications/ngx */
    "./node_modules/@ionic-native/local-notifications/ngx/index.js");
    /* harmony import */


    var _modals_language_language_page__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ../modals/language/language.page */
    "./src/app/modals/language/language.page.ts");
    /* harmony import */


    var _modals_currency_list_currency_list_page__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ../modals/currency-list/currency-list.page */
    "./src/app/modals/currency-list/currency-list.page.ts");
    /* harmony import */


    var src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! src/providers/app-events/app-events.service */
    "./src/providers/app-events/app-events.service.ts");

    var SettingsPage = /*#__PURE__*/function () {
      function SettingsPage(navCtrl, modalCtrl, config, storage, loading, appEventsService, shared, iab, socialSharing, plt, appVersion, oneSignal, localNotifications) {
        _classCallCheck(this, SettingsPage);

        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.config = config;
        this.storage = storage;
        this.loading = loading;
        this.appEventsService = appEventsService;
        this.shared = shared;
        this.iab = iab;
        this.socialSharing = socialSharing;
        this.plt = plt;
        this.appVersion = appVersion;
        this.oneSignal = oneSignal;
        this.localNotifications = localNotifications;
        this.setting = {};
      }

      _createClass(SettingsPage, [{
        key: "turnOnOffNotification",
        value: function turnOnOffNotification(value) {
          if (this.setting.localNotification == false) {
            this.localNotifications.cancel(1).then(function (result) {});
          } else {
            this.localNotifications.schedule({
              id: 1,
              title: this.config.notifTitle,
              text: this.config.notifText,
              every: this.config.notifDuration
            });
          }

          this.updateSetting();
        }
      }, {
        key: "changeDarkMode",
        value: function changeDarkMode() {
          this.config.darkMode = this.setting.darkMode;
          this.updateSetting();
        }
      }, {
        key: "updateSetting",
        value: function updateSetting() {
          console.log(this.setting);
          this.storage.set('setting', this.setting);
        }
      }, {
        key: "openLoginPage",
        value: function openLoginPage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var modal;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.modalCtrl.create({
                      component: _modals_login_login_page__WEBPACK_IMPORTED_MODULE_11__["LoginPage"],
                      componentProps: {
                        'hideGuestLogin': true
                      }
                    });

                  case 2:
                    modal = _context.sent;
                    _context.next = 5;
                    return modal.present();

                  case 5:
                    return _context.abrupt("return", _context.sent);

                  case 6:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "openCurrencyPage",
        value: function openCurrencyPage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var modal;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.modalCtrl.create({
                      component: _modals_currency_list_currency_list_page__WEBPACK_IMPORTED_MODULE_17__["CurrencyListPage"]
                    });

                  case 2:
                    modal = _context2.sent;
                    _context2.next = 5;
                    return modal.present();

                  case 5:
                    return _context2.abrupt("return", _context2.sent);

                  case 6:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "logOut",
        value: function logOut() {
          this.shared.logOut();
        }
      }, {
        key: "openAccountPage",
        value: function openAccountPage() {
          this.navCtrl.navigateRoot("my-account");
        }
      }, {
        key: "openSite",
        value: function openSite() {
          this.loading.autoHide(2000);
          this.iab.create(this.config.siteUrl, "blank");
        } //============================================================================================
        //turning on off local  notification

      }, {
        key: "onOffPushNotification",
        value: function onOffPushNotification() {
          var _this = this;

          this.storage.get('registrationId').then(function (registrationId) {
            var dat = {};
            dat.device_id = registrationId;
            if (_this.setting.notification == false) dat.is_notify = 0;else dat.is_notify = 1;

            _this.config.postHttp('notify_me', dat).then(function (data) {
              if (data.success == 1) {
                _this.updateSetting();
              }
            }, function (response) {
              console.log(response);
            });
          });
        }
      }, {
        key: "hideShowFooterMenu",
        value: function hideShowFooterMenu() {
          this.appEventsService.publish('setting', this.setting);
          this.updateSetting();
        }
      }, {
        key: "hideShowCartButton",
        value: function hideShowCartButton() {
          this.appEventsService.publish('setting', this.setting);
          this.updateSetting();
        }
      }, {
        key: "showModal",
        value: function showModal(value) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var modal, _modal, _modal2, _modal3;

            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    if (!(value == 'language')) {
                      _context3.next = 9;
                      break;
                    }

                    _context3.next = 3;
                    return this.modalCtrl.create({
                      component: _modals_language_language_page__WEBPACK_IMPORTED_MODULE_16__["LanguagePage"]
                    });

                  case 3:
                    modal = _context3.sent;
                    _context3.next = 6;
                    return modal.present();

                  case 6:
                    return _context3.abrupt("return", _context3.sent);

                  case 9:
                    if (!(value == 'privacyPolicy')) {
                      _context3.next = 18;
                      break;
                    }

                    _context3.next = 12;
                    return this.modalCtrl.create({
                      component: _modals_privacy_policy_privacy_policy_page__WEBPACK_IMPORTED_MODULE_12__["PrivacyPolicyPage"]
                    });

                  case 12:
                    _modal = _context3.sent;
                    _context3.next = 15;
                    return _modal.present();

                  case 15:
                    return _context3.abrupt("return", _context3.sent);

                  case 18:
                    if (!(value == 'termServices')) {
                      _context3.next = 27;
                      break;
                    }

                    _context3.next = 21;
                    return this.modalCtrl.create({
                      component: _modals_term_services_term_services_page__WEBPACK_IMPORTED_MODULE_13__["TermServicesPage"]
                    });

                  case 21:
                    _modal2 = _context3.sent;
                    _context3.next = 24;
                    return _modal2.present();

                  case 24:
                    return _context3.abrupt("return", _context3.sent);

                  case 27:
                    _context3.next = 29;
                    return this.modalCtrl.create({
                      component: _modals_refund_policy_refund_policy_page__WEBPACK_IMPORTED_MODULE_14__["RefundPolicyPage"]
                    });

                  case 29:
                    _modal3 = _context3.sent;
                    _context3.next = 32;
                    return _modal3.present();

                  case 32:
                    return _context3.abrupt("return", _context3.sent);

                  case 33:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          this.getStoredSettings();
        }
      }, {
        key: "getStoredSettings",
        value: function getStoredSettings() {
          var _this2 = this;

          this.storage.get('setting').then(function (val) {
            if (val != null || val != undefined) {
              _this2.setting = val;
            } else {
              _this2.setting.localNotification = true;
              _this2.setting.notification = true;
              _this2.setting.cartButton = true;
              _this2.setting.footer = true;
              _this2.setting.darkMode = false;
            }

            _this2.changeDarkMode();
          });
        }
      }, {
        key: "getNameFirstLetter",
        value: function getNameFirstLetter() {
          return this.shared.getNameFirstLetter();
        }
      }, {
        key: "rateUs",
        value: function rateUs() {
          var _this3 = this;

          this.loading.autoHide(2000);

          if (this.plt.is('ios')) {
            this.iab.create(this.config.packgeName.toString(), "_system");
          } else if (this.plt.is('android')) {
            this.appVersion.getPackageName().then(function (val) {
              _this3.iab.create("https://play.google.com/store/apps/details?id=" + val, "_system");
            });
          }
        }
      }, {
        key: "share",
        value: function share() {
          var _this4 = this;

          this.loading.autoHide(2000);

          if (this.plt.is('ios')) {
            this.socialSharing.share(this.config.packgeName.toString(), this.config.appName, this.config.packgeName.toString(), this.config.packgeName.toString()).then(function () {})["catch"](function () {});
          } else if (this.plt.is('android')) {
            this.appVersion.getPackageName().then(function (val) {
              _this4.socialSharing.share(_this4.config.appName, _this4.config.appName, "", "https://play.google.com/store/apps/details?id=" + val).then(function () {})["catch"](function () {});
            });
          }
        }
      }, {
        key: "showAd",
        value: function showAd() {
          this.loading.autoHide(2000);
          this.appEventsService.publish('showAd', "");
        }
      }, {
        key: "showOption",
        value: function showOption(value) {
          if (this.config.wishListPage && value == "wishListPage" && this.shared.customerData.customers_id != null) {
            return true;
          } else if (this.config.editProfilePage && value == "editPage" && this.shared.customerData.customers_id != null) {
            return true;
          } else if (value == "address" && this.shared.customerData.customers_id != null) {
            return true;
          } else if (this.config.myOrdersPage && value == "myOrdersPage" && this.shared.customerData.customers_id != null) {
            return true;
          } else if (this.config.contactUsPage && value == "contactPage") {
            return true;
          } else if (this.config.aboutUsPage && value == "aboutUsPage") {
            return true;
          } else if (this.config.newsPage && value == "newsPage") {
            return true;
          } else if (this.config.introPage && value == "introPage") {
            return true;
          } else if (this.config.shareApp && value == "sharePage") {
            return true;
          } else if (this.config.rateApp && value == "ratePage") {
            return true;
          } else if (this.config.settingPage && value == "settingsPage") {
            return true;
          } else return false;
        }
      }, {
        key: "openPage",
        value: function openPage(value) {
          this.navCtrl.navigateForward(this.config.currentRoute + '/' + value);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return SettingsPage;
    }();

    SettingsPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_10__["Storage"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"]
      }, {
        type: src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_18__["AppEventsService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]
      }, {
        type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_8__["InAppBrowser"]
      }, {
        type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_9__["SocialSharing"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
      }, {
        type: _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_6__["AppVersion"]
      }, {
        type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_7__["OneSignal"]
      }, {
        type: _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_15__["LocalNotifications"]
      }];
    };

    SettingsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-settings',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./settings.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/settings/settings.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./settings.page.scss */
      "./src/app/settings/settings.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_10__["Storage"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"], src_providers_app_events_app_events_service__WEBPACK_IMPORTED_MODULE_18__["AppEventsService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"], _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_8__["InAppBrowser"], _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_9__["SocialSharing"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_6__["AppVersion"], _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_7__["OneSignal"], _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_15__["LocalNotifications"]])], SettingsPage);
    /***/
  }
}]);
//# sourceMappingURL=settings-settings-module-es5.js.map